<?php
ob_start();

/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_DEFAULT));

  //require(DIR_WS_INCLUDES . 'template_top.php');
?>

 
 <?php
 
 
 /*Replace Titles start ob_start(); is on top*/
 //ob_start();
/* 
 $buffer=ob_get_contents();
    ob_end_clean();
 
 
 
 $titlessss = 'ADM Sneezeguards - Sneeze Guard Adjustable | Food Supply Store';
    $buffer = preg_replace('/(<title>)(.*?)(<\/title>)/i', '$1' . $titlessss . '$3', $buffer);


    $keyword = 'Sneeze Guard, Adjustable sneezeguards, sneeze guards posts, Buffet Food guards';

  //add anew desc and author
$buffer = str_replace('name="keywords" content="Sneeze Guard, sneeze guard glass, Portable Food Guards, Sneezeguards, Restaurant Supply Equipment"', 'name="keywords" content="'.$keyword.'"', $buffer);


    echo $buffer;
	
	
	
	*/
	
	

 
require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28436015-1', { 'optimize_id': 'GTM-5ZJRN8F'});
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>
<title>Sneeze Guard Adjustable | Tempered Glass - ADM Sneezeguards</title>
<!-- End Google Add Conversion -->

<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<!--<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">-->
<meta name="description" content="ADM Sneezeguards manufactures for Hospital service industry, we offer industry standard Desktop and Reception Counter Sneeze Guard & tempered glass business days.">
<meta name="keywords" content="Sneeze Guard custom line, Adjustable sneezeguards, sneeze guards posts, Buffet Food guards
">
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">
<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>



<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />





<link rel="stylesheet" type="text/css" href="stylesheet.css">

<style type="text/css">
<!--
.style2 {font-family: Tahoma; font-size: 12; line-height: 13px}
.style3 {font-family: Tahoma; font-size: 12; line-height: 17px; font-weight: bold}
.style6 {font-weight: bold; font-size: 12}
.TopLargeText {font-family: Tahoma; font-weight: bold; color: #C7F917; font-size: 20}
-->


p {
    font-family: "Times New Roman", Times, serif;
    color: #000000;
  
}
</style>


<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>
</head>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>


<?php
  if (!$detect->isMobile())
{
	$fontsize="font-size: 13px !important;";
}
else{
	$fontsize="font-size: 16px !important;";
	?>
	<style>
	.modText {
    font-size: 16px !important;
	line-height: 24px !important;
	}
	</style>
	
<?php
}
?>

<script>

//$("meta[name='keywords']").attr("content","Adjustable sneezeguards, sneeze guards posts, Buffet Food guards");
</script>
<?php
  /*Replace Titles End */
 
 
 
  if (!$detect->isMobile())
{
	//echo'<td id="ex1" align=center width="190" valign="top">';
}
else{
	echo'<td id="ex1" align=center width="190" valign="top">';

}

?>
<?php
    $portable_id="";
     $category_list=tep_db_query("select distinct parent_id as pid from ".TABLE_CATEGORIES." where parent_id!=0");
      $category_list_array=array();
      $i=0;
      while($category_list_row=tep_db_fetch_array($category_list)){
      	$category_list_array[$i]=$category_list_row['pid'];
    	$i++;
      }
       $ids=implode(', ', $category_list_array);
      
      $category_list=tep_db_query("select c.categories_id as pid, cd.categories_name as cname from ".TABLE_CATEGORIES." as c, ".TABLE_CATEGORIES_DESCRIPTION." as cd where c.categories_id in (".$ids.") and c.categories_id=cd.categories_id order by sort_order");
      $category_list_array=array();
      $i=0;
      while($category_list_row=tep_db_fetch_array($category_list)){
        if($category_list_row['cname']=="Adjustable"){
      	     $portable_id=$category_list_row['pid'];
        }
      }
      //echo $portable_id;
      $category_list=tep_db_query("select c.categories_id as pid from ".TABLE_CATEGORIES." as c where c.parent_id='".$portable_id."' order by sort_order");
      $category_list_array=array();
      $i=0;
      while($category_list_row=tep_db_fetch_array($category_list)){
	     $category_list_array[$i]=$category_list_row['pid'];
         $i++;
      }
	  $category_list_array[0]=81;
	  $category_list_array[1]=80;
	  //var_dump($category_list_array);
    // Open Table
    $result = mysql_query(
              "SELECT * FROM Models");
    if (!$result) {
      echo("<P>Error performing query: " .
           mysql_error() . "</P>");
      exit();
    }
    
  
    // Display all models
    $i=0;
    $j=0;
    while ( $row = mysql_fetch_array($result) ) {
    //Now only show the data for the Model in the address bar
	if($row["Name"] == "Orbit-360" || $row["Name"] == "Orbit-900" || $row["Name"] == "A-452" || $row["Name"] == "A-475" || $row["Name"] == "B-950" || $row["Name"] == "B-445" || $row["Name"] == "I-750" || $row["Name"] == "B-950" ||  $row["Name"] == "B-950-SWIVEL" ){           
	
	$Height = $row["Height"];
      $EndPan = $row["EndPan"];
      $Depth = $row["Depth"];
      $TopGlass = $row["TopGlass"];
      $HeatLamps = $row["HeatLamps"];
      $Lighting = $row["Lighting"];
      $FrontGlass = $row["FrontGlass"];
      $Tubing = $row["Tubing"];
      echo('<P>');
      echo('<TABLE WIDTH="751" HEIGHT="110" BORDER="0" class="custom" CELLPADDING=5>');
      echo('<tr>');
      echo('<td align=right width=20% style="font-size:16px !important;">');
	  
	  if($row["Name"] == "A-452" || $row["Name"] == "A-475" || $row["Name"] == "I-750")
	  {
		//echo('<A HREF="'.tep_href_link('info.php', 'Model='.$row["Name"].($i>2?"&mid=".$category_list_array[$j]:"")).'"><IMG SRC="images/Models/'.$row["Name"].'.jpg" BORDER=0>' . '</A>');
		echo('<A HREF="adjustable.php"><IMG alt="sneeze guard" title="sneeze guard for office" SRC="images/Models/'.$row["Name"].'.jpg" BORDER=0>' . '</A>');
		
	  }
	  else{
		echo('<A HREF="'.tep_href_link('info.php', 'Model='.$row["Name"].($i>2?"&mid=".$category_list_array[$j]:"")).'"><IMG alt="sneeze guard" title="sneeze guard for office" SRC="images/Models/'.$row["Name"].'.jpg" BORDER=0>' . '</A>');  
	  }
	  
      
	  
	  
      echo('</td>');
      echo('<td width=5 align=right>');
      echo('<IMG alt="sneeze guard" title="sneeze guard for office" SRC=images/shortVertSep.jpg>');
      echo('</td>');
      echo('<td valign=top>');
      echo('<div class="linkClass">');
	  //echo $j;
	  
	  if($row["Name"] == "A-452" || $row["Name"] == "A-475" || $row["Name"] == "I-750")
	  {
	//  echo('<A HREF="'.tep_href_link('info.php', 'Model='.$row["Name"].($i>2?"&mid=".$category_list_array[$j++]:"")).'">Model Name: ' . $row["Name"] . '</A><BR>');   
	echo('<A HREF="adjustable.php" style="'.$fontsize.'">Model Name: ' . $row["Name"] . '</A><BR>');
	  }
	  else{
	   echo('<A HREF="'.tep_href_link('info.php', 'Model='.$row["Name"].($i>2?"&mid=".$category_list_array[$j++]:"")).'" style="font-size:16px !important;">Model Name: ' . $row["Name"] . '</A><BR>');   
	  }
      
	  
	  
	  
      echo('</div>');
      echo('<div class="modText">');
      echo('Height: ' . $Height . '<br>');
      if($Depth != "0"){
      echo('Depth: ' . $Depth . '<br>');
      }
      echo('Glass: ' . $FrontGlass . '<br>');
      echo('Tubing: ' . $Tubing . '<br>');
      echo('</div>');
      echo('</td>');
      echo('<td valign=center align=left width="5">');
      //if($EndPan || $HeatLamps || $Lighting  != "0"){
      echo('<IMG alt="sneeze guard" title="sneeze guard for office" SRC=images/shortVertSep.jpg>');
      //}
      echo('</td>');
      echo('<td valign=top width=11%>');
      //if($EndPan || $HeatLamps || $Lighting  != "0"){
      echo('<div class="modName">');
      echo("<B style=''.$fontsize.''>Options:</B><BR>");
      echo('</div>');
      echo('<div class="modText">');
      if($EndPan != "0")
      echo("End Panels " . "<br>");
      if($HeatLamps != "0")
      echo("Heat Lamps " . "<br>");
      if($Lighting != "0")
      echo("Lighting " . "<br>");
      //}
      echo("Finishes");
      echo('</div>');
      echo('</td>');
      echo('</tr>');
      echo('</TABLE>');
      echo('<P>');
      $i++;
      }
      }
      
?>
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
