<?php
/*
  $Id: update_supertracker.php,v 1.0 2005/08/23

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

     include("includes/application_top.php");
		 
		 //First, let's check if the supertracker db table exists at all
		 $check_query = "show tables from " . DB_DATABASE . " like 'supertracker'";
		 $check_result = tep_db_query ($check_query);
     if (tep_db_num_rows($check_result) < 1){
		   //First time user, so just install the new database schema
			 $insert_query  = "CREATE TABLE `supertracker` (
                        `tracking_id` bigint(20) NOT NULL auto_increment,
                        `ip_address` varchar(15) NOT NULL default '',
                        `browser_string` varchar(255) NOT NULL default '',
                        `country_code` char(2) NOT NULL default '',
                        `country_name` varchar(100) NOT NULL default '',
                        `customer_id` int(11) NOT NULL default '0',
                        `order_id` int(11) NOT NULL default '0',
                        `referrer` varchar(255) NOT NULL default '',
                        `referrer_query_string` varchar(255) NOT NULL default '',
                        `landing_page` varchar(255) NOT NULL default '',
                        `exit_page` varchar(100) NOT NULL default '',
                        `time_arrived` datetime NOT NULL default '0000-00-00 00:00:00',
                        `last_click` datetime NOT NULL default '0000-00-00 00:00:00',
                        `num_clicks` int(11) NOT NULL default '1',
                        `added_cart` varchar(5) NOT NULL default 'false',
                        `completed_purchase` varchar(5) NOT NULL default 'false',
                        `categories_viewed` varchar(255) NOT NULL default '',
                        `products_viewed` varchar(255) NOT NULL default '',
                        `cart_contents` mediumtext NOT NULL,
                        `cart_total` int(11) NOT NULL default '0',
                         PRIMARY KEY  (`tracking_id`)
                         ) TYPE=MyISAM AUTO_INCREMENT=1 ;";
				@$insert_result = tep_db_query($insert_query);
				if ($insert_result) echo 'New Supertracker Database Table Successfully Created';
				else echo 'There was a problem in trying to create the database. You may need to do this manually using the following SQL code: <br>' . $insert_query;
		 }
		 else {
		   //Existing User, so we need to update the schema to the new one
			 $check_query = "show fields from supertracker";
			 $check_result = tep_db_query($check_query);
			 $fields = array();
			 while ($check_row = tep_db_fetch_array($check_result)) {
			   $fields[$check_row['Field']] = 1; 
			 }

			 //Drop the sesskey field if it exists
			 if (isset($fields[sesskey])) {
         $alter_query = "ALTER TABLE `supertracker` DROP `sesskey`";
				 $alter_result = tep_db_query($alter_query);
				 if ($alter_result) echo 'Deleted sesskey field from database<br>';  
		 }
		 
		 //Add the browser_string field if it doesn't already exist
		 if (!isset($fields[browser_string])) {
		   $alter_query = "ALTER TABLE `supertracker` ADD `browser_string` VARCHAR( 255 ) NOT NULL AFTER `ip_address` ;";
			 $alter_result = tep_db_query($alter_query);
			 if ($alter_result) echo 'Added browser_string field to database<br>';			 
		 } 
		 
		 //Add the cart_total field if it doesn't already exist
		 if (!isset($fields[cart_total])) {
		   $alter_query = "ALTER TABLE `supertracker` ADD `cart_total` VARCHAR( 255 ) NOT NULL AFTER `cart_contents` ;";
			 $alter_result = tep_db_query($alter_query);
			 if ($alter_result) echo 'Added cart_total field to database<br>';			 
		 } 
		 
		 //Change the cart_contents field type to Mediumtext
			 if (isset($fields[sesskey])) {
		   $alter_query = "ALTER TABLE `supertracker` CHANGE `cart_contents` `cart_contents` MEDIUMTEXT NOT NULL";
			 $alter_result = tep_db_query($alter_query);
			 if ($alter_result) echo 'Changed cart_contents field type to MEDIUMTEXT<br>';			 
			 }		 
		 }//End Actions for existing users
			 
		   echo 'End of Report'; 		 
?>