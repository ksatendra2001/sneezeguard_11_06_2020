<?php
/*
  $Id: ht_product_title_category.php v1.0 20101122 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_INSERT_PRODUCT_TITLE_CATEGORY_TITLE', 'Product Title - Category Name' );
  define( 'MODULE_HEADER_TAGS_INSERT_PRODUCT_TITLE_CATEGORY_DESCRIPTION', 'Show the category name as part of the product head title.' );

?>
