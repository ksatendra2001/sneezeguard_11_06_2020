<?php
/*
  $Id: ht_all_pages_title_string.php v1.0 20101129 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_ALL_PAGES_TITLE_STRING_TITLE', 'All Pages Title - String' );
  define( 'MODULE_HEADER_TAGS_ALL_PAGES_TITLE_STRING_DESCRIPTION', 'Show a string of characters in every page head title.' );

?>
