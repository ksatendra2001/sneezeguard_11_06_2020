<?php
/*
  $Id: ht_site_verification.php v1.0 20101128 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_SITE_VERIFICATION_TITLE', 'Google Site Verification' );
  define( 'MODULE_HEADER_TAGS_SITE_VERIFICATION_DESCRIPTION', 'Add the Google-specific site verification tag to all pages.' );

?>
