<?php
/*
  $Id: ht_other_pages_title_store_name.php v1.0 20101129 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_OTHER_PAGES_TITLE_STORE_NAME_TITLE', 'Other Pages - Store Name in Title' );
  define( 'MODULE_HEADER_TAGS_OTHER_PAGES_TITLE_STORE_NAME_DESCRIPTION', 'Add the store name to the head title on pages that do not have a head title.' );

?>
