<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_HEADER_TAGS_EMAIL_VALIDATOR_TITLE', 'Email Validator');
  define('MODULE_HEADER_TAGS_EMAIL_VALIDATOR_DESCRIPTION', 'Add live create account Email Validation to the shop');
?>