<?php
/*
  $Id: ht_copyright.php v1.0 20110103 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2011 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_COPYRIGHT_TITLE', 'Site Copyright' );
  define( 'MODULE_HEADER_TAGS_COPYRIGHT_DESCRIPTION', 'Add the copyright tag to all pages.' );

?>
