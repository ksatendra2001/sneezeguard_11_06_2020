<?php
/*
  $Id: ht_product_title_manufacturer.php v1.0 20101129 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_PRODUCT_TITLE_MANUFACTURER_TITLE', 'Product Title - Manufacturer Name' );
  define( 'MODULE_HEADER_TAGS_PRODUCT_TITLE_MANUFACTURER_DESCRIPTION', 'Add the name of the current manufacturer to the product page head title' );
?>
