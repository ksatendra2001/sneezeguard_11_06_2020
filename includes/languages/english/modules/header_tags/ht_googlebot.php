<?php
/*
  $Id: ht_googlebot.php v1.0 20101128 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_GOOGLEBOT_TITLE', 'Googlebot Meta Tag' );
  define( 'MODULE_HEADER_TAGS_GOOGLEBOT_DESCRIPTION', 'Add a googlebot meta tag to all pages.' );

?>
