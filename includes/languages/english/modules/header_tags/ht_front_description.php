<?php
/*
  $Id: ht_front_description.php v1.0 20101122 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_FRONT_DESCRIPTION_TITLE', 'Front Page - Description' );
  define( 'MODULE_HEADER_TAGS_FRONT_DESCRIPTION_DESCRIPTION', 'Add a meta description to the store front page.' );

?>
