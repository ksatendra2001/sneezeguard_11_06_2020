<?php
/*
  $Id: ht_category_keywords_insert.php v1.0 20110415 Kymation $
  $Loc: catalog/includes/languages/english/modules/header_tags/ $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2011 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_CATEGORY_KEYWORDS_INSERT_NEW_TITLE', 'Category Meta Keywords - Insert' );
  define( 'MODULE_HEADER_TAGS_CATEGORY_KEYWORDS_INSERT_NEW_DESCRIPTION', 'Insert a new keywords tag for each category in the meta description.' );
?>
