<?php
/*
  $Id: ht_author.php v1.0 20110103 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2011 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_AUTHOR_TITLE', 'Site Author' );
  define( 'MODULE_HEADER_TAGS_AUTHOR_DESCRIPTION', 'Add the author tag to all pages.' );

?>
