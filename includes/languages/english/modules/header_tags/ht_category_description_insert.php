<?php
/*
  $Id: ht_category_description_insert.php v1.0 20101129 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_CATEGORY_DESCRIPTION_INSERT_NEW_TITLE', 'Category Meta Description - Insert' );
  define( 'MODULE_HEADER_TAGS_CATEGORY_DESCRIPTION_INSERT_NEW_DESCRIPTION', 'Insert a new description for each category in the meta description.' );
?>
