<?php
/*
  $Id: ht_other_pages_title_string.php v1.0 20101129 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_OTHER_PAGES_TITLE_STRING_TITLE', 'Other Pages Title - String' );
  define( 'MODULE_HEADER_TAGS_OTHER_PAGES_TITLE_STRING_DESCRIPTION', 'Show a string of characters in every page head title where there is no other text.' );

?>
