<?php
/*
  $Id: ht_all_pages_title_store_name.php v1.0 20101129 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_ALL_PAGE_TITLE_STORE_NAME_TITLE', 'All Pages - Store Name in Title' );
  define( 'MODULE_HEADER_TAGS_ALL_PAGE_TITLE_STORE_NAME_DESCRIPTION', 'Add the store name to the head title on all pages.' );

?>
