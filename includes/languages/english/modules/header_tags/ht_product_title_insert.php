<?php
/*
  $Id: ht_insert_product_title v1.0 20101122 Kymation $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define( 'MODULE_HEADER_TAGS_PRODUCT_TITLE_INSERT_NEW_TITLE', 'Product Title - Insert New' );
  define( 'MODULE_HEADER_TAGS_PRODUCT_TITLE_INSERT_NEW_DESCRIPTION', 'Insert a new title for each product in the head title.' );
?>
