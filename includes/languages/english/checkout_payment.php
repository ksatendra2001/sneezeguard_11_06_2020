<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Checkout');
define('NAVBAR_TITLE_2', 'Payment Method');

define('HEADING_TITLE', 'Payment Information');

define('TABLE_HEADING_BILLING_ADDRESS', 'Billing Address');
define('TEXT_SELECTED_BILLING_DESTINATION', 'Please choose from your address book where you would like the invoice to be sent to.');
define('TITLE_BILLING_ADDRESS', 'Billing Address:');

define('TABLE_HEADING_PAYMENT_METHOD', 'Payment Method');
define('TEXT_SELECT_PAYMENT_METHOD', 'Please select the preferred payment method to use on this order.');
define('TITLE_PLEASE_SELECT', 'Please Select');
define('TEXT_ENTER_PAYMENT_INFORMATION', 'This is currently the only payment method available to use on this order.');

define('TABLE_HEADING_COMMENTS', 'Add Comments About Your Order');

define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'Continue Checkout Procedure');
define('TEXT_CONTINUE_CHECKOUT_PROCEDURE', 'to confirm this order.');
//<!-- BOF Agree2Terms_v2 jQuery -->
define ('TEXT_EDIT', 'Change');
define ('CONDITION_AGREEMENT', 'I am agree to  terms of agreement ');
define ('CONDITIONS', 'Conditions and terms of use.');
define ('CONDITION_AGREEMENT_ERROR', 'Please agree to terms of agreement');
define ('TEXT_AGREE_CLOSE', 'I Agree');
define ('TEXT_AGREE_PRESS', '( press it to Agree to  terms of agreement)');
define ('TERMSCONDITIONS', '<b>Please press "I Agree" Button in Bottom to continue </b></br>  Except where noted, most orders can be shipped on the following day after they are placed. We currently ship via FedEx. You will be charged shipping based upon the weight of the merchandise ordered, including the packaging.<br><br>Please call us prior to returning any merchandise at 1-800-690-0002. An RMA number will be issued for the return.  All returned merchandise must be in resalable condition for a refund to be issued.<br><br>Our return policy: Merchandise purchased on our web site may be returned within 30 days of receipt of purchase, for a refund or exchange with our authorization. The merchandise must be unused and in the original sealed package, unopened.<br><br>Glass orders: All orders with glass are considered custom and non-returnable.<br><br>Portable orders: All portables are considered custom and non-returnable, due to the fact that they can be used for an event one time only.<br><br>Units that involve custom depth measurements (ES29, ES82, & ED20) are non-returnable, as they are built specifically to your dimensions.<br><br>Refunds will be issued for the amount of the merchandise, less a 20% restocking fee. Shipping is NOT refundable.<br><br>REFUNDS will be processed within 10 business days of the date that we receive the returned merchandise. We will process the refund in the time period we promised, provided our return instructions have been followed. Your credit card company may take up to 30 days to post the refund to your account.<br><br>IF YOU RECEIVED FREE SHIPPING and you are returning non-defective merchandise, you will not receive a full refund. We will refund the price of the merchandise returned, less a 20% restocking fee, and the actual shipping costs.  ');
//<!-- BOF Agree2Terms_v2 jQuery -->



define('NAVBAR_TITLE_1', 'Fizetés');
define('NAVBAR_TITLE_2', 'Számlázási mód');

define('HEADING_TITLE', 'Számlázás');

define('TABLE_HEADING_BILLING_ADDRESS', 'Számlázási címem:');
define('TEXT_SELECTED_BILLING_DESTINATION', '');
define('TITLE_BILLING_ADDRESS', 'Erre a címre kapom a számlát:');

define('TABLE_HEADING_PAYMENT_METHOD', 'Fizetési módja:');
define('TEXT_SELECT_PAYMENT_METHOD', 'Válassz fizetési módot.');
define('TITLE_PLEASE_SELECT', 'Kérjük, válassz');
define('TEXT_ENTER_PAYMENT_INFORMATION', 'Jelenleg ez az egyetlen elérhetõ fizetési mód.');

define('TABLE_HEADING_COMMENTS', 'Megjegyzésem:');

define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', '');
define('TEXT_CONTINUE_CHECKOUT_PROCEDURE', 'Megrendelésem áttekintése.');

//kgt - discount coupons
//define('TABLE_HEADING_COUPON', 'Itt add meg prómóciós, kedvezményre jogosító kuponod kódját!<br />
//(Pontosan add meg, és ellenõrizd a következõ oldalon. Ha valami nem oké add meg itt újra!)' );
//end kgt - discount coupons


//kgt - discount coupons
define('TABLE_HEADING_COUPON', 'Do you have a promotional code or discount coupon?' );
//end kgt - discount coupons
?>
