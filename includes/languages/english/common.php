<?php
/*
  $Id: common.php,v 1.4 2002/11/19 01:48:08 dgw_ Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Common Questions');
define('HEADING_TITLE', 'Common Questions');

define('TEXT_INFORMATION', '<p class="style4">Where do you fabricate/get your parts from?</p>
<p class="style2">All our parts are fabricated here in Concord, California and we are proud to say "Made in USA" for all of our Sneezeguard lines.</p>
<p class="style4">Do you offer dealer discounts?</p>
<p class="style2"> Yes, please call for information on our dealer discount program. (1-800-690-0002)</p>
<p class="style2">_________________________________________________ </p>
<p class="style2 style3"><strong>What are the available finishes?</strong></p>
<div align=center><p class="style2"><img src="images/Finishes.jpg" width="640" height="270"></p>
  <p class="style8"><strong>PDF Flange Layout: </strong><a href="http://www.esneezeguards.com/catalog/PDF/flange.pdf"><img src="http://www.esneezeguards.com/catalog/images/pdf_icon.gif" width="50" height="50"></a></p>
</div>
<p class="style2">_________________________________________________ </p>
<p class="style4">Do I have to order over       the internet?</p>
<p class="style2"> No&hellip; Placing       orders over the internet is only for your convenience. </p>
<p class="style2"> If you       prefer, our knowledgeable and friendly staff is here to assist you. (1-800-690-0002)</p>
<p class="style2">_________________________________________________ </p>
<p class="style4">Is custom Glass Sizing offered?</p>
<p class="style2"> Yes&hellip; we can custom order glass to any length, lead time on Custom Glass is 4-5 Business days. (1-800-690-0002)</p>
<p class="style2">See below for our sizing recommendations.</p>
<p class="style2">_________________________________________________ </p>
<p class="style4">How do I determine Glass Length?</p>
<p class="style2"> To detetmine glass length, take the center to center dimension of your posts and subtract 1 7/8&quot; inches.</p>
<p class="style2">Additional Glass sizing information is located under the &quot;Post Dimension&quot; button in each product category.</p>
<p class="style2">_________________________________________________ </p>
<p class="style2"><span class="style5">How far can I       span each sneezeguard post?</span><br>
  <br>
Using: 1/4 glass, don&rsquo;t exceed 42<br>
<br>
Using: 3/8 glass, don&rsquo;t exceed 54 </p>
<p class="style2"> &nbsp;Any length       beyond the above noted sizes will exceed the physical glass       limitations. To span further apart, may cause the glass       panels to bow &amp; even explode!</p>
<p class="style2"> <strong> Caution:</strong> Companies that offer &ldquo;Cross-supports       to span long openings&rdquo; are improperly&nbsp;exceeding the glass panel/s       tensile-strength and posing a danger to you and your clients.</p>
<p class="style2">_________________________________________________ </p>
<p class="style4">How       can we offer such low prices?</p>
<p class="style2"><strong>&nbsp;We&nbsp;have       the ability to manufacture all of our own products in house through the use       of :</strong></p>
<p class="style2"> ..... CNC       mandrel bending machines</p>
<p class="style2"> ..... CNC       machining stations</p>
<p class="style2"> ..... CNC       turning centers</p>
<p class="style2"> ..... CNC       Swiss screw machines</p>
<p class="style2"> ..... Full       Powder-Coating facility</p>
<p class="style2"> ..... Full       finishing / polishing facility</p>
<p class="style2"> .....       Automated order processing and confirmation system</p>
<p class="style2"> &nbsp;<strong>All       of these above capabilities under one roof; plus .....</strong></p>
<p class="style2"> ..... Every       part and process manufactured to its absolute highest quality</p>
<p class="style2"> .....       Large ready-to-ship inventories</p>
<p class="style2"> We take       pride in&nbsp;passing all savings that comes with being an industry leader. <em> In-fact,&nbsp;we thank every&nbsp;order we receive by shipping it on time. </u>&nbsp;&nbsp;</em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p class="style2"><br>
  _________________________________________________</p>
<p class="style2"><strong><br>
    <span class="style6">Can my Express       Food-Guard be modified for my application?</span></strong><br>
  <br>
  Yes&hellip; We do limit our modifications to certain models, call for assistance.</p>
<p class="style2"><br>
  _________________________________________________<br>
  <br>
  <span class="style6"><strong>Do you have any       additional models or custom units available?</strong></span><br>
  <br>
  Our Express Food-Guard line of sneezeguards are designed to be fast-track,&nbsp;&nbsp;&nbsp;       in-stock &amp; ready for immediate shipment program. In order to maintain this       program, we must restrict the number of models and modifications to this       line. </p>
<p class="style2"> <strong><u>Custom       sneezeguards</u>:</strong> are available from our parent company at: <a href="http://www.sneezeguard.com/index.html"> www.sneezeguard.com </a>Here you will find       various shapes, styles and sizes; all with numerous options and       made-to-order specific requirements. </p>
<p class="style2">Custom Dept. Telephone: 1-800-805-1114</p>
<p class="style2">Email: <a href="mailto:sales@sneezeguards.com">sales@sneezeguards.com</a></p>
<p class="style1">&nbsp;</p>');
?>