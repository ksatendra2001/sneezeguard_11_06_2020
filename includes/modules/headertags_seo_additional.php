<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
*/


 if (HEADER_TAGS_ENABLE_ADDITIONAL_WORDS == 'true') {
     include('includes/languages/' . $language . '/headertags_seo_additional.php'); 
     $hts_query = tep_db_query("select products_head_additional_words from products_description where products_id = '" . (int)$_GET['products_id'] . "' and language_id = '". (int)$languages_id . "' order by products_head_additional_words");
     $hts_data = NULL;      
      
     if (tep_db_num_rows($hts_query) > 0) {
         $hts = tep_db_fetch_array($hts_query);
         $hts_data = explode(',', $hts['products_head_additional_words']); 
         $cnt = count($hts_data);
         $words = '';       
         
         for ($i = 0; $i < $cnt; ++$i) {
             $words .= '<span">';
             
             $separator ='';
             if ($i > 0) {
                 $separator = ($i == ($cnt - 1) ? ' and ' : ', ');
             }
              
             $words .= '<a class="hts_additional" href="' . tep_href_link('product_info.php', 'products_id=' . (int)$_GET['products_id']) . '">';
             $words .= '</a></span>' . $separator; //don't link the separator

             $words .= '<a class="hts_additional" href="' . tep_href_link('product_info.php', 'products_id=' . (int)$_GET['products_id']) . '">';
             $words .= trim($hts_data[$i]) . '</a></span>';             
         } 
         
         ?>
           <div class="col-sm-<?php echo $content_width; ?>  hts_additional_comntainer">
            <div><?php echo MODULE_CONTENT_PRODUCT_INFO_HTS_CAPTION; ?></div>
            <?php echo $words; ?>
           </div>
      <?php
     }
 }
 ?>
    