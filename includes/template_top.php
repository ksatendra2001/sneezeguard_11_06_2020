<?php    
    //$browser = $_SERVER['HTTP_USER_AGENT'];
    //if($_SERVER["PHP_SELF"]!="/catalog/browser.php"){ 
    //     $version=explode(";", $browser);
    //     $version=explode(" ", $version[1]);
    //    if($version[1]=="MSIE"){
    //        $version=explode(".", $version[2]);
    //        if(intval($version[0])<8){
    //            header("location:browser.php");
    //        }    
    //    }
    //}    
    
?>
<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  $oscTemplate->buildBlocks();

  if (!$oscTemplate->hasBlocks('boxes_column_left')) {
    $oscTemplate->setGridContentWidth($oscTemplate->getGridContentWidth() + $oscTemplate->getGridColumnWidth());
  }

  if (!$oscTemplate->hasBlocks('boxes_column_right')) {
    $oscTemplate->setGridContentWidth($oscTemplate->getGridContentWidth() + $oscTemplate->getGridColumnWidth());
  }
?>

<!DOCTYPE HTML>
<html <?php echo HTML_PARAMS; ?>>
<head>
<!-- Start Google Add Conversion -->
<!-- Global site tag (gtag.js) - Google Ads: 1072651700 -->


<script async src='https://www.googletagmanager.com/gtag/js?id=AW-1072651700'></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28436015-1', { 'optimize_id': 'GTM-5ZJRN8F'});
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>





<meta name="wot-verification" content="d661a6606b7ccfd15aaf"/>


<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<meta name="description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs.">
<meta name="keywords" content="Sneeze Guard, sneeze guard glass, Portable Food Guards, Sneezeguards, Restaurant Supply Equipment">
<title><?php echo tep_output_string_protected($oscTemplate->getTitle()); ?></title>


<base href="'. (($request_type == "SSL") ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG .'" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">
<link rel="canonical" href="https://www.sneezeguard.com/'.$_SERVER['REQUEST_URI'] .'">
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>


<meta property="og:url" content="https://www.sneezeguard.com/'.$_SERVER['REQUEST_URI'] .'"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta property="twitter:card" content="ADM Sneeze Guard Portable"/>
<meta property="twitter:image" content="https://twitter.com/ASneezeguards/photo"/>
<meta property="twitter:site" content="@ASneezeguards"/>
<meta property="twitter:url" content="https://twitter.com/ASneezeguards"/>




<!--
<script src="js/jquery-1.7.2.min.js"></script>
<link href="css/lightbox.css" rel="stylesheet" />
<link href="css/t/lightbox.css" rel="stylesheet" />
<script src="js/lightbox.js"></script>
<script src="js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="js/jquery-1.7.2.1.min.js"></script>
<link href="css/screen.css" rel="stylesheet" />
<script src="js/jquery.smooth-scroll.min.js"></script>
<link href="css/screen.css" rel="stylesheet" />
<script src="js/lightbox.js"></script>
-->


<?php
  if (tep_not_null(JQUERY_DATEPICKER_I18N_CODE)) {
?>
<script type="text/javascript" src="ext/jquery/ui/i18n/jquery.ui.datepicker-<?php echo JQUERY_DATEPICKER_I18N_CODE; ?>.js"></script>
<script type="text/javascript">
$.datepicker.setDefaults($.datepicker.regional['<?php echo JQUERY_DATEPICKER_I18N_CODE; ?>']);
</script>
<?php
  }
?>

<script type="text/javascript" src="ext/jquery/bxGallery/jquery.bxGallery.1.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.css" />
<script type="text/javascript" src="ext/jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="ext/960gs/<?php echo ((stripos(HTML_PARAMS, 'dir="rtl"') !== false) ? 'rtl_' : ''); ?>960_24_col.css" />
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<!-- second meta tag START...... -->
<?/*php echo $oscTemplate->getBlocks('header_tags'); */?>
<!-- second meta tag END...... -->
<link rel="icon" href="images/favicon.ico" type="img/ico">
<script type="text/javascript">
function getxmlHttpObj()
{
		//alert("hell")
		var xmlHttp;
		try
		{
			// Firefox, Opera 8.0+, Safari
			xmlHttp=new XMLHttpRequest();
		}
		catch (e)
		{
			// Internet Explorer
			try
			{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				try
				{
					xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e)
				{
					alert("Your browser does not support AJAX!");
					return false;
				}
			}
		}
		return xmlHttp;
}

function getPrice(product_id, opt_id, attr_arr)
{
		opt_id = document.getElementById("optionsid").value;
        opts_id=opt_id.split(",");
        //alert("kskdck");
        //alert(document.forms['cart_quantity'].elements['id['+opts_id[1]+']'].value);
        //alert(document.forms['cart_quantity'].elements['id['+opts_id[2]+']'].value);
        //alert(document.forms['cart_quantity'].elements['id['+opts_id[3]+']'].value);
        //alert(document.forms['cart_quantity'].elements['id['+opts_id[4]+']'].value);
        //alert(document.forms['cart_quantity'].elements['id['+opts_id[5]+']'].value);
        height=document.forms['cart_quantity'].elements['id['+opts_id[1]+']'].value;
        width=document.forms['cart_quantity'].elements['id['+opts_id[2]+']'].value;
        glass='EP11 '+width+'" '+'Glass (Squared Corners)';
        
        alert(product_name_price[glass]['1']);
        document.getElementById("display_price").innerHTML =product_name_price[glass]['1'];
		xmlHttpObj = getxmlHttpObj();
		//alert(option_value+ +product_id)
		xmlHttpObj.onreadystatechange=function()
		{
			if(xmlHttpObj.readyState==4)
			{
				val=xmlHttpObj.responseText.split('$');
				if(attr_arr==125){
					i=parseInt(val[0]);
					document.getElementById('left-post').innerHTML=i/2;
					document.getElementById('right-post').innerHTML=i/2;
				}
				if(attr_arr==126){
					document.getElementById('right-post').innerHTML=val[0];
					document.getElementById('left-post').innerHTML="0.00"
				}
				if(attr_arr==127){
					document.getElementById('left-post').innerHTML=val[0];
					document.getElementById('right-post').innerHTML="0.00"
				}
								
				document.getElementById("display_price").innerHTML =val[1];
			}
		}
		xmlHttpObj.open("GET","ajax_onchange_price.php?price="+price+"&option_id="+opt_id+"&product_id="+product_id+"&product_opt="+attr_arr,true);
		xmlHttpObj.send();
		
}
function validateForm(){
   if(document.getElementById('glass-face').value==0){
     return false; 
   }
   else{
     return true;
   } 
}
function changeAdditionalImage(obj){
    str='<img src="'+obj+'" id="changeimageaddchangeimageadd"/><br id="brcleareboth"/>';
    $("#additional_image").fadeOut(500, function(){        
        $(this).html(str); 
        $(this).fadeIn(300);       
    });
}

function changeImage(bay){
    $("#additional_image").fadeOut(100, function(){
        $(this).html('<img id="changeimageaddchangeimageadd" src="images/<?=isset($_REQUEST['Model'])?$_REQUEST['Model']:$category_name?>/'+bay+'.jpg" /><br id="brcleareboth" />');
        $(this).fadeIn(100);
        //$("#product_image").css("opacity","1.0");
        //$(".select-option").css("background","none");
        //$(".test-hide").css("opacity","1.0");
        //$(".message_p").remove();
        //$("#message_wp").remove();
    });
}
</script>
<style>
#changeimageadd{width:100%;}
#brcleareboth{clear:both;}
</style>




<!--<script type="text/javascript">
var a = new Image(); a.src = 'root.gif';
var b = new Image(); b.src = 'hover.gif';
var c = new Image(); c.src = 'active.gif';
</script>-->
<style type="text/css">
<!--
.style1 {
	font-family: Tahoma;
	font-weight: bold;
}
.style2 {font-family: Tahoma;float:left;
	font-size: 12;color:#CCCCCC;}
.style3 {color: #C7F900}
.style4 {
	font-family: Tahoma;
	color: #C7f900;
	font-weight: bold;
	float:left;
	font-size: 11px;
}
.style5 {
	color: #C7f900;
	font-weight: bold;
}
.style6 {color: #C7f900;float:left;
	font-size: 12;}
.style7 {
	font-size: 22px
}
.style8 {font-family: Tahoma; color: #C7f900; }
-->
</style>
<style type="text/css">
    .message_w {
    border: none;
}
</style>




<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-28436015-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

 gtag('config', 'UA-28436015-1', { 'optimize_id': 'GTM-5ZJRN8F'});
</script>





<!-- <script type="text/javascript">
	$(document).ready(function() {
}
	function news_feed_call(){
	$.ajax({
		
		type:'POST',
		url:'https://www.sneezeguard.com/news_rss.php',
		async: false
	}).success(function(){
        setTimeout(function(){news_feed_call();}, 2000000);
    });
	}

</script> -->
<!-- RSS Feed Strat -->
<?php
	  if (!isset($lng) || (isset($lng) && !is_object($lng))) {
	    include_once(DIR_WS_CLASSES . 'language.php');
	    $lng = new language;
	  }

	  reset($lng->catalog_languages);
	  while (list($key, $value) = each($lng->catalog_languages)) {
	?>
	<link rel="alternate" type="application/rss+xml" title="<?php echo STORE_NAME . ' - ' . BOX_INFORMATION_RSS; ?>" href="<?php echo FILENAME_RSS, '?language=' . $key;  ?>">
	<?php
	  }
	?>
<!-- RSS Feed End -->


</head>
<body itemscope="" itemtype="http://schema.org/WebPage">


<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>

<?php
/*
<!--

<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>

-->
*/
?>
<?php 
require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
		
		$check= basename($_SERVER['PHP_SELF']);
        if($detect->isMobile() || $detect->isTablet())
		{
 			//if($check=='login.php'||$check=='checkout_shipping.php'||$check=='checkout_payment.php'||$check=='account.php'||$check=='checkout_payment_address.php' ||$check=='checkout_confirmation.php'||$check=='checkout_success.php'||$check=='create_account.php'||$check=='create_account_success.php'||$check=='checkout_shipping_address.php'||$check=='account_edit.php'||$check=='address_book.php' ||$check=='address_book_process.php'||$check=='account_password.php' ||$check=='account_history.php'||$check=='account_history_info.php'||$check=='account_newsletters.php'||$check=='account_notifications.php'){
			//}else{
				require(DIR_WS_INCLUDES . 'header.php'); 
			//}

       }
	   else
	   {
	   		
				require(DIR_WS_INCLUDES . 'header.php'); 
			
	   }
 
 ?>

