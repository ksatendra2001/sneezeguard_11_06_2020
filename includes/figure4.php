<!DOCTYPE html>
<html>
<head>
	<title>figure4</title>
</head>
<style type="text/css">
	/*Fourth div  */
  .fourth {
    height: 200px;
    width: 160px;
    border-bottom: 2px solid black;
    /*float: left;*/  
    /*margin-left: 40px;*/
  }.fourth_a{
    height: 50px;
    width: 59px;
    position: absolute;
    margin-top: 148px;
    margin-left: 50px;
    border: 2px solid black;
    border-bottom-color: white;
    z-index: 2;
  }.fourth_a_a{
    margin-left: -8.9px;
    margin-top: -9px;
    transform: rotate(40deg);
    background: white;
    height: 20px;
    width: 15px;
    border: 2px solid black;
    border-radius: 100%;
    border-right-color: white;
    z-index: 4;
    position: absolute;
  }.fourth_a_b{
    margin-left: 50px;
    margin-top: -9px;
    transform: rotate(149deg);
    background: white;
    height: 20px;
    width: 15px;
    border: 2px solid black;
    border-radius: 100%;
    border-right-color: white;
    z-index: 4;
    position: absolute;
  }
</style>
<body>

<!-- Fourth drawing -->

<div class="fourth">
 <div class="fourth_a">
    <div class="fourth_a_a"></div>
    <div class="fourth_a_b"></div>
  </div>
</div>

</body>
</html>