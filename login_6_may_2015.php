<?php

/*

  $Id$



  osCommerce, Open Source E-Commerce Solutions

  http://www.oscommerce.com



  Copyright (c) 2010 osCommerce



  Released under the GNU General Public License

*/



  require('includes/application_top.php');

// redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled (or the session has not started)

  if ($session_started == false) {

    tep_redirect(tep_href_link(FILENAME_COOKIE_USAGE));

  }



  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_LOGIN);



  $error = false;

  if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'process') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)) {

    $email_address = tep_db_prepare_input($HTTP_POST_VARS['email_address']);

    $password = tep_db_prepare_input($HTTP_POST_VARS['password']);



// Check if email exists

    $check_customer_query = tep_db_query("select customers_id, customers_firstname, customers_password, customers_email_address, customers_default_address_id from " . TABLE_CUSTOMERS . " where customers_email_address = '" .encrypt_email(tep_db_input($email_address), ENCRYPTION_KEY_EMAIL) . "'");

    if (!tep_db_num_rows($check_customer_query)) {

      $error = true;

    } else {

      $check_customer = tep_db_fetch_array($check_customer_query);

// Check that password is good

      if (!tep_validate_password($password, $check_customer['customers_password'])) {

        $error = true;

      } else {

        if (SESSION_RECREATE == 'True') {

          tep_session_recreate();

        }



// migrate old hashed password to new phpass password

        if (tep_password_type($check_customer['customers_password']) != 'phpass') {

          tep_db_query("update " . TABLE_CUSTOMERS . " set customers_password = '" . tep_encrypt_password($password) . "' where customers_id = '" . (int)$check_customer['customers_id'] . "'");

        }



        $check_country_query = tep_db_query("select entry_country_id, entry_zone_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$check_customer['customers_id'] . "' and address_book_id = '" . (int)$check_customer['customers_default_address_id'] . "'");

        $check_country = tep_db_fetch_array($check_country_query);



        $customer_id = $check_customer['customers_id'];

        $customer_default_address_id = $check_customer['customers_default_address_id'];

        $customer_first_name = $check_customer['customers_firstname'];

        $customer_country_id = $check_country['entry_country_id'];

        $customer_zone_id = $check_country['entry_zone_id'];

        tep_session_register('customer_id');

        tep_session_register('customer_default_address_id');

        tep_session_register('customer_first_name');

        tep_session_register('customer_country_id');

        tep_session_register('customer_zone_id');



        tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_of_last_logon = now(), customers_info_number_of_logons = customers_info_number_of_logons+1 where customers_info_id = '" . (int)$customer_id . "'");



// reset session token

        $sessiontoken = md5(tep_rand() . tep_rand() . tep_rand() . tep_rand());



// restore cart contents

        

		tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_of_last_logon = now(), customers_info_number_of_logons = customers_info_number_of_logons+1 where customers_info_id = '" . (int)$customer_id . "'");

		

		tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "'");

        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "'");

		

		$cart->restore_contents();
        
        $wishlist->add_wishlist();


        if (sizeof($navigation->snapshot) > 0) {

          $origin_href = tep_href_link($navigation->snapshot['page'], tep_array_to_string($navigation->snapshot['get'], array(tep_session_name())), $navigation->snapshot['mode']);

          $navigation->clear_snapshot();

          tep_redirect($origin_href);

        } else {
            if($wishlist->isNotLogin) {
                $wishlist->isNotLogin = false;
                tep_redirect(tep_href_link("wishlist.php"));
            } else {
                $wishlist->isNotLogin = false;
                tep_redirect(tep_href_link(FILENAME_DEFAULT));
            }

        }

      }

    }

  }



  if ($error == true) {

    $messageStack->add('login', TEXT_LOGIN_ERROR);

  }



  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_LOGIN, '', 'SSL'));



  require(DIR_WS_INCLUDES . 'template_top.php');

?>
<script>
	$(document).ready(function(){
		if ($.browser.msie  && parseInt($.browser.version, 10) === 7) {
   			$("#ex1").css('width',"100%");
   			$(".price_table").css("text-align","left");
		}
   	});

</script>
<style type="text/css">
    .message_w {
    border: 2px solid #ff0000;
}

    
    .item{
    background: url("img/shadow_wide.png") no-repeat center bottom;
    padding-bottom: 6px;
    display: inline-block;
    margin-bottom: 30px;
    position:relative;
}

.item .delete{
    background:url('img/delete_icon.png') no-repeat;
    width:37px;
    height:38px;
    position:absolute;
    cursor:pointer;
    top:10px;
    right:-80px
}

.item a{
    background-color: #FAFAFA;
    border: none;
    display: block;
    padding: 10px;
    text-decoration: none;
}

.item:first-child .delete:before{
    background:url('img/tooltip.png') no-repeat;
    content:'.';
    text-indent:-9999px;
    overflow:hidden;
    width:145px;
    height:90px;
    position:absolute;
    right:-110px;
    top:-95px;
}

.item a img{
    display:block;
    border:none;
}
.c_msg{background:none;}

#confirmOverlay{
    width:100%;
    height:100%;
    position:fixed;
    top:0;
    left:0;
    background:url('jquery.confirm/ie.png');
    background: -moz-linear-gradient(rgba(11,11,11,0.1), rgba(11,11,11,0.6)) repeat-x rgba(11,11,11,0.2);
    background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(11,11,11,0.1)), to(rgba(11,11,11,0.6))) repeat-x rgba(11,11,11,0.2);
    z-index:100000;
}

#confirmBox{
    background:url('jquery.confirm/body_bg.jpg') repeat-x left bottom #e5e5e5;
    width:500px;
    position:absolute;
    left:50%;
    top:40%;
    margin:-130px 0 0 -230px !important;
    border: 1px solid rgba(33, 33, 33, 0.6);
    
    -moz-box-shadow: 0 0 2px rgba(255, 255, 255, 0.6) inset;
    -webkit-box-shadow: 0 0 2px rgba(255, 255, 255, 0.6) inset;
    box-shadow: 0 0 2px rgba(255, 255, 255, 0.6) inset;
}

#confirmBox h1,
#confirmBox p{
    font:26px/1 'Cuprum','Lucida Sans Unicode', 'Lucida Grande', sans-serif;
    background:url('jquery.confirm/header_bg.jpg') repeat-x left bottom #f5f5f5;
    padding: 18px 25px;
    text-shadow: 1px 1px 0 rgba(255, 255, 255, 0.6);
    color:#666;
}

#confirmBox h1{
    letter-spacing:0.3px;
    color:#888;
}

#confirmBox p{
    background:none;
    font-size:16px;
    line-height:1.4;
    padding-top: 35px;
}

#confirmButtons{
    padding:15px 0 25px;
    text-align:center;
}

#confirmBox .button{
    display:inline-block;
    background:url('jquery.confirm/buttons.png') no-repeat;
    color:white;
    position:relative;
    height: 33px;
    
    font:17px/33px 'Cuprum','Lucida Sans Unicode', 'Lucida Grande', sans-serif;
    
    margin-right: 15px;
    padding: 0 35px 0 40px;
    text-decoration:none;
    border:none;
}

#confirmBox .button:last-child{ margin-right:0;}

#confirmBox .button span{
    position:absolute;
    top:0;
    right:-5px;
    background:url('jquery.confirm/buttons.png') no-repeat;
    width:5px;
    height:33px
}

#confirmBox .blue{              background-position:left top;text-shadow:1px 1px 0 #5889a2;}
#confirmBox .blue span{         background-position:-195px 0;}
#confirmBox .blue:hover{        background-position:left bottom;}
#confirmBox .blue:hover span{   background-position:-195px bottom;}

#confirmBox .gray{              background-position:-200px top;text-shadow:1px 1px 0 #707070;}
#confirmBox .gray span{         background-position:-395px 0;}
#confirmBox .gray:hover{        background-position:-200px bottom;}
#confirmBox .gray:hover span{   background-position:-395px bottom;}
</style>
 <table width="100%" border="0" style="font:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bolder;  ">

  <tr>

    <td><font color="#A24E2A;">1. Log In</font>	</td>

    <td>2. Address Information</td>

    <td>3. Shipping & Delivery</td>

    <td>4. Payment Options</td>

    <td>5. Order Review</td>

    <td>6. Order Receipt</td>

  </tr>

</table>



<div class="form_white" style="height:400px; border: 1px #666666 solid;" >

 <h3  align="left" style="padding:5px; font-family:Arial, Helvetica, sans-serif; color:#000000; ">Please Sign In to your Sneezeguards.com account or Proccess as a guest</h3> <?php// echo HEADING_TITLE; ?></h1>



<?php

  if ($messageStack->size('login') > 0) {

    echo $messageStack->output('login');

  }

?>



   <!-- <script type="text/javascript">

            display=0;

         $(function(){

                $(".old_user").css("opacity","0.3");

                $(".test-hide").css("opacity","0.3");

                var cssObj={

                    "background-color":"#111",

                    "border-style":"solid",

                    "border-width":"2px",

                    "border-color":"#C7F900"};

                $(".new_user").css(cssObj);

                $("#message_w").html("");

                setInterval(action_event, 2000);

         });

            action_event = function(){

                $(".old_user").css("opacity","1.0");

                var cssObj={

                    "background":"none",

                    "border":"none",

                    "box-shadow":"none"};

                $(".new_user").css(cssObj);

                $(".test-hide").css("opacity","1.0");

                $("#message_w").html("");

                if(display==0){

                    display++;

                    $(".new_user").css("opacity","0.3");

                    $(".test-hide").css("opacity","0.3");

                    var cssObj={

                        "background-color":"#111",

                        "border-style":"solid",

                        "border-width":"2px",

                        "border-color":"#C7F900"};

                    $(".old_user").css(cssObj);

                    $(".message_w").css({"left":"100px"});

                    $("#message_w").html("");

                    setInterval(action_event2, 2000);

                }

            };

             action_event2 = function(){

                $(".new_user").css("opacity","1.0");

                 var cssObj={

                    "background":"none",

                    "border":"none",

                    "box-shadow":"none"};

                $(".old_user").css(cssObj);

                $(".test-hide").css("opacity","1.0");

                $(".message_p").remove();

            };

   </script>

   <style type="text/css">

        .message_p{

            position:relative;

            z-index: 1000000;

        }

        .message_w{

            position:absolute;

            color:#C7F900;

            text-shadow:2px 2px 3px #111;

            font-size: 22px;

            left:200px;

            top:50px;

            font-weight: bold;

            text-align: center;

        }

   </style>-->

<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr>

<td colspan="3" style="background:url(img/TopBak.jpg) !important ; height:26px; border:1px solid #ccc;">&nbsp;</td>

</tr>

<tr  >



<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">

<table width="100%" cellpadding="2" cellspacing="2">

<tr>



<td width="55%"  style="border-right:1px solid #ccc;" >

  <h2><?php echo HEADING_NEW_CUSTOMER; ?></h2>

<table width="100%"><tr><td>

 <span style="font:Arial, Helvetica, sans-serif ; color:#242424;"> <input type="radio" name="buy" value="male" id="male" ><label for="male">I am purchasing for Business or Institution</label></<br><br /><br />

<input type="radio" name="buy" value="female"  id="female"  ><label for="female">I am purchasing for Home, Gift or Personal Use</label></span></td><td valign="top" align="right"> <?php echo '<a href="'.tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL').'">'.tep_image_button('continue.gif', IMAGE_BUTTON_CONTINUE_SHOPPING).'</a>'; ?></td></tr></table>



    



  <h2><?php echo HEADING_RETURNING_CUSTOMER; ?></h2>



    <p><?php echo TEXT_RETURNING_CUSTOMER; ?></p>



    <?php echo tep_draw_form('login', tep_href_link(FILENAME_LOGIN, 'action=process', 'SSL'), 'post', '', true); ?>



    <table border="0" cellspacing="0" cellpadding="2" width="100%">

      <tr>

        <td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>

        <td class="fieldValue"><?php echo tep_draw_input_field('email_address'); ?></td>

      </tr>

      <tr>

        <td class="fieldKey"><?php echo ENTRY_PASSWORD; ?></td>

        <td class="fieldValue"><?php echo tep_draw_password_field('password'); ?><br>

		<u><?php echo '<a href="' . tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL') . '">' . TEXT_PASSWORD_FORGOTTEN . '</a>'; ?></u></td> <td rowspan="2" colspan="2" align="right"><?php echo tep_image_submit('btn_logIn.gif', IMAGE_BUTTON_LOGIN, "button"); ?></td>

      </tr>

    </table>



    <p><?php// echo '<a href="' . tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL') . '">' . TEXT_PASSWORD_FORGOTTEN . '</a>'; ?></p>



    <p align="right"><?php// echo tep_image_submit('button_login.gif', IMAGE_BUTTON_LOGIN, "button"); ?></p>

 </td>

 <td>

<a href="privacy.php"><img src="img/lock.gif" border="0" align="right" /></a

></td>

<td><p><strong><font size="+2" face="Arial, Helvetica, sans-serif"><a href="privacy.php">Privacy Policy</a></font></strong>

	    </p> <p  style="padding-right:10px;" ><font size="-2" face="Arial, Helvetica, sans-serif">Information about our customers is an important part of our business, and we are NOT in the business of selling it to others. All personal information we gather will be used solely for the purpose of processing and fulfilling your order. You will not be automatically placed on any mailing lists and the only messages you will receive will be about the status of your order, including order confirmation and shipping notifications. If you would like to learn more, please read our detailed<a href="privacy.php"> <u>privacy policy </u>.</a></font> </p></td>

      </tr>

</table>

</td></tr></table></div>

    </form>
<?php

  require(DIR_WS_INCLUDES . 'template_bottom.php');

  require(DIR_WS_INCLUDES . 'application_bottom.php');

?>

