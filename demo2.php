<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>HTML5 Video Player</title>



<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">
<meta property="og:url" content="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>" />
<meta property="og:type" content="website"/>
<meta property="og:title" content="ADM Sneezeguards - Sneeze Guard Portable | Restaurant Food Guards" />
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneezeguard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta property="twitter:card" content="ADM Sneeze Guard Portable"/>
<meta property="twitter:image" content="https://twitter.com/ASneezeguards/photo"/>
<meta property="twitter:site" content="@ASneezeguards"/>
<meta property="twitter:url" content="https://twitter.com/ASneezeguards"/>



  <!-- Include the VideoJS Library -->
  <script src="video.js" type="text/javascript" charset="utf-8"></script>

  <script type="text/javascript">
    // Must come after the video.js library

    // Add VideoJS to all video tags on the page when the DOM is ready
    VideoJS.setupAllWhenReady();

    /* ============= OR ============ */

    // Setup and store a reference to the player(s).
    // Must happen after the DOM is loaded
    // You can use any library's DOM Ready method instead of VideoJS.DOMReady

    /*
    VideoJS.DOMReady(function(){
      
      // Using the video's ID or element
      var myPlayer = VideoJS.setup("example_video_1");
      
      // OR using an array of video elements/IDs
      // Note: It returns an array of players
      var myManyPlayers = VideoJS.setup(["example_video_1", "example_video_2", video3Element]);

      // OR all videos on the page
      var myManyPlayers = VideoJS.setup("All");

      // After you have references to your players you can...(example)
      myPlayer.play(); // Starts playing the video for this player.
    });
    */

    /* ========= SETTING OPTIONS ========= */

    // Set options when setting up the videos. The defaults are shown here.

    /*
    VideoJS.setupAllWhenReady({
      controlsBelow: false, // Display control bar below video instead of in front of
      controlsHiding: true, // Hide controls when mouse is not over the video
      defaultVolume: 0.85, // Will be overridden by user's last volume if available
      flashVersion: 9, // Required flash version for fallback
      linksHiding: true // Hide download links when video is supported
    });
    */

    // Or as the second option of VideoJS.setup
    
    /*
    VideoJS.DOMReady(function(){
      var myPlayer = VideoJS.setup("example_video_1", {
        // Same options
      });
    });
    */

  </script>

  <!-- Include the VideoJS Stylesheet -->
  <link rel="stylesheet" href="video-js.css" type="text/css" media="screen" title="Video JS">
</head>
<body  bgcolor="#000000">




<?php
require_once("Mobile_Detect.php");
$detect = new Mobile_Detect();
if (!$detect->isMobile())
{
?>




  <!-- Begin VideoJS -->
  <div class="video-js-box">
    <!-- Using the Video for Everybody Embed Code http://camendesign.com/code/video_for_everybody -->
    <video id="example_video_1" class="video-js" width="640" height="480" controls="controls" preload="auto" poster="pic.jpg" autoplay="true" >
      <source src="sneezegaurd/upload2/videos/<?php echo $_GET["name"]; ?>.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <source src="sneezegaurd/upload2/videos/<?php echo $_GET["name"]; ?>.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="sneezegaurd/upload2/videos/<?php echo $_GET["name"]; ?>.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "sneezeguard/upload/videos/<?php echo $_GET["name"]; ?>.mp4","autoPlay":true,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="640" height="480" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
    </video>
    <!-- Download links provided for devices that can't play video in the browser. -->
    <p class="vjs-no-video"><strong>Download Video:</strong>
      <a href="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.mp4">MP4</a>,
      <a href="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.webm">WebM</a>,
      <a href="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.ogv">Ogg</a><br>
      <!-- Support VideoJS by keeping this link. -->
      </p>
  </div>
  <!-- End VideoJS -->

  
  
  <?php

}
else
{
?>




  <!-- Begin VideoJS -->
  <div class="video-js-box">
    <!-- Using the Video for Everybody Embed Code http://camendesign.com/code/video_for_everybody -->
    <video controls autoplay id="example_video_1" class="video-js" width="840" height="780" controls="controls" preload="auto" poster="pic.jpg" autoplay="true" >
      <source src="sneezegaurd/upload2/videos/<?php echo $_GET["name"]; ?>.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <source src="sneezegaurd/upload2/videos/<?php echo $_GET["name"]; ?>.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="sneezegaurd/upload2/videos/<?php echo $_GET["name"]; ?>.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "sneezeguard/upload/videos/<?php echo $_GET["name"]; ?>.mp4","autoPlay":true,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="640" height="480" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
    </video>
    <!-- Download links provided for devices that can't play video in the browser. -->
    <p class="vjs-no-video"><strong>Download Video:</strong>
      <a href="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.mp4">MP4</a>,
      <a href="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.webm">WebM</a>,
      <a href="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.ogv">Ogg</a><br>
      <!-- Support VideoJS by keeping this link. -->
      </p>
  </div>
  <!-- End VideoJS -->

<?php
}
?>
  
  
  
  
  
</body>
</html>