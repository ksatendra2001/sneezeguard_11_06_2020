<?php
  require('includes/application_top.php');
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);
 // require(DIR_WS_INCLUDES . 'template_top.php');
  
  
  require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28436015-1', { 'optimize_id': 'GTM-5ZJRN8F'});
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>
<title>Sneeze Guard for Banks | Custom Library - ADM Sneezeguards</title>
<!-- End Google Add Conversion -->

<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<!--<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">-->
<meta name="description" content="Shop Sneeze Guards, Covid-19 Protection and restaurant equipment in US at best prices on ADMSneezeguards. In-stock Glass Barriers for offices are ready to ship.">
<meta name="keywords" content="Covid-19 Protection, Coronavirus Updates, coronavirus Prevention Sneeze Guard, Glass Divider choose from, Custom Library ADM Sneezeguards">
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">
<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>



<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />


<link rel="stylesheet" type="text/css" href="stylesheet.css">

<style type="text/css">
<!--
.style2 {font-family: Tahoma; font-size: 12; line-height: 13px}
.style3 {font-family: Tahoma; font-size: 12; line-height: 17px; font-weight: bold}
.style6 {font-weight: bold; font-size: 12}
.TopLargeText {font-family: Tahoma; font-weight: bold; color: #C7F917; font-size: 20}
-->
</style>


<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>
</head>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>


  <?php
  
   $ImageData=mysql_query("SELECT * FROM  customlib WHERE publish='1' ") or die(mysql_error());
	
?>
    
            <link rel="stylesheet" href="flexslider.css" type="text/css" media="screen" />
			<link rel="stylesheet" href="colorbox2.css" />
      	
<?
if (!$detect->isMobile())
{
?>   
            <style type="text/css">
               
          
        body
        {
            margin: 0;
            padding: 0;
            background-color: #171717;
        }
        #contributor
        {
            color: Gray;
            margin: 0;
            padding: 0;
            margin-top: 4px;
            text-align: center;
        }
        #contributor a
        {
            color: Gray;
        }
         ul.slide li{
                list-style:none;
                display:inline-block;
                margin: 5px;
                padding:5px;
                border:1px solid #292929;
            }
            
            .hover_image img
            {
            opacity:0.4;
            filter:alpha(opacity=40); /* For IE8 and earlier */
            }
            li.flex-active img
            {
            opacity:1.0 !important;
            filter:alpha(opacity=100) !important; /* For IE8 and earlier */
            }
            .hover_image img:hover
            {
            opacity:1.0 !important;
            filter:alpha(opacity=100) !important; /* For IE8 and earlier */
            }
    </style>
	



	
    <table class="category-List" style="width: 100%;">
        <tr>
            <td align="center"><h2 style="padding-left:0px; font-size:16px; color:#f4f4f4; text-decoration:none; text-align:center;"><?=$_REQUEST['Model']?> Custom Library</h2></td>
            
           
            
        </tr>
<tr>
<td>
    <section class="slider">
        <div id="carousel" class="flexslider">
            <ul class="slider slide">
                <?PHP 
				//print_r($ROWDATA);
				while($ROWDATA = mysql_fetch_array($ImageData))
			
			{  
			 echo '<li class="group1" title="'.$ROWDATA['linkname'].'" style="text-align:center;font-weight:bold;" href="sneezegaurd/customlib/images2/'.$ROWDATA['imagename2'].'"><br><img alt="sneeze guard" title="sneeze guard" src="sneezegaurd/customlib/images2/'.$ROWDATA['imagename2'].'" width="100" ><br style="clear:both">'.$ROWDATA['linkname'].'</li>';
                    
			
			}
                ?>  
                <br style="clear:both">            
            </ul>
            <div style="clear: both;"></div>
        </div>
        <div id="slider" class="flexslider">
            <ul class="slides">
                <?PHP 
               
                ?>
            </ul>
        </div>
    </section>
</td>
</tr>
    </table>
<?
}
else{
?>
<td id="ex1" align=center width="190" valign="top">

<style>
.form_white h2 {
    color: black;
    font-size: 31px;
}
.contentss{
	font-size:22px;
}

p {
    font-family: "Times New Roman", Times, serif;
    color: ffffff;
    font-size: 24px;
    /* padding-left: 35px; */
}

.form_white td {
    background: #FFFFFF !important;
    color: #000000;
    font-size: 22px;
}
.main img{
    width: 736px;
}
</style>

 <style type="text/css">
               
          
        body
        {
            margin: 0;
            padding: 0;
            background-color: #171717;
        }
        #contributor
        {
            color: Gray;
            margin: 0;
            padding: 0;
            margin-top: 4px;
            text-align: center;
        }
        #contributor a
        {
            color: Gray;
        }
         ul.slide li{
                list-style:none;
                margin: 5px;
                padding:5px;
                border:1px solid #292929;
				font-size:30px;
            }
			 ul.slide li img{
                width:48% !important;
            }
            
            .hover_image img
            {
            opacity:0.4;
            filter:alpha(opacity=40); /* For IE8 and earlier */
            }
            li.flex-active img
            {
            opacity:1.0 !important;
            filter:alpha(opacity=100) !important; /* For IE8 and earlier */
            }
            .hover_image img:hover
            {
            opacity:1.0 !important;
            filter:alpha(opacity=100) !important; /* For IE8 and earlier */
            }
    </style>
	

	
    <table class="category-List" style="width: 100%;">
        <tr>
            <td align="center"><h2 style="padding-left:0px; font-size:32px; color:#f4f4f4; text-decoration:none; text-align:center;"><?=$_REQUEST['Model']?> Custom Library</h2></td>
            
           
            
        </tr>
<tr>
<td>
    <section class="slider">
        <div id="carousel" class="flexslider">
		
            <ul class="slider slide">
                <?PHP 
				//print_r($ROWDATA);
				while($ROWDATA = mysql_fetch_array($ImageData))
			
			{  
			 echo '<li class="group1" title="'.$ROWDATA['linkname'].'" style="text-align:center;font-weight:bold;" href="sneezegaurd/customlib/images2/'.$ROWDATA['imagename2'].'"><br><img alt="sneeze guard" title="sneeze guard" src="sneezegaurd/customlib/images2/'.$ROWDATA['imagename2'].'" style="width: 183px;" ><br style="clear:both">'.$ROWDATA['linkname'].'</li>';
                    
			
			}
                ?>  
                <br style="clear:both">            
            </ul>
            <div style="clear: both;"></div>
        </div>
        <div id="slider" class="flexslider">
            <ul class="slides">
                <?PHP 
               
                ?>
            </ul>
        </div>
    </section>
</td>
</tr>
    </table>
<?
}
?>


	
    <script src="jquery-1.6.2.min.js"></script>
            <script type="text/javascript" src="jquery.flexslider.js"></script>
			
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript" src="jquery.colorbox2.js"></script>
              <script type="text/javascript">
			  $(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
				$(".group1").colorbox({rel:'group1', transition:"none", height:"75%"});
				$(".group2").colorbox({rel:'group2', transition:"fade"});
				$(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
				$(".group4").colorbox({rel:'group4', slideshow:true});
				$(".ajax").colorbox();
				$(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
				$(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
				$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
				$(".inline").colorbox({inline:true, width:"50%"});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});

				$('.non-retina').colorbox({rel:'group5', transition:'none'})
				$('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});
				
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});
   
		
		
      </script> 
	  
	  
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
</body>
</html>