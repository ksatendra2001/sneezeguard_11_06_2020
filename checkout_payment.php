<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

// if the customer is not logged on, redirect them to the login page
  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

// if there is nothing in the customers cart, redirect them to the shopping cart page
  if ($cart->count_contents() < 1) {
    tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
  }

// if no shipping method has been selected, redirect the customer to the shipping method selection page
  if (!tep_session_is_registered('shipping')) {
    tep_redirect(tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
  }

// avoid hack attempts during the checkout procedure by checking the internal cartID
  if (isset($cart->cartID) && tep_session_is_registered('cartID')) {
    if ($cart->cartID != $cartID) {
      tep_redirect(tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
    }
  }

// Stock Check
  if ( (STOCK_CHECK == 'true') && (STOCK_ALLOW_CHECKOUT != 'true') ) {
    $products = $cart->get_products();
    for ($i=0, $n=sizeof($products); $i<$n; $i++) {
      if (tep_check_stock($products[$i]['id'], $products[$i]['quantity'])) {
        tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
        break;
      }
    }
  }

// if no billing destination address was selected, use the customers own address as default
  if (!tep_session_is_registered('billto')) {
    tep_session_register('billto');
    $billto = $customer_default_address_id;
  } else {
// verify the selected billing address
    if ( (is_array($billto) && empty($billto)) || is_numeric($billto) ) {
      $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$billto . "'");
      $check_address = tep_db_fetch_array($check_address_query);

      if ($check_address['total'] != '1') {
        $billto = $customer_default_address_id;
        if (tep_session_is_registered('payment')) tep_session_unregister('payment');
      }
    }
  }

  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;

  if (!tep_session_is_registered('comments')) tep_session_register('comments');
  if (isset($HTTP_POST_VARS['comments']) && tep_not_null($HTTP_POST_VARS['comments'])) {
    $comments = tep_db_prepare_input($HTTP_POST_VARS['comments']);
  }

  $total_weight = $cart->show_weight();
  $total_count = $cart->count_contents();

// load all enabled payment modules
  require(DIR_WS_CLASSES . 'payment.php');
  $payment_modules = new payment;

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_PAYMENT);

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));

  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<!-- BOF Agree2Terms_v2 jQuery -->
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<script type='text/javascript' src='js/jquery.simplemodal.js'></script>
<script type='text/javascript' src='js/osx.js'></script>
<script language="javascript"><!--
function rowOverEffect(object) {
  if (document.checkout_payment.elements[object].parentNode.parentNode.className != 'moduleRowSelected') {
    document.checkout_payment.elements[object].parentNode.parentNode.className = 'moduleRowOver';
  }
}

function rowOutEffect(object) {
  if (document.checkout_payment.elements[object].checked) {
    document.checkout_payment.elements[object].parentNode.parentNode.className = 'moduleRowSelected';
  } else {
    document.checkout_payment.elements[object].parentNode.parentNode.className = 'infoBoxContents';
  }
}

function checkboxRowEffect(object) {
  document.checkout_payment.elements[object].checked = !document.checkout_payment.elements[object].checked;
  if(document.checkout_payment.elements[object].checked) {
    document.checkout_payment.elements[object].parentNode.parentNode.className = 'moduleRowSelected';
  } else {
    document.checkout_payment.elements[object].parentNode.parentNode.className = 'moduleRowOver';
  }
}

function check_agree(TheForm) {
  if (TheForm.agree.checked) {
    return true;
  } else {
    alert(unescape('<?php echo CONDITION_AGREEMENT_ERROR; ?>'));
    return false;
  }

}
var win = null;
function NewWindow(mypage,myname,w,h,scroll){
LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
settings =
'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
win = window.open(mypage,myname,settings)
}
//--></script>
<!--<link type='text/css' href='css/osx.css' rel='stylesheet' media='screen' />-->
<!-- EOF Agree2Terms_v2 jQuery -->
<script>
  $(document).ready(function(){
    if ($.browser.msie  && parseInt($.browser.version, 10) === 7) {
        $("#ex1").css('width',"100%");
        $(".price_table").css("text-align","left");
    }
    });

</script>


<?php
if (!$detect->isMobile())
{
?> 
<style>

.processccc{color:#605b5b; font-size:12px;}
</style>
<div class="" style="background-color:white;" >

<table width="100%" border="0" style="font:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bolder;  ">
  <tr>

    <td class="processccc">1.Log In</td>

    <td class="processccc">2. Address Information</td>

    <td class="processccc">3. Shipping & Delivery</td>

    <td class="processccc"><b style="color:Red;">4. Payment Options</b></td>

    <td class="processccc">5. Order Review</td>

    <td class="processccc">6. Order Receipt</td>

  </tr>
</table>


<div class="form_white" style="height:auto !important; " >
<h3  align="left" style="padding:5px; font-family:Arial, Helvetica, sans-serif; color:#000000; font-size:16px;">Please enter your payment information</h3>  
<script type="text/javascript"><!--
var selected;

function selectRowEffect(object, buttonSelect) {
  if (!selected) {
    if (document.getElementById) {
      selected = document.getElementById('defaultSelected');
    } else {
      selected = document.all['defaultSelected'];
    }
  }

  if (selected) selected.className = 'moduleRow';
  object.className = 'moduleRowSelected';
  selected = object;

// one button is not an array
  if (document.checkout_payment.payment[0]) {
    document.checkout_payment.payment[buttonSelect].checked=true;
  } else {
    document.checkout_payment.payment.checked=true;
  }
}

function rowOverEffect(object) {
  if (object.className == 'moduleRow') object.className = 'moduleRowOver';
}

function rowOutEffect(object) {
  if (object.className == 'moduleRowOver') object.className = 'moduleRow';
}
//--></script>
 <?php if (isset($$payment->form_action_url)) {
    $form_action_url = $$payment->form_action_url;
  } else {
    $form_action_url = tep_href_link(FILENAME_CHECKOUT_CONFIRMATION, '', 'SSL');
  } 
//<!-- BOF Agree2Terms_v2 jQuery -->
  echo tep_draw_form('checkout_confirmation', $form_action_url, 'post', 'onsubmit="return check_agree(this);"');
//<!-- EOF Agree2Terms_v2 jQuery -->
?>
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="/* border-bottom:1px solid #ccc; *//* border-left:1px solid #ccc; */border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td width="55%"  style="" >
<?php echo $payment_modules->javascript_validation(); ?>


<?php echo tep_draw_form('checkout_payment', tep_href_link(FILENAME_CHECKOUT_CONFIRMATION, '', 'SSL'), 'post', 'onsubmit="return check_form();"', true,'ie'); ?>

<div class="contentContainer" style="text-align: left;">

<?php
  if (isset($HTTP_GET_VARS['payment_error']) && is_object(${$HTTP_GET_VARS['payment_error']}) && ($error = ${$HTTP_GET_VARS['payment_error']}->get_error())) {
?>

  <div class="contentText">
    <?php echo '<strong>' . tep_output_string_protected($error['title']) . '</strong>'; ?>

    <p class="messageStackError"><?php echo tep_output_string_protected($error['error']); ?></p>
  </div>

<?php
  }
?>

  
  <div style="clear: both;"></div>

  <table> <tr><td><h2  style="color:black; text-align:left;"><?php echo TABLE_HEADING_PAYMENT_METHOD; ?></h2></td><td><img src="img/creditCards.jpg" width="156" height="50" /></td></tr></table>

<?php
  $selection = $payment_modules->selection();

  if (sizeof($selection) > 1) {
?>

  <div class="contentText" style="font-size:13px;">
    <div style="float: right;">
	      <?php // echo '<strong>' . TITLE_PLEASE_SELECT . '</strong>'; ?>
    </div>

 <?php echo TEXT_SELECT_PAYMENT_METHOD; ?>
  </div>

<?php
    } elseif ($free_shipping == false) {
?>
  <div class="contentText" style="font-size:13px;">
    <?php echo TEXT_ENTER_PAYMENT_INFORMATION; ?>
  </div>

<?php
    }
?>

  <div class="contentText">

<?php
  $radio_buttons = 0;
  for ($i=0, $n=sizeof($selection); $i<$n; $i++) {
?>

    <table border="0" width="100%" cellspacing="0" cellpadding="2" style="padding-left:20px">

<?php
    if ( ($selection[$i]['id'] == $payment) || ($n == 1) ) {
      echo '      <tr id="defaultSelected" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, ' . $radio_buttons . ')" style="font-size:15px;">' . "\n";
    } else {
      echo '      <tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, ' . $radio_buttons . ')" style="font-size:15px;">' . "\n";
	  
    }
?>
<!--
 <td align="Left" width="10px">
      </td>
-->
<?php
    if (sizeof($selection) > 1) {
      echo tep_draw_radio_field('payment', $selection[$i]['id'], ($selection[$i]['id'] == $payment));
    } else {
      echo tep_draw_hidden_field('payment', $selection[$i]['id']);
    }
?>

  
        <td><strong><?php echo $selection[$i]['module']; ?></strong></td>
       
      </tr>

<?php
    if (isset($selection[$i]['error'])) {
?>

      <tr>
        <td colspan="2"><?php echo $selection[$i]['error']; ?></td>
      </tr>

<?php
    } elseif (isset($selection[$i]['fields']) && is_array($selection[$i]['fields'])) {
?>

      <tr>
        <td colspan="2"><table border="0" cellspacing="2" cellpadding="2" style="padding-left:20px" >

<?php
      for ($j=0, $n2=sizeof($selection[$i]['fields']); $j<$n2; $j++) {
?>

          <tr>
            <td><?php echo $selection[$i]['fields'][$j]['title']; ?></td>
            <td><?php echo $selection[$i]['fields'][$j]['field']; ?></td>
          </tr>

<?php
      }
?>

        </table></td>
      </tr>

<?php
    }
?>

    </table>

<?php
    $radio_buttons++;
  }
?>

  </div>
  <?php
  if (is_array($payment_modules->modules)) {
    echo $payment_modules->process_button();
  }

//<!-- BOF Agree2Terms_v2 jQuery -->
		  //echo tep_draw_form('checkout_confirmation', $form_action_url, 'post', 'onsubmit="return check_agree(this);"');
		  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CONDITIONS);
?>
			<table width="100%" class="" border="0" style="border-collapse: collapse">
				<tr>
		            <td>
						<table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
						
						
			              <tr class="infoBoxContents" style="font-size: 13px;">
			                <td align="right" border="0" colspan="2">
			          			<b>
								<?php echo CONDITION_AGREEMENT; ?></b>
							</td>
							
							<td onclick="window.document.checkout_payment.agree.checked = !window.document.checkout_payment.agree.checked;" align="right" width="20px" >
							<!-- aler for read conditions and term-->
							
							

							
							
							<input id="myCheck" class="osx" type="checkbox" name="agree" value="true" />
							
<script>							
function check() {
    document.getElementById("myCheck").checked = true;
}
</script>
						


<!--

<script>
							function Mycheck(obj) {
                         if (obj.checked) {
                           alert('Read Conditions and terms of use frist');
                        }

                        return true;
                   }
							</script>
<script>


function undisableBtn() {
    document.getElementById("myBtn").disabled = false;
}
</script>	-->

						

			       <!--
				   <a href="checkout_payment.php" onClick="alert('Read Conditions and terms of use frist!')" >
							<input type="checkbox" name="agree" id="myBtn" disabled  />
							</a>
				   <input type="checkbox" value="true" name="agree" class="osx"  onclick="return Mycheck(this);" />
							
							
							
							<!--/echo tep_draw_checkbox_field('agree','true', false, 'onclick="window.document.checkout_payment.agree.checked = !window.document.checkout_payment.agree.checked;"');-->
							
							
							</td>
			          	  </tr>
						  <tr class="infoBoxContents">
						  	<td align="right" colspan="2">
							  <a href="javascript:void(0);" class="osx">
							  	<u style="font-size: 13px;" ><?php echo CONDITIONS; ?></u>
							  </a>
							</td>
		              	  </tr>
						</table>
		            </td>
		          </tr>
		            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
		          </tr>
		          <tr>
		            <td><table border="0" width="100%" cellspacing="1" cellpadding="2"  class="infoBox">
		              <tr class="infoBoxContents">
		                <td align="right"><?php //echo tep_image_submit('button_confirm_order.gif', IMAGE_BUTTON_CONFIRM_ORDER); ?></td>
		              </tr>
		            </table></td>
		          </tr>
		          <tr>
		            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
		          </tr>
		    </table></form></td>
		    </tr>
        </table>
        <style>
            #osx-modal-content, #osx-modal-data {display:none;}
            /* Overlay */
            #osx-overlay {background-color:#000; cursor:wait;}
            /* Container */
            #osx-container {background-color:#eee; color:#000; font: 16px/24px "Lucida Grande",Arial,sans-serif; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; border-radius:0 0 6px 6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
            #osx-container a {color:#ddd;}
            #osx-container #osx-modal-title {color:#000; background-color:#ddd; border-bottom:1px solid #ccc; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #f4f4f4;}
            #osx-container .close {display:none; position:absolute; right:0; top:0;}
            #osx-container .close a {display:block; color:#777; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
            #osx-container .close a:hover {color:#000;}
            #osx-container #osx-modal-data {font-size:12px; padding:6px 12px;}
            #osx-container h2 {margin:10px 0 6px;}
            #osx-container p {margin-bottom:10px;}
            #osx-container span {color:#777;}
        </style>
        <!-- OSX CSS not foud so comment done on 11june 2015 -->
		<div id="osx-modal-content">
			<div id="osx-modal-title"><?=CONDITIONS?></div>
			<div class="close"><button class="simplemodal-close"  onclick="check()">x</button></div>
			<div id="osx-modal-data" style="overflow:scroll;height:570px;">
				<?
				//=TERMSCONDITIONS
				$res=tep_db_query("select * from ".TABLE_CONDITION_AND_TERM);
                while($row=tep_db_fetch_array($res)){
                echo$msg_con_term=$row["message"];
	            }
				?>
				<p><button class="simplemodal-close"  onclick="check()"><?=TEXT_AGREE_CLOSE?></button> <span><?=TEXT_AGREE_PRESS?></span></p>
			</div>
		</div>
  <h2><?php echo TABLE_HEADING_BILLING_ADDRESS; ?></h2>

  <div class="contentText">
   
   
   <table style="padding-left:20px">
    	<tbody><tr><td rowspan="2"><?php echo tep_address_format($order->billing['format_id'], $order->billing, 1, ' ', '<br />'); ?></td></tr>
        
        <tr><td valign="top" align="center" width="200px"> <br><br><div class="buttonAction"><a id="tdb1" href="<?php echo tep_href_link(FILENAME_CHECKOUT_PAYMENT_ADDRESS, '', 'SSL'); ?>"><img src="img/new_icons/change_address.png" style="width: 165px;"></a></span></div></td>
        </tr>
    </tbody></table>
   <!--
   
    <table style="padding-left:20px">
    	<tr><td rowspan="2"><?php echo tep_address_format($order->billing['format_id'], $order->billing, 1, ' ', '<br />'); ?></td></tr>
        
		<tr><td valign="top" align="center" width="200px"> <?php //echo TEXT_SELECTED_BILLING_DESTINATION; ?><br /><br /><div class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CHANGE_ADDRESS, 'home', tep_href_link(FILENAME_CHECKOUT_PAYMENT_ADDRESS, '', 'SSL')); ?></td>
        </tr>
    </table>-->
    </div>

   </div>
</td>
<td valign="top">
 
 <?php
// Discount Code 2.6 - start
if (MODULE_ORDER_TOTAL_DISCOUNT_STATUS == 'true') {
?>
<h2>Dealer code <?php /*echo TEXT_DISCOUNT_CODE; */?></h2>

<table> 
        <tr><td colspan="2" style="font-size:13px;padding-left:10px;"><b>Do you have a Dealer code  ? Enter dealer code here :</b></td></tr>
		<tr><td style="padding-left:10px;">
<?php echo tep_draw_input_field('discount_code', $sess_discount_code, 'size="10"'); ?>
<?php
}
// Discount Code 2.6 - end
?>
   </td></tr>     
    </table>
  </div>
   <h2><?php echo TABLE_HEADING_COMMENTS; ?></h2>

  <div class="contentText" style="padding-left:10px;">
    <?php echo tep_draw_textarea_field('comments', 'soft', '50', '5', $comments); ?>
    
      
	  <?php 
	  
	  /* kgt - discount coupons */
	if( MODULE_ORDER_TOTAL_DISCOUNT_COUPON_STATUS == 'true' ) {
?>
<h2 style="font-size:13px;"><?php echo TABLE_HEADING_COUPON; ?></h2>

  <div class="contentText">
  	 </div>
   
        <div class="contentText" style="font-size: 14px;margin-left: 24px;">
        <?php echo ENTRY_DISCOUNT_COUPON.' '.tep_draw_input_field('coupon', '', 'size="15"', $coupon); ?>
  	 </div>
		
<?php
	}
/* end kgt - discount coupons */ 
?>
  <div class="contentText">
  
    
	<div style="float: right; width: 85%; padding-top: 35px; padding-left: 15%;">
		<?php
			preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
			if(count($matches)<2){
			  preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
			}
			if (count($matches)>1){
			  echo '<div style="float: right;"><input type="image" class="updatebutton" src="img/new_icons/continue.png" alt="" button="" onclick="javascript:document.forms["checkout_payment"].submit();" style="width:50%"></div>';
			}else{
			  echo '<div style="float: right;"><input type="image" class="updatebutton" src="img/new_icons/continue.png" style="width:50%" alt="" button="" onclick="javascript:document.forms["checkout_payment"].submit();"></div>';
			}

		?>

    <!--<div style="float: right;"><?php //echo tep_image_submit("continue.gif", IMAGE_BUTTON_CONFIRM_ORDER, 'button'); ?></div>-->
  </div>
</div>

<script type="text/javascript">
$('#coProgressBar').progressbar({
  value: 66
});
</script>
</td>
</tr>
</table>
</form>
</td>
</tr>
</table>
</div>

<?
}
else{
?>
<td id="ex1" align=center width="190" valign="top">
<style>

.processccc{color:#605b5b; font-size:12px;}
</style>
<div class="" style="background-color:white;" >

<table width="100%" border="0" style="font:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bolder;  ">
  <tr>

    <td class="processccc">1.Log In</td>

    <td class="processccc">2. Address Information</td>

    <td class="processccc">3. Shipping & Delivery</td>

    <td class="processccc"><b style="color:Red;">4. Payment Options</b></td>

    <td class="processccc">5. Order Review</td>

    <td class="processccc">6. Order Receipt</td>

  </tr>
</table>


<div class="form_white" style="height:auto !important; " >
<h3  align="left" style="padding:5px; font-family:Arial, Helvetica, sans-serif; color:#000000; font-size:24px;">Please enter your payment information</h3>  
<script type="text/javascript"><!--
var selected;

function selectRowEffect(object, buttonSelect) {
  if (!selected) {
    if (document.getElementById) {
      selected = document.getElementById('defaultSelected');
    } else {
      selected = document.all['defaultSelected'];
    }
  }

  if (selected) selected.className = 'moduleRow';
  object.className = 'moduleRowSelected';
  selected = object;

// one button is not an array
  if (document.checkout_payment.payment[0]) {
    document.checkout_payment.payment[buttonSelect].checked=true;
  } else {
    document.checkout_payment.payment.checked=true;
  }
}

function rowOverEffect(object) {
  if (object.className == 'moduleRow') object.className = 'moduleRowOver';
}

function rowOutEffect(object) {
  if (object.className == 'moduleRowOver') object.className = 'moduleRow';
}
//--></script>
 <?php if (isset($$payment->form_action_url)) {
    $form_action_url = $$payment->form_action_url;
  } else {
    $form_action_url = tep_href_link(FILENAME_CHECKOUT_CONFIRMATION, '', 'SSL');
  } 
//<!-- BOF Agree2Terms_v2 jQuery -->
  echo tep_draw_form('checkout_confirmation', $form_action_url, 'post', 'onsubmit="return check_agree(this);"');
//<!-- EOF Agree2Terms_v2 jQuery -->
?>
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="/* border-bottom:1px solid #ccc; *//* border-left:1px solid #ccc; */border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td   style="" >
<?php echo $payment_modules->javascript_validation(); ?>


<?php echo tep_draw_form('checkout_payment', tep_href_link(FILENAME_CHECKOUT_CONFIRMATION, '', 'SSL'), 'post', 'onsubmit="return check_form();"', true,'ie'); ?>

<div class="contentContainer" style="text-align: left;">

<?php
  if (isset($HTTP_GET_VARS['payment_error']) && is_object(${$HTTP_GET_VARS['payment_error']}) && ($error = ${$HTTP_GET_VARS['payment_error']}->get_error())) {
?>

  <div class="contentText">
    <?php echo '<strong>' . tep_output_string_protected($error['title']) . '</strong>'; ?>

    <p class="messageStackError"><?php echo tep_output_string_protected($error['error']); ?></p>
  </div>

<?php
  }
?>

  
  <div style="clear: both;"></div>

  <table style="width:97%; margin-left:30px;"> <tr><td><h2  style="color:black; text-align:left; font-size:24px;"><?php echo TABLE_HEADING_PAYMENT_METHOD; ?></h2></td><td><img src="img/creditCards.jpg" style="width:355px;" /></td></tr></table>

<?php
  $selection = $payment_modules->selection();

  if (sizeof($selection) > 1) {
?>

  <div class="contentText" style="font-size:23px; margin-left:20px;">
    <div style="float: right;">
	      <?php // echo '<strong>' . TITLE_PLEASE_SELECT . '</strong>'; ?>
    </div>

 <?php echo TEXT_SELECT_PAYMENT_METHOD; ?>
  </div>

<?php
    } elseif ($free_shipping == false) {
?>
  <div class="contentText" style="font-size:23px; margin-left:20px;">
    <?php echo TEXT_ENTER_PAYMENT_INFORMATION; ?>
  </div>

<?php
    }
?>

  <div class="contentText">

<?php
  $radio_buttons = 0;
  for ($i=0, $n=sizeof($selection); $i<$n; $i++) {
?>

    <table border="0" width="100%" cellspacing="0" cellpadding="2" style="padding-left:20px">

<?php
    if ( ($selection[$i]['id'] == $payment) || ($n == 1) ) {
      echo '      <tr id="defaultSelected" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, ' . $radio_buttons . ')" style="font-size:15px;">' . "\n";
    } else {
      echo '      <tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, ' . $radio_buttons . ')" style="font-size:15px;">' . "\n";
	  
    }
?>
<!--
 <td align="Left" width="10px">
        </td>
-->
<?php
    if (sizeof($selection) > 1) {
      echo tep_draw_radio_field('payment', $selection[$i]['id'], ($selection[$i]['id'] == $payment));
    } else {
      echo tep_draw_hidden_field('payment', $selection[$i]['id']);
    }
?>


<br />
        <td><strong style="font-size:24px;"><?php echo $selection[$i]['module']; ?></strong></td>
       
      </tr>

<?php
    if (isset($selection[$i]['error'])) {
?>

      <tr>
        <td colspan="2"><?php echo $selection[$i]['error']; ?></td>
      </tr>

<?php
    } elseif (isset($selection[$i]['fields']) && is_array($selection[$i]['fields'])) {
?>
<style>
.contentText, .contentText table {
    /* padding: 5px 0 5px 0; */
    font-size: 25px;
    line-height: 1.5;
}
.input type 
</style>

      <tr style="font-size:23px;">
        <td colspan="2">
		
		<table border="0" cellspacing="2" cellpadding="2" style="padding-left:20px">


          <tbody><tr>
            <td>Credit Card Owner:</td>
            <td><input type="text" name="cc_owner" style="width:296px;height: 40px; font-size:25px"></td>
          </tr>


          <tr>
            <td>Credit Card Number:</td>
            <td><input type="text" name="cc_number" style="width:296px;height: 40px; font-size:25px"></td>
          </tr>


          <tr>
            <td>Credit Card Expiry Date:</td>
            <td>
			<select name="cc_expires_month" onchange="getPrice(this.form)"  style="width:150px;height: 40px; font-size:25px">
			<option value="01">January</option>
			<option value="02">February</option>
			<option value="03">March</option>
			<option value="04">April</option>
			<option value="05">May</option>
			<option value="06">June</option>
			<option value="07">July</option>
			<option value="08">August</option>
			<option value="09">September</option>
			<option value="10">October</option>
			<option value="11">November</option>
			<option value="12">December</option>
			</select>&nbsp;
			<select name="cc_expires_year" onchange="getPrice(this.form)" style="width:140px;height: 40px; font-size:25px">
			<option value="18">2018</option>
			<option value="19">2019</option>
			<option value="20">2020</option>
			<option value="21">2021</option>
			<option value="22">2022</option>
			<option value="23">2023</option>
			<option value="24">2024</option>
			<option value="25">2025</option>
			<option value="26">2026</option>
			<option value="27">2027</option>
			</select></td>
          </tr>


        </tbody></table>
		
		<!--
		
		<table border="0" cellspacing="2" cellpadding="2" style="padding-left:20px" >

<?php
      for ($j=0, $n2=sizeof($selection[$i]['fields']); $j<$n2; $j++) {
?>

          <tr>
            <td><?php echo $selection[$i]['fields'][$j]['title']; ?></td>
            <td><?php echo $selection[$i]['fields'][$j]['field']; ?></td>
          </tr>

<?php
      }
?>

        </table>
		
		-->
		
		</td>
      </tr>

<?php
    }
?>

    </table>

<?php
    $radio_buttons++;
  }
?>

  </div>
  <?php
  if (is_array($payment_modules->modules)) {
    echo $payment_modules->process_button();
  }

//<!-- BOF Agree2Terms_v2 jQuery -->
		  //echo tep_draw_form('checkout_confirmation', $form_action_url, 'post', 'onsubmit="return check_agree(this);"');
		  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CONDITIONS);
?>
			<table width="100%" class="" border="0" style="border-collapse: collapse">
				<tr>
		            <td>
						<table border="0" width="100%" cellspacing="0" cellpadding="0" style="border-collapse: collapse">
						
						
			              <tr class="infoBoxContents" style="font-size: 23px;">
			                <td align="right" border="0" colspan="2">
			          			<b>
								<?php echo CONDITION_AGREEMENT; ?></b>
							</td>
							
							<td onclick="window.document.checkout_payment.agree.checked = !window.document.checkout_payment.agree.checked;" align="right" width="20px" >
							<!-- aler for read conditions and term-->
							
							

							
							
							<input id="myCheck" class="osx" type="checkbox" style="width: 38px; height: 26px;" name="agree" value="true" />
							
<script>							
function check() {
    document.getElementById("myCheck").checked = true;
}
</script>
						


<!--

<script>
							function Mycheck(obj) {
                         if (obj.checked) {
                           alert('Read Conditions and terms of use frist');
                        }

                        return true;
                   }
							</script>
<script>


function undisableBtn() {
    document.getElementById("myBtn").disabled = false;
}
</script>	-->

						

			       <!--
				   <a href="checkout_payment.php" onClick="alert('Read Conditions and terms of use frist!')" >
							<input type="checkbox" name="agree" id="myBtn" disabled  />
							</a>
				   <input type="checkbox" value="true" name="agree" class="osx"  onclick="return Mycheck(this);" />
							
							
							
							<!--/echo tep_draw_checkbox_field('agree','true', false, 'onclick="window.document.checkout_payment.agree.checked = !window.document.checkout_payment.agree.checked;"');-->
							
							
							</td>
			          	  </tr>
						  <tr class="infoBoxContents">
						  	<td align="right" colspan="2">
							  <a href="javascript:void(0);" class="osx">
							  	<u style="font-size: 23px;" ><?php echo CONDITIONS; ?></u>
							  </a>
							</td>
		              	  </tr>
						</table>
		            </td>
		          </tr>
		            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
		          </tr>
		          <tr>
		            <td><table border="0" width="100%" cellspacing="1" cellpadding="2"  class="infoBox">
		              <tr class="infoBoxContents">
		                <td align="right"><?php //echo tep_image_submit('button_confirm_order.gif', IMAGE_BUTTON_CONFIRM_ORDER); ?></td>
		              </tr>
		            </table></td>
		          </tr>
		          <tr>
		            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
		          </tr>
		    </table></form></td>
		    </tr>
        </table>
        <style>
            #osx-modal-content, #osx-modal-data {display:none;}
            /* Overlay */
            #osx-overlay {background-color:#000; cursor:wait;}
            /* Container */
            #osx-container {background-color:#eee; color:#000; font: 16px/24px "Lucida Grande",Arial,sans-serif; padding-bottom:4px; width:600px; -moz-border-radius-bottomleft:6px; -webkit-border-bottom-left-radius:6px; -moz-border-radius-bottomright:6px; -webkit-border-bottom-right-radius:6px; border-radius:0 0 6px 6px; -moz-box-shadow:0 0 64px #000; -webkit-box-shadow:0 0 64px #000;}
            #osx-container a {color:#ddd;}
            #osx-container #osx-modal-title {color:#000; background-color:#ddd; border-bottom:1px solid #ccc; font-weight:bold; padding:6px 8px; text-shadow:0 1px 0 #f4f4f4;}
            #osx-container .close {display:none; position:absolute; right:0; top:0;}
            #osx-container .close a {display:block; color:#777; font-weight:bold; padding:6px 12px 0; text-decoration:none; text-shadow:0 1px 0 #f4f4f4;}
            #osx-container .close a:hover {color:#000;}
            #osx-container #osx-modal-data {font-size:12px; padding:6px 12px;}
            #osx-container h2 {margin:10px 0 6px;}
            #osx-container p {margin-bottom:10px;}
            #osx-container span {color:#777;}
        </style>
        <!-- OSX CSS not foud so comment done on 11june 2015 -->
		<div id="osx-modal-content">
			<div id="osx-modal-title"><?=CONDITIONS?></div>
			<div class="close"><button class="simplemodal-close"  onclick="check()">x</button></div>
			<div id="osx-modal-data" style="overflow:scroll;height:570px;">
				<?
				//=TERMSCONDITIONS
				$res=tep_db_query("select * from ".TABLE_CONDITION_AND_TERM);
                while($row=tep_db_fetch_array($res)){
                echo$msg_con_term=$row["message"];
	            }
				?>
				<p><button class="simplemodal-close"  onclick="check()"><?=TEXT_AGREE_CLOSE?></button> <span><?=TEXT_AGREE_PRESS?></span></p>
			</div>
		</div>
  <h2 style="font-size:25px;"><?php echo TABLE_HEADING_BILLING_ADDRESS; ?></h2>

  <div class="contentText">
   
   
   <table style="padding-left:20px; width:100%">
    	<tbody><tr>
		<td style="width:40%"><?php echo tep_address_format($order->billing['format_id'], $order->billing, 1, ' ', '<br />'); ?></td>
		<td style="width:60%" valign="middle" align="center" align="left" > <br><br><div class="buttonAction"><a id="tdb1" href="<?php echo tep_href_link(FILENAME_CHECKOUT_PAYMENT_ADDRESS, '', 'SSL'); ?>"><img src="img/new_icons/change_address.png" style="width: 288px;"></a></span></div></td>
        </tr>
    </tbody></table>
   <!--
   
    <table style="padding-left:20px">
    	<tr><td rowspan="2"><?php echo tep_address_format($order->billing['format_id'], $order->billing, 1, ' ', '<br />'); ?></td></tr>
        
		<tr><td valign="top" align="center" width="200px"> <?php //echo TEXT_SELECTED_BILLING_DESTINATION; ?><br /><br /><div class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CHANGE_ADDRESS, 'home', tep_href_link(FILENAME_CHECKOUT_PAYMENT_ADDRESS, '', 'SSL')); ?></td>
        </tr>
    </table>-->
    </div>

   </div>
<!--</td>
<td valign="top">
 -->
 <?php
// Discount Code 2.6 - start
if (MODULE_ORDER_TOTAL_DISCOUNT_STATUS == 'true') {
?>
<h2 style="font-size:25px;">Dealer code <?php /*echo TEXT_DISCOUNT_CODE; */?></h2>

<table> 
        <tr><td colspan="2" style="font-size:22px;padding-left:20px;"><b>Do you have a Dealer code  ? Enter dealer code here :</b></td></tr>
		<tr><td style="padding-left:20px;">
		<input type="text" name="discount_code" style="font-size:25px; width:290px; height:40px;">
<?php //echo tep_draw_input_field('discount_code', $sess_discount_code, 'size="20"'); ?>
<?php
}
// Discount Code 2.6 - end
?>
   </td></tr>     
    </table>
  </div>
   <h2 style="font-size:25px;"><?php echo TABLE_HEADING_COMMENTS; ?></h2>

  <div class="contentText" style="padding-left:10px; text-align:center; font-size:24px;">
    <?php echo tep_draw_textarea_field('comments', 'soft', '100', '10', $comments); ?>
    
      
	  <?php 
	  
	  /* kgt - discount coupons */
	if( MODULE_ORDER_TOTAL_DISCOUNT_COUPON_STATUS == 'true' ) {
?>
<h2 style="font-size:22px;"><?php echo TABLE_HEADING_COUPON; ?></h2>

  <div class="contentText">
  	 </div>
   
        <div class="contentText" style="font-size: 24px;margin-left: 24px;">
		 <?php echo ENTRY_DISCOUNT_COUPON; ?><input type="" name="coupon"  style="font-size:25px; width:290px; height:40px;">
        <?php// echo ENTRY_DISCOUNT_COUPON.' '.tep_draw_input_field('coupon', '', 'size="15"', $coupon); ?>
  	 </div>
		
<?php
	}
/* end kgt - discount coupons */ 
?>
  <div class="contentText">
  
    
	<div style="float: right; width: 85%; padding-top: 35px; padding-left: 15%;">
		<?php
			preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
			if(count($matches)<2){
			  preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
			}
			if (count($matches)>1){
			  echo '<div style="float: right;"><input type="image" class="updatebutton" src="img/new_icons/continue.png" alt="" button="" onclick="javascript:document.forms["checkout_payment"].submit();" style="width:85%"></div>';
			}else{
			  echo '<div style="float: right;"><input type="image" class="updatebutton" src="img/new_icons/continue.png" style="width:85%" alt="" button="" onclick="javascript:document.forms["checkout_payment"].submit();"></div>';
			}

		?>

    <!--<div style="float: right;"><?php //echo tep_image_submit("continue.gif", IMAGE_BUTTON_CONFIRM_ORDER, 'button'); ?></div>-->
  </div>
</div>

<script type="text/javascript">
$('#coProgressBar').progressbar({
  value: 66
});
</script>
</td>
</tr>
</table>
</form>
</td>
</tr>
</table>
</div>

<?
}

?>

<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
