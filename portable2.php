<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_DEFAULT));

  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<?php
    $portable_id="";
     $category_list=tep_db_query("select distinct parent_id as pid from ".TABLE_CATEGORIES." where parent_id!=0");
      $category_list_array=array();
      $i=0;
      while($category_list_row=tep_db_fetch_array($category_list)){
      	$category_list_array[$i]=$category_list_row['pid'];
    	$i++;
      }
       $ids=implode(', ', $category_list_array);
      
      $category_list=tep_db_query("select c.categories_id as pid, cd.categories_name as cname from ".TABLE_CATEGORIES." as c, ".TABLE_CATEGORIES_DESCRIPTION." as cd where c.categories_id in (".$ids.") and c.categories_id=cd.categories_id order by sort_order");
      $category_list_array=array();
      $i=0;
      while($category_list_row=tep_db_fetch_array($category_list)){
        if($category_list_row['cname']=="Portable"){
      	     $portable_id=$category_list_row['pid'];
        }
      }
      
      $category_list=tep_db_query("select c.categories_id as pid from ".TABLE_CATEGORIES." as c where c.parent_id=".$portable_id." order by sort_order");
      $category_list_array=array();
      $i=0;
      while($category_list_row=tep_db_fetch_array($category_list)){
	     $category_list_array[$i]=$category_list_row['pid'];
         $i++;
      }
      
    // Open Table
    $result = mysql_query(
              "SELECT * FROM Models");
    if (!$result) {
      echo("<P>Error performing query: " .
           mysql_error() . "</P>");
      exit();
    }
    
  $i=0;
    // Display all models
    while ( $row = mysql_fetch_array($result) ) {
    //Now only show the data for the Model in the address bar
	if($row["Name"] == "Orbit-720" || $row["Name"] == "B-950P-GLASS" || $row["Name"] == "EP-950-ACRYLIC" ||$row["Name"] == "Portable"  || $row["Name"] == "ALLIN1"){
	
	$Height = $row["Height"];
      $EndPan = $row["EndPan"];
      $Depth = $row["Depth"];
      $TopGlass = $row["TopGlass"];
      $HeatLamps = $row["HeatLamps"];
      $Lighting = $row["Lighting"];
      $FrontGlass = $row["FrontGlass"];
      $Tubing = $row["Tubing"];
      echo('<P>');
      echo('<TABLE WIDTH="751" HEIGHT="110" BORDER="0" class="custom" CELLPADDING=5>');
      echo('<tr>');
      echo('<td align=right width=20%>');
	  
      echo('<A HREF="'.tep_href_link('info.php', 'Model='.$row["Name"].'&pid='.$category_list_array[$i]).'">' . '<IMG SRC="images/Models/' . $row["Name"] . '.jpg" BORDER=0>' . '</A>');
      echo('</td>');
      echo('<td width=5 align=right>');
      echo('<IMG SRC=images/shortVertSep.jpg>');
      echo('</td>');
      echo('<td valign=top>');
      echo('<div class="linkClass">');
	  if($row["Name"] == "ALLIN1"){
	   echo('<A HREF="javascript:submit()">Model Name: ' . $row["Name"] . '</A><BR>');
	  }else{
      echo('<A HREF="'.tep_href_link('info.php', 'Model='.$row["Name"].'&pid='.$category_list_array[$i]).'">Model Name: ' . $row["Name"] . '</A><BR>');}
      echo('</div>');
      echo('<div class="modText">');
      echo('Height: ' . $Height . '<br>');
      if($Depth != "0"){
      echo('Depth: ' . $Depth . '<br>');
      }
      echo('Glass: ' . $FrontGlass . '<br>');
      echo('Tubing: ' . $Tubing . '<br>');
      echo('</div>');
      echo('</td>');
      echo('<td valign=center align=left width="5">');
      //if($EndPan || $HeatLamps || $Lighting  != "0"){
      echo('<IMG SRC=images/shortVertSep.jpg>');
      //}
      echo('</td>');
      echo('<td valign=top width=11%>');
      //if($EndPan || $HeatLamps || $Lighting  != "0"){
      echo('<div class="modName">');
      echo("<B>Options:</B><BR>");
      echo('</div>');
      echo('<div class="modText">');
      if($EndPan != "0")
      echo("End Panels " . "<br>");
      if($HeatLamps != "0")
      echo("Heat Lamps " . "<br>");
      if($Lighting != "0")
      echo("Lighting " . "<br>");
      //}
      //echo("Finishes");
      echo('</div>');
      echo('</td>');
      echo('</tr>');
      echo('</TABLE>');
      echo('<P>');
      $i++;
      }
      }
      
?>
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>