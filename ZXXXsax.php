

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en">
<head>
<!-- Start Google Add Conversion -->
<!-- Global site tag (gtag.js) - Google Ads: 1072651700 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-1072651700"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-1072651700');
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>

<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<meta name="description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs.">
<meta name="keywords" content="Sneeze Guard, sneeze guard glass, Portable Food Guards, Sneezeguards, Restaurant Supply Equipment">
<title>ADM Sneezeguards - Sneeze Guard Portable | Restaurant Food Guards</title>
<base href="http://localhost/sneezeguard/" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">
<link rel="canonical" href="https://www.sneezeguard.com//sneezeguard/wishlist.php">
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>


<!--
<script src="js/jquery-1.7.2.min.js"></script>
<link href="css/lightbox.css" rel="stylesheet" />
<link href="css/t/lightbox.css" rel="stylesheet" />
<script src="js/lightbox.js"></script>
<script src="js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="js/jquery-1.7.2.1.min.js"></script>
<link href="css/screen.css" rel="stylesheet" />
<script src="js/jquery.smooth-scroll.min.js"></script>
<link href="css/screen.css" rel="stylesheet" />
<script src="js/lightbox.js"></script>
-->



<script type="text/javascript" src="ext/jquery/bxGallery/jquery.bxGallery.1.1.min.js"></script>
<link rel="stylesheet" type="text/css" href="ext/jquery/fancybox/jquery.fancybox-1.3.4.css" />
<script type="text/javascript" src="ext/jquery/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="ext/960gs/960_24_col.css" />
<link rel="stylesheet" type="text/css" href="stylesheet.css" />
<!-- second meta tag START...... -->
<!-- second meta tag END...... -->
<link rel="icon" href="images/favicon.ico" type="img/ico">
<script type="text/javascript">
function getxmlHttpObj()
{
		//alert("hell")
		var xmlHttp;
		try
		{
			// Firefox, Opera 8.0+, Safari
			xmlHttp=new XMLHttpRequest();
		}
		catch (e)
		{
			// Internet Explorer
			try
			{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				try
				{
					xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e)
				{
					alert("Your browser does not support AJAX!");
					return false;
				}
			}
		}
		return xmlHttp;
}

function getPrice(product_id, opt_id, attr_arr)
{
		opt_id = document.getElementById("optionsid").value;
        opts_id=opt_id.split(",");
        //alert("kskdck");
        //alert(document.forms['cart_quantity'].elements['id['+opts_id[1]+']'].value);
        //alert(document.forms['cart_quantity'].elements['id['+opts_id[2]+']'].value);
        //alert(document.forms['cart_quantity'].elements['id['+opts_id[3]+']'].value);
        //alert(document.forms['cart_quantity'].elements['id['+opts_id[4]+']'].value);
        //alert(document.forms['cart_quantity'].elements['id['+opts_id[5]+']'].value);
        height=document.forms['cart_quantity'].elements['id['+opts_id[1]+']'].value;
        width=document.forms['cart_quantity'].elements['id['+opts_id[2]+']'].value;
        glass='EP11 '+width+'" '+'Glass (Squared Corners)';
        
        alert(product_name_price[glass]['1']);
        document.getElementById("display_price").innerHTML =product_name_price[glass]['1'];
		xmlHttpObj = getxmlHttpObj();
		//alert(option_value+ +product_id)
		xmlHttpObj.onreadystatechange=function()
		{
			if(xmlHttpObj.readyState==4)
			{
				val=xmlHttpObj.responseText.split('$');
				if(attr_arr==125){
					i=parseInt(val[0]);
					document.getElementById('left-post').innerHTML=i/2;
					document.getElementById('right-post').innerHTML=i/2;
				}
				if(attr_arr==126){
					document.getElementById('right-post').innerHTML=val[0];
					document.getElementById('left-post').innerHTML="0.00"
				}
				if(attr_arr==127){
					document.getElementById('left-post').innerHTML=val[0];
					document.getElementById('right-post').innerHTML="0.00"
				}
								
				document.getElementById("display_price").innerHTML =val[1];
			}
		}
		xmlHttpObj.open("GET","ajax_onchange_price.php?price="+price+"&option_id="+opt_id+"&product_id="+product_id+"&product_opt="+attr_arr,true);
		xmlHttpObj.send();
		
}
function validateForm(){
   if(document.getElementById('glass-face').value==0){
     return false; 
   }
   else{
     return true;
   } 
}
function changeAdditionalImage(obj){
    str='<img src="'+obj+'" style="width:100%"/><br style="clear:both;"/>';
    $("#additional_image").fadeOut(500, function(){        
        $(this).html(str); 
        $(this).fadeIn(300);       
    });
}

function changeImage(bay){
    $("#additional_image").fadeOut(100, function(){
        $(this).html('<img src="images//'+bay+'.jpg" style="width:100%"/><br style="clear:both;"/>');
        $(this).fadeIn(100);
        //$("#product_image").css("opacity","1.0");
        //$(".select-option").css("background","none");
        //$(".test-hide").css("opacity","1.0");
        //$(".message_p").remove();
        //$("#message_wp").remove();
    });
}
</script>
<!--<script type="text/javascript">
var a = new Image(); a.src = 'root.gif';
var b = new Image(); b.src = 'hover.gif';
var c = new Image(); c.src = 'active.gif';
</script>-->
<style type="text/css">
<!--
.style1 {
	font-family: Tahoma;
	font-weight: bold;
}
.style2 {font-family: Tahoma;float:left;
	font-size: 12;color:#CCCCCC;}
.style3 {color: #C7F900}
.style4 {
	font-family: Tahoma;
	color: #C7f900;
	font-weight: bold;
	float:left;
	font-size: 11px;
}
.style5 {
	color: #C7f900;
	font-weight: bold;
}
.style6 {color: #C7f900;float:left;
	font-size: 12;}
.style7 {
	font-size: 22px
}
.style8 {font-family: Tahoma; color: #C7f900; }
-->
</style>
<style type="text/css">
    .message_w {
    border: none;
}
</style>




<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-28436015-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28436015-1');
</script>





<!-- <script type="text/javascript">
	$(document).ready(function() {
}
	function news_feed_call(){
	$.ajax({
		
		type:'POST',
		url:'https://www.sneezeguard.com/news_rss.php',
		async: false
	}).success(function(){
        setTimeout(function(){news_feed_call();}, 2000000);
    });
	}

</script> -->
<!-- RSS Feed Strat -->
	<link rel="alternate" type="application/rss+xml" title="ADM Sneezeguards - Catalog Feed" href="rss.php?language=en">
	<!-- RSS Feed End -->


</head>
<body itemscope="" itemtype="http://schema.org/WebPage">




<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>
<style>

</style>
<script>

</script>


 <link rel="alternate" type="application/rss+xml" title="ADM Sneezeguards Catalog Feed English" href="rss.php?language=en" />
<table border="0" width="850" height="78" align="center" class="test-hide">
<tr>
	<td width="50%" align="left" valign="top"><a href="index.php"><img src="images/logo.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="450" height="72" /></a></td>


	<td align="left" valign=bottom width="40%">
		<div class="toplink">
		<IMG SRC="images/bulletpt.jpg" alt=""> <A HREF="http://localhost/sneezeguard/contact.php"  style="color:white;">Contact Us</A>
		<IMG SRC="images/bulletpt.jpg" alt=""> <A HREF="http://localhost/sneezeguard/term.php"  style="color:white;">Helpful Terminology</A>
		</div>
	</td>
    <td  width="10%" align="left" valign="top">
        <table cellpadding=0 cellspacing=0>
            <tr>
                <td width="22">
                    <a href=http://localhost/sneezeguard/wishlist.php>
                        <img src=images/wishlist_icon.png height=32 border=0 align="left" title="Sneeze Guard" alt="Sneeze Guard">
                    </a>
                </td>
                <td width="60" align=right>
                    <div align="left">
                        <span class=ch1 style="color:white;font-size: 13px; "> My Wishlist</span><br>
                        <a class=ml1 href=http://localhost/sneezeguard/wishlist.php style="color:white;font-size: 14px;">
                            (42) items
                        </a>
                    </div>
                </td>
            </tr>
        </table>
    </td>
		<td  width="10%" align="left" valign="top"><table cellpadding=0 cellspacing=0>
         <tr><td width="22"><a href=http://localhost/sneezeguard/shopping_cart.php><img src=images/m06.gif width=22 height=32 border=0 align="left" title="Sneeze Guard" alt="Sneeze Guard"></a></td>
         <td width="60" align=right><div align="left"><span class=ch1 style="color:white;font-size: 13px;"> Shopping Cart</span><br>
             <a class=ml1 href=http://localhost/sneezeguard/shopping_cart.php style="color:white;font-size: 14px;">(0) items</a></div></td></tr>
        </table><br /> 		<IMG SRC="images/bulletpt.jpg" alt=""> <a href="http://localhost/sneezeguard/logoff.php" style="color:white;font-size: 13px;">Log Off</a>		</td>
</tr>
</table>

<table border="0" width="856" height="40" align="center" margin="0" cellpadding="0" cellspacing="0" class="test-hide">

<tr>

<td>
<a href="http://localhost/sneezeguard/index.php" onmouseover="document.images['s1a'].src='images/navbar/homedown.jpg';" onmouseout="document.images['s1a'].src='images/navbar/homeup.jpg';"><img src="images/navbar/homeup.jpg" name="s1a" height="40" width="74" alt="" border="0" align="center"></a>
</td>

<td>
<a href="http://localhost/sneezeguard/index.php?cPath=86" onmouseover="document.images['s2a'].src='images/navbar/instockdown.jpg';" onmouseout="document.images['s2a'].src='images/navbar/instockup.jpg';"><img src="images/navbar/instockup.jpg" name="s2a" height="40" width="102" alt="" border="0" align="center"></a>
</td>
<td>
<a href="http://localhost/sneezeguard/custom.php" onmouseover="document.images['s3a'].src='images/navbar/customdown.jpg';" onmouseout="document.images['s3a'].src='images/navbar/customup.jpg';"><img src="images/navbar/customup.jpg" name="s3a" height="40" width="103" alt="" border="0" align="center"></a>
</td>
<td>
<a href="http://localhost/sneezeguard/adjustable.php" onmouseover="document.images['s4a'].src='images/navbar/adjustabledown.jpg';" onmouseout="document.images['s4a'].src='images/navbar/adjustableup.jpg';"><img src="images/navbar/adjustableup.jpg" name="s4a" height="40" width="137" alt="" border="0" align="center"></a>
</td>
<td>
<a href="http://localhost/sneezeguard/portable.php" onmouseover="document.images['s5a'].src='images/navbar/portabledown.jpg';" onmouseout="document.images['s5a'].src='images/navbar/portableup.jpg';"><img src="images/navbar/portableup.jpg" name="s5a" height="40" width="140" alt="" border="0" align="center"></a>
</td>
<td>
<a href="http://localhost/sneezeguard/common.php" onmouseover="document.images['s6a'].src='images/navbar/orbitdown.jpg';" onmouseout="document.images['s6a'].src='images/navbar/orbitup.jpg';"><img src="images/navbar/orbitup.jpg" name="s6a" height="40" width="147" alt="" border="0" align="center"></a>
</td>
<td>
<a href="http://localhost/sneezeguard/addons.php" onmouseover="document.images['s7a'].src='images/navbar/shelvingdown.jpg';" onmouseout="document.images['s7a'].src='images/navbar/shelvingup.jpg';"><img src="images/navbar/shelvingup.jpg" name="s7a" height="40" width="153" alt="" border="0" align="center"></a>
</td>
</tr>
<tr><td  background="background.jpg" align="center"colspan=10><a href="http://localhost/sneezeguard/index.php"><img src="images/b01.gif" width=157 height=43 border=0 title="Sneeze Guard" alt="Sneeze Guard"><a href="http://localhost/sneezeguard/products_new.php"><img src="images/b02.gif" width=184 height=43 border=0 title="Sneeze Guard" alt="Sneeze Guard"></a><a href="http://localhost/sneezeguard/account.php"><img src="images/b03.gif" width=169 height=43 border=0 title="Sneeze Guard" alt="Sneeze Guard" /></a><a href="http://localhost/sneezeguard/shopping_cart.php"><img src=images/b04.gif width=184 height=43 border=0 title="Sneeze Guard" alt="Sneeze Guard"></a><a href="http://localhost/sneezeguard/checkout_shipping.php"><img src=images/b05.gif width=155 height=43 border=0 title="Sneeze Guard" alt="Sneeze Guard"></a></td>
     </tr>

</table>




<table border="0" width="856" align=center BACKGROUND="images/middleback.jpg" class="mainTable2">



	
<!--
<tr>
    <td colspan="2" >
        <!--div id="direction_show1">
            <div id="image1"></div>
            <div id="image2"></div>
            <div id="image3"></div>
        </div-->
        <!--div style="position: relative;"><div id="horizontal_line" style="width: 4px; height:387px;display:none;left: 236px;top: 54px;z-index: 102; background:#ff0000;position: absolute;"></div></div-->
        
                <!--div class="message_p" style="z-index: 102;"><div class="message_w" id="message_w"></div><div id="message_line" style="background:#ff0000;display:none;bottom: -442px;height: 4px;left: 237px;position: absolute;width: 174px;z-index: 102;"></div-->
        <div class="message_pw"><div id="message_wp"></div></div>
           <!-- </td>
</tr>-->
 <td id="ex1" align=center width="190" valign="top">




 
    <script type="text/javascript">
        var formAction = 'http://localhost/sneezeguard/wishlist.php?language=english&action=add_wishlist_to_cart';
        $(document).ready(function(){
            $(".add-model").click(function(){
                var newForm = $('<form>', {
                    'action': formAction,
                    'method': 'post'                   
                });
                var inputclass = $(this).attr('data-name');
                var product_id = $("."+inputclass);
                $.each(product_id, function(key, product){                    
                    var qty = parseInt($(product).attr("data-qty"));
                    var datacustom = $(product).attr("data-custom");
                    var model_name = $(product).attr("data-model-name");
                    var model_type = $(product).attr("data-model-type");
                    while(qty > 0) {
                        $(newForm).append($('<input>', {
                            'name': 'products_id[]',
                            'value': product.value,
                            'type': 'hidden'
                        }));
                        $(newForm).append($('<input>', {
                            'name'  : 'data_custom[]',
                            'value' : JSON.stringify({custom : datacustom,  id : product.value, model_name : model_name, model_type : model_type}),
                            'type'  : 'hidden'
                        }));
                        qty--;
                    }
                });
                $("form[name='custom-form']").html($(newForm).html());
                $("form[name='custom-form']").removeAttr("onsubmit");
                $("form[name='custom-form']").submit();                
            });
        })
    </script>
	
		<style>
	h2 {
    font-size: 19px;
    color: #AA471C;
    font-weight: bold;
}

	</style>
		
    <table border="0" align="center" width="850" class="mainTable2" style="background-color: white;">
        <tbody>
            <tr>
                <td colspan="2">      
                    <div class="message_pw"><div id="message_wp"></div></div>
                </td>
            </tr>
            <tr>
                <td align="center" width="190" valign="top" id="ex1">
                    <div style="height:auto !important;   margin-bottom:40px; padding-bottom:70px;" class="">
                        <div id="custom-form">
                            <form name="custom-form"  id=""  action="http://localhost/sneezeguard/wishlist.php?language=english&action=add_wishlist_to_cart" method="post" onsubmit="return validateForm();">                            </form>
                        </div>
                        <div class="contentContainer">
                                                            <h2 align="left" >
                                    <font color="black">
                                        &nbsp;&nbsp; My Wishlist 
                                    </font>
                                </h2>
                                  <div class="contentText">
								  
								  
    <div class="price_table">
    <table border="0" width="98%" cellspacing="0" cellpadding="8"  >
    <style>
        .add-model {
            float:right;
            margin-right:10px;
        }
		
		
		td {
    color: #333131;
    font-family: tahoma,verdana,arial;
}
A:link {
    text-decoration: none;
    color: #3d3c3c;
}
    </style>

<tr><td><table border='0' width='100%' cellspacing='0' cellpadding='0' style='border:1px dotted #666;padding:0;'><tr class='headtop'><td style='background-color:#dbdbdb'><h1 class='tree-view' style='color:black;'>EP15-2BAY<input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button class="add-model" data-name="EP15-2BAY"/><a href="http://localhost/sneezeguard/wishlist.php?model_name=EP15&model_type=2BAY&action=remove_model_wishlist" style="float:right;margin-right:27px;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width:60px;" alt="Remove" title=" Remove "></a></h1></td></tr><b style="color:red;"><pre>Array
(
    [0] => Array
        (
            [id] => 1451
            [val] => EP15-22\"
            [qty] => 5
            [custom] => Yes
            [model_name] => EP15
            [model_type] => 2BAY
        )

    [1] => Array
        (
            [id] => 4665
            [val] => EP15-22\"
            [qty] => 1
            [custom] => Yes
            [model_name] => EP15
            [model_type] => 2BAY
        )

    [2] => Array
        (
            [id] => 4371
            [val] => EP15-22\"
            [qty] => 2
            [custom] => Yes
            [model_name] => EP15
            [model_type] => 2BAY
        )

    [3] => Array
        (
            [id] => 4673
            [val] => EP15-22\"
            [qty] => 2
            [custom] => Yes
            [model_name] => EP15
            [model_type] => 2BAY
        )

    [4] => Array
        (
            [id] => 624
            [val] => EP15-22\"
            [qty] => 1
            [custom] => Yes
            [model_name] => EP15
            [model_type] => 2BAY
        )

    [5] => Array
        (
            [id] => 609
            [val] => EP15-22\"
            [qty] => 1
            [custom] => Yes
            [model_name] => EP15
            [model_type] => 2BAY
        )

    [6] => Array
        (
            [id] => 606
            [val] => EP15-22\"
            [qty] => 1
            [custom] => Yes
            [model_name] => EP15
            [model_type] => 2BAY
        )

    [7] => Array
        (
            [id] => 4152
            [val] => EP15-24\"LYT
            [qty] => 1
            [custom] => Yes
            [model_name] => EP15
            [model_type] => 2BAY
        )

    [8] => Array
        (
            [id] => 4150
            [val] => EP15-12\"LYT
            [qty] => 1
            [custom] => Yes
            [model_name] => EP15
            [model_type] => 2BAY
        )

    [9] => Array
        (
            [id] => 614
            [val] => EP15-22\" 24\"
            [qty] => 1
            [custom] => Yes
            [wt] => 1
            [model_name] => EP15
            [model_type] => 2BAY
        )

    [10] => Array
        (
            [id] => 612
            [val] => EP15-22\" 12\"
            [qty] => 1
            [custom] => Yes
            [wt] => 1
            [model_name] => EP15
            [model_type] => 2BAY
        )

    [11] => Array
        (
            [id] => 617
            [val] => EP15-22\" 42\"
            [qty] => 1
            [custom] => Yes
            [wt] => 1
            [model_name] => EP15
            [model_type] => 2BAY
        )

    [12] => Array
        (
            [id] => 615
            [val] => EP15-22\" 30\"
            [qty] => 1
            [custom] => Yes
            [wt] => 1
            [model_name] => EP15
            [model_type] => 2BAY
        )

)
</b>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/Image.0042.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-FLANGE COVER 1 PIECE</strong></a><br>1'' Flange cover quantity 1 piece ( finish to match post )<form name="cart_quantityEP15-2BAY14510"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=5" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="1451"/><input type="hidden" name="products_qty" value="5"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-22\\\&quot;&quot;,&quot;id&quot;:&quot;1451&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">5<input type="hidden" name="products_id[]" value="1451" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-22\&quot;" data-qty="5" /><input type="hidden" name="custom_val[]" value="EP15-22\&quot;" /><input type="hidden" name="custom_type[]" value="Yes" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY14510'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=1451&qty=5&val=EP15-22\&quot;&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-22\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$55.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/RADIUS1.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-Frosted Glass</strong></a><br>EP5-Frosted Glass<form name="cart_quantityEP15-2BAY46651"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="4665"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-22\\\&quot;&quot;,&quot;id&quot;:&quot;4665&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="4665" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-22\&quot;" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP15-22\&quot;" /><input type="hidden" name="custom_type[]" value="Yes" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY46651'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=4665&qty=1&val=EP15-22\&quot;&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-22\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$187.50</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/shoping_cart.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-22"  Adjustable Brackets (Pairs)</strong></a><br>EP15-22"  Adjustable Brackets (Pairs)<form name="cart_quantityEP15-2BAY43712"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=2" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="4371"/><input type="hidden" name="products_qty" value="2"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-22\\\&quot;&quot;,&quot;id&quot;:&quot;4371&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">2<input type="hidden" name="products_id[]" value="4371" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-22\&quot;" data-qty="2" /><input type="hidden" name="custom_val[]" value="EP15-22\&quot;" /><input type="hidden" name="custom_type[]" value="Yes" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY43712'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=4371&qty=2&val=EP15-22\&quot;&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-22\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$364.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/ep15_90degree_ss.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-22"  90 Degree Post Brushed Stainless Steel</strong></a><br>EP15-22"  90 Degree Post Brushed Stainless Steel<p>Includes: Post, Flange, and Bracketry<form name="cart_quantityEP15-2BAY46733"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=2" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="4673"/><input type="hidden" name="products_qty" value="2"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-22\\\&quot;&quot;,&quot;id&quot;:&quot;4673&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">2<input type="hidden" name="products_id[]" value="4673" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-22\&quot;" data-qty="2" /><input type="hidden" name="custom_val[]" value="EP15-22\&quot;" /><input type="hidden" name="custom_type[]" value="Yes" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY46733'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=4673&qty=2&val=EP15-22\&quot;&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-22\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$384.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/EP15CENTSS.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-22"  Center Post Brushed Stainless Steel</strong></a><br>EP15-22"  Center Post Brushed Stainless Steel<p>Includes: Post, Flange, and Bracketry<form name="cart_quantityEP15-2BAY6244"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="624"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-22\\\&quot;&quot;,&quot;id&quot;:&quot;624&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="624" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-22\&quot;" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP15-22\&quot;" /><input type="hidden" name="custom_type[]" value="Yes" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY6244'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=624&qty=1&val=EP15-22\&quot;&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-22\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$96.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/ep15.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-22"  Right Post Brushed Stainless Steel</strong></a><br>EP15-22"  Right Post Brushed Stainless Steel<p>Includes: Post, Flange, Bracketry and<b> 2 clip</b><form name="cart_quantityEP15-2BAY6095"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="609"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-22\\\&quot;&quot;,&quot;id&quot;:&quot;609&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="609" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-22\&quot;" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP15-22\&quot;" /><input type="hidden" name="custom_type[]" value="Yes" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY6095'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=609&qty=1&val=EP15-22\&quot;&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-22\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$74.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/EP15SIDESS.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-22"  End Post Brushed Stainless Steel</strong></a><br>EP15-22"  End Post Brushed Stainless Steel <p>Includes: Post, Flange, Bracketry and<b> 2 clip</b><form name="cart_quantityEP15-2BAY6066"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="606"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-22\\\&quot;&quot;,&quot;id&quot;:&quot;606&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="606" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-22\&quot;" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP15-22\&quot;" /><input type="hidden" name="custom_type[]" value="Yes" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY6066'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=606&qty=1&val=EP15-22\&quot;&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-22\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$74.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/lightbar_cart.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-24"LYT </strong></a><br>EP15-24"LYT  LIGHT BAR WITH BRACKETS, END CAPS AND MATCHING ACCENTS<form name="cart_quantityEP15-2BAY41527"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="4152"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-24\\\&quot;LYT&quot;,&quot;id&quot;:&quot;4152&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="4152" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-24\&quot;LYT" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP15-24\&quot;LYT" /><input type="hidden" name="custom_type[]" value="Yes" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY41527'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=4152&qty=1&val=EP15-24\&quot;LYT&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-24\&quot;LYT"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$146.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/lightbar_cart.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-12"LYT </strong></a><br>EP15-12"LYT  LIGHT BAR WITH BRACKETS, END CAPS AND MATCHING ACCENTS<form name="cart_quantityEP15-2BAY41508"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="4150"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-12\\\&quot;LYT&quot;,&quot;id&quot;:&quot;4150&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="4150" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-12\&quot;LYT" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP15-12\&quot;LYT" /><input type="hidden" name="custom_type[]" value="Yes" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY41508'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=4150&qty=1&val=EP15-12\&quot;LYT&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-12\&quot;LYT"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$73.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/EP15GLASS.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-22" 24"Glass (Radiused Corners)</strong></a><br><p>EP15-22" 24"Glass <b>(Radiused Corners)</b></p><p>    Glass Kit Contains:</p><p>Qty 1- 20 1/2 x 22 1/8</p><p></p><form name="cart_quantityEP15-2BAY6149"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="614"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-22\\\&quot; 24\\\&quot;&quot;,&quot;id&quot;:&quot;614&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="614" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-22\&quot; 24\&quot;" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP15-22\&quot; 24\&quot;" /><input type="hidden" name="custom_type[]" value="Yes" /><input type="hidden" name="wt[]" value="1" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY6149'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=614&qty=1&val=EP15-22\&quot; 24\&quot;&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-22\&quot; 24\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$150.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/EP15GLASS.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-22" 12"Glass (Radiused Corners)</strong></a><br><p>EP15-22" 12"Glass <b>(Radiused Corners)</b></p><p>    Glass Kit Contains:</p><p>Qty 1- 20 1/2 x 10 1/8</p><p></p><form name="cart_quantityEP15-2BAY61210"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="612"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-22\\\&quot; 12\\\&quot;&quot;,&quot;id&quot;:&quot;612&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="612" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-22\&quot; 12\&quot;" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP15-22\&quot; 12\&quot;" /><input type="hidden" name="custom_type[]" value="Yes" /><input type="hidden" name="wt[]" value="1" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY61210'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=612&qty=1&val=EP15-22\&quot; 12\&quot;&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-22\&quot; 12\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$104.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/EP15GLASS.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-22" 42"Glass (Radiused Corners)</strong></a><br><p>EP15-22" 42"Glass <b>(Radiused Corners)</b></p><p>    Glass Kit Contains:</p><p>Qty 1- 20 1/2 x 40 1/8</p><p></p><form name="cart_quantityEP15-2BAY61711"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="617"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-22\\\&quot; 42\\\&quot;&quot;,&quot;id&quot;:&quot;617&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="617" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-22\&quot; 42\&quot;" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP15-22\&quot; 42\&quot;" /><input type="hidden" name="custom_type[]" value="Yes" /><input type="hidden" name="wt[]" value="1" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY61711'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=617&qty=1&val=EP15-22\&quot; 42\&quot;&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-22\&quot; 42\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$222.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/EP15GLASS.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP15-22" 30"Glass (Radiused Corners)</strong></a><br><p>EP15-22" 30"Glass <b>(Radiused Corners)</b></p><p>    Glass Kit Contains:</p><p>Qty 1- 20 1/2 x 28 1/8</p><p></p><form name="cart_quantityEP15-2BAY61512"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="615"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP15-22\\\&quot; 30\\\&quot;&quot;,&quot;id&quot;:&quot;615&quot;,&quot;model_name&quot;:&quot;EP15&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="615" class="EP15-2BAY" data-model-name="EP15" data-model-type="2BAY" data-custom="EP15-22\&quot; 30\&quot;" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP15-22\&quot; 30\&quot;" /><input type="hidden" name="custom_type[]" value="Yes" /><input type="hidden" name="wt[]" value="1" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP15-2BAY61512'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=615&qty=1&val=EP15-22\&quot; 30\&quot;&custom=Yes&model_name=EP15&model_type=2BAY&action=remove_product_wishlist&attr=EP15-22\&quot; 30\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$174.00</strong></td>      </tr></table>   </td> 
                </tr></table></td></tr><tr><td><table border='0' width='100%' cellspacing='0' cellpadding='0' style='border:1px dotted #666;padding:0;'><tr class='headtop'><td style='background-color:#dbdbdb'><h1 class='tree-view' style='color:black;'>EP36-2BAY<input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button class="add-model" data-name="EP36-2BAY"/><a href="http://localhost/sneezeguard/wishlist.php?model_name=EP36&model_type=2BAY&action=remove_model_wishlist" style="float:right;margin-right:27px;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width:60px;" alt="Remove" title=" Remove "></a></h1></td></tr><b style="color:red;"><pre>Array
(
    [0] => Array
        (
            [id] => 1442
            [val] => EP36
            [qty] => 3
            [custom] => 
            [model_name] => EP36
            [model_type] => 2BAY
        )

    [1] => Array
        (
            [id] => 5187
            [val] => EP36
            [qty] => 1
            [custom] => 
            [model_name] => EP36
            [model_type] => 2BAY
        )

    [2] => Array
        (
            [id] => 362
            [val] => EP36
            [qty] => 1
            [custom] => 
            [model_name] => EP36
            [model_type] => 2BAY
        )

    [3] => Array
        (
            [id] => 343
            [val] => EP36
            [qty] => 1
            [custom] => 
            [model_name] => EP36
            [model_type] => 2BAY
        )

    [4] => Array
        (
            [id] => 340
            [val] => EP36
            [qty] => 1
            [custom] => 
            [model_name] => EP36
            [model_type] => 2BAY
        )

    [5] => Array
        (
            [id] => 4171
            [val] => EP36-30\"LYT
            [qty] => 1
            [custom] => 
            [model_name] => EP36
            [model_type] => 2BAY
        )

    [6] => Array
        (
            [id] => 4172
            [val] => EP36-36\"LYT
            [qty] => 1
            [custom] => 
            [model_name] => EP36
            [model_type] => 2BAY
        )

    [7] => Array
        (
            [id] => 353
            [val] => EP36-30\"
            [qty] => 1
            [custom] => 
            [wt] => 1
            [model_name] => EP36
            [model_type] => 2BAY
        )

    [8] => Array
        (
            [id] => 354
            [val] => EP36-36\"
            [qty] => 1
            [custom] => 
            [wt] => 1
            [model_name] => EP36
            [model_type] => 2BAY
        )

    [9] => Array
        (
            [id] => 348
            [val] => EP36-Select EP36 Left End
            [qty] => 1
            [custom] => 
            [wt] => 1
            [model_name] => EP36
            [model_type] => 2BAY
        )

    [10] => Array
        (
            [id] => 346
            [val] => EP36-Select EP36 Right End
            [qty] => 1
            [custom] => 
            [wt] => 1
            [model_name] => EP36
            [model_type] => 2BAY
        )

)
</b>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=1442"><img src="images/Image.0042.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP36-FLANGE COVER 1 PIECE</strong></a><br>1'' Flange cover quantity 1 piece ( finish to match post )<form name="cart_quantityEP36-2BAY14420"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=3" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="1442"/><input type="hidden" name="products_qty" value="3"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP36&quot;,&quot;id&quot;:&quot;1442&quot;,&quot;model_name&quot;:&quot;EP36&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">3<input type="hidden" name="products_id[]" value="1442" class="EP36-2BAY" data-model-name="EP36" data-model-type="2BAY" data-custom="EP36" data-qty="3" /><input type="hidden" name="custom_val[]" value="EP36" /><input type="hidden" name="custom_type[]" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP36-2BAY14420'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=1442&qty=3&val=EP36&custom=&model_name=EP36&model_type=2BAY&action=remove_product_wishlist&attr=EP36"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$33.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=5187"><img src="images/RADIUS1.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP36-Frosted Glass</strong></a><br>EP36-Frosted Glass<form name="cart_quantityEP36-2BAY51871"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="5187"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP36&quot;,&quot;id&quot;:&quot;5187&quot;,&quot;model_name&quot;:&quot;EP36&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="5187" class="EP36-2BAY" data-model-name="EP36" data-model-type="2BAY" data-custom="EP36" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP36" /><input type="hidden" name="custom_type[]" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP36-2BAY51871'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=5187&qty=1&val=EP36&custom=&model_name=EP36&model_type=2BAY&action=remove_product_wishlist&attr=EP36"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$283.50</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=362"><img src="images/EP36CENTSS.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP36  Center Post Brushed Stainless Steel</strong></a><br>EP36  Center Post Brushed Stainless Steel<p>Includes: Post, Flange, and Bracketry<form name="cart_quantityEP36-2BAY3622"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="362"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP36&quot;,&quot;id&quot;:&quot;362&quot;,&quot;model_name&quot;:&quot;EP36&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="362" class="EP36-2BAY" data-model-name="EP36" data-model-type="2BAY" data-custom="EP36" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP36" /><input type="hidden" name="custom_type[]" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP36-2BAY3622'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=362&qty=1&val=EP36&custom=&model_name=EP36&model_type=2BAY&action=remove_product_wishlist&attr=EP36"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$142.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=343"><img src="images/ep36.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP36  Right Post Brushed Stainless Steel</strong></a><br>EP36  Right Post Brushed Stainless Steel<p>Includes: Post, Flange, and Bracketry<form name="cart_quantityEP36-2BAY3433"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="343"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP36&quot;,&quot;id&quot;:&quot;343&quot;,&quot;model_name&quot;:&quot;EP36&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="343" class="EP36-2BAY" data-model-name="EP36" data-model-type="2BAY" data-custom="EP36" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP36" /><input type="hidden" name="custom_type[]" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP36-2BAY3433'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=343&qty=1&val=EP36&custom=&model_name=EP36&model_type=2BAY&action=remove_product_wishlist&attr=EP36"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$98.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=340"><img src="images/EP36SIDESS.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP36  Left Post Brushed Stainless Steel</strong></a><br>EP36  Left Post Brushed Stainless Steel<p>Includes: Post, Flange, and Bracketry<form name="cart_quantityEP36-2BAY3404"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="340"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP36&quot;,&quot;id&quot;:&quot;340&quot;,&quot;model_name&quot;:&quot;EP36&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="340" class="EP36-2BAY" data-model-name="EP36" data-model-type="2BAY" data-custom="EP36" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP36" /><input type="hidden" name="custom_type[]" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP36-2BAY3404'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=340&qty=1&val=EP36&custom=&model_name=EP36&model_type=2BAY&action=remove_product_wishlist&attr=EP36"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$98.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=4171"><img src="images/lightbar_cart.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP36-30"LYT </strong></a><br>EP36-30"LYT  LIGHT BAR WITH BRACKETS, END CAPS AND MATCHING ACCENTS<form name="cart_quantityEP36-2BAY41715"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="4171"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP36-30\\\&quot;LYT&quot;,&quot;id&quot;:&quot;4171&quot;,&quot;model_name&quot;:&quot;EP36&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="4171" class="EP36-2BAY" data-model-name="EP36" data-model-type="2BAY" data-custom="EP36-30\&quot;LYT" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP36-30\&quot;LYT" /><input type="hidden" name="custom_type[]" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP36-2BAY41715'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=4171&qty=1&val=EP36-30\&quot;LYT&custom=&model_name=EP36&model_type=2BAY&action=remove_product_wishlist&attr=EP36-30\&quot;LYT"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$182.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=4172"><img src="images/lightbar_cart.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP36-36"LYT </strong></a><br>EP36-36"LYT  LIGHT BAR WITH BRACKETS, END CAPS AND MATCHING ACCENTS<form name="cart_quantityEP36-2BAY41726"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="4172"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP36-36\\\&quot;LYT&quot;,&quot;id&quot;:&quot;4172&quot;,&quot;model_name&quot;:&quot;EP36&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="4172" class="EP36-2BAY" data-model-name="EP36" data-model-type="2BAY" data-custom="EP36-36\&quot;LYT" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP36-36\&quot;LYT" /><input type="hidden" name="custom_type[]" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP36-2BAY41726'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=4172&qty=1&val=EP36-36\&quot;LYT&custom=&model_name=EP36&model_type=2BAY&action=remove_product_wishlist&attr=EP36-36\&quot;LYT"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$218.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=353"><img src="images/2115GLASS.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP36-30"Glass (Radiused Corners)</strong></a><br><p>EP36-30"Glass <b>(Radiused Corners)</b></p><p>    Glass Kit Contains:</p><p>Qty 2- 11 1/2" x 28 1/8"</p><p></p><form name="cart_quantityEP36-2BAY3537"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="353"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP36-30\\\&quot;&quot;,&quot;id&quot;:&quot;353&quot;,&quot;model_name&quot;:&quot;EP36&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="353" class="EP36-2BAY" data-model-name="EP36" data-model-type="2BAY" data-custom="EP36-30\&quot;" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP36-30\&quot;" /><input type="hidden" name="custom_type[]" /><input type="hidden" name="wt[]" value="1" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP36-2BAY3537'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=353&qty=1&val=EP36-30\&quot;&custom=&model_name=EP36&model_type=2BAY&action=remove_product_wishlist&attr=EP36-30\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$247.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=354"><img src="images/2115GLASS.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP36-36"Glass (Radiused Corners)</strong></a><br><p>EP36-36"Glass <b>(Radiused Corners)</b></p><p>    Glass Kit Contains:</p><p>Qty 2- 11 1/2" x 34 1/8"</p><p></p><form name="cart_quantityEP36-2BAY3548"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="354"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP36-36\\\&quot;&quot;,&quot;id&quot;:&quot;354&quot;,&quot;model_name&quot;:&quot;EP36&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="354" class="EP36-2BAY" data-model-name="EP36" data-model-type="2BAY" data-custom="EP36-36\&quot;" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP36-36\&quot;" /><input type="hidden" name="custom_type[]" /><input type="hidden" name="wt[]" value="1" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP36-2BAY3548'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=354&qty=1&val=EP36-36\&quot;&custom=&model_name=EP36&model_type=2BAY&action=remove_product_wishlist&attr=EP36-36\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$279.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=348"><img src="images/EP36ENDPAN.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP36 Left End </strong></a><br>EP36 Left End<p>Includes: 1/4" Clear Tempered Glass and Bracketry<form name="cart_quantityEP36-2BAY3489"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="348"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP36-Select EP36 Left End&quot;,&quot;id&quot;:&quot;348&quot;,&quot;model_name&quot;:&quot;EP36&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="348" class="EP36-2BAY" data-model-name="EP36" data-model-type="2BAY" data-custom="EP36-Select EP36 Left End" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP36-Select EP36 Left End" /><input type="hidden" name="custom_type[]" /><input type="hidden" name="wt[]" value="1" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP36-2BAY3489'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=348&qty=1&val=EP36-Select EP36 Left End&custom=&model_name=EP36&model_type=2BAY&action=remove_product_wishlist&attr=EP36-Select EP36 Left End"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$96.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=346"><img src="images/EP36ENDPAN.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP36 Right End</strong></a><br>EP36 Right End<p>Includes: 1/4" Clear Tempered Glass and Bracketry<form name="cart_quantityEP36-2BAY34610"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="346"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP36-Select EP36 Right End&quot;,&quot;id&quot;:&quot;346&quot;,&quot;model_name&quot;:&quot;EP36&quot;,&quot;model_type&quot;:&quot;2BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="346" class="EP36-2BAY" data-model-name="EP36" data-model-type="2BAY" data-custom="EP36-Select EP36 Right End" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP36-Select EP36 Right End" /><input type="hidden" name="custom_type[]" /><input type="hidden" name="wt[]" value="1" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP36-2BAY34610'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=346&qty=1&val=EP36-Select EP36 Right End&custom=&model_name=EP36&model_type=2BAY&action=remove_product_wishlist&attr=EP36-Select EP36 Right End"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$96.00</strong></td>      </tr></table>   </td> 
                </tr></table></td></tr><tr><td><table border='0' width='100%' cellspacing='0' cellpadding='0' style='border:1px dotted #666;padding:0;'><tr class='headtop'><td style='background-color:#dbdbdb'><h1 class='tree-view' style='color:black;'>EP5-1BAY<input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button class="add-model" data-name="EP5-1BAY"/><a href="http://localhost/sneezeguard/wishlist.php?model_name=EP5&model_type=1BAY&action=remove_model_wishlist" style="float:right;margin-right:27px;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width:60px;" alt="Remove" title=" Remove "></a></h1></td></tr><b style="color:red;"><pre>Array
(
    [0] => Array
        (
            [id] => 648
            [val] => EP5-22\"
            [qty] => 6
            [custom] => 
            [model_name] => EP5
            [model_type] => 1BAY
        )

    [1] => Array
        (
            [id] => 666
            [val] => EP5-22\" 30\"
            [qty] => 2
            [custom] => 
            [wt] => 1
            [model_name] => EP5
            [model_type] => 1BAY
        )

    [2] => Array
        (
            [id] => 663
            [val] => EP5-22\" 11-1/2\"
            [qty] => 1
            [custom] => Yes
            [wt] => 1
            [model_name] => EP5
            [model_type] => 1BAY
        )

)
</b>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=648"><img src="images/EP5SIDESS.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP5-22"  End Post Brushed Stainless Steel</strong></a><br>EP5-22"  End Post Brushed Stainless Steel<p>Includes: Post, Flange, and Bracketry<form name="cart_quantityEP5-1BAY6480"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=6" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="648"/><input type="hidden" name="products_qty" value="6"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP5-22\\\&quot;&quot;,&quot;id&quot;:&quot;648&quot;,&quot;model_name&quot;:&quot;EP5&quot;,&quot;model_type&quot;:&quot;1BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">6<input type="hidden" name="products_id[]" value="648" class="EP5-1BAY" data-model-name="EP5" data-model-type="1BAY" data-custom="EP5-22\&quot;" data-qty="6" /><input type="hidden" name="custom_val[]" value="EP5-22\&quot;" /><input type="hidden" name="custom_type[]" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP5-1BAY6480'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=648&qty=6&val=EP5-22\&quot;&custom=&model_name=EP5&model_type=1BAY&action=remove_product_wishlist&attr=EP5-22\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$384.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=666"><img src="images/EP5GLASSNORAD.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP5-22" 30"Glass (Squared Corners)</strong></a><br><p>EP5-22" 30"Glass (Squared Corners)</p><p>Glass Kit Contains:</p><p>Qty 1- 20 1/2 x 28 1/8</p><p></p><form name="cart_quantityEP5-1BAY6661"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=2" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="666"/><input type="hidden" name="products_qty" value="2"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP5-22\\\&quot; 30\\\&quot;&quot;,&quot;id&quot;:&quot;666&quot;,&quot;model_name&quot;:&quot;EP5&quot;,&quot;model_type&quot;:&quot;1BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">2<input type="hidden" name="products_id[]" value="666" class="EP5-1BAY" data-model-name="EP5" data-model-type="1BAY" data-custom="EP5-22\&quot; 30\&quot;" data-qty="2" /><input type="hidden" name="custom_val[]" value="EP5-22\&quot; 30\&quot;" /><input type="hidden" name="custom_type[]" /><input type="hidden" name="wt[]" value="1" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP5-1BAY6661'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=666&qty=2&val=EP5-22\&quot; 30\&quot;&custom=&model_name=EP5&model_type=1BAY&action=remove_product_wishlist&attr=EP5-22\&quot; 30\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$248.00</strong></td>      </tr></table>   </td> 
                </tr>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><img src="images/EP5GLASSNORAD.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></td>    <td ><a href="javascript:void(0)"><strong>EP5-22" 11-1/2"Glass (Squared Corners) Custom Products**</strong></a><br><p>EP5-22" 11-1/2"Glass (Squared Corners)</p><p>Glass Kit Contains:</p><p>Qty 1- 20 1/2 x 9 5/8</p><p></p><form name="cart_quantityEP5-1BAY6632"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="663"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;EP5-22\\\&quot; 11-1\/2\\\&quot;&quot;,&quot;id&quot;:&quot;663&quot;,&quot;model_name&quot;:&quot;EP5&quot;,&quot;model_type&quot;:&quot;1BAY&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="663" class="EP5-1BAY" data-model-name="EP5" data-model-type="1BAY" data-custom="EP5-22\&quot; 11-1/2\&quot;" data-qty="1" /><input type="hidden" name="custom_val[]" value="EP5-22\&quot; 11-1/2\&quot;" /><input type="hidden" name="custom_type[]" value="Yes" /><input type="hidden" name="wt[]" value="1" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityEP5-1BAY6632'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=663&qty=1&val=EP5-22\&quot; 11-1/2\&quot;&custom=Yes&model_name=EP5&model_type=1BAY&action=remove_product_wishlist&attr=EP5-22\&quot; 11-1/2\&quot;"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$61.00</strong></td>      </tr></table>   </td> 
                </tr></table></td></tr><tr><td><table border='0' width='100%' cellspacing='0' cellpadding='0' style='border:1px dotted #666;padding:0;'><tr class='headtop'><td style='background-color:#dbdbdb'><h1 class='tree-view' style='color:black;'>Parts-List</h1></td></tr><b style="color:red;"><pre>Array
(
    [0] => Array
        (
            [id] => 1392
            [val] => type
            [qty] => 1
            [custom] => beyond
            [model_name] => Parts
            [model_type] => List
        )

)
</b>      <tr class="tree-hide"><td valign="top"><table border="0" cellspacing="0" cellpadding="0" >  <tr ><table border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px dotted #444;" ><tr><td>    <td align="left"  width="120px" valign="top" ><a href="http://localhost/sneezeguard/product_info1.php?products_id=1392"><img src="images/flange.jpg" alt="Sneeze Guard" title=" Sneeze Guard " width="100" height="80" /></a></td>    <td ><a href="javascript:void(0)"><strong>EP5 SS Flange</strong></a><br><p>Stainless Steel Flange Round</p>
<p>For 1'' Tubing</p>
<p>Comes with hardware</p>
<p>Mounting screw not included</p><form name="cart_quantityParts-List13920"  id=""  action="http://localhost/sneezeguard/product_info1.php?action=add_wishlist_to_cart&products_qty=1" method="post" onsubmit="return validateForm();"><input type="hidden" name="products_id" value="1392"/><input type="hidden" name="products_qty" value="1"/><input type="hidden" name="data_custom" value="{&quot;custom&quot;:&quot;type&quot;,&quot;id&quot;:&quot;1392&quot;,&quot;model_name&quot;:&quot;Parts&quot;,&quot;model_type&quot;:&quot;List&quot;}"/></form>     </td><td valign="top" align="left" width="5%">1<input type="hidden" name="products_id[]" value="1392" class="Parts-List" data-model-name="Parts" data-model-type="List" data-custom="type" data-qty="1" /><input type="hidden" name="custom_val[]" value="type" /><input type="hidden" name="custom_type[]" value="beyond" /><br><br><table><tr><td><input type="image" style="width: 81px;" src="includes/languages/english/images/buttons/add_to_cartss.png" alt="Add to Cart" title=" Add to Cart " button onclick="javascript:document.forms['cart_quantityParts-List13920'].submit();"/></td><td><a href="http://localhost/sneezeguard/wishlist.php?products_id=1392&qty=1&val=type&custom=beyond&model_name=Parts&model_type=List&action=remove_product_wishlist&attr=type"><img src="includes/languages/english/images/buttons/remove_wishlist.png" style="width: 59px;margin-left: 17px;"></a></td></tr></table></td>        <td align="right" valign="top" width="5%"style="color:red ; font-size:12px; font-weight:bold;" ><strong style="color:black;font-size: 17px;">$8.00</strong></td>      </tr></table>   </td> 
                </tr></table></td></tr>	
    </table>
      



	  
    </form>
	                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>   


	

<!-- bodyContent //-->


</td>

<style>
	/*For Image*/
/*#PS_load{
    position: fixed;
    display: block;
    height: 534px;
    width: 630px;
    z-index: 103;
    top: 9%;
    left: 24%;
    background: white;
    overflow: hidden;
}.print_img{
    width: 985px;
    margin-bottom: 10px;
    clip: rect(0px,820px,452px,160px);
    margin-left: -177px;
}#PS_load p{
    margin-top: 0px;
    margin-bottom: 10px;
    margin-left: 40%;
    color: red;
  }.close{
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
    margin-top: 10px;
}*/
/*End for image*/
.PS_load{
    position: fixed;
    display: block;
    height: fit-content;
    width: 400px;
    z-index: 103;
    top: 44%;
    left: 35%;
    background: white;
    overflow: hidden;
}.print_img{
    width: 985px;
    margin-bottom: 10px;
    clip: rect(0px,820px,452px,160px);
    margin-left: -177px;
}.PS_load p{
    margin-bottom: 12px;
    color: red;
    text-align: center;
    font-weight: bold;
  }.close{
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
    margin-top: 10px;
}.h1{
	text-align: center;
    font-size: larger;
    margin-top: 10px;
    margin-bottom: 10px;
    color: red;
}.TB_overlay{
	background-color: #000;
	    opacity: 0.75;
}
</style>
<script type="text/javascript" src="./dist/html2canvas.js"></script>
<script type="text/javascript" src="./dist/canvas2image.js"></script>
  <!-- SOCIAL MEDIA SHARE LINKS START -->
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

      <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

  <!-- SOCIAL MEDIA SHARE LINKS END -->
<script>
// Start Print screen popup

document.addEventListener("keyup", function (e) {
    var keyCode = e.keyCode ? e.keyCode : e.which;
            if (keyCode == 44) {
                stopPrntScr();
            }
        });
function stopPrntScr() {
        html2canvas(document.body, { type: 'view' }).then(function(canvas) {
 var img = canvas.toDataURL("image/png");
 
 var customer_id='10581';
 
// Canvas2Image.saveAsJPEG(canvas); 
  $.ajax({
    type: 'POST',
    url: 'print_screen_images/print_scrn_img.php',
    data: {
      image: img,customer_id: customer_id
    },success:function(a){
      
    }
  });
    $("body").append("<div class='PS_load'><div class='close' onclick='PS_closeWindowButton()'><img src='img/close.png' title='Sneeze Guard' alt='Sneeze Guard'></div><p>IP-::1</p><h1 class='h1'>Thank You</h1></div>");//add loader to the page
    // <img class='print_img' src='"+img+"' '>
    $("body").append("<div id='TB_overlay' class='TB_overlay'></div>");
    // $('#TB_load').show();
    // $("#TB_overlay").addClass("TB_overlayBG");
});  
}
function PS_closeWindowButton(){
  $('.PS_load').hide();
  $('.TB_overlay').hide();
}


// End Print screen popup

</script>
<script>

//remove comment for restriction of back button
/* f
history.pushState(null, null, window.location);
window.addEventListener('popstate', function () {
    alert("Our website prevents the back button action from working to keep integrity of shopping carts, please use the tabs at the top of page to navigate.");
    history.pushState(null, null, window.location);
});
*/

</script> 


     </form>
	 
	 
	 
	 
	 
<script>
/*
function save_ip_reviat(){
	
		var ip=::1	var cate_id=	var bay=	
	
     $.ajax({
      type:'post',
      url:"includes/save_revit_ip.php",
      data:{ip:ip,cate_id:cate_id,bay:bay},
      success: function(data){
      window.location.reload(true);
	}
      });
  //alert(data);
}*/

$(document).ready(function() {
	
	var ip ='::1';
	 $.ajax({
      type:'post',
      url:"includes/check_ip_valdation_quote.php",
      data:{ip:ip},
      success: function(data){
     // window.location.reload(true);
	  //alert(data);
	  //$('#check_quote_ip').val(data);
	  $('#check_quote_ip').val('quote_ip_not_exist');
	  
	}
      });
	  
	   $.ajax({
      type:'post',
      url:"includes/check_ip_valdation_layout.php",
      data:{ip:ip},
      success: function(data){
     // window.location.reload(true);
	  //alert(data);
	  //$('#check_layout_ip').val(data);
	  $('#check_layout_ip').val('layout_ip_not_exist');
	  
	}
      });
	  
	
});


 function save_ip_revit(){
    	
	
	
	
	var ip='::1';
	var bay='1BAY';
	var cate_id='';
	var customer_id='10581';
	var customer_city='';
	var customer_state='';
	var customer_country='';
  var category_name = '';
  var bay = '1BAY';

var name_revit=$("#name_revit").val()
  var email_revit=$("#email_revit").val()

      var reggname = /^[a-zA-Z]+[a-zA-Z]$/;
      var regg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if(reggname.test(name_revit) == true)
  {

    if(regg.test(email_revit))
    {

       var email_revit_secure = email_revit;

  	//alert(customer_id);
       $.ajax({
        type:'post',
        url:"includes/save_revit_ip.php",
        data:{ip:ip,cate_id:cate_id,bay:bay,customer_id:customer_id,customer_city:customer_city,customer_state:customer_state,customer_country:customer_country,name_revit:name_revit,email_revit_secure:email_revit},
        success: function(data){
          window.location.href = "images/"+category_name+"/"+category_name+"_"+bay+"_revit.rvt";
          console.log(window.location.href = "images/"+category_name+"/"+category_name+"_"+bay+"_revit.rvt");
       // window.location.reload(true);
  	  //alert(data);
  	     }
      });

     }else{ 
      alert("You have entered an invalid email address!");
      // document.form1.text1.focus();
    }
}else{ 
      alert("Invalid name given.");
}
  
  }


</script>


<script>

 function save_ip_quote(){
    	
	
	
	
	var ip='::1';
	var bay='1BAY';
	var cate_id='';
	var customer_id='10581';
	var customer_city='';
	var customer_state='';
	var customer_country='';
	var name_quote=$("#name_quote").val()
	var email_quote=$("#email_quote").val()
      
      var reggname = /^[a-zA-Z]+[a-zA-Z]$/;
      var regg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if(reggname.test(name_quote) == true)
  {

    if(regg.test(email_quote))
    {

       var email_quote_secure = email_quote;
	     var total_quote_price=$("#total").text();
	     var totla_length=  $(".total").text();
		 //var bay='1BAY';
	/*if(cate_id=='79' || cate_id=='40')
	{
	totla_length=totla_length;	
	}
	else{
		totla_length=0;
	}*/

	//<input placeholder="Name" type="text" name="name_quote" id="name_quote" required />
    //<input placeholder="Email" type="email" name="email_quote" id="email_quote" required />
	
     $.ajax({
      type:'post',
      url:"includes/save_quote_ip.php",
      data:{ip:ip,cate_id:cate_id,bay:bay,customer_id:customer_id,customer_city:customer_city,customer_state:customer_state,customer_country:customer_country,name_quote:name_quote,email_quote_secure:email_quote,total_quote_price:total_quote_price,totla_length:totla_length},
      success: function(data){
 // abc(this.form); 
if(customer_id==0)
{
	
}
else{
customer_quote_data();	
}

     // window.location.reload(true);
	  //alert(data);
	}
      });

    }else{ 
      alert("You have entered an invalid email address!");
  }
}else{ 
      alert("Invalid name given.");
}


  
  }


</script>


<script>

 function save_ip_layout(form){

		    			
			
			
			
			var ip='::1';
			var bay='1BAY';
			var cate_id='';
			var customer_id='10581';
			var customer_city='0';
			var customer_state='0';
			var customer_country='0';
			
			var name_layout=$("#name_layout").val();
			var email_layout=$("#email_layout").val();

      var reggname = /^[a-zA-Z]+[a-zA-Z]$/;
      var regg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  // if(reggname.test(name_layout) == true)
  // {

    // if(regg.test(email_layout) == true)
    // {

        var email_layout_secure = email_layout;


			//alert(bay);
		     $.ajax({
		      type:'post',
		      url:"includes/save_layout_ip.php",
		      data:{ip:ip,cate_id:cate_id,bay:bay,customer_id:customer_id,customer_city:customer_city,customer_state:customer_state,customer_country:customer_country,name_layout:name_layout,email_layout_secure:email_layout},
		      success: function(data){
            		layout();
		      //window.location.reload(true);
			  //alert(data);
			   }
		      });

		// }else{ 
      // alert("You have entered an invalid email address!");

    // }
  // }

  // else{ 
      // alert("Invalid name given.");

    // }
}


</script>

<script>




function customer_quote_data(){



var  products_id = c_glass_post_val=c_glass_face=c_glass_face_val=c_glass_mult=c_glass_right=c_glass_right_val=c_glass_right_mult=c_glass_left=c_glass_left_val=c_glass_left_mult=rostedglass_id=rostedglass_val=c_glass_a_light=c_glass_a_val_light=c_glass_b_light=c_glass_b_val_light=c_glass_c_light=c_glass_c_val_light=c_glass_d_light=c_glass_d_val_light=c_glass_adjustable_a=c_glass_adjustable_b=c_glass_adjustable_c=c_glass_adjustable_d=c_glass_adjustable_a_val=c_glass_adjustable_b_val=c_glass_adjustable_c_val=c_glass_adjustable_d_val=c_glass_right_arc_val=c_glass_left_arc_val=c_glass_a_arc_val=c_glass_b_arc_val=c_glass_c_arc_val=c_glass_d_arc_val=c_glass_a=c_glass_a_val=c_glass_a_mult=c_glass_b=c_glass_b_val=c_glass_b_mult=c_glass_c=c_glass_c_val=c_glass_c_multc_glass_d=c_glass_d_val=c_glass_d_mult=is_custom=product_type=msg=ckall=shelves='';





	var inps = document.getElementsByName('products_id[]');
			
			
			var product_id=[];
          //  var inps = document.getElementsByName('pname[]');
				for (var i = 0; i <inps.length; i++) {
				var inp=inps[i];
					//alert("pname["+i+"].value="+inp.value);
					product_id.push(inp.value);
				}
console.log(inps);
	var c_glass_post_val = document.getElementById("c_glass_post_val").value;
	        
	var c_glass_face = document.getElementById("c_glass_face").value;
	var c_glass_face_val = document.getElementById("c_glass_face_val").value;
	var c_glass_mult = document.getElementById("c_glass_mult").value;

	       
	var c_glass_right = document.getElementById("c_glass_right").value;
	var c_glass_right_val = document.getElementById("c_glass_right_val").value;
	var c_glass_right_mult = document.getElementById("c_glass_right_mult").value;

	var c_glass_left = document.getElementById("c_glass_left").value;
	var c_glass_left_val = document.getElementById("c_glass_left_val").value;
	var c_glass_left_mult = document.getElementById("c_glass_left_mult").value;
			
	var rostedglass_id = document.getElementById("rostedglass_id").value;
	var rostedglass_val = document.getElementById("rostedglass_val").value;
			
			
	var c_glass_a_light = document.getElementById("c_glass_a_light").value;
	var c_glass_a_val_light = document.getElementById("c_glass_a_val_light").value;

	var c_glass_b_light = document.getElementById("c_glass_b_light").value;
	var c_glass_b_val_light = document.getElementById("c_glass_b_val_light").value;

	var c_glass_c_light = document.getElementById("c_glass_c_light").value;
	var c_glass_c_val_light = document.getElementById("c_glass_c_val_light").value;

	var c_glass_d_light = document.getElementById("c_glass_d_light").value;
	var c_glass_d_val_light = document.getElementById("c_glass_d_val_light").value;
			
	var c_glass_adjustable_a = document.getElementById("c_glass_adjustable_a").value;
	var c_glass_adjustable_b = document.getElementById("c_glass_adjustable_b").value;
	var c_glass_adjustable_c = document.getElementById("c_glass_adjustable_c").value;
	var c_glass_adjustable_d = document.getElementById("c_glass_adjustable_d").value;

	var c_glass_adjustable_a_val = document.getElementById("c_glass_adjustable_a_val").value;
	var c_glass_adjustable_b_val = document.getElementById("c_glass_adjustable_b_val").value;

	var c_glass_adjustable_c_val = document.getElementById("c_glass_adjustable_c_val").value;
	var c_glass_adjustable_d_val = document.getElementById("c_glass_adjustable_d_val").value;
			
			
	// var c_glass_right_arc_val = document.getElementById("c_glass_right_arc_val").value;
	// var c_glass_left_arc_val = document.getElementById("c_glass_left_arc_val").value;
			
	// var c_glass_a_arc_val = document.getElementById("c_glass_a_arc_val").value;
	// var c_glass_b_arc_val = document.getElementById("c_glass_b_arc_val").value;
	// var c_glass_c_arc_val = document.getElementById("c_glass_c_arc_val").value;
	// var c_glass_d_arc_val = document.getElementById("c_glass_d_arc_val").value;

			
	var c_glass_a = document.getElementById("c_glass_a").value;
	var c_glass_a_val = document.getElementById("c_glass_a_val").value;
	var c_glass_a_mult = document.getElementById("c_glass_a_mult").value;

	var c_glass_b = document.getElementById("c_glass_b").value;
	var c_glass_b_val = document.getElementById("c_glass_b_val").value;
	var c_glass_b_mult = document.getElementById("c_glass_b_mult").value;
			
	var c_glass_c = document.getElementById("c_glass_c").value;
	var c_glass_c_val = document.getElementById("c_glass_c_val").value;
	var c_glass_c_mult = document.getElementById("c_glass_c_mult").value;

	var c_glass_d = document.getElementById("c_glass_d").value;
	var c_glass_d_val = document.getElementById("c_glass_d_val").value;
	var c_glass_d_mult = document.getElementById("c_glass_d_mult").value;
	var is_custom = document.getElementById("is_custom").value;
			
	var product_type = document.getElementById("product_type").value;
	var msg = document.getElementById("msg").value;
	var ckall = document.getElementById("ckall").value;

	
				var customer_idd='10581';
		
		
			$.ajax({
				type:"POST",
				url:"includes/save_data_for_quote_oredr.php",
				data:{c_glass_post_val:c_glass_post_val,c_glass_face:c_glass_face,c_glass_face_val:c_glass_face_val,
c_glass_mult:c_glass_mult,c_glass_right:c_glass_right,c_glass_right_val:c_glass_right_val,c_glass_right_mult:c_glass_right_mult,c_glass_left:c_glass_left,c_glass_left_val:c_glass_left_val,c_glass_left_mult:c_glass_left_mult,rostedglass_id:rostedglass_id,rostedglass_val:rostedglass_val,c_glass_a_light:c_glass_a_light,c_glass_a_val_light:c_glass_a_val_light,c_glass_b_light:c_glass_b_light,
c_glass_b_val_light:c_glass_b_val_light,c_glass_c_light:c_glass_c_light,c_glass_c_val_light:c_glass_c_val_light,c_glass_d_light:c_glass_d_light,c_glass_d_val_light:c_glass_d_val_light,
c_glass_adjustable_a:c_glass_adjustable_a,c_glass_adjustable_b:c_glass_adjustable_b,c_glass_adjustable_c:c_glass_adjustable_c,c_glass_adjustable_d:c_glass_adjustable_d,c_glass_adjustable_a_val:c_glass_adjustable_a_val,c_glass_adjustable_b_val:c_glass_adjustable_b_val,c_glass_adjustable_c_val:c_glass_adjustable_c_val,c_glass_adjustable_d_val:c_glass_adjustable_d_val,c_glass_a:c_glass_a,
c_glass_a_val:c_glass_a_val,c_glass_a_mult:c_glass_a_mult,c_glass_b:c_glass_b,c_glass_b_val:c_glass_b_val,c_glass_b_mult:c_glass_b_mult,c_glass_c:c_glass_c,c_glass_c_val:c_glass_c_val,c_glass_c_mult:c_glass_c_mult,c_glass_d:c_glass_d,c_glass_d_val:c_glass_d_val,c_glass_d_mult:c_glass_d_mult,is_custom:is_custom,product_type:product_type,msg:msg,ckall:ckall,products_id:product_id,customer_id:customer_idd,shelves:shelves},
				success: function(){

				}
			});


}





  function abc(is){
  	if (!document.getElementById('name_quote').value) {
  		document.getElementById('input_reuired').style.display = 'block';
  		
  	}else if(!document.getElementById('email_quote').value){
  		
  		document.getElementById('input_reuired').style.display = 'block';
  	}else{
  		    if(myFunction(document.forms['cart_quantity'])){
        $("body").append("<div id='TB_load'><img src='"+tb_pathToImage+"'></div>");//add loader to the page
        $("body").append("<div id='TB_overlay'></div><div id='TB_window'></div>");
        $('#TB_load').show();
        $("#TB_overlay").addClass("TB_overlayBG");
		
        html2canvas(document.getElementsByClassName("mainTable2")).then(function(canvas) {
			
			
			 var var1=var2=var3=var4=var5=var6=var7=var8=var9=var10=var11=glass_corner=lightbar=postfinish=adjtb=frosted=texttt1=texttt2=var12=var13=var14=var15=var16=round_a=round_b=round_c=round_d=round_face_a=round_face_b=round_face_c=round_face_d=round_type=gotocornerpostss=endpaneltype=gotoroundglassss="";
			
			
			
			var inps = document.getElementsByName('products_id[]');
			
			
			var product_id=[];
          //  var inps = document.getElementsByName('pname[]');
				for (var i = 0; i <inps.length; i++) {
				var inp=inps[i];
					//alert("pname["+i+"].value="+inp.value);
					product_id.push(inp.value);
				}
			//.alert(product_id);
			
            var pic=canvas.toDataURL("image/jpeg");
            var id_for_logo = ;
            var bay_for_logo = '';
			
			
			
			
				var customer_idd='10581';
		
				//alert(customer_idd);
			//savaquoteee
			var total_quote_price=$("#total").text();
	     var totla_length=  $(".total").text();
		 //var bay='1BAY';
		 
		  end=$("input#glass-face").val();

			
            pic = pic.replace(/^data:image\/(png|jpg);base64,/, "");
            $.ajax({
                type: "POST",
                url: "includes/script.php",
                data: { 
                    img: pic, 
                    id: id_for_logo, 
                    bay: bay_for_logo,total_quote_price:total_quote_price,totla_length:totla_length,
					customer_idd:customer_idd
                },
                cache: false,
                contentType: "application/x-www-form-urlencoded",
                success: function(data, textStatus, request){
					
			
                modal.style.display = "none";   
                tb_show("","pop.php?KeepThis=true&TB_iframe=true&height=500&width=600","");
				
				
				
                //$("#scrn").href="img/screenshot/scrn.jpg";
                // var link = document.createElement('a');
                // link.className="thickbox";
                // //alert(link.className);
                // link.href = "img/screenshot/scrn.jpg";
                // document.body.appendChild(link);
                // link.click();
				//alert(data);
				$("#text_fields").html(data); 
				
				
            },
            error: function (request, textStatus, errorThrown) {
                alert("some error");
            }
        })
        });
    }
  	}
 }
  	
  function layout(){



//fix code for convert div into image START........
// html2canvas(document.querySelector("#additional_image")).then(canvas => {
//     // document.body.appendChild(canvas)
//     // download(canvas, 'myimage.png');
//     // var img    = canvas.toDataURL("image/png");
//     // document.write('<img src="'+img+'"/>');
//     var dataURL = canvas.toDataURL("image/png;base64");
// 	$.ajax({
// 	  type: 'POST',
// 	  url: 'includes/save_div_in_to_image.php',
// 	  data: { data: dataURL},
// 	  success: function (data) {
// 	    	console.log(data);
// 	    }
// 	});
// });
//fix code for convert div into image END........






  	//  	  	if (!document.getElementById('name_layout').value) {
  	// 	document.getElementById('input_reuired').style.display = 'block';
  	// }else if(!document.getElementById('email_layout').value){
  	// 	document.getElementById('input_reuired').style.display = 'block';
  	// }
  	// else{
    if(myFunction(document.forms['cart_quantity'])){
		$("body").append("<div id='TB_load'><img src='"+tb_pathToImage+"'></div>");//add loader to the page
        $("body").append("<div id='TB_overlay'></div><div id='TB_window'></div>");
        $('#TB_load').show();
        $("#TB_overlay").addClass("TB_overlayBG");
        var form=document.forms['cart_quantity'];
        var bay=form.type.value;
		//alert(category_name);
		//alert($(".glass_a").css("top"));
		//alert($(".glass_a").css("left"));
        var var1=var2=var3=var4=var5=var6=var7=var8=var9=var10=var11=glass_corner=lightbar=postfinish=adjtb=frosted=texttt1=texttt2=var12=var13=var14=var15=var16=round_a=round_b=round_c=round_d=round_face_a=round_face_b=round_face_c=round_face_d=round_type=gotocornerpostss=endpaneltype="";
		
		//alert(category_name);
		//var customer_idd='10581';
		
				var customer_idd='10581';
		
				//alert(customer_idd);
		if(category_name=='EP11' ||category_name=='EP12'){
		endpaneltype=form.end_options_type.options[form.end_options_type.selectedIndex].text;
		}
		
		if(category_name=='Ring-EP5' ||category_name=='EP5'){
		var check_arc_glass=$('#arc_glass_status :selected').val();
		//alert(check_arc_glass);
		if(check_arc_glass=='yes')
		{
		var13=check_arc_glass;
		var14=$('#arc_glass_value :selected').val();
		var15=$('#endpanel_arc_glass_value :selected').val();
		var16=$('#arc_glass_type_value :selected').val();
		}
		}
		
		var gotocornerpostss=$("input[name='gotocornerpostcheck']:checked").val();
		
		if(gotocornerpostss=="1")
		{
		var9=form.posttype.options[form.posttype.selectedIndex].text;
		var10=form.degree.options[form.degree.selectedIndex].text;
		var11=$("input[name='corner_post']:checked").val();
		if(category_name=='B950 SWIVEL' || category_name=='EP15')
		{

		}
		else{
			 if(bay=="2BAY"){
			 }
			 else{
			 	var12=form.noofcornerpost.options[form.noofcornerpost.selectedIndex].text;  
			 }
				
		}

		

		}
		//alert(var9);alert(var10);alert(var11);
		//alert(var12);
		var gotoroundglassss=$("input[name='gotoroundglasscheck']:checked").val();
		 if(gotoroundglassss=="1")
            {
            round_type=form.round_type.options[form.round_type.selectedIndex].value;	
			
			console.log(round_type);
            }
		
        if(bay=="1BAY"){
            if(form.face_length!==undefined){
                var1=form.face_length.options[form.face_length.selectedIndex].text;
            }else{
        
                var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
            }

            if(gotoroundglassss=="1")
            {
            	round_a=form.round_face_radius_a.options[form.round_face_radius_a.selectedIndex].text;

            	round_face_a=form.round_face_length_a.options[form.round_face_length_a.selectedIndex].value;

            }


      
        }else if(bay=="2BAY"||bay=="2BAYEXT"){
            var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
            var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
      	    if(gotoroundglassss=="1")
            {
	            round_a=form.round_face_radius_a.options[form.round_face_radius_a.selectedIndex].text;
	            round_b=form.round_face_radius_b.options[form.round_face_radius_b.selectedIndex].text;

            	round_face_a=form.round_face_length_a.options[form.round_face_length_a.selectedIndex].value;
            	round_face_b=form.round_face_length_b.options[form.round_face_length_b.selectedIndex].value;

            }
        }else if(bay=="3BAY"){
            var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
            var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
            var3=form.face_length_c.options[form.face_length_c.selectedIndex].text;
      	    if(gotoroundglassss=="1")
            {
	            round_a=form.round_face_radius_a.options[form.round_face_radius_a.selectedIndex].text;
	            round_b=form.round_face_radius_b.options[form.round_face_radius_b.selectedIndex].text;
	            round_c=form.round_face_radius_c.options[form.round_face_radius_c.selectedIndex].text;

            	round_face_a=form.round_face_length_a.options[form.round_face_length_a.selectedIndex].value;
            	round_face_b=form.round_face_length_b.options[form.round_face_length_b.selectedIndex].value;
            	round_face_c=form.round_face_length_c.options[form.round_face_length_c.selectedIndex].value;
            }      
        }else if(bay=="4BAY"){
            var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
            var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
            var3=form.face_length_c.options[form.face_length_c.selectedIndex].text;
            var4=form.face_length_d.options[form.face_length_d.selectedIndex].text;
      	    if(gotoroundglassss=="1")
            {
	            round_a=form.round_face_radius_a.options[form.round_face_radius_a.selectedIndex].text;
	            round_b=form.round_face_radius_b.options[form.round_face_radius_b.selectedIndex].text;
	            round_c=form.round_face_radius_c.options[form.round_face_radius_c.selectedIndex].text;
	            round_d=form.round_face_radius_d.options[form.round_face_radius_d.selectedIndex].text;

            	round_face_a=form.round_face_length_a.options[form.round_face_length_a.selectedIndex].value;
            	round_face_b=form.round_face_length_b.options[form.round_face_length_b.selectedIndex].value;
            	round_face_c=form.round_face_length_c.options[form.round_face_length_c.selectedIndex].value;	            
            	round_face_d=form.round_face_length_d.options[form.round_face_length_d.selectedIndex].value;	
           
            }  

        }
        if(form.post_height!== undefined){
            var5=form.post_height.options[form.post_height.selectedIndex].text;  
        }
        if(form.right_length!== undefined){
            var6=form.right_length.options[form.right_length.selectedIndex].text;
        }
        if(form.left_length!== undefined){
            var7=form.left_length.options[form.left_length.selectedIndex].text;
        }
		if(category_name=='Ring-EP5'){
		glass_corner=form.rounded_corners.options[form.rounded_corners.selectedIndex].text;
		lightbar=form.light_bar.options[form.light_bar.selectedIndex].text;
		postfinish=form.choose_finish.options[form.choose_finish.selectedIndex].text;
		adjtb=form.adjustable.options[form.adjustable.selectedIndex].text;
		frosted=form.roasted_glass.options[form.roasted_glass.selectedIndex].text;
		}
		if(category_name=="B950P"){
			var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
		}
		if(category_name=="ALLIN1"){
			var1=form.optn.options[form.optn.selectedIndex].text;
		}
		if(category_name=="EP950"){
			var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
		}
		
		if(category_name=="Light Bar"){
			texttt1 =document.querySelector('.glass_b').firstChild.data;
			texttt2 =document.querySelector('.glass_c').firstChild.data;
			
		}
		
		if(category_name=="Heat Lamp"){
			texttt1 =document.querySelector('.glass_b').firstChild.data;
			texttt2 =document.querySelector('.glass_c').firstChild.data;
		}
		//alert(texttt1);
		//alert(texttt2);
		//alert(category_name);
		//alert(var1);
	
		// var15=form.endpanel_arc_glass_value.value;
		// var16=form.arc_glass_type_value.value;
        end=$("input#glass-face").val();
        $.ajax({
            type: "POST",
            url: "includes/script1.php",
            data: { 
                mod:category_name, bay:bay, face1:var1, face2:var2,face3:var3,face4:var4,post:var5,left:var7,right:var6,end:end,tot:tot1,osc:"",im_id:"",sv:"",img:img_ajx, ptype:var9, pdegree:var10, pposi:var11, corny:gotocornerpostss, glass_corner:glass_corner, lightbar:lightbar, postfinish:postfinish, adjtb:adjtb, frosted:frosted, texttt1:texttt1, texttt2:texttt2, nocorpost:var12,arc_glass:var13,arc_glass_value:var14,endpanel_arc_glass_value:var15,arc_glass_type_value:var16,round_rad_a:'R-'+round_a,round_rad_b:'R-'+round_b,round_rad_c:'R-'+round_c,round_rad_d:'R-'+round_d,round_face_a:round_face_a,round_face_b:round_face_b,round_face_c:round_face_c,round_face_d:round_face_d,gotoroundglass:gotoroundglassss,round_type:round_type,endpaneltype:endpaneltype,customer_idd:customer_idd},
            cache: false,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, textStatus, request){
				 modal.style.display = "none";
                tb_show("","pop1.php?KeepThis=true&TB_iframe=true&height=500&width=600","");
				//alert(data);
            },
            error: function (request, textStatus, errorThrown) {
                alert("some error");
            }
			
        });
    }
}
  // }
  function layout1(){
    if(myFunction(document.forms['cart_quantity'])){
		$("body").append("<div id='TB_load'><img src='"+tb_pathToImage+"'></div>");//add loader to the page
        $("body").append("<div id='TB_overlay'></div><div id='TB_window'></div>");
        $('#TB_load').show();
        $("#TB_overlay").addClass("TB_overlayBG");
        var form=document.forms['cart_quantity'];
        if(category_name=="ALLIN1"){
            var1=form.optn.options[form.optn.selectedIndex].text;
            end="";
        }else{
            var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;;
            end=$("input#glass-face").val();
        }
    
        $.ajax({
            type: "POST",
            url: "includes/script1.php",
            data: { 
                mod:category_name, bay:"", face1:var1, face2:"",face3:"",face4:"",post:"",left:"",right:"",end:end,tot:tot1,osc:"",im_id:"",sv:"",img:img_ajx
            },
            cache: false,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, textStatus, request){
                tb_show("","pop1.php?KeepThis=true&TB_iframe=true&height=500&width=600","");
            },
            error: function (request, textStatus, errorThrown) {
                alert("some error");
            }
        });
    }
  }
  
  

</script>







<!-- Trigger/Open The Modal -->

<!-- The Modal 
<div id="myModal" class="modal" style="background-image: url('img/mail/backgroung_imgs.jpg');">
-->
<div id="myModalsss" class="modalss" >
<div id="contactFormsssss">
 <span class="closessss">&times;</span>
<p class="maintextsssss" >BEST PRICE GUARANTEE</p>
<hr />
<!--
<p class="othtext" ><img src="img/mail/mail_img.png" id="mail_img">Subscribe to our mailing list to get the update to your email inbox.....</p>
-->
<div style="width:100%;height: 30%;">

<div style="width:30%;float:left;height: 150px;"><img src="img/social/new_img_bestprice.jpg" id="mail_imgsssss" title="Sneeze Guard" alt="Sneeze Guard"></div>
<div style="width:100%"><p class="othtextsssss" >
<i style="color:black;">


<center>When ordering at ADM Sneezeguards, you will get the Best Price Guarantee. If you can find a better price, not only we will match it, but we will even give you an additional 5% discount off of the matched price !</center></i>


</p></div>
</div>
<div>


</div>
    
</div>

</div>



<script>
// Get the modal
var modalss = document.getElementById("myModalsss");
 
// Get the <span> element that closes the modal
var spansss = document.getElementsByClassName("closessss")[0];

// When the user clicks the button, open the modal 
function show_guarantee()
 {
  modalss.style.display = "block";
  
 
}



// When the user clicks on <span> (x), close the modal
spansss.onclick = function() {
  modalss.style.display = "none";
}




</script>



<style>

/* The Modal (background) */
.modalss {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 102; /* Sit on top */
 
}

/* Modal Content */
.modalss-content {

}

/* The Close Button */
.closessss {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.closessss:hover,
.closessss:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}


@import "https://fonts.googleapis.com/css?family=Raleway";
* { box-sizing: border-box; }
body { 
  margin: 0; 
  padding: 0; 
  background: #171717;
 
  
}
h1{ margin: 0; }

#contactFormsssss { 
  

  border: 10px solid #818181; 
  width: 500px;
  background: #fff;
  position: fixed;
  padding: 17px;
  font-family:Verdana, Geneva, sans-serif;
  top:50%;
  left:50%;
  transform: translate(-50%,-50%);
  -webkit-transform: translate(-50%,-50%)
      
  
}





.maintextsssss{
font-size:30px; color:#026773;		
color:#026773;	
font-family: fantasy;
padding-left: 13px;
}

.othtextsssss{
	color:black;
	font-size:13px;
	font-family: sans-serif;
}
.smalltextsssss{
	color:black;
	font-size:13px;
	margin: 14px;
	font-family: sans-serif;
}

#mail_imgsssss{width:90%;}
</style>














<style>

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 102; /* Sit on top */
 
}

/* Modal Content */
.modal-content {

}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}


/*@import "https://fonts.googleapis.com/css?family=Raleway";*/
* { box-sizing: border-box; }

#contact { 
  -webkit-user-select: none; /* Chrome/Safari */        
  -moz-user-select: none; /* Firefox */
  -ms-user-select: none; /* IE10+ */
  margin: 4em auto;
  width: 100px; 
  height: 30px; 
  line-height: 30px;
  background: teal;
  color: white;
  font-weight: 700;
  text-align: center;
  cursor: pointer;
  border: 1px solid white;
}

#contact:hover { background: #666; }
#contact:active { background: #444; }

#contactForm { 
  

  border: 10px solid #818181; 
  width: 500px;
  background: #fff;
  position: fixed;
  padding: 33px;
  font-family:Verdana, Geneva, sans-serif;
  top:50%;
  left:50%;
  transform: translate(-50%,-50%);
  -webkit-transform: translate(-50%,-50%)
      
  
}



.formBtn { 
  width: 140px;
  display: inline-block;
  
  background: teal;
  color: #2f9db3;
  font-weight: 100;
  font-size: 1.2em;
  border: none;
  border-radius: 12px;
  color:white;
}

#revit { 
  width: 140px;
  display: inline-block;
  
  background: teal;
  color: #2f9db3;
  font-weight: 100;
  font-size: 1.2em;
  border: none;
  border-radius: 12px;
  color:white;
}

.formBtn22 { 
  width: 140px;
  display: inline-block;
  
  background: teal;
  color: #2f9db3;
  font-weight: 100;
  font-size: 1.2em;
  border: none;
  border-radius: 12px;
  color:white;
}


#name_quote {
	width:90%;
	border:2px solid #ccb793;
	border-radius: 3px;
}

#email_quote {
	width:90%;
	border:2px solid #ccb793;
	border-radius: 3px;
}

#name_layout {
	width:90%;
	border:2px solid #ccb793;
	border-radius: 3px;
}

#email_layout {
	width:90%;
	border:2px solid #ccb793;
	border-radius: 3px;
}

#name_revit {
  width:90%;
  border:2px solid #ccb793;
  border-radius: 3px;
}

#email_revit {
  width:90%;
  border:2px solid #ccb793;
  border-radius: 3px;
}



.maintext{
font-size:30px; color:#026773;		
color:#026773;	
font-family: fantasy;
padding-left: 13px;
}

.othtext{
	color:black;
	font-size:15px;
	font-family: sans-serif;
}
.smalltext{
	color:black;
	font-size:13px;
	margin: 14px;
	font-family: sans-serif;
}

#mail_img{width:80%;}
</style>







<!-- Trigger/Open The Modal -->

<!-- The Modal 
<div id="myModal" class="modal" style="background-image: url('img/mail/backgroung_imgs.jpg');">
-->
<div id="myModal" class="modal" >
<div id="contactForm">
 <span class="close">&times;</span>
<p class="maintext" >YOUR EMAIL IS NEEDED</p>
<hr />
<!--
<p class="othtext" ><img src="img/mail/mail_img.png" id="mail_img">Subscribe to our mailing list to get the update to your email inbox.....</p>
-->
<div style="width:100%;height: 69px;">
<div style="width:20%;float:left;"><img src="img/mail/mail_img.png" id="mail_img" title="Sneeze Guard" alt="Sneeze Guard"></div>
<div style="width:90%"><p class="othtext" ><i>To save or print we need you email address and you will also be added to our mailing list for future sales, discounts and new products.....</i></p>
</div>
</div>
<div id="input_reuired" style="display: none;">
	<p style="text-align: center;color: red;">*Please enter name and email</p>
</div>



  <div> 
<input placeholder="Name" type="text" name="name_quote" id="name_quote" style="display:none;margin: .8em auto;display: block;padding: .4em;"  />
    <input placeholder="Email" type="email" name="email_quote" id="email_quote" style="display:none;margin: .8em auto;display: block;padding: .4em;"  />
   
    <input type="submit" value="Subscribe" class="formBtn" onclick="save_ip_quote();" style="display:none;margin: .8em auto;display: block;padding: .4em;" />

<!-- Save Revit Start -->
  
<input placeholder="Name" type="text" name="name_revit" id="name_revit" style="display:none;margin: .8em auto;padding: .4em;"  />

    <input placeholder="Email" type="email" name="email_revit" id="email_revit" style="display:none;margin: .8em auto;padding: .4em;"  />

     <input type="submit" value="Subscribe" class="" id="revit" onclick="save_ip_revit();" style="display:none;margin: .8em auto;padding: .4em;" />
	
<!-- Save Revit End -->
	
	<input placeholder="Name" type="text" name="name_layout" id="name_layout" style="display:none;margin: .8em auto;display: block;padding: .4em;"  />
    <input placeholder="Email" type="email" name="email_layout" id="email_layout" style="display:none;margin: .8em auto;display: block;padding: .4em;"  />
   
    <input type="submit" value="Subscribe" class="formBtn22" onclick="save_ip_layout(this.form);" style="display:none;margin: .8em auto;display: block;padding: .4em;" />
	
	


	
   <center class="smalltext">*By completing this form you are signing up to receive our emails and can unsubscribe at anytime.....</center>
</div>
    
</div>

</div>





<script>
// Get the modal
var modal = document.getElementById("myModal");

var name_quote = document.getElementById("name_quote");
var email_quote = document.getElementById("email_quote");
var formBtn = document.getElementsByClassName("formBtn");


var name_layout = document.getElementById("name_layout");
var email_layout = document.getElementById("email_layout");
var formBtn22 = document.getElementsByClassName("formBtn22");


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
function open_signup_form_quote()
 {
	 
	var check_quote_ip=$("#check_quote_ip").val(); 
	 
	if(check_quote_ip=='quote_ip_exist')
	{
	save_ip_quote();
	// abc(this.form);	
	}	
	else{
	modal.style.display = "block";
  
  $('#name_revit').css("display","none");
  $('#email_revit').css("display","none");
  $('#revit').css("display","none");

  $('#name_quote').css("display","block");
  $('#email_quote').css("display","block");
  $('.formBtn').css("display","block");
  
   $('#name_layout').css("display","none");
  $('#email_layout').css("display","none");
  $('.formBtn22').css("display","none");
  	
	}	
  
  
}

// Luv Start
function open_signup_form_rivet()
 {
  var check_quote_ip=$("#check_quote_ip").val(); 
   
  if(check_quote_ip=='quote_ip_exist')
  {
  save_ip_quote();
  // abc(this.form); 
  } 
  else{
  modal.style.display = "block";
  
    $('#name_quote').css("display","none");
  $('#email_quote').css("display","none");
  $('.formBtn').css("display","none");

  $('#name_revit').css("display","block");
  $('#email_revit').css("display","block");
  $('#revit').css("display","block");
  
   $('#name_layout').css("display","none");
  $('#email_layout').css("display","none");
  $('.formBtn22').css("display","none");
    
  // save_ip_revit();
  } 
  
 
}
// Luv End

function open_signup_form_layout()
 {
	 
//for offline
	  layout();


// remove comment when make it online
	 /*
	var check_layout_ip=$("#check_layout_ip").val(); 
	 
	if(check_layout_ip=='layout_ip_exist')
	{
	// layout();
	save_ip_layout(this.form);	
	}	
	else{
		modal.style.display = "block";

  $('#name_revit').css("display","none");
  $('#email_revit').css("display","none");
  $('#revit').css("display","none");
  
	  $('#name_quote').css("display","none");
	  $('#email_quote').css("display","none");
	  $('.formBtn').css("display","none");
	  
	   $('#name_layout').css("display","block");
	  $('#email_layout').css("display","block");
	  $('.formBtn22').css("display","block");
	} 
  
  */
  
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
  document.getElementById('input_reuired').style.display = "none"
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
   // modal.style.display = "none";
  }
}






</script>


	 
	 
	 
	 
<input type="hidden" id="check_quote_ip" name="check_quote_ip" value="">
<input type="hidden" id="check_layout_ip" name="check_layout_ip" value="">


</tr>



</table>
<table border="0" width="856" height="10" align="center" background="images/backBottom.jpg">
    <tr>
        <td align=center></td>
    </tr>
</table>
<table border="0" width="856" height="30" align="center" style="background:#272727;">
    <tr>
    <td style="padding-left: 15px; text-align: center;">

      <a href="https://www.sneezeguard.com/" class="fb-share-button" data-layout="button" style="top: -3px;">Share</a>

     <a href="https://twitter.com/share?ref_src=" class="twitter-share-button" data-show-count="false">Tweet</a>

     <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://www.sneezeguard.com/index.php?cPath=86" onclick="window.open(this.href, 'mywin',
'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" ><img src="https://chillyfacts.com/wp-content/uploads/2017/06/LinkedIN.gif" alt="" width="54" height="20" title="Sneeze Guard" alt="Sneeze Guard"/></a>

  <a href="http://feeds.feedburner.com/AdmSneezeguards" target="_blank" title="Catalog Feed" onclick="rss_feed();">
        <img src="social_icons/rss-icon.png" alt="Sneeze Guard" title=" Sneeze Guard " style="width: 20px;">
  </a>

</td>
</tr>
</table>

<table border="0" width="856" height="30" align="center" style="background:#6e6e64;">
    <tr>
         <td width="185" height="55" style="padding-left: 15px"><img src="images/m24.gif" align="center" vspace="12" title="Sneeze Guard" alt="Sneeze Guard"></td>
		 <td width="475" style="padding-left: 15px"><div align="center"><a href="http://feeds.feedburner.com/AdmSneezeguardsNews"><img src="images/comstr.jpg" title="Sneeze Guard" alt="Sneeze Guard"></a></div></td>
		 <!--<td width="73" style="padding-left: 15px"><a href="https://www.facebook.com/admsneezeguards/" target="_blank"><img src="img/social/facebookimage.png" width="49" height="49" align="right" title="Sneeze Guard" alt="Sneeze Guard"></a></td>-->
		 <td width="73" style="padding-left: 15px"><a href="http://info.nsf.org/Certified/Food/Listings.asp?Company=0U560&Standard=002" target="_blank"><img src="images/nsflogo.jpg" width="49" height="49" align="right" title="Sneeze Guard" alt="Sneeze Guard"></a></td>
		 <td width="115" style="padding-left: 15px"><img src="images/SSL.gif" width="89" height="49" align="left" title="Sneeze Guard" alt="Sneeze Guard"></td>
		 </tr>
</table>
<span style="color:red;" id="text_fields"></span>
<table border="0" width="850" align="center">
<tr>
    <td width="100%" height="45" align="center" valign="middle">
    <font size="2" face=Tahoma>
    <font size='2' color=white>
  
  
  
    <B>Copyright&copy;2020  ADM Sneezeguards A division of Advanced Design Manufacturing L.L.C.</B> | <A HREF='http://localhost/sneezeguard/shipping.php'>Shipping and Return</A> | <A HREF='http://localhost/sneezeguard/privacy.php'>Privacy Notice</A> | <A HREF='http://localhost/sneezeguard/custom_library.php'><b>Custom Library</b></A> |<A HREF='http://localhost/sneezeguard/conditions.php'><b>Terms of Use</b></A> | <A HREF='http://localhost/sneezeguard/projects.php'><B>Projects</B></A>  | <A HREF='http://localhost/sneezeguard/archive.php'><B>Archive</B></A> |<A HREF='http://localhost/sneezeguard/contact_us_dealer.php'><B>Dealer Inquiries</B></A> |<A HREF='http://localhost/sneezeguard/trade_show_program.php'><B>Trade Show Display Program</B></A><A HREF="http://localhost/sneezeguard/sneezeguard-layout-design.php"><B> | Sneeze guard Layout Design</B></A>

    <font size='2' color=white>
    <br>
	
    <center><!--<b style="font-size:15px;">Follow us on-</b>--> 
	
	<a href="https://www.facebook.com/admsneezeguards" target="_blank"><img src="img/social/facebook.png" style="width:50px;" alt="Follow us on Facebook" title="Follow us on Facebook"></a>  
	<a href="https://twitter.com/ASneezeguards" target="_blank"><img src="img/social/twittee.png" style="width:50px;" alt="Follow us on Twitter" title="Follow us on Twitter"></a>  
	<a href="https://www.linkedin.com/company/adm-sneezeguards" target="_blank"><img src="img/social/linklind.png" style="width:50px;" alt="Follow us on LinkedIn" title="Follow us on LinkedIn"> </a>  
	<a href="https://www.pinterest.com/admsneezeguards1" target="_blank"> <img src="img/social/pintrest.png" style="width:50px;" alt="Follow us on Pinterest" title="Follow us on Pinterest"> </a>  
	<a href="https://www.instagram.com/nickpadm" target="_blank"> <img src="img/social/instagram.png" style="width:50px;" alt="Follow us on Instagram" title="Follow us on Instagram"> </a>  
	<a href="https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg" target="_blank"> <img src="img/social/youtube.png" style="width:50px;" alt="Follow us on YouTube" title="Follow us on YouTube"> </a> 
	</center>
    <C><font size='1' color=#ffffff>Advanced Design Mfg. L.L.C. d.b.a. ADM Sneezeguards is listed and certified as a custom sneezeguard manufacturer by N.S.F., all style numbers and names of units shown on this website and subsequent literature are strictly for reference purposes only.</C>
    </font></font>
    </td>
</tr>
</table>
<script src="jquery.confirm/jquery.confirm.js"></script>
<script type="text/javascript">



function rss_feed(){
  var test_rss_variable = 'hiii';
             $.ajax({
                type: "POST",
                url: "./rss.php",
                data: { 
                    test_rss:test_rss_variable
                },
                success: function(){
                      console.log("Success rss_feed xml file created.");            
                }
            });
}





    function wishlist()
    {    
        var form = document.forms['cart_quantity'];
        if(myFunction(document.forms['cart_quantity'])){
            var bay=form.type.value;
            var var1=var2=var3=var4=var5=var6=var7=var8="";
            if(bay=="1BAY"){
                if(form.face_length!==undefined){
                    var1=form.face_length.options[form.face_length.selectedIndex].text;
                }else{
                    var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
                }
            }else if(bay=="2BAY"){
                var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
                var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
            }else if(bay=="3BAY"){
                var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
                var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
                var3=form.face_length_c.options[form.face_length_c.selectedIndex].text;
            }else if(bay=="4BAY"){
                var1=form.face_length_a.options[form.face_length_a.selectedIndex].text;
                var2=form.face_length_b.options[form.face_length_b.selectedIndex].text;
                var3=form.face_length_c.options[form.face_length_c.selectedIndex].text;
                var4=form.face_length_d.options[form.face_length_d.selectedIndex].text;
            }
            if(form.post_height!== undefined){
                var5=form.post_height.options[form.post_height.selectedIndex].text;  
            }
            if(form.right_length!== undefined){
                var6=form.right_length.options[form.right_length.selectedIndex].text;
            }
            if(form.left_length!== undefined){
                var7=form.left_length.options[form.left_length.selectedIndex].text;
            }
            end=$("input#glass-face").val();
            $.ajax({
                type: "POST",
                url: "includes/script1.php",
                data: { 
                    mod:category_name, bay:bay, face1:var1, face2:var2,face3:var3,face4:var4,post:var5,left:var7,right:var6,end:end,tot:tot1,osc:osc,im_id:im_id,sv:"save",img:img_ajx
                },
                cache: false,
                contentType: "application/x-www-form-urlencoded",
                success: function(data, textStatus, request){
                    //tb_show("","pop1.php?KeepThis=true&TB_iframe=true&height=500&width=600","");
                    var action = $("form[name='cart_quantity']").attr("action");
                    action = action.replace("add_product", "add_wishlist");
                    $("form[name='cart_quantity']").attr("action", action);  
                    $("form[name='cart_quantity']").removeAttr("onsubmit");
                    $("form[name='cart_quantity']").submit();                    
                },
                error: function (request, textStatus, errorThrown) {
                    alert("some error");
                }
            });
            return false;
        }else{
            return false;
        }
    }
    </script>

</body>
</html>
