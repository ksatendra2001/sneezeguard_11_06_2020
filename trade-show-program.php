<?php
/*
  $Id: contact_us.php,v 1.42 2003/06/12 12:17:07 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CONTACT_US);
       $tradeData=mysql_query("SELECT * FROM  trade_show WHERE id='2'") or die(mysql_error());
	   $tradeDataArray=mysql_fetch_array($tradeData);
	  
  $error = false;
  if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'send')) {
    $name = tep_db_prepare_input($HTTP_POST_VARS['name']);
    $email_address = tep_db_prepare_input($HTTP_POST_VARS['email']);
    $enquiry = tep_db_prepare_input($HTTP_POST_VARS['enquiry']);

    if (tep_validate_email($email_address)) {
      tep_mail(STORE_OWNER, $tradeDataArray['email1'], EMAIL_SUBJECT, $enquiry, $name, $email_address);

      tep_redirect(tep_href_link(FILENAME_CONTACT_US, 'action=success'));
    } else {
      $error = true;

      $messageStack->add('contact', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
    }
  }

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_CONTACT_US));
  
  
  
    require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-1072651700');
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>
<title>Sneeze Guard For Bank | Trade Show Program | ADM Sneezeguards</title>
<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<meta name="description" content="Buy online products related to sneeze guard portable for your office, Bank, Hospital, Bank, Grocery During Coronavirus with latest innovative designs.">
<meta name="keywords" content="Glass Barrier, Glass Barrier for Office, Glass Barrier for Hospital, Glass Barrier for Bank, Glass Barrier for Grocery">

<link rel="icon" href="images/favicon.ico" type="img/ioc">
<link rel="canonical" href="https://www.sneezeguard.com/<?php echo $_SERVER['REQUEST_URI']; ?>">
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>



<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />





<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<!--<title><?php echo TITLE; ?></title>-->

<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>">
<link rel="stylesheet" type="text/css" href="stylesheet.css">

<style type="text/css">
<!--
.style2 {font-family: Tahoma; font-size: 12; line-height: 13px}
.style3 {font-family: Tahoma; font-size: 12; line-height: 17px; font-weight: bold}
.style6 {font-weight: bold; font-size: 12}
.TopLargeText {font-family: Tahoma; font-weight: bold; color: #C7F917; font-size: 20}
-->
</style>


<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>

</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->



  
  <?php
	if (!$detect->isMobile())
	{
	?>	
	<style>
	input[type=text] {
		width: 313px;
    height: 35px;
    font-size: 14px;
    border: 2px solid #d1cbcb;
    border-radius: 8px;
    padding: 5px;
	}
	
	textarea {
		width: 500px;
    height: 250px;
    font-size: 14px;
    border: 2px solid #d1cbcb;
    border-radius: 8px;
    padding: 5px;
	}
	</style>

<?
}

else{
?>

<style>
	input[type=text] {
    width: 407px;
    height: 50px;
    font-size: 22px;
    border: 2px solid #d1cbcb;
    border-radius: 8px;
    padding: 5px;
}
.form_white h2 {
    color: black;
    font-size: 31px;
}
p {
    font-family: "Times New Roman", Times, serif;
    color: ffffff;
    font-size: 25px;
    /* padding-left: 35px; */
}
	
	textarea {
    width: 609px;
    height: 250px;
    font-size: 25px;
    border: 2px solid #d1cbcb;
    border-radius: 8px;
    padding: 5px;
}
input[type=image]{width:40%}
.teesh p strong span{font-size:28px !important;}
.teesh1 p span{font-size:28px !important;}
	</style>

<td id="ex1" align=center width="190" valign="top">

	
	
<?
}
?>

<!-- body //-->
<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;">
<!-- 
<tr>
<td colspan="3" style="background:url(img/TopBak.jpg) !important ; height:26px; border:1px solid #ccc;">&nbsp;</td>
</tr>-->


<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td width="35%"  style="border-right:1px solid #ccc;" valign="top" >
<table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
<!-- body_text //-->
    <td width="100%" valign="top"><?php echo tep_draw_form('trade_show_program', tep_href_link('trade_show_program.php', 'action=send')); ?><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading" style="color:black;"><h2><?php echo 'Dealer Information'; ?></h2></td>
          </tr>
        </table></td>
      </tr>
      
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
        
      </tr>
<?php
  if ($messageStack->size('contact') > 0) {
?>
      <tr>
        <td><?php echo $messageStack->output('contact'); ?></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
<?php
  }

  if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'success')) {
?>
      <tr>
        <td class="main" align="center"><?php echo tep_image(DIR_WS_IMAGES . 'table_background_man_on_board.gif', HEADING_TITLE, '0', '0', 'align="left"') . TEXT_SUCCESS; ?></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="1" cellpadding="2" class="continue">
          <tr class="continue">
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td width="10"><?php echo tep_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
                <td align="right"><?php echo '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . tep_image_button('button_continue.gif', IMAGE_BUTTON_CONTINUE) . '</a>'; ?></td>
                <td width="10"><?php echo tep_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
<?php
  } else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="1" cellpadding="2" class="continue">
          <tr class="continue">
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <?php echo '<div align="center" class="teesh">'.$tradeDataArray['text1'].'</div>';
               ?>
			     <?php echo '<div align="center" class="teesh1">'.$tradeDataArray['text2'].'</div>';
               ?>
                <p align="center" style="color:black">Thank you for your interest.</p>
                <td class="main"  ><?php //echo 'Company Name:'; ?></td>
              </tr>
              <!--<tr>
                <td class="main" ><?php echo tep_draw_input_field('name',' ','style="border: 2px; border-style: solid; border-color: black;"'); ?></td>
              </tr>
              <tr>
                <td class="main"><?php //echo ENTRY_EMAIL; ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo tep_draw_input_field('email',' ','style="border: 2px; border-style: solid; border-color: black;"'); ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo 'Message:'; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_textarea_field('enquiry', 'soft', 100, 15); ?></td>
              </tr>
			  -->
			  <tr>
                <td class="main"><input type="text" name="name" placeholder="Company Name" style="border: 2px; border-style: solid; border-color: black;"></td>
              </tr>
              <tr>
                <td class="main"></td>
              </tr>
              <tr>
                <td class="main"><input type="text" name="email" placeholder="Company Email" style="border: 2px; border-style: solid; border-color: black;"></td>
              </tr>
              <tr>
                <td class="main"></td>
              </tr>
              <tr>
                <td><textarea name="enquiry" cols="100" rows="15" placeholder="Message"></textarea></td>
              </tr>
			  
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="1" cellpadding="2" class="continue">
          <tr class="continue">
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td width="10"><?php echo tep_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
                <td align="right"><?php echo tep_image_submit('button_continue.gif', IMAGE_BUTTON_CONTINUE); ?></td>
                <td width="10"><?php echo tep_draw_separator('pixel_trans.gif', '10', '1'); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
    </table></form></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
</td></tr></table></td></tr></table>
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
