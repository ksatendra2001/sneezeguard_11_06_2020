<?php
    ob_start();
    ini_set('max_upload_size','400M');
    $one_hide="visibility: hidden;display: none;";
    require('includes/application_top.php');
    require(DIR_WS_INCLUDES . 'template_top.php');
?>
<script type="text/javascript" src="../sneezegaurd/jscripts/tiny_mce/tiny_mce.js"></script><!-- Adding TinyMCE in form!! -->
<script type="text/javascript">
	tinyMCE.init({
          
        // add these two lines for absolute urls
        remove_script_host : false,
        convert_urls : false,

               // General options
         mode : "textareas",
        theme : "advanced",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "forecolor,backcolor,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		//theme_advanced_resizing : true,
		theme_advanced_resizing : false,
		theme_advanced_path : false,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->

<?php
  $res=tep_db_query("select * from homepage_content");//Fetching the popups from database!
    while($row=tep_db_fetch_array($res)){
        if($row['content_id']==1){
            $content_name1=$row["content_name"];//Saving that popups in variables!
            $description1=$row["description"];
            $production_time1=$row["production_time"];
            $total_model1=$row["total_model"];
            
        }else if($row['content_id']==2){
            $content_name2=$row["content_name"];//Saving that popups in variables!
            $description2=$row["description"];
            $production_time2=$row["production_time"];
            $total_model2=$row["total_model"];
        }else if($row['content_id']==3){
            $content_name3=$row["content_name"];//Saving that popups in variables!
            $description3=$row["description"];
            $production_time3=$row["production_time"];
            $total_model3=$row["total_model"];
        }else if($row['content_id']==4){
            $content_name4=$row["content_name"];//Saving that popups in variables!
            $description4=$row["description"];
            $production_time4=$row["production_time"];
            $total_model4=$row["total_model"];
        }else if($row['content_id']==5){
            $content_name5=$row["content_name"];//Saving that popups in variables!
            $description5=$row["description"];
            $production_time5=$row["production_time"];
            $total_model5=$row["total_model"];
        }
    }

//    echo $msg_one.$msg_two.$msg_three.$msg_four;
?>
<form name="upcust_popup" action="" method="post" enctype="multipart/form-data"><!-- Making table for showing the popup values fetched from the database!! -->
<table border="1" width="100%" cellspacing="8" cellpadding="6"  style="border-collapse:collapse; margin-top:30px;">
    <tr style="background:#706B5A;">
        <td style="width: 15%">
            <b>Catagory</b>
        </td>
        <td style="width: 45%">
            <b>Description</b>
        </td>
		
		<td style="width: 15%">
            <b>Production Time</b>
        </td>
		
		
		<td style="width: 15%">
            <b>Total Model</b>
        </td>
		
        <td style="width: 10%">
            <b>Action</b>
        </td>
    </tr>
	
	
	 <tr>
        <td style="width: 15%">
          <?php echo $content_name1; ?>
        </td>
        <td class="main" style="text-align: justify;width: 45%;">
            <?php echo $description1; ?>
        </td>
		<td class="main" style="text-align: justify;width: 15%;">
            <?php echo $production_time1; ?>
        </td>
		<td class="main" style="text-align: justify;width: 15%;">
            <?php echo $total_model1; ?>
        </td>
        <td style="width: 10%">
            <input type="submit" name="edit1" value="Edit">
        </td>
    </tr>



	 <tr>
        <td style="width: 15%">
          <?php echo $content_name2; ?>
        </td>
        <td class="main" style="text-align: justify;width: 45%;">
            <?php echo $description2; ?>
        </td>
		<td class="main" style="text-align: justify;width: 15%;">
            <?php echo $production_time2; ?>
        </td>
		<td class="main" style="text-align: justify;width: 15%;">
            <?php echo $total_model2; ?>
        </td>
        <td style="width: 10%">
            <input type="submit" name="edit2" value="Edit">
        </td>
    </tr>


	 <tr>
        <td style="width: 15%">
          <?php echo $content_name3; ?>
        </td>
        <td class="main" style="text-align: justify;width: 45%;">
            <?php echo $description3; ?>
        </td>
		<td class="main" style="text-align: justify;width: 15%;">
            <?php echo $production_time3; ?>
        </td>
		<td class="main" style="text-align: justify;width: 15%;">
            <?php echo $total_model3; ?>
        </td>
        <td style="width: 10%">
            <input type="submit" name="edit3" value="Edit">
        </td>
    </tr>

	 <tr>
        <td style="width: 15%">
          <?php echo $content_name4; ?>
        </td>
        <td class="main" style="text-align: justify;width: 45%;">
            <?php echo $description4; ?>
        </td>
		<td class="main" style="text-align: justify;width: 15%;">
            <?php echo $production_time4; ?>
        </td>
		<td class="main" style="text-align: justify;width: 15%;">
            <?php echo $total_model4; ?>
        </td>
        <td style="width: 10%">
            <input type="submit" name="edit4" value="Edit">
        </td>
    </tr>

  
	 <tr>
        <td style="width: 15%">
          <?php echo $content_name5; ?>
        </td>
        <td class="main" style="text-align: justify;width: 45%;">
            <?php echo $description5; ?>
        </td>
		<td class="main" style="text-align: justify;width: 15%;">
            <?php echo $production_time5; ?>
        </td>
		<td class="main" style="text-align: justify;width: 15%;">
            <?php echo $total_model5; ?>
        </td>
        <td style="width: 10%">
            <input type="submit" name="edit5" value="Edit">
        </td>
    </tr>
  
</table>

</form>    
<?php 
    $edit_text=$val="";
    if(isset($_POST["edit1"])){//if In-stock
		$content_name=$content_name1;
		$description=$description1;
		$production_time=$production_time1;
		$total_model=$total_model1;
		
        $val=1;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit2"])){//if Special Price Product
       $content_name=$content_name2;
		$description=$description2;
		$production_time=$production_time2;
		$total_model=$total_model2;
        $val=2;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit3"])){//if Custom
       $content_name=$content_name3;
		$description=$description3;
		$production_time=$production_time3;
		$total_model=$total_model3;
        $val=3;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit4"])){//If Adjsutable
        $content_name=$content_name4;
		$description=$description4;
		$production_time=$production_time4;
		$total_model=$total_model4;
        $val=4;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit5"])){//if Portable
        $content_name=$content_name5;
		$description=$description5;
		$production_time=$production_time5;
		$total_model=$total_model5;
        $val=5;
        $one_hide='display: block;visibility: visible;';
    }
//    echo $edit_text;
?>
<hr style="width: 60%;margin-left: 200px;margin-top: 20px; border-radius: 10px 0 0 0;"><!-- Horizontal rule for split the form in two parts!! -->
<form name="upcust_updt" action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="content_id" value="<?php echo $val;?>">
    <table style="<?php echo $one_hide;?>"><!-- A new form which is updating the Popups!! -->
	
    <tr>
        <td style="width: 300px">
            Content Head:
			
        </td>
        <td>
           <input type="text" name="content_name" value="<?php echo $content_name;?>">
        </td>
    </tr>
	
	
	<tr>
        <td style="width: 300px">
            Content Description:
			
        </td>
        <td>
            <textarea name="description"><?php echo $description;?></textarea>
        </td>
    </tr>
	
	 <tr>
        <td style="width: 300px">
            Production Time:
			
        </td>
        <td>
           <input type="text" name="production_time" value="<?php echo $production_time;?>">
        </td>
    </tr>
	
	 <tr>
        <td style="width: 300px">
            Total Model:
			
        </td>
        <td>
           <input type="text" name="total_model" value="<?php echo $total_model;?>">
        </td>
    </tr>
    <tr>
        <td colspan='2'>
            <center><input type="submit" name="update" value="Update Values" /></center>
        </td>
    </tr>
    
</table>
    <span style="color: #e5e5e5"></span>
</form>    
<?php
    if(isset($_POST['update'])){
		
        $descriptionss=$_POST['description'];
        $content_namess=$_POST['content_name'];
        $production_timess=$_POST['production_time'];
        $total_modelss=$_POST['total_model'];
		$content_id=$_POST['content_id'];
		
		
        //$temp1=str_replace("\n", "", $temp);//replacing all the new lines from the updates popup!!
       // $temp2= str_replace("\r", "", $temp1);//Replacing all the new carriage return
        //$descriptionss=str_replace("</p> <p>", "</p><p>", $temp2);//this line helps the text to remove the white spaces!!
//        $temp3=  str_replace("</p>", "<br/>", $temp1);
//        $update_val='<span>'.$temp3.'</span>';
//          str_replace("<p>", "", $temp1);
        echo $descriptionss;
        
            if(tep_db_query("UPDATE `homepage_content` SET `content_name`='".$content_namess."',`description`='".$descriptionss."',`production_time`='".$production_timess."',`total_model`='".$total_modelss."' WHERE `content_id`='".$content_id."'")){//update query for all popups!!
               // header('Location: ' .tep_href_link($page='homepage_content.php'));//refresh script!
            }
        
    }

?>    
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>