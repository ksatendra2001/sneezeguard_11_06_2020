<?php
ob_start();
ini_set('max_upload_size','400M');
$one_hide="visibility: hidden;display: none;";
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License

  Discount Code 2.9
  http://high-quality-php-coding.com/
*/
  require('includes/application_top.php');
  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<script type="text/javascript" src="../sneezegaurd/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
          
        // add these two lines for absolute urls
        remove_script_host : false,
        convert_urls : false,

               // General options
         mode : "textareas",
        theme : "advanced",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "forecolor,backcolor,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		//theme_advanced_resizing : true,
		theme_advanced_resizing : false,
		theme_advanced_path : false,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->

<?php
    $res=tep_db_query("select * from ".TABLE_CUSTOM_POPUPS);
    while($row=tep_db_fetch_array($res)){
        if($row['id']==72){
            $msg_one=$row["post_popup"];
            $msg_two=$row["face_popup"];
            $msg_five=$row["adjustable_popup"];
            $msg_six=$row["cart_popup"];
        }else if($row['id']==55){
            $msg_three=$row["post_popup"];
        }else if($row['id']==114){
            $msg_four=$row["option_popup"];
        }
    }
//    echo $msg_one.$msg_two.$msg_three.$msg_four;
?>
<form name="upcust_popup" action="" method="post" enctype="multipart/form-data">
<table border="1" width="100%" cellspacing="8" cellpadding="6"  style="border-collapse:collapse; margin-top:30px;">
    <tr style="background:#706B5A;">
        <td style="width: 380px">
            <b>Popup Catagory</b>
        </td>
        <td>
            <b>Popup Message</b>
        </td>
        <td style="width: 35px">
            <b>Action</b>
        </td>
    </tr>
    <tr>
        <td style="width: 360px">
            Custom Popups for all:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_two; ?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit1" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Post Message Popup for EP5 & EP15:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_one;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit2" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Post Message Popup for EP11 & EP12:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_three;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit3" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Popups for ED20,ES29 ES82,ES53:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_four;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit4" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Popups for Adjustable Glass:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_five;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit5" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Popups for Add to Cart & Quote Shipping Button:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_six;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit6" value="Edit">
        </td>
    </tr>
</table>

</form>    
<?php 
    $edit_text=$val="";
    if(isset($_POST["edit1"])){
        $edit_text=$msg_two;
        $val=0;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit2"])){
        $edit_text=$msg_one;
        $val=1;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit3"])){
        $edit_text=$msg_three;
        $val=2;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit4"])){
        $edit_text=$msg_four;
        $val=3;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit5"])){
        $edit_text=$msg_five;
        $val=4;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit6"])){
        $edit_text=$msg_six;
        $val=5;
        $one_hide='display: block;visibility: visible;';
    }
//    echo $edit_text;
?>
<hr style="width: 60%;margin-left: 200px;margin-top: 20px; border-radius: 10px 0 0 0;">
<form name="upcust_updt" action="" method="post" enctype="multipart/form-data">
    <table style="<?php echo $one_hide;?>">
    <tr>
        <td style="width: 300px">
            Change Custom Message:<input type="hidden" name="id" value="<?php echo $val;?>">
        </td>
        <td>
            <textarea name="update_val"><?php echo $edit_text;?></textarea>
        </td>
    </tr>
    <tr>
        <td colspan='2'>
            <center><input type="submit" name="update" value="Update Values" /></center>
        </td>
    </tr>
    
</table>
</form>    
<?php
    if(isset($_POST['update'])){
        $update_val=$_POST['update_val'];
        if($_POST['id']==0){
            if(tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET message= '".$update_val."', opiton1_popup= '".$update_val."', left_popup = '".$update_val."', right_popup = '".$update_val."', face_popup = '".$update_val."'")){
                header('Location: ' .tep_href_link($page='custom_popup.php'));
            }
        }else if($_POST['id']==1){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET post_popup= '".$update_val."' where id=71 or id=72");
            header('Location: ' .tep_href_link($page='custom_popup.php'));
        }else if($_POST['id']==2){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET post_popup= '".$update_val."' where id=55 or id=56");
            header('Location: ' .tep_href_link($page='custom_popup.php'));
        }else if($_POST['id']==3){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET option_popup= '".$update_val."' where id=110 or id=111 or id=113 or id=114");
            header('Location: ' .tep_href_link($page='custom_popup.php'));
        }else if($_POST['id']==4){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET adjustable_popup = '".$update_val."'");
            header('Location: ' .tep_href_link($page='custom_popup.php'));
        }else if($_POST['id']==5){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET cart_popup = '".$update_val."'");
            header('Location: ' .tep_href_link($page='custom_popup.php'));
        }
    }

?>    
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>