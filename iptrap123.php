<?php
// Error Redirects from many different states of Error, and handles them in one file.
// Records the output in the database for error trapping and also hacking attempts.
// You will recieve many 404 errors during a hacking attempt and i expect you would be very surprised if you knew how many attempts your site gets in a day!
// Also recorded Errors will show up any scripting errors, missing images, malformed code etc... giving you a much better web site
// Written by FImble for osCommerce
// osCommerce Forums URL -  http://forums.oscommerce.com/index.php?showuser=15542
// Add on URL http://addons.oscommerce.com/info/5914
// to hire me ... linuxux.group@gmail.com
// If you have concerns about security please get in touch with me for fast expert service,
// hacked? I am more than capable of clearing malicious code from your site, and securing it to prevent further attempts.

  require('includes/application_top.php');
  $Maintain=tep_db_query("DELETE FROM " . TABLE_IPTRAP . "  WHERE date < FROM_UNIXTIME(UNIX_TIMESTAMP()- ".LINUX_ERROR_DELETE_RECORD. "*24*60*60)");
        $ban_array = array(
                       array('id' => 'BLACK',
                              'text' => BLACK_LIST),
                        array('id' => 'WHITE',
                              'text' => WHITE_LIST));
  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  if (tep_not_null($action)) {
    switch ($action) {
      case 'insert':
        $IP_IP = tep_db_prepare_input(trim($_POST['IP_Number']));
        $BAN= tep_db_prepare_input('BLACK');
        tep_db_query("INSERT INTO " . TABLE_IPTRAP . " (IP_Number,BAN,date,BY_TRAP,BY_ADMIN,HostName,USerAgent) VALUES ('".$IP_IP."','".$BAN."',now(),'',1, 'Not Known, added manually by Admin', 'Not Known, added manually by Admin');");
        tep_redirect(tep_href_link(FILENAME_IP_TRAP));
        break;
      case 'save':
        $IPtrap_id = tep_db_prepare_input($_GET['tID']);
        $IP_IP = tep_db_prepare_input($_POST['IP_Number']);
        $BAN= tep_db_prepare_input($_POST['BAN']);
        tep_db_query("UPDATE " . TABLE_IPTRAP . " SET IP_Number = '".$IP_IP."', BAN = '".$BAN."'  WHERE IPid = '". $IPtrap_id."';");
        tep_redirect(tep_href_link(FILENAME_IP_TRAP));
        break;
      case 'deleteconfirm':
        $IP_IP = tep_db_prepare_input($_GET['tID']);
        tep_db_query("DELETE FROM " . TABLE_IPTRAP . " WHERE IPid = '".(int) $IP_IP."';");
        tep_redirect(tep_href_link(FILENAME_IP_TRAP));
        break;
    }
  }
    require(DIR_WS_INCLUDES . 'template_top.php');
?>
<table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo LINUXUK_IPTRAP_HEADING_TITLE . LINUXUK_HTTP_IPTRAP_VERSION; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>

        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo LINUXUK_IP_TRAP_IP_NUMBER; ?></td>
                <td class="dataTableHeadingContent"><?php echo LINUXUK_IP_TRAP_SET_ADMIN; ?></td>
                <td class="dataTableHeadingContent"><?php echo LINUXUK_IP_TRAP_SET_TRAP; ?></td>
                <td class="dataTableHeadingContent"><?php echo LINUXUK_IP_TRAP_USER_AGENT; ?></td>
                <td class="dataTableHeadingContent"><?php echo LINUXUK_IP_TRAP_HOST_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo LINUXUK_IP_TRAP_SET_DATE; ?></td>
                <td class="dataTableHeadingContent"><?php echo LINUXUK_IP_TRAP_LISTED; ?></td>
              </tr>
<?php
  $ip_trap_query_raw = "select IPid, date,IP_Number,UserAgent,HostName,BAN,BY_ADMIN,BY_TRAP from " . TABLE_IPTRAP . "" . " order by date DESC";
  $trap_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $ip_trap_query_raw, $trap_query_numrows);
  $trap_query = tep_db_query($ip_trap_query_raw);
  while ($trapped = tep_db_fetch_array($trap_query)) {
    if ((!isset($_GET['tID']) || (isset($_GET['tID']) && ($_GET['tID'] == $trapped['IPid']))) && !isset($trInfo) && (substr($action, 0, 3) != 'new')) {
      $trInfo = new objectInfo($trapped);
    }

    if (isset($trInfo) && is_object($trInfo) && ($trapped['IPid'] == $trInfo->IPid)) {
      echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_IP_TRAP, 'page=' . $_GET['page'] . '&tID=' . $trInfo->IPid . '&action=edit') . '\'">' . "\n";
    } else {
      echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_IP_TRAP, 'page=' . $_GET['page'] . '&tID=' . $trapped['IPid']) . '\'">' . "\n";
    }
?>
                <td class="dataTableContent"><?php echo '<a href="http://ip-lookup.net/index.php?ip=' . $trapped['IP_Number'] . '" target="_blank"class="Ban">' . $trapped['IP_Number'] . '<a/>'; ?></td>
                <td class="dataTableContent"><?php echo ($trapped['BY_ADMIN']==1? tep_image(DIR_WS_IMAGES . 'icon_green.gif'): tep_image(DIR_WS_IMAGES . 'icon_red.gif'));?></td>
                <td class="dataTableContent"><?php echo ($trapped['BY_TRAP']==1? tep_image(DIR_WS_IMAGES . 'icon_green.gif'): tep_image(DIR_WS_IMAGES . 'icon_red.gif'));?></td>
                <td class="dataTableContent"><?php echo $trapped['UserAgent']; ?></td>
                <td class="dataTableContent"><?php echo $trapped['HostName']; ?></td>
                <td class="dataTableContent"><?php echo $trapped['date']; ?></td>
                <td class="dataTableContent"><?php echo $trapped['BAN']; ?></td>
              </tr>
<?php
  }
?>
              <tr>
                <td colspan="7"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $trap_split->display_count($trap_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_BANS); ?></td>
                    <td class="smallText" align="right"><?php echo $trap_split->display_links($trap_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
<?php
  if (empty($action)) {
?>
                  <tr>
                    <td class="smallText" colspan="7" align="right"><?php echo tep_draw_button(LINUXUK_IP_TRAP_ADD_NEW, 'plus', tep_href_link(FILENAME_IP_TRAP, 'page=' . $_GET['page'] . '&action=new')); ?></td>
                  </tr>
<?php
  }
?>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<b>' .LINUXUK_IP_TRAP_NEW . '</b>');
      $contents = array('form' => tep_draw_form('IP_Number', FILENAME_IP_TRAP, 'page=' . $_GET['page'] . '&action=insert'));
      $contents[] = array('text' => LINUXUK_IP_TRAP_NEW);
      $contents[] = array('text' => '<br />'.LINUXUK_IP_TRAP_IP_NUMBER.':<br />' . tep_draw_input_field('IP_Number'));
      $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link('sitesafe_iptrap.php',  'page=' . $_GET['page']) ));
      break;
    case 'edit':
      $heading[] = array('text' => LINUXUK_IP_TRAP_EDIT);
      $contents = array('form' => tep_draw_form('IP_Number', FILENAME_IP_TRAP, 'page=' . $_GET['page'] . '&tID=' . $trInfo->IPid  . '&action=save'));
      $contents[] = array('text' => LINUXUK_IP_TRAP_CHANGE);
      $contents[] = array('text' => '<br />'.LINUXUK_IP_TRAP_IP_NUMBER.':<br />' . tep_draw_input_field('IP_Number', $trInfo->IP_Number));
      $contents[] = array('text' => '<br />'.LINUXUK_IP_TRAP_CHOOSE.':<br />' .tep_draw_pull_down_menu('BAN', $ban_array, $trInfo->BAN));
      $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_IP_TRAP, 'page=' . $_GET['page'] . '&tID=' . $trInfo->IPid)));
      break;
    case 'delete':
      $heading[] = array('text' => LINUXUK_IP_TRAP_DELETE);
      $contents = array('form' => tep_draw_form('IP_Number', FILENAME_IP_TRAP, 'page=' . $_GET['page'] . '&tID=' . $trInfo->IPid  . '&action=deleteconfirm'));
      $contents[] = array('text' => LINUXUK_IP_TRAP_DELETE_CONFIRM);
      $contents[] = array('text' => '<br /><b>' . $trInfo->IP_Number . '' . '</b>');
      $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_IP_TRAP, 'page=' . $_GET['page'] . '&tID=' . $trInfo->IPid)));
      break;
    default:
      if (is_object($trInfo)) {
        $heading[] = array('text' => '<b>' . $trInfo->IP_Number . '</b>');
        $contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_IP_TRAP, 'page=' . $_GET['page'] . '&tID=' . $trInfo->IPid . '&action=edit')) . tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(FILENAME_IP_TRAP, 'page=' . $_GET['page'] . '&tID=' . $trInfo->IPid . '&action=delete')));
        $contents[] = array('text' => LINUXUK_IP_TRAP_SELECT . $trInfo->IP_Number);
      }
      break;
  }
  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";
    $box = new box;
    echo $box->infoBox($heading, $contents);
    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>

<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>