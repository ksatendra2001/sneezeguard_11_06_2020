<?php
include 'header.php';
?>
<table border="0" width="856" height="40" valign=center align=center BACKGROUND="middleback.jpg">
<tr>
<td align=center>
<font color=white size=4>
<b>Terms of Use</b><br><br>
<table width=751 align=center background="squareTerms.jpg" height=99 cellpadding=15>
<tr>
<td>
<div class="modText">
<?php
echo"
The use of this website is subject to the following terms of use:<p>

    * The content of the pages of this website is for your general information and use only. It is subject to change without notice.<p>
    
    * Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.<p>
    
    * Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.<p>

    * This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.<p>
    
    * All trademarks reproduced in this website which are not the property of, or licensed to, the operator are acknowledged on the website.<p>
    
    * Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.<p>
    
    * From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).<p>
    
    * You may not create a link to this website from another website or document without Sneezeguard.coms prior written consent.
"
?>
</div>
</td>
</tr>
</table>
</font>
</td>
</tr>
</table>



<table border="0" width="856" height="30" align="center" BACKGROUND="backBottom.jpg">
<tr>
<td align=center>
</td>
</tr>

<?php
include 'footer.php';
?>
