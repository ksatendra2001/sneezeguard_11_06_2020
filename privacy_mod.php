<?php
require('includes/application_top.php');
  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<table border="0" height="40" valign=center align=center BACKGROUND="middleback.jpg">
<tr>
<td align=center>
<font color=white size=4>
<b>Privacy Notice</b><br><br>
<table align=center background="squarePriv.jpg" height=99 cellpadding=15>
<tr>
<td>
<div class="modText">
<?php
echo
"Information We Collect

The information we gather from customers helps us personalize and continually improve your shopping experience at sneezeguard.com. Here are the types of information we gather.

We receive and store any information you enter on our Web site or give us in any other way. You can choose not to provide certain information, but this can limit your access to many of our features. We use the information that you provide for such purposes as responding to your requests, customizing future shopping for you, improving our stores, and communicating with you.

By providing your email address you agree to receive marketing emails from sneezeguard.com and may unsubscribe at any time. We receive and store certain types of information whenever you interact with us. For example, like many Web sites, we use "cookies," and we obtain certain types of information when your Web browser accesses sneezeguard.com.

Cookies

Sneezeguard.com uses �cookies� to improve our customer�s shopping experience. The cookie itself does not contain personal information although it will enable us to relate your use of this site to information that you have specifically and knowingly provided.

Information Sharing

Information about our customers is an important part of our business, and we are NOT in the business of selling it to others.

We work in conjunction with other companies and individuals to perform functions on our behalf. Examples include fulfilling orders, delivering orders, and processing credit card payments. They have access to address information needed to perform their functions, but may not use it for other purposes.

We release account and other personal information when we believe release is appropriate to comply with the law; enforce or apply our Conditions of Use and other agreements; or protect the rights, property, or safety of sneezeguard.com, our users, or others. This includes exchanging information with other companies and organizations for fraud protection and credit risk reduction. Obviously, however, this does not include selling, renting, sharing, or otherwise disclosing personally identifiable information from customers for commercial purposes in violation of the commitments set forth in this Privacy Notice.
Information Security

We work to protect the security of your information during transmission by using Secure Sockets Layer (SSL) software, which encrypts information you input.

We reveal only the last four digits of your credit card numbers when confirming an order. Of course, we transmit the entire credit card number to the appropriate credit card company during order processing.

It is important for you to protect against unauthorized access to your password and to your computer. Be sure to sign off and close your browser window when finished using a shared computer."
?>
</div>
</font>
</td>
</tr>
</table>
</font>
</td>
</tr>
</table>

<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
