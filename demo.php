<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
<?php  
  
  $modelllname=$_GET['modelname'];
  if(strpos($modelllname,'EP-950') !== false || strpos($modelllname,'Ring-EP5') !== false || strpos($modelllname,'B-950P-GLASS') !== false || strpos($modelllname,'Ring-EP6') !== false || strpos($modelllname,'B-950-SWIVEL') !== false){
  
  ?>
  
   <title>Sneeze Guard Barriers Shields | <?php echo$modelllname; ?> | ADM Sneezeguards</title>
  
  
  <?php
  }
  
  

  else{
  ?>
  
  <title>Sneeze Guard Barriers Shields | <?php echo$modelllname; ?> Guards | ADM Sneezeguards</title>
  
  <?php
  }
  ?>
  
  

<meta name="description" content="Desktop protective sneeze guard, or Glass Barrier, are portable and protect against germs. Glass can be installed to protect customers and employees from virus.">
<meta name="keywords" content="Desktop protective sneeze guard, protect customers through Glass, Glass Barrier for Medical centers, Glass for Supermarket, Protect against from virus Sneeze guard">



<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">


<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />



  <!-- Include the VideoJS Library -->
  <script src="video.js" type="text/javascript" charset="utf-8"></script>

  <script type="text/javascript">
    // Must come after the video.js library

    // Add VideoJS to all video tags on the page when the DOM is ready
    VideoJS.setupAllWhenReady();

    /* ============= OR ============ */

    // Setup and store a reference to the player(s).
    // Must happen after the DOM is loaded
    // You can use any library's DOM Ready method instead of VideoJS.DOMReady

    /*
    VideoJS.DOMReady(function(){
      
      // Using the video's ID or element
      var myPlayer = VideoJS.setup("example_video_1");
      
      // OR using an array of video elements/IDs
      // Note: It returns an array of players
      var myManyPlayers = VideoJS.setup(["example_video_1", "example_video_2", video3Element]);

      // OR all videos on the page
      var myManyPlayers = VideoJS.setup("All");

      // After you have references to your players you can...(example)
      myPlayer.play(); // Starts playing the video for this player.
    });
    */

    /* ========= SETTING OPTIONS ========= */

    // Set options when setting up the videos. The defaults are shown here.

    /*
    VideoJS.setupAllWhenReady({
      controlsBelow: false, // Display control bar below video instead of in front of
      controlsHiding: true, // Hide controls when mouse is not over the video
      defaultVolume: 0.85, // Will be overridden by user's last volume if available
      flashVersion: 9, // Required flash version for fallback
      linksHiding: true // Hide download links when video is supported
    });
    */

    // Or as the second option of VideoJS.setup
    
    /*
    VideoJS.DOMReady(function(){
      var myPlayer = VideoJS.setup("example_video_1", {
        // Same options
      });
    });
    */

  </script>

  <!-- Include the VideoJS Stylesheet -->
  <link rel="stylesheet" href="video-js.css" type="text/css" media="screen" title="Video JS">
</head>
<body  bgcolor="#000000">

  


<?php
require_once("Mobile_Detect.php");
$detect = new Mobile_Detect();
if (!$detect->isMobile())
{
?>



 <!-- Begin VideoJS -->
  <div class="video-js-box">
    <!-- Using the Video for Everybody Embed Code http://camendesign.com/code/video_for_everybody -->
    <video id="example_video_1" class="video-js" width="640" height="480" controls="controls" preload="auto" poster="pic.jpg" autoplay="true" >
      <source src="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <source src="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.ogv" type='video/ogg; codecs="theora, vorbis"' />
    
<?php
/*


	<!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "sneezeguard/upload/videos/<?php echo $_GET["name"]; ?>.mp4","autoPlay":true,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="640" height="480" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
	  
	  */
	  
	?>  
	  
    </video>
    <!-- Download links provided for devices that can't play video in the browser. -->
    <p class="vjs-no-video"><strong>Download Video:</strong>
      <a href="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.mp4">MP4</a>,
      <a href="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.webm">WebM</a>,
      <a href="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.ogv">Ogg</a><br>
      <!-- Support VideoJS by keeping this link. -->
      </p>
  </div>
  <!-- End VideoJS -->

  
  
  
  


  <?php

}
else
{
?>


 <!-- Begin VideoJS -->
  <div class="video-js-box">
    <!-- Using the Video for Everybody Embed Code http://camendesign.com/code/video_for_everybody -->
    <video controls autoplay id="example_video_1" class="video-js" width="840" height="780" controls="controls" preload="auto" poster="pic.jpg" autoplay="true" >
      <source src="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' />
      <source src="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.webm" type='video/webm; codecs="vp8, vorbis"' />
      <source src="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.ogv" type='video/ogg; codecs="theora, vorbis"' />
      <!-- Flash Fallback. Use any flash video player here. Make sure to keep the vjs-flash-fallback class. -->
      <object id="flash_fallback_1" class="vjs-flash-fallback" width="640" height="264" type="application/x-shockwave-flash"
        data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf">
        <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
        <param name="allowfullscreen" value="true" />
        <param name="flashvars" value='config={"playlist":["pic.jpg", {"url": "sneezeguard/upload/videos/<?php echo $_GET["name"]; ?>.mp4","autoPlay":true,"autoBuffering":true}]}' />
        <!-- Image Fallback. Typically the same as the poster image. -->
        <img src="pic.jpg" width="840" height="780" alt="Poster Image"
          title="No video playback capabilities." />
      </object>
    </video>
    <!-- Download links provided for devices that can't play video in the browser. -->
    <p class="vjs-no-video"><strong>Download Video:</strong>
      <a href="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.mp4">MP4</a>,
      <a href="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.webm">WebM</a>,
      <a href="sneezegaurd/upload/videos/<?php echo $_GET["name"]; ?>.ogv">Ogg</a><br>
      <!-- Support VideoJS by keeping this link. -->
      </p>
  </div>
  <!-- End VideoJS -->

  
  
  
  
<?php
}
?>
 
  <?php  
  
  $modelllname=$_GET['modelname'];
  if(strpos($modelllname,'EP-950') !== false){
  
  ?>
  
  <h1 style="font-size: 1px;">Sneeze Guard for Office, Restaurant | Glass Shields | EP-950</h1>
  
  
  <?php
  }
  else{
  ?>
  
  <h1 style="font-size: 1px;">Sneeze Guard for Office, Restaurant | Glass Shields | <?php echo$modelllname; ?></h1>
  
  <?php
  }
  ?>
  
  
  
  
</body>
</html>