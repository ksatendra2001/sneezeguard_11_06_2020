
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="jquery.table2excel.js"></script>
<script>
$(document).ready(function() {

  function exportTableToCSV($table, filename) {

    var $rows = $table.find('tr:has(td)'),

      // Temporary delimiter characters unlikely to be typed by keyboard
      // This is to avoid accidentally splitting the actual contents
      tmpColDelim = String.fromCharCode(11), // vertical tab character
      tmpRowDelim = String.fromCharCode(0), // null character

      // actual delimiter characters for CSV format
      colDelim = '","',
      rowDelim = '"\r\n"',

      // Grab text from table into CSV formatted string
      csv = '"' + $rows.map(function(i, row) {
        var $row = $(row),
          $cols = $row.find('td');

        return $cols.map(function(j, col) {
          var $col = $(col),
            text = $col.text();

          return text.replace(/"/g, '""'); // escape double quotes

        }).get().join(tmpColDelim);

      }).get().join(tmpRowDelim)
      .split(tmpRowDelim).join(rowDelim)
      .split(tmpColDelim).join(colDelim) + '"';

    // Deliberate 'false', see comment below
    if (false && window.navigator.msSaveBlob) {

      var blob = new Blob([decodeURIComponent(csv)], {
        type: 'text/csv;charset=utf8'
      });

      // Crashes in IE 10, IE 11 and Microsoft Edge
      // See MS Edge Issue #10396033
      // Hence, the deliberate 'false'
      // This is here just for completeness
      // Remove the 'false' at your own risk
      window.navigator.msSaveBlob(blob, filename);

    } else if (window.Blob && window.URL) {
      // HTML5 Blob        
      var blob = new Blob([csv], {
        type: 'text/csv;charset=utf-8'
      });
      var csvUrl = URL.createObjectURL(blob);

      $(this)
        .attr({
          'download': filename,
          'href': csvUrl
        });
    } else {
      // Data URI
      var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

      $(this)
        .attr({
          'download': filename,
          'href': csvData,
          'target': '_blank'
        });
    }
  }

  // This must be a hyperlink
  $(".export").on('click', function(event) {
    // CSV
    var args = [$('#dvData>table'), 'costomer_data.csv'];

    exportTableToCSV.apply(this, args);

    // If CSV, don't do event.preventDefault() or return false
    // We actually need this to be a typical hyperlink
  });
});

//Warning: file_get_contents(D:\xampp\htdocs\magento\magmi\state\): failed to open stream: No such file or directory in D:\xampp\htdocs\magento\magmi\web\progress_parser.php on line 25
</script>

<?php
########################INSTALL #################################################
# This little hack gives you the possibility to export your customers's email addresses  list in a SYLK format readable by Excel
# I use it to upload the file in  an other newsletter script than the admin one which is not powerfull enough for my needs
# This script only select email addresses from customers who subscribe to the newsletter

# 1 . fill with your database data the lines 23-24-25-26
# 2 . upload this file in the admin directory
# 3 . in admin/includes/boxes/tools.php add  '<a href="' . tep_href_link('mail_export.php', '', 'NONSSL') . '" class="menuBoxContentLink" target="_blank">Email Export</a><br>'.
# 4 . That's it !
###################################################################################
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require('includes/application_top.php');
//require(DIR_WS_INCLUDES . 'template_top.php');
//header ( 'Content-disposition: filename=exported_email_'.date("Ymd").'.txt' );
//header ( 'Content-type: application/octetstream' );
//header ( 'Pragma: no-cache' );
//header ( 'Expires: 0' );


//only newsletter subscriber
//$queryResult = tep_db_query('SELECT `customers_email_address` AS email, `customers_firstname` AS firstname, `customers_lastname` AS lastname, `entry_street_address` AS address, `entry_city` AS city, `entry_state` AS state, `entry_postcode` AS zip, `customers_telephone` AS phone FROM `customers` cu, `address_book` ab WHERE cu.customers_default_address_id = ab.address_book_id AND `customers_newsletter` =1 LIMIT 0 , 9999');

//All customer
$queryResult = tep_db_query('SELECT `customers_email_address` AS email, `customers_firstname` AS firstname, `customers_lastname` AS lastname, `entry_street_address` AS address, `entry_city` AS city, `entry_state` AS state, `entry_postcode` AS zip, `customers_telephone` AS phone FROM `customers` cu, `address_book` ab WHERE cu.customers_default_address_id = ab.address_book_id LIMIT 0 , 99999');
echo'
<br /><br />
<button><a href="#" class="export">Export Data into Excel</a></button>
<br /><br />
<div id="dvData">
<table width="auto" border=1 id="dataTable" class="table table-bordered">
<tr>
<th>Sr. No</th>
<th>Customer Email</th>
<th>First Name</th>
<th>Last Name</th>
<th>Address</th>
<th>City</th>
<th>State</th>
<th>Zip</th>
<th>Phone No</th>
</tr>
';
$sr_n=0;
while ($row = tep_db_fetch_array($queryResult)) {
	$sr_n=$sr_n+1;
	echo'<tr>
	<td>'.$sr_n.'</td>
	<td>'.decrypt_email($row['email'], ENCRYPTION_KEY_EMAIL).'</td>
	<td>'.$row['firstname'].'</td>
	<td>'.$row['lastname'].'</td>
	<td>'.$row['address'].'</td>
	<td>'.$row['city'].'</td>
	<td>'.$row['state'].'</td>
	<td>'.$row['zip'].'</td>
	<td>'.$row['phone'].'</td>
	</tr>
	';
	
}

echo'</table>
</div>';

//require(DIR_WS_INCLUDES . 'template_bottom.php');
//  require(DIR_WS_INCLUDES . 'application_bottom.php');
?> 