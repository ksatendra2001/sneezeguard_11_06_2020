<?php
    ob_start();
    ini_set('max_upload_size','400M');
    $one_hide="visibility: hidden;display: none;";
    require('includes/application_top.php');
    require(DIR_WS_INCLUDES . 'template_top.php');
?>
<script type="text/javascript" src="../sneezegaurd/jscripts/tiny_mce/tiny_mce.js"></script><!-- Adding TinyMCE in form!! -->
<script type="text/javascript">
	tinyMCE.init({
          
        // add these two lines for absolute urls
        remove_script_host : false,
        convert_urls : false,

               // General options
         mode : "textareas",
        theme : "advanced",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "forecolor,backcolor,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		//theme_advanced_resizing : true,
		theme_advanced_resizing : false,
		theme_advanced_path : false,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->

<form name="upcust_popup" action="" method="post" enctype="multipart/form-data"><!-- Making table for showing the popup values fetched from the database!! -->
<table border="1" width="100%" cellspacing="8" cellpadding="6"  style="border-collapse:collapse; margin-top:30px;">
    <tr style="background:#706B5A;">
        <td style="width: 15%">
            <b>Catagory</b>
        </td>
        <td style="width: 45%">
            <b>Description</b>
        </td>
		
		<td style="width: 15%">
            <b>Production Time</b>
        </td>
		
		
		<td style="width: 15%">
            <b>Total Model</b>
        </td>
		
        <td style="width: 10%">
            <b>Action</b>
        </td>
    </tr>

<?php
  $res=tep_db_query("select * from homepage_content ORDER BY `priority` ASC");//Fetching the popups from database!
    while($row=tep_db_fetch_array($res)){

            $content_name1=$row["content_name"];//Saving that popups in variables!
            $description1=$row["description"];
            $production_time1=$row["production_time"];
            $total_model1=$row["total_model"];  
            $status_flag1=$row["status_flag"];
            $id = $row["content_id"];  

	 echo '<tr>';
        echo '<td style="width: 15%">';
            echo $content_name1; 
        echo '</td>';
        echo '<td class="main" style="text-align: justify;width: 45%;">';
            echo $description1;
        echo '</td>';
		echo '<td class="main" style="text-align: justify;width: 15%;">';
            echo $production_time1;
        echo '</td>';
		echo '<td class="main" style="text-align: justify;width: 15%;">';
            echo $total_model1;
        echo '</td>';
        echo '<td style="width: 10%">';
            echo '<input type="submit" name="edit_'.$id.'" value="Edit">';
            echo '<input type="hidden" name="content_idd_'.$id.'" value="'.$id.'">';
        // if($status_flag1 == 1){
        //     echo '<input type="submit" name="status_flag_'.$id.'" value="Hide">';
        //  }else{
        //     echo '<input type="submit" name="status_flag_'.$id.'" value="Show">';
        // }
        //     echo '<input type="hidden" name="id" value="'.$id.'">';
        echo '</td>';
    echo '</tr>';
    }
//    echo $msg_one.$msg_two.$msg_three.$msg_four;
?>	
  
</table>

</form>    
<?php 
    $sql=tep_db_query("SELECT MAX( content_id ) FROM `homepage_content`");
    $roww=tep_db_fetch_array($sql);
    $i=$roww['MAX( content_id )'];

    for ($ii=1; $ii <= $i; $ii++) { 
        # code...
        if(!isset($_POST["edit_".$ii])){//if In-stock

        }else{

            if(isset($_POST["edit_".$ii])){//if In-stock

                $content_idd = $_POST["content_idd_".$ii];

                $res=tep_db_query("select * from homepage_content where content_id=".$content_idd);//Fetching the popups from database!
                while($row=tep_db_fetch_array($res)){
                $content_name=$row["content_name"];
                $description=$row["description"];
                $production_time=$row["production_time"];
                $total_model=$row["total_model"];
                $priority_value=$row["priority"];
                $status_flag=$row["status_flag"];

                $val=$content_idd;
                $one_hide='display: block;visibility: visible;';
            }
        }
    }

}


?>
<hr style="width: 60%;margin-left: 200px;margin-top: 20px; border-radius: 10px 0 0 0;"><!-- Horizontal rule for split the form in two parts!! -->
<form name="upcust_updt" action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="content_id" value="<?php echo $val;?>">
    <table style="<?php echo $one_hide;?>"><!-- A new form which is updating the Popups!! -->
	
    <tr>
        <td style="width: 300px">
            Content Head:
			
        </td>
        <td>
           <input type="text" name="content_name" value="<?php echo $content_name;?>">
        </td>
    </tr>
	
	
	<tr>
        <td style="width: 300px">
            Content Description:
			
        </td>
        <td>
            <textarea name="description"><?php echo $description;?></textarea>
        </td>
    </tr>
	
	 <tr>
        <td style="width: 300px">
            Production Time:
			
        </td>
        <td>
           <input type="text" name="production_time" value="<?php echo $production_time;?>">
        </td>
    </tr>
	
	 <tr>
        <td style="width: 300px">
            Total Model:
			
        </td>
        <td>
           <input type="text" name="total_model" value="<?php echo $total_model;?>">
        </td>
    </tr>
    <tr>
        <td style="width: 300px">
            Priority:
            
        </td>
        <td>
           <input type="text" name="priority" value="<?php echo $priority_value;?>">
        </td>
    </tr>
    <tr>
        <td style="width: 300px">
            Visibility:
            
        </td>
        <td>
            <select name="visibility">
            <?php
                 if($status_flag == 1){
                    // echo '<input type="button" name="visibility" value="Hide">';
                    echo '<option value="1">Show</option>';
                    echo '<option value="0">Hide</option>';
                 }else{
                    // echo '<input type="button" name="visibility" value="Show">';
                    echo '<option value="0">Hide</option>';
                    echo '<option value="1">Show</option>';
                }
            ?>
            </select>
        </td>
    </tr>    
    <tr>
        <td colspan='2'>
            <center><input type="submit" name="update" value="Update Values" /></center>
        </td>
    </tr>
    
</table>
    <span style="color: #e5e5e5"></span>
</form>    
<?php
    if(isset($_POST['update'])){
		
        $descriptionss=$_POST['description'];
        $content_namess=$_POST['content_name'];
        $production_timess=$_POST['production_time'];
        $total_modelss=$_POST['total_model'];
        $content_id=$_POST['content_id'];
        $priority=$_POST['priority'];
		$visibility=$_POST['visibility'];
		
		// print_r($visibility);
        //$temp1=str_replace("\n", "", $temp);//replacing all the new lines from the updates popup!!
       // $temp2= str_replace("\r", "", $temp1);//Replacing all the new carriage return
        //$descriptionss=str_replace("</p> <p>", "</p><p>", $temp2);//this line helps the text to remove the white spaces!!
//        $temp3=  str_replace("</p>", "<br/>", $temp1);
//        $update_val='<span>'.$temp3.'</span>';
//          str_replace("<p>", "", $temp1);
        // echo $descriptionss;
        
            if(tep_db_query("UPDATE `homepage_content` SET `content_name`='".$content_namess."',`description`='".$descriptionss."',`production_time`='".$production_timess."',`total_model`='".$total_modelss."',`status_flag`='".$visibility."',`priority`='".$priority."' WHERE `content_id`='".$content_id."'")){//update query for all popups!!
               // header('Location: ' .tep_href_link($page='homepage_content.php'));//refresh script!
            }
            header('Location: '.$_SERVER['REQUEST_URI']);
        
    }else{}
?>    
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>