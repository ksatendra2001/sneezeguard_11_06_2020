<?php
// Error Redirects from many different states of Error, and handles them in one file.
// Records the output in the database for error trapping and also hacking attempts.
// You will recieve many 404 errors during a hacking attempt and i expect you would be very surprised if you knew how many attempts your site gets in a day!
// Also recorded Errors will show up any scripting errors, missing images, malformed code etc... giving you a much better web site
// Written by FImble for osCommerce
// osCommerce Forums URL -  http://forums.oscommerce.com/index.php?showuser=15542
// Add on URL http://addons.oscommerce.com/info/5914
// to hire me ... linuxux.group@gmail.com
// If you have concerns about security please get in touch with me for fast expert service,
// hacked? I am more than capable of clearing malicious code from your site, and securing it to prevent further attempts.

  require('includes/application_top.php');
  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  if (tep_not_null($action)) {
    switch ($action) {
      case 'insert':
        $hn_IP = tep_db_prepare_input(trim($_POST['linuxuk_ban_bots_names']));
        tep_db_query("INSERT INTO " . TABLE_LINUXUK_BAN_BOTS . " (linuxuk_ban_bots_names,linuxuk_ban_bots_date) VALUES ('".$hn_IP."',now());");
        tep_redirect(FILENAME_BAN_BOTS);
        break;


      case 'save':
        $hntrap_id = tep_db_prepare_input($_GET['tID']);
        $hn_IP = tep_db_prepare_input($_POST['linuxuk_ban_bots_names']);
        tep_db_query("UPDATE " . TABLE_LINUXUK_BAN_BOTS . " SET linuxuk_ban_bots_names = '".$hn_IP."'  WHERE linuxuk_ban_bots_id = '". $hntrap_id."';");
        tep_redirect(FILENAME_BAN_BOTS);
        break;



      case 'deleteconfirm':
        $hn_IP = tep_db_prepare_input($_GET['tID']);
        tep_db_query("DELETE FROM ". TABLE_LINUXUK_BAN_BOTS . " WHERE linuxuk_ban_bots_id = '".(int) $hn_IP."';");
        tep_redirect(tep_href_link(FILENAME_BAN_BOTS));
        break;

    }
  }
      require(DIR_WS_INCLUDES . 'template_top.php');
?>
<table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo LINUXUK_BAN_BOTS_HEADER  . LINUXUK_HTTP_IPTRAP_VERSION; ?></td>  
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo LINUXUK_IP_TRAP_BAN_BOTS_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo LINUXUK_IP_TRAP_BAN_BOTS_DATE; ?></td>
              </tr>
<?php
  $hn_trap_query_raw = "select linuxuk_ban_bots_id, linuxuk_ban_bots_date, linuxuk_ban_bots_names from " . TABLE_LINUXUK_BAN_BOTS . " order by linuxuk_ban_bots_date DESC";
  $trap_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $hn_trap_query_raw, $trap_query_numrows);
  $trap_query = tep_db_query($hn_trap_query_raw);
  while ($trapped = tep_db_fetch_array($trap_query)) {
    if ((!isset($_GET['tID']) || (isset($_GET['tID']) && ($_GET['tID'] == $trapped['linuxuk_ban_bots_id']))) && !isset($trInfo) && (substr($action, 0, 3) != 'new')) {
      $trInfo = new objectInfo($trapped);
    }

    if (isset($trInfo) && is_object($trInfo) && ($trapped['linuxuk_ban_bots_id'] == $trInfo->linuxuk_ban_bots_id)) {
      echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_BAN_BOTS, 'page=' . $_GET['page'] . '&tID=' . $trInfo->linuxuk_ban_bots_id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_BAN_BOTS, 'page=' . $_GET['page'] . '&tID=' . $trapped['linuxuk_ban_bots_id']) . '\'">' . "\n";
    }
?>
                <td class="dataTableContent"><?php echo $trapped['linuxuk_ban_bots_names']; ?></td>
                <td class="dataTableContent"><?php echo $trapped['linuxuk_ban_bots_date']; ?></td>
              </tr>
<?php
  }
?>
              <tr>
                <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $trap_split->display_count($trap_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_BOTS); ?></td>
                    <td class="smallText" align="right"><?php echo $trap_split->display_links($trap_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
<?php
  if (empty($action)) {
?>
                  <tr>
                    <td class="smallText" colspan="5" align="right"><?php echo tep_draw_button(LINUXUK_BOT_ADD_NEW, 'plus', tep_href_link(FILENAME_BAN_BOTS, 'page=' . $_GET['page'] . '&action=new')); ?></td>
                  </tr>

<?php
  }
?>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'new':
      $heading[] = array('text' => '<b>' .LINUXUK_IP_BAN_BOT_NEW . '</b>');
      $contents = array('form' => tep_draw_form('linuxuk_ban_bots_names', FILENAME_BAN_BOTS, 'page=' . $_GET['page'] . '&action=insert'));
      $contents[] = array('text' => LINUXUK_TRAP_BAN_BOT_NEW);
      $contents[] = array('text' => '<br />'.LINUXUK_BAN_BOT_NUMBER.':<br />' . tep_draw_input_field('linuxuk_ban_bots_names'));
      $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_BAN_BOTS,  'page=' . $_GET['page']) ));
      break;
    case 'edit':
      $heading[] = array('text' => LINUXUK_IP_TRAP_BAN_BOT_EDIT);
      $contents = array('form' => tep_draw_form('linuxuk_ban_bots_names', FILENAME_BAN_BOTS, 'page=' . $_GET['page'] . '&tID=' . $trInfo->linuxuk_ban_bots_id  . '&action=save'));
      $contents[] = array('text' => LINUXUK_IP_TRAP_BAN_BOT_CHANGE);
      $contents[] = array('text' => '<br />'.LINUXUK_IP_TRAP_BAN_BOT.':<br />' . tep_draw_input_field('linuxuk_ban_bots_names', $trInfo->linuxuk_ban_bots_names));
      $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_BAN_BOTS, 'page=' . $_GET['page'] . '&tID=' . $trInfo->linuxuk_ban_bots_id)));
      break;
    case 'delete':
      $heading[] = array('text' => LINUXUK_IP_TRAP_DELETE);
      $contents = array('form' => tep_draw_form('linuxuk_ban_bots_names', FILENAME_BAN_BOTS, 'page=' . $_GET['page'] . '&tID=' . $trInfo->linuxuk_ban_bots_id  . '&action=deleteconfirm'));
      $contents[] = array('text' => LINUXUK_IP_TRAP_DELETE_CONFIRM);
      $contents[] = array('text' => '<br /><b>' . $trInfo->linuxuk_ban_bots_names . '' . '</b>');
      $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_BAN_BOTS, 'page=' . $_GET['page'] . '&tID=' . $trInfo->linuxuk_ban_bots_id)));
      break;
    default:
      if (is_object($trInfo)) {
        $heading[] = array('text' => '<b>' . $trInfo->linuxuk_ban_bots_names . '</b>');
        $contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_BAN_BOTS, 'page=' . $_GET['page'] . '&tID=' . $trInfo->linuxuk_ban_bots_id . '&action=edit')) . tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(FILENAME_BAN_BOTS, 'page=' . $_GET['page'] . '&tID=' . $trInfo->linuxuk_ban_bots_id . '&action=delete')));
        $contents[] = array('text' => LINUXUK_IP_TRAP_SELECT . $trInfo->linuxuk_ban_bots_names);
      }
      break;
  }
  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";
    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>