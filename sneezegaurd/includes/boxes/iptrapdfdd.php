<?php
// IP Trap V6 23-05-2013
// http://www.linuxuk.co.uk
// Trap those IP Numbers!
// This is a major upgrade to previous version, and is not upgradable from previous versions.
// osCommerce Forums URL -  http://forums.oscommerce.com/index.php?showuser=15542
// osCommerce support Forum -  http://forums.oscommerce.com/topic/340290-ip-trap-version-3-released/
// Add on URL http://addons.oscommerce.com/info/5914
// to hire me ... linuxux.group@gmail.com
// If you have concerns about security please get in touch with me for fast expert service,
// hacked? I am more than capable of clearing malicious code from your site....

  $cl_box_groups[] = array(
    'heading' => BOX_HEADING_TRAP,
    'apps' => array(
     array(
        'code' => FILENAME_IP_TRAP,
        'title' => BOX_IP_TRAP,
        'link' => tep_href_link(FILENAME_IP_TRAP)
      ),
      array(
        'code' => FILENAME_BAN_BOTS,
        'title' => BOX_BAN_AGENT,
        'link' => tep_href_link(FILENAME_BAN_BOTS)
      ),
      array(
        'code' => FILENAME_LINUXUK_HTTP_ERROR_LOG,
        'title' => BOX_LINUXUK_ERROR_HEADING,
        'link' => tep_href_link(FILENAME_LINUXUK_HTTP_ERROR_LOG)
      )
    )
  );
?>