<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  $cl_box_groups[] = array(
    'heading' => BOX_HEADING_REPORTS,
    'apps' => array(
      array(
        'code' => FILENAME_STATS_PRODUCTS_VIEWED,
        'title' => BOX_REPORTS_PRODUCTS_VIEWED,
        'link' => tep_href_link(FILENAME_STATS_PRODUCTS_VIEWED)
      ),
      array(
        'code' => FILENAME_STATS_PRODUCTS_PURCHASED,
        'title' => BOX_REPORTS_PRODUCTS_PURCHASED,
        'link' => tep_href_link(FILENAME_STATS_PRODUCTS_PURCHASED)
      ),
      //kgt - discount coupons report
      array(
        'code' => FILENAME_STATS_CUSTOMERS,
        'title' => BOX_REPORTS_ORDERS_TOTAL,
        'link' => tep_href_link(FILENAME_STATS_CUSTOMERS)
      ),
      array(
      'code' => FILENAME_STATS_DISCOUNT_COUPONS,
      'title' => BOX_REPORTS_DISCOUNT_COUPONS ,
      'link' => tep_href_link(FILENAME_STATS_DISCOUNT_COUPONS)
      ),
    //end kgt - discount coupons 
	  array(
        'code' => FILENAME_STATS_SALES,
        'title' => BOX_REPORTS_SALES,
        'link' => tep_href_link(FILENAME_STATS_SALES)
         ),
	  array(
        'code' => FILENAME_STATS_MONTHLY_SALES,
        'title' => BOX_REPORTS_MONTHLY_SALES,
        'link' => tep_href_link(FILENAME_STATS_MONTHLY_SALES)
      ),
	  array(
        'code' => FILENAME_IP_LOG,
        'title' => "Mail Ip Log",
        'link' => tep_href_link(FILENAME_IP_LOG)
      ),
	  array(
        'code' => FILENAME_IP_REVIT,
        'title' => "View Revit Downloaded IP",
        'link' => tep_href_link(FILENAME_IP_REVIT)
      ),
	  array(
        'code' => FILENAME_IP_SAVE_LAYOUT,
        'title' => "View Save Layout IP",
        'link' => tep_href_link(FILENAME_IP_SAVE_LAYOUT)
      ),
	  array(
        'code' => FILENAME_IP_SAVE_QUOTE,
        'title' => "view_save_quote_ip",
        'link' => tep_href_link(FILENAME_IP_SAVE_QUOTE)
      ),
	  array(
        'code' => FILENAME_VIEW_REPORTED_ISSUE,
        'title' => "View Reported Issue",
        'link' => tep_href_link(FILENAME_VIEW_REPORTED_ISSUE)
      ),
	  array(
        'code' => FILENAME_IP_THANK_YOU,
        'title' => "View Print Screen Popup IP",
        'link' => tep_href_link(FILENAME_IP_THANK_YOU)
      )

    )
  );
?>
