<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  $cl_box_groups[] = array(
    'heading' => BOX_HEADING_CATALOG,
    'apps' => array(
      array(
        'code' => FILENAME_CATEGORIES,
        'title' => BOX_CATALOG_CATEGORIES_PRODUCTS,
        'link' => tep_href_link(FILENAME_CATEGORIES)
      ),
	  
      array(
        'code' => FILENAME_PRODUCTS_ATTRIBUTES,
        'title' => BOX_CATALOG_CATEGORIES_PRODUCTS_ATTRIBUTES,
        'link' => tep_href_link(FILENAME_PRODUCTS_ATTRIBUTES)
      ),
      array(
        'code' => FILENAME_MANUFACTURERS,
        'title' => BOX_CATALOG_MANUFACTURERS,
        'link' => tep_href_link(FILENAME_MANUFACTURERS)
      ),
      array(
        'code' => FILENAME_REVIEWS,
        'title' => BOX_CATALOG_REVIEWS,
        'link' => tep_href_link(FILENAME_REVIEWS)
      ),
      array(
        'code' => FILENAME_SPECIALS,
        'title' => BOX_CATALOG_SPECIALS,
        'link' => tep_href_link(FILENAME_SPECIALS)
      ),
	  
	 // Discount Code 2.6 - start
array(
'code' => FILENAME_DISCOUNT_CODES,
'title' => BOX_CATALOG_DISCOUNT_CODES,
'link' => tep_href_link(FILENAME_DISCOUNT_CODES)
),
// Discount Code 2.6 - end

/*
// Discount Code for costomer
array(
'code' => 'Discount Customer',
'title' => 'Discount Coupon',
'link' => 'discount_codes_customer.php'
),*/


// Discount Code for costomer - end
	  
           ///kgt - discount coupons
      array(
        'code' => FILENAME_PRODUCTS_EXPECTED,
        'title' => BOX_CATALOG_PRODUCTS_EXPECTED,
        'link' => tep_href_link(FILENAME_PRODUCTS_EXPECTED)
      ),     
      array(
     'code' => FILENAME_DISCOUNT_COUPONS,
     'title' => BOX_CATALOG_DISCOUNT_COUPONS,
     'link' => 'coupons.php'
     ),
     //end kgt - discount coupons  
	 array(
        'code' => FILENAME_LIGHTBAR,
        'title' => BOX_CATALOG_LIGHTBAR,
        'link' => tep_href_link(FILENAME_LIGHTBAR)
      ),
	     array(
        'code' => FILENAME_AUDIOVIDIO,
        'title' => BOX_CATALOG_AUDIO_VIDIO,
        'link' => tep_href_link(FILENAME_AUDIOVIDIO)
      ),array(
        'code' => FILENAME_MAIN_PAGE_POPUP,
        'title' => BOX_CATALOG_MAIN_PAGE_POPUP,
        'link' => tep_href_link(FILENAME_MAIN_PAGE_POPUP)
      ),array(
        'code' => FILENAME_CUSTOM_PAGE_POPUP,
        'title' => BOX_CATALOG_CUSTOM_PAGE_POPUP,
        'link' => tep_href_link(FILENAME_CUSTOM_PAGE_POPUP)
      ),  array(
        'code' => FILENAME_NEWPRODUCTVIDEO,
        'title' => BOX_CATALOG_FILENAME_NEWPRODUCTVIDEO,
        'link' => tep_href_link(FILENAME_NEWPRODUCTVIDEO)
      ),   array(
        'code' => FILENAME_VIDEOUPLODE,
        'title' => BOX_CATALOG_VIDEOUPLODE,
        'link' => tep_href_link(FILENAME_VIDEOUPLODE)
      ),  array(
        'code' => FILENAME_CUSTOM_LIBRARY,
        'title' => BOX_CATALOG_CUSTOM_LIBRARY,
        'link' => tep_href_link(FILENAME_CUSTOM_LIBRARY)
      ), array(
        'code' => FILENAME_CHANGETEXT,
        'title' => BOX_CATALOG_CHANGETEXT,
        'link' => tep_href_link(FILENAME_CHANGETEXT)
      ), array(
        'code' => FILENAME_TRADESHOW,
        'title' => BOX_CATALOG_TRADESHOW,
        'link' => tep_href_link(FILENAME_TRADESHOW)
      ), array(
        'code' => FILENAME_CUSTOM_POPUP,
        'title' => BOX_CATALOG_POPUP,
        'link' => tep_href_link(FILENAME_CUSTOM_POPUP)
      ), array(
        'code' => FILENAME_EDIT_TERM,
        'title' => "Edit Terms",
        'link' => tep_href_link(FILENAME_EDIT_TERM)
      ), array(
        'code' => FILENAME_CART_MESSAGE,
        'title' => BOX_CATALOG_MESSAGE,
        'link' => tep_href_link(FILENAME_CART_MESSAGE)
      ), array(
        'code' => FILENAME_WT_VAL,
        'title' => "Weight Value",
        'link' => tep_href_link(FILENAME_WT_VAL)
      ), array(
        'code' => FILENAME_STATUS_SPECIAL_PRICE,
        'title' => "Special Price Status",
        'link' => tep_href_link(FILENAME_STATUS_SPECIAL_PRICE)
      ), array(
        'code' => FILENAME_HOMEPAGE_CONTENT,
        'title' => "Change Homepage Content",
        'link' => tep_href_link(FILENAME_HOMEPAGE_CONTENT)
      ), array(
        'code' => FILENAME_HOMEPAGE_CONTENT_IMG,
        'title' => "Change Homepage Content Image",
        'link' => tep_href_link(FILENAME_HOMEPAGE_CONTENT_IMG)
      ), array(
        'code' => FILENAME_GLASS_DISCOUNT,
        'title' => "Glass discount",
        'link' => tep_href_link(FILENAME_GLASS_DISCOUNT)
      ), array(
        'code' => FILENAME_ADDITIONAL_IMAGES,
        'title' => "Additional images",
        'link' => tep_href_link(FILENAME_ADDITIONAL_IMAGES)
      ), array(
        'code' => FILENAME_INSTALLTION_IMAGES,
        'title' => "Installation images",
        'link' => tep_href_link(FILENAME_INSTALLTION_IMAGES)
      ), array(
        'code' => FILENAME_HEADINGS,
        'title' => "Add Headings",
        'link' => tep_href_link(FILENAME_HEADINGS)
      ), array(
        'code' => FILENAME_PARAGRAPHSS,
        'title' => "Add Paragraph",
        'link' => tep_href_link(FILENAME_PARAGRAPHSS)
      ), array(
        'code' => FILENAME_PRODUCTS_MULTI,
        'title' => "Multi-product",
        'link' => tep_href_link(FILENAME_PRODUCTS_MULTI)
      )
    )
  );
?>
