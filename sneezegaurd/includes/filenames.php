<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

// define the filenames used in the project
  define('FILENAME_ACTION_RECORDER', 'action_recorder.php');
  define('FILENAME_ADMINISTRATORS', 'administrators.php');
  define('FILENAME_BACKUP', 'backup.php');
  define('FILENAME_BANNER_MANAGER', 'banner_manager.php');
  define('FILENAME_BANNER_STATISTICS', 'banner_statistics.php');
  define('FILENAME_CACHE', 'cache.php');
  define('FILENAME_CATALOG_ACCOUNT_HISTORY_INFO', 'account_history_info.php');
  define('FILENAME_CATEGORIES', 'categories.php');
  define('FILENAME_AUDIOVIDIO', 'audio_vidio.php');
  define('FILENAME_MAIN_PAGE_POPUP', 'main_page.php');
  define('FILENAME_CUSTOM_PAGE_POPUP', 'custom_page.php');
   define('FILENAME_CUSTOM_LIBRARY', 'custom_library.php');
  define('FILENAME_NEWPRODUCTVIDEO', 'new_product_video.php');
  define('FILENAME_VIDEOUPLODE', 'video_upload.php');
  define('FILENAME_TRADESHOW', 'trade_show.php');
  define('FILENAME_CUSTOM_POPUP', 'custom_popup.php');
  define('FILENAME_CHANGETEXT', 'change_text.php');
   define('FILENAME_LIGHTBAR', 'lightbar_video.php');
  define('FILENAME_CONFIGURATION', 'configuration.php');
  define('FILENAME_COUNTRIES', 'countries.php');
  define('FILENAME_CURRENCIES', 'currencies.php');
  define('FILENAME_CUSTOMERS', 'customers.php');
  define('FILENAME_DEFAULT', 'index.php');
  define('FILENAME_DEFINE_LANGUAGE', 'define_language.php');
  define('FILENAME_GEO_ZONES', 'geo_zones.php');
  define('FILENAME_LANGUAGES', 'languages.php');
  define('FILENAME_LOGIN', 'login.php');
  define('FILENAME_MAIL', 'mail.php');
  define('FILENAME_MANUFACTURERS', 'manufacturers.php');
  define('FILENAME_MODULES', 'modules.php');
  define('FILENAME_NEWSLETTERS', 'newsletters.php');
  define('FILENAME_ORDERS', 'orders.php');
  define('FILENAME_ORDERS_INVOICE', 'invoice.php');
  define('FILENAME_ORDERS_PACKINGSLIP', 'packingslip.php');
  define('FILENAME_ORDERS_STATUS', 'orders_status.php');
  define('FILENAME_POPUP_IMAGE', 'popup_image.php');
  define('FILENAME_PRICES_PER', 'prices_per.php');
  define('FILENAME_PRODUCTS_ATTRIBUTES', 'products_attributes.php');
  define('FILENAME_PRODUCTS_EXPECTED', 'products_expected.php');
  define('FILENAME_REVIEWS', 'reviews.php');
  define('FILENAME_SEC_DIR_PERMISSIONS', 'sec_dir_permissions.php');
  define('FILENAME_SERVER_INFO', 'server_info.php');
  define('FILENAME_SHIPPING_MODULES', 'shipping_modules.php');
  define('FILENAME_SPECIALS', 'specials.php');
  define('FILENAME_STATS_CUSTOMERS', 'stats_customers.php');
  define('FILENAME_STATS_PRODUCTS_PURCHASED', 'stats_products_purchased.php');
  define('FILENAME_STATS_PRODUCTS_VIEWED', 'stats_products_viewed.php');
  define('FILENAME_STORE_LOGO', 'store_logo.php');
  define('FILENAME_TAX_CLASSES', 'tax_classes.php');
  define('FILENAME_TAX_RATES', 'tax_rates.php');
  define('FILENAME_VERSION_CHECK', 'version_check.php');
  define('FILENAME_WHOS_ONLINE', 'whos_online.php');
  define('FILENAME_ZONES', 'zones.php');
  define('FILENAME_STATS_MONTHLY_SALES', 'stats_monthly_sales.php');
  define('FILENAME_CART_MESSAGE', 'cart_message.php');
  // BOF: Order Editor
  define('FILENAME_ORDERS_EDIT', 'edit_orders.php');
  
  
  define('FILENAME_IP_LOG', 'mail_ip_log.php');
  define('FILENAME_IP_REVIT', 'view_revit_ip.php');
  define('FILENAME_IP_SAVE_LAYOUT', 'view_save_layout_ip.php');
  define('FILENAME_IP_SAVE_QUOTE', 'view_save_quote_ip.php');
  define('FILENAME_IP_THANK_YOU', 'view_save_print_screen_popup_ip.php');
  
  
  define('FILENAME_ORDERS_EDIT_ADD_PRODUCT', 'edit_orders_add_product.php');
  define('FILENAME_ORDERS_EDIT_AJAX', 'edit_orders_ajax.php');
// EOF: Order Editor
	define('FILENAME_WT_VAL', 'wieght_mult.php');
	define('FILENAME_EDIT_TERM', 'condition_and_term.php');
	define('FILENAME_STATUS_SPECIAL_PRICE', 'change_status_special_price_product.php');
	define('FILENAME_HOMEPAGE_CONTENT', 'homepage_content.php');
  define('FILENAME_HOMEPAGE_CONTENT_IMG', 'home_page_content_imges.php');
  define('FILENAME_ADDITIONAL_IMAGES', 'additional_images.php');
  
  define('FILENAME_INSTALLTION_IMAGES', 'add_installation_images.php');
  define('FILENAME_HEADINGS', 'add_headings.php');
  define('FILENAME_PARAGRAPHSS', 'add_paragraph.php');
  
  
  define('FILENAME_VIEW_REPORTED_ISSUE', 'view_reported_issue.php');
  
  
  
  
  
  define('FILENAME_GLASS_DISCOUNT', 'discount_on_off_for_glass.php');
//sale tex
	define('FILENAME_STATS_SALES', 'stats_sales.php');
    define('FILENAME_PRODUCTS_MULTI', 'products_multi.php');
//end sale tex

define('FILENAME_DISCOUNT_CODES', 'discount_codes.php'); // Discount Code 2.6
define('FILENAME_DISCOUNT_CODES_CUSTOMER', 'discount_codes_customer.php'); // Discount Code 2.6



//kgt - discount coupons
  define('FILENAME_DISCOUNT_COUPONS','coupons.php');
  define('FILENAME_DISCOUNT_COUPONS_MANUAL', 'coupons_manual.html');
  define('FILENAME_DISCOUNT_COUPONS_EXCLUSIONS', 'coupons_exclusions.php');
  //end kgt - discount coupons
  
  //kgt - discount coupons report
	define('FILENAME_STATS_DISCOUNT_COUPONS', 'stats_discount_coupons.php');
  //end kgt - discount coupons report

  
  
    /***** Begin View Counter *****/
  require(DIR_WS_INCLUDES . 'view_counter_defines.php');
  /***** End View Counter *****/
?>
