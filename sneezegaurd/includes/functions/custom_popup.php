<?php
    ob_start();
    ini_set('max_upload_size','400M');
    $one_hide="visibility: hidden;display: none;";
    require('includes/application_top.php');
    require(DIR_WS_INCLUDES . 'template_top.php');
?>
<script type="text/javascript" src="../sneezegaurd/jscripts/tiny_mce/tiny_mce.js"></script><!-- Adding TinyMCE in form!! -->
<script type="text/javascript">
	tinyMCE.init({
          
        // add these two lines for absolute urls
        remove_script_host : false,
        convert_urls : false,

               // General options
         mode : "textareas",
        theme : "advanced",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "forecolor,backcolor,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		//theme_advanced_resizing : true,
		theme_advanced_resizing : false,
		theme_advanced_path : false,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->

<?php
    $res=tep_db_query("select * from ".TABLE_CUSTOM_POPUPS);//Fetching the popups from database!
    while($row=tep_db_fetch_array($res)){
        if($row['id']==72){
            $msg_one=$row["post_popup"];//Saving that popups in variables!
            $msg_two=$row["face_popup"];
            $msg_five=$row["adjustable_popup"];
            $msg_six=$row["cart_popup"];
            $msg_nine=$row["option_popup"];
        }else if($row['id']==55){
            $msg_three=$row["post_popup"];
        }else if($row['id']==114){
            $msg_four=$row["option_popup"];
        }else if($row['id']==120){
            $msg_seven=$row['message'];
            $msg_eight=$row['opiton1_popup'];
        }else if($row['id']==56){
            $msg_ten=$row['glass_popup'];
        } else if($row['id']==200) {
            $msg_eleven = $row['message'];
        }
    }
//    echo $msg_one.$msg_two.$msg_three.$msg_four;
?>
<form name="upcust_popup" action="" method="post" enctype="multipart/form-data"><!-- Making table for showing the popup values fetched from the database!! -->
<table border="1" width="100%" cellspacing="8" cellpadding="6"  style="border-collapse:collapse; margin-top:30px;">
    <tr style="background:#706B5A;">
        <td style="width: 380px">
            <b>Popup Catagory</b>
        </td>
        <td>
            <b>Popup Message</b>
        </td>
        <td style="width: 35px">
            <b>Action</b>
        </td>
    </tr>
    <tr>
        <td style="width: 360px">
            Custom Popups for all:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_two; ?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit1" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Post Message Popup for EP5 & EP15:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_one;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit2" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Post Message Popup for EP11 & EP12:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_three;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit3" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Popups for ED20,ES29 ES82,ES53:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_four;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit4" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Popups for Adjustable Glass:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_five;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit5" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Popups for Add to Cart & Quote Shipping Button:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_six;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit6" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Popup for Heat Lamp Centerline Message:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_seven;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit7" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Popup for Heat Lamp Infinite Control Message:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_eight;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit8" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Custom Popup for EP5 Froasted Glass:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_nine;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit9" value="Edit">
        </td>
    </tr>
	<tr>
        <td style="width: 350px">
            Custom Popup for 3/8 Glass for EP5/EP15/EP11/EP12/EP21/EP22/EP36/ES31/ES40/ES67/ES73:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_ten;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit10" value="Edit">
        </td>
    </tr>
    <tr>
        <td style="width: 350px">
            Wish list popup:
        </td>
        <td class="main" style="text-align: justify;">
            <?php echo $msg_eleven;?>
        </td>
        <td style="width: 35px">
            <input type="submit" name="edit11" value="Edit">
        </td>
    </tr>
</table>

</form>    
<?php 
    $edit_text=$val="";
    if(isset($_POST["edit1"])){//if CUSTOM POPUP for ALL is going to update then the popup value is shown in TEXTAREA
        $edit_text=$msg_two;
        $val=0;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit2"])){//if Custom Post Message Popup for EP5 & EP15 is going to update then the popup value is shown in TEXTAREA
        $edit_text=$msg_one;
        $val=1;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit3"])){//if Custom Post Message Popup for EP11 & EP12 is going to update then the popup value is shown in TEXTAREA
        $edit_text=$msg_three;
        $val=2;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit4"])){//if Custom Popups for ED20,ES29 ES82,ES53 is going to update then the popup value is shown in TEXTAREA
        $edit_text=$msg_four;
        $val=3;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit5"])){//if Custom Popups for Adjustable Glass is going to update then the popup value is shown in TEXTAREA
        $edit_text=$msg_five;
        $val=4;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit6"])){//if Custom Popups for Add to Cart & Quote Shipping Button is going to update then the popup value is shown in TEXTAREA
        $edit_text=$msg_six;
        $val=5;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit7"])){//if Custom Popups for Add to Cart & Quote Shipping Button is going to update then the popup value is shown in TEXTAREA
        $edit_text=$msg_seven;
        $val=6;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit8"])){//if Custom Popups for Add to Cart & Quote Shipping Button is going to update then the popup value is shown in TEXTAREA
        $edit_text=$msg_eight;
        $val=7;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit9"])){//if Custom Popups for Add to Cart & Quote Shipping Button is going to update then the popup value is shown in TEXTAREA
        $edit_text=$msg_nine;
        $val=8;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit10"])){//if Custom Popups for Add to Cart & Quote Shipping Button is going to update then the popup value is shown in TEXTAREA
        $edit_text=$msg_ten;
        $val=9;
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST["edit11"])){//For wish list message
        $edit_text=$msg_eleven;
        $val=10;
        $one_hide='display: block;visibility: visible;';
    }
//    echo $edit_text;
?>
<hr style="width: 60%;margin-left: 200px;margin-top: 20px; border-radius: 10px 0 0 0;"><!-- Horizontal rule for split the form in two parts!! -->
<form name="upcust_updt" action="" method="post" enctype="multipart/form-data">
    <table style="<?php echo $one_hide;?>"><!-- A new form which is updating the Popups!! -->
    <tr>
        <td style="width: 300px">
            Change Custom Message:<input type="hidden" name="id" value="<?php echo $val;?>">
        </td>
        <td>
            <textarea name="update_val"><?php echo $edit_text;?></textarea>
        </td>
    </tr>
    <tr>
        <td colspan='2'>
            <center><input type="submit" name="update" value="Update Values" /></center>
        </td>
    </tr>
    
</table>
    <span style="color: #e5e5e5"></span>
</form>    
<?php
    if(isset($_POST['update'])){
        $temp=$_POST['update_val'];
        $temp1=str_replace("\n", "", $temp);//replacing all the new lines from the updates popup!!
        $temp2= str_replace("\r", "", $temp1);//Replacing all the new carriage return
        $update_val=str_replace("</p><p>", "</p><p>", $temp2);//this line helps the text to remove the white spaces!!
//        $temp3=  str_replace("</p>", "<br/>", $temp1);
//        $update_val='<span>'.$temp3.'</span>';
//          str_replace("<p>", "", $temp1);
        echo $update_val;
        if($_POST['id']==0){
            if(tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET message= '".$update_val."', opiton1_popup= '".$update_val."', left_popup = '".$update_val."', right_popup = '".$update_val."', face_popup = '".$update_val."' where id <> 120")){//update query for all popups!!
                header('Location: ' .tep_href_link($page='custom_popup.php'));//refresh script!
            }
        }else if($_POST['id']==1){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET post_popup= '".$update_val."' where id=71 or id=72");//again update query for the post popup of specific products
            header('Location: ' .tep_href_link($page='custom_popup.php'));//agin refreshin the page!
        }else if($_POST['id']==2){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET post_popup= '".$update_val."' where id=55 or id=56");//update query for the post popup for snother specific products!
            header('Location: ' .tep_href_link($page='custom_popup.php'));//now again refreshing the page!
        }else if($_POST['id']==3){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET option_popup= '".$update_val."' where id=110 or id=111 or id=113 or id=114");//update query for the products which having bay option popup
            header('Location: ' .tep_href_link($page='custom_popup.php'));//again and again refresh query
        }else if($_POST['id']==4){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET adjustable_popup = '".$update_val."'");//same update query!
            header('Location: ' .tep_href_link($page='custom_popup.php'));//refreshing query
        }else if($_POST['id']==5){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET cart_popup = '".$update_val."'");//update query
            header('Location: ' .tep_href_link($page='custom_popup.php'));//refreshing query
        }else if($_POST['id']==6){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET message = '".$update_val."' where id=120");//update query
            header('Location: ' .tep_href_link($page='custom_popup.php'));//refreshing query
        }else if($_POST['id']==7){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET opiton1_popup = '".$update_val."' where id=120");//update query
            header('Location: ' .tep_href_link($page='custom_popup.php'));//refreshing query
        }else if($_POST['id']==8){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET option_popup = '".$update_val."' where id=72");//update query
            header('Location: ' .tep_href_link($page='custom_popup.php'));//refreshing query
        }else if($_POST['id']==9){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET glass_popup = '".$update_val."' where id=56 or id=72 or id=71 or id=55 or id=57 or id=58 or id=59 or id=61 or id=62 or id=63 or id=64");//update query
            header('Location: ' .tep_href_link($page='custom_popup.php'));//refreshing query
        }else if($_POST['id']==10){
            tep_db_query("UPDATE ".TABLE_CUSTOM_POPUPS." SET message = '".$update_val."' where id=200");//update query
            header('Location: ' .tep_href_link($page='custom_popup.php'));//refreshing query
        }
    }

?>    
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>