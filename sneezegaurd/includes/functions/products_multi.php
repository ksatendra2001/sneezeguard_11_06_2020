<?php
/*
  $Id: products_multi.php v2.8

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2017 osCommerce

  Released under the GNU General Public License
  
*/

  // Multiple Products Manager
  function tep_array_merge($array1, $array2, $array3 = '') {
    if ($array3 == '') $array3 = array();
    if (function_exists('array_merge')) {
      $array_merged = array_merge($array1, $array2, $array3);
    } else {
      while (list($key, $val) = each($array1)) $array_merged[$key] = $val;
      while (list($key, $val) = each($array2)) $array_merged[$key] = $val;
      if (sizeof($array3) > 0) while (list($key, $val) = each($array3)) $array_merged[$key] = $val;
    }

    return (array) $array_merged;
  }

// saca la categoria padre.
  function tep_get_parent_cat($categ_id = '') {
    global $current_category_id;
	    
	$parent_category_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$categ_id . "'");
	$parent_category = tep_db_fetch_array($parent_category_query);
   
   	$categ = $parent_category['parent_id'];
	  
    return $categ;
  }
?>