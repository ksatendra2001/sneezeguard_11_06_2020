<?php
    ob_start();
    ini_set('max_upload_size','400M');
    $one_hide="visibility: hidden;display: none;";
    require('includes/application_top.php');
    require(DIR_WS_INCLUDES . 'template_top.php');
?>
<script type="text/javascript" src="../sneezegaurd/jscripts/tiny_mce/tiny_mce.js"></script><!-- Adding TinyMCE in form!! -->
<script type="text/javascript">
	tinyMCE.init({
          
        // add these two lines for absolute urls
        remove_script_host : false,
        convert_urls : false,

               // General options
         mode : "textareas",
        theme : "advanced",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "forecolor,backcolor,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		//theme_advanced_resizing : true,
		theme_advanced_resizing : false,
		theme_advanced_path : false,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->
<h1 class="pageHeading">Edit Customer Dashboard</h1>
<form name="upcust_popup" action="" method="post" enctype="multipart/form-data"><!-- Making table for showing the popup values fetched from the database!! -->
<table border="1" width="100%" cellspacing="8" cellpadding="6"  style="border-collapse:collapse; margin-top:30px;">
    <tr style="background:#706B5A;">
        <td style="width: 15%">
            <b>No.</b>
        </td>
        <td style="width: 45%">
            <b>Content Head</b>
        </td>
		
		<td style="width: 15%">
            <b>Short Description</b>
        </td>
		
		
		<td style="width: 15%">
            <b>Status</b>
        </td>
		
        <td style="width: 10%">
            <b>Action</b>
        </td>
    </tr>

<?php
$sr_no=1;
  $res=tep_db_query("select * from customer_dashboard_data ORDER BY `id` ASC");//Fetching the popups from database!
    while($row=tep_db_fetch_array($res)){

            $heading=$row["heading"];//Saving that popups in variables!
            $short_description=$row["short_description"];
            $status=$row["status"];
			if($status==1)
			{
			$statuss='Active';	
			}
			else{
			$statuss='Inactive';
			}
            $id = $row["id"];  

	 echo '<tr>';
        echo '<td style="width: 15%">';
            echo $sr_no; 
        echo '</td>';
		 echo '<td style="width: 15%">';
            echo $heading; 
        echo '</td>';
        echo '<td class="main" style="text-align: justify;width: 45%;">';
            echo $short_description;
        echo '</td>';
		echo '<td class="main" style="text-align: justify;width: 15%;">';
            echo $statuss;
        echo '</td>';
	
        echo '<td style="width: 10%">';
            echo '<input type="submit" name="edit_'.$id.'" value="Edit">';
            echo '<input type="hidden" name="content_idd_'.$id.'" value="'.$id.'">';
        // if($status_flag1 == 1){
        //     echo '<input type="submit" name="status_flag_'.$id.'" value="Hide">';
        //  }else{
        //     echo '<input type="submit" name="status_flag_'.$id.'" value="Show">';
        // }
        //     echo '<input type="hidden" name="id" value="'.$id.'">';
        echo '</td>';
    echo '</tr>';
	
	$sr_no++;
    }
//    echo $msg_one.$msg_two.$msg_three.$msg_four;
?>	
  
</table>

</form>   

 
<?php 


$dd=date_default_timezone_set('America/Los_Angeles'); 
//echo date("Y-m-d h:i:s");

    $sql=tep_db_query("SELECT MAX( id ) FROM `customer_dashboard_data`");
    $roww=tep_db_fetch_array($sql);
    $i=$roww['MAX( id )'];

    for ($ii=1; $ii <= $i; $ii++) { 
        # code...
        if(!isset($_POST["edit_".$ii])){//if In-stock

        }else{

            if(isset($_POST["edit_".$ii])){//if In-stock

                $content_idd = $_POST["content_idd_".$ii];

                $res=tep_db_query("select * from customer_dashboard_data where id=".$content_idd);//Fetching the popups from database!
                while($row=tep_db_fetch_array($res)){
                $heading=$row["heading"];
                $description=$row["short_description"];
               
                $status_flag=$row["status"];

                $val=$content_idd;
                $one_hide='display: block;visibility: visible;';
            }
        }
    }

}


?>
<hr style="width: 60%;margin-left: 200px;margin-top: 20px; border-radius: 10px 0 0 0;"><!-- Horizontal rule for split the form in two parts!! -->
<form name="upcust_updt" action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="content_id" value="<?php echo $val;?>">
    <table style="<?php echo $one_hide;?>"><!-- A new form which is updating the Popups!! -->
	
    <tr>
        <td style="width: 300px">
            Content Head:
			
        </td>
        <td>
           <input type="text" name="content_name" value="<?php echo $heading;?>">
        </td>
    </tr>
	
	
	<tr>
        <td style="width: 300px">
            Content Description:
			
        </td>
        <td>
            <textarea name="description"><?php echo $description;?></textarea>
        </td>
    </tr>
	
	
	
   
    <tr>
        <td style="width: 300px">
            Status:
            
        </td>
        <td>
            <select name="visibility">
            <?php
                 if($status_flag == 1){
                    // echo '<input type="button" name="visibility" value="Hide">';
                    echo '<option value="1">Active</option>';
                    echo '<option value="0">Inactive</option>';
                 }else{
                    // echo '<input type="button" name="visibility" value="Show">';
                    echo '<option value="0">Inactive</option>';
                    echo '<option value="1">Active</option>';
                }
            ?>
            </select>
        </td>
    </tr>    
    <tr>
        <td colspan='2'>
            <center><input type="submit" name="update" value="Update Values" /></center>
        </td>
    </tr>
    
</table>
    <span style="color: #e5e5e5"></span>
</form>    
<?php
    if(isset($_POST['update'])){
		
        $short_description=$_POST['description'];
        $headings=$_POST['content_name'];
       
        $id=$_POST['content_id'];
		$status=$_POST['visibility'];
		
		$dd=date_default_timezone_set('America/Los_Angeles');
		$date=date("Y-m-d h:i:s");
        
            if(tep_db_query("UPDATE `customer_dashboard_data` SET `heading`='$headings',`short_description`='$short_description',`status`='$status',`date_modified`='$date' WHERE `id`='$id'")){//update query for all popups!!
            
			
            }
            header('Location: '.$_SERVER['REQUEST_URI']);
        
    }else{}
?>    
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>