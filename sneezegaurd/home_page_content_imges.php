<?php
    ob_start();
    ini_set('max_upload_size','400M');
    $one_hide="visibility: hidden;display: none;";
    require('includes/application_top.php');
    require(DIR_WS_INCLUDES . 'template_top.php');
	
	error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<script type="text/javascript" src="../sneezegaurd/jscripts/tiny_mce/tiny_mce.js"></script><!-- Adding TinyMCE in form!! -->
<script type="text/javascript">
	tinyMCE.init({
          
        // add these two lines for absolute urls
        remove_script_host : false,
        convert_urls : false,

               // General options
         mode : "textareas",
        theme : "advanced",
		skin : "o2k7",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "forecolor,backcolor,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		//theme_advanced_resizing : true,
		theme_advanced_resizing : false,
		theme_advanced_path : false,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->

<form name="upcust_popup" action="" method="post" enctype="multipart/form-data"><!-- Making table for showing the popup values fetched from the database!! -->
<table border="1" width="100%" cellspacing="8" cellpadding="6"  style="border-collapse:collapse; margin-top:30px;">
    <tr style="background:#706B5A;">
        <td style="width: 15%">
            <b>Catagory</b>
        </td>
        <td style="width: 45%">
            <b>Picture</b>
        </td>		
        <td style="width: 10%">
            <b>Action</b>
        </td>
    </tr>
	
<?php
  $res=tep_db_query("select * from homepage_content_img");//Fetching the popups from database!
    while($row=tep_db_fetch_array($res)){
            $picture=$row["pic"];
            $id=$row["content_id"];
	 echo '<tr>';
        echo '<td style="width: 15%">';
            echo $row["content_name"];
        echo '</td>';
        echo '<td class="main" style="text-align: justify;width: 45%;">';
            echo '<img src="images/homepage_images/'.$picture.'" style="width:131px;">';
        echo '</td>';
        echo '<td style="width: 10%">';
            echo '<input type="submit" name="edit_'.$id.'" value="Edit">';
            echo '<input type="hidden" name="row_id_'.$id.'" value="'.$id.'">';
        echo '</td>';
    echo '</tr>';
    }
?>
	
  
</table>

</form>    
<?php 

    // $i=0;
    $sql=tep_db_query("SELECT MAX( content_id ) FROM `homepage_content`");
    $roww=tep_db_fetch_array($sql);
    $i=$roww['MAX( content_id )'];

    for ($ii=1; $ii <= $i; $ii++) { 
        # code...
        if(!isset($_POST["edit_".$ii])){//if In-stock

        }else{

            if(isset($_POST["edit_".$ii])){//if In-stock
                $content_id=$_POST["row_id_".$ii];

                $ress=tep_db_query("select * from homepage_content_img where content_id=".$content_id);//Fetching the popups from database!
                while($roww=tep_db_fetch_array($ress)){

                    $content_name=$roww['content_name'];
                    $picture=$roww['pic'];
                    $val=$content_id;
                    $one_hide='display: block;visibility: visible;';
                 }
            }
        }

    }


?>
<hr style="width: 60%;margin-left: 200px;margin-top: 20px; border-radius: 10px 0 0 0;">
<!-- Horizontal rule for split the form in two parts!! -->
<form name="upcust_updt" action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="content_id" value="<?php echo $val;?>">
    <table style="<?php echo $one_hide;?>"><!-- A new form which is updating the Popups!! -->
	
    <tr>
        <td style="width: 300px">
            Content Head:
			
        </td>
        <td>
           <input type="text" name="content_name" value="<?php echo $content_name;?>">
        </td>
    </tr>

	 <tr>
        <td style="width: 300px">
            Image:
			
        </td>
        <td>
		<strong>Main Image <small>(130 x 98px)</small></strong><br><a href="/images/homepage_images/<?php echo$picture; ?>" target="_blank"><?php echo$picture; ?></a> |
		
           <input type="file" name="fileToUpload" id="fileToUpload">
        </td>
    </tr>
    <tr>
        <td colspan='2'>
            <center><input type="submit" name="update" value="Update Values" /></center>
        </td>
    </tr>
    
</table>
    <span style="color: #e5e5e5"></span>
</form>    
<?php
    if(isset($_POST['update'])){
		
	$content_namess=$_POST['content_name'];
	$content_id=$_POST['content_id'];	

$file=$_FILES['fileToUpload'];


$ext_str = "gif,jpg,jpeg";

$allowed_extensions=explode(',',$ext_str);
//print_r($allowed_extensions);
$max_file_size = 10485760;//10 mb remember 1024bytes =1kbytes /* check allowed extensions here */

$ext = substr($file['name'], strrpos($file['name'], '.') + 1); //get file extension from last sub string from last . character

if (!in_array($ext, $allowed_extensions) ) {

$msg="only".$ext_str." files allowed to upload"; // exit the script by warning
$flag=0;

} /* check file size of the file if it exceeds the specified size warn user */



//if(!move_uploaded_file($file['tmp_name'],$upload_directory.$file['name'])){

//$path=md5(microtime()).'.'.$ext;
$path=basename($_FILES["fileToUpload"]["name"]);
$uploaddir = 'images/homepage_images/';
$uploadfile = $uploaddir . basename($path);
 

//if(move_uploaded_file($file['tmp_name'],$upload_directory.$path)){

if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $uploadfile)) 
{
	
if(tep_db_query("UPDATE `homepage_content_img` SET `content_name`='".$content_namess."',`pic`='".$path."' WHERE `content_id`='".$content_id."'"))
{//update query for all popups!!
                header('Location: '.$_SERVER['REQUEST_URI']);           // header('Location: ' .tep_href_link($page='home_page_content_imges.php'));//refresh script!
}
//echo'<br />';	

   // echo "File is valid, and was successfully uploaded.\n";
} 
else 
{
   $msg="Possible file upload attack!\n";
   $flag=0;
}
	
}
        
        
    

?>    
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>