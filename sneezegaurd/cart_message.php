<?php
    ob_start();
    ini_set('max_upload_size','400M');
    $one_hide=$two_hide="visibility: hidden;display: none;";

    require('includes/application_top.php');
    require(DIR_WS_INCLUDES . 'template_top.php');
?>
<?php
    $var1=array();
    $i=0;
    $var=tep_db_query("select * from ".TABLE_CART_MESSAGE);
    while($row=tep_db_fetch_array($var)){
        $var1[$i]=$row['value'];
        $i++;
    }   
?>

<form name="upcust_message" action="" method="post" enctype="multipart/form-data"><!-- Making table for showing the popup values fetched from the database!! -->
    <table border="1" width="900px" cellspacing="8" cellpadding="6"  style="border-collapse:collapse; margin-top:30px;margin-left: 20px">
        <tr style="width:200px;">
            <td style="background-color: #CCCCCC;width: 200px  "><h3>Heat Lamp Message</h3></td>
        </tr>
        <tr>
            <td>1.</td>
            <td># This products production leadtime is <?php echo $var1[0];?> business days.</td>
            <td rowspan="2" style="text-align: center;width: 80px"> <input type="submit" name="one" value="Edit"></td>
        </tr>
        <tr>
            <td>2.</td>
            <td>## This products production leadtime is <?php echo $var1[1];?> business days.</td>
            
        </tr>
        </table>
        <table border="1" width="900px" cellspacing="8" cellpadding="6"  style="border-collapse:collapse; margin-top:30px;margin-left: 20px">
        <tr >
            <td style="background-color: #CCCCCC;width: 200px "><h3>Glass Type Message</h3></td>
        </tr>
        <tr>
            <td>1.</td>
            <td>** for square corner glass <?php echo $var1[2];?> business days.</td>
            <td rowspan="2" style="text-align: center; width: 80px"><input type="submit" name="two" value="Edit"></td>
        </tr>
        <tr>
            <td>2.</td>
            <td>*** for radius corner glass <?php echo $var1[3];?> business days.</td>
            
        </tr>
    </table>
</form>
<?php
    if(isset($_POST['one'])){
        $one_hide='display: block;visibility: visible;';
    }else if(isset($_POST['two'])){
        $two_hide='display: block;visibility: visible;';
    }
?>
<form name="upcust_message_update" action="" method="post" enctype="multipart/form-data">
<div style="margin-left: 20px; <?php echo $one_hide;?>">
<h3>Change The message of HEAT LAMP</h3>
<table style="margin-left: 40px;">
    <tr style="height:40px">
        <td># This products production leadtime is <input type="text" name="text1" value="<?php echo $var1[0];?>"> business days.</td>
    </tr>
    <tr>
        <td>## This products production leadtime is <input type="text" name="text2" value="<?php echo $var1[1];?>"> business days.</td>
    </tr>
    <tr>        
        <td style="text-align: center"><input type="submit" name="submit1" value="Update"></td>
    </tr>
</table>
</div>
<div style="margin-left: 20px;<?php echo $two_hide;?>">
<h3>Change The message of Squared/Circle Glass</h3>
<table style="margin-left: 40px;">
    <tr style="height:40px">
        <td>** for square corner glass <input type="text" name="text3" value="<?php echo $var1[2];?>"> business days.</td>
    </tr>
    <tr>
        <td>*** for radius corner glass <input type="text" name="text4" value="<?php echo $var1[3];?>"> business days.</td>
    </tr>
    <tr>
        <td style="text-align: center"><input type="submit" name="submit2" value="Update"></td>
    </tr>
</table>
</div>
</form>
<?php
    $temp="1-5";
    if(isset($_POST['submit1'])){
        $value1=$_POST['text1'];
        $value2=$_POST['text2'];
        tep_db_query("update ".TABLE_CART_MESSAGE. " set value= '".$value1."' where id=1");
        tep_db_query("update ".TABLE_CART_MESSAGE. " set value= '".$value2."' where id=2");
        header('Location: ' .tep_href_link($page='cart_message.php'));
    }else if(isset($_POST['submit2'])){
        $value1=$_POST['text3'];
        $value2=$_POST['text4'];
        tep_db_query("update ".TABLE_CART_MESSAGE. " set value= '".$value1."' where id=3");
        tep_db_query("update ".TABLE_CART_MESSAGE. " set value= '".$value2."' where id=4");
        header('Location: ' .tep_href_link($page='cart_message.php'));
    }
?>

<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>