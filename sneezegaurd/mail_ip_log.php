<?php
    ob_start();
    ini_set('max_upload_size','400M');
    $one_hide="visibility: hidden;display: none;";
    require('includes/application_top.php');
    require(DIR_WS_INCLUDES . 'template_top.php');
?>
<h3>MAIL IP LOG</h3>
<table border=1>
    <tr>
        <th>
            S.No.
        </th>
        <th>
            Mail ID
        </th>
        <th>
            IP
        </th>
        <th>
            Date
        </th>
		<th>
			Subject(mail)
		</th>
		<th>
			Content(mail)
		</th>
		<th>
            Send From(Option)
        </th>
        <th>
            Image
        </th>
<?php
$i=1;
    $res=tep_db_query("select * from ".TABLE_MAIL_LOG." order by id desc");//Fetching the popups from database!
    while($row=tep_db_fetch_array($res)){
		if($row['image']!=""){
			$image='<a href="/img/screenshot/'.$row['image'].'.jpg" target="_blank"><img src="/img/screenshot/'.$row['image'].'.jpg" width="10%"></a>';
		}else{
			$image="Not Available";
		}
        echo '<tr>';
        echo '<td>'.$i.'</td><td>'.$row['mail_id'].'</td><td>'.$row['ip'].'</td><td>'.$row['date_time'].'</td><td>'.$row['subject'].'</td><td>'.$row['content'].'</td><td>'.($row['type']==""?"Not Available":$row['type']).'</td><td align="center">'.$image.'</td>';
        echo '</tr>';
        $i++;
    }
?>

<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>