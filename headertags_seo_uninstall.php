<?php
/*
  $Id: headertags_seo_update.php, v 2.6.0 by Jack_mcs

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce
  Portions Copyright 2009 oscommerce-solution.com  

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  if (isset($_POST['action']) && $_POST['action'] == 'process') {
      if (isset($_POST['install'])) {
          tep_redirect(tep_href_link('headertags_seo_install.php'));
          exit;
      } else if (isset($_POST['rerun'])) {
          unlink('headertags_seo_uninstall.php');
          tep_redirect(tep_href_link('headertags_seo_install.php'));
          exit;               
      } else if (isset($_POST['delete'])) {
          unlink('headertags_seo_uninstall.php');
          tep_redirect(tep_href_link('index.php'));
          exit;          
      } else if (isset($_POST['goto'])) {    
          tep_redirect(tep_href_link('index.php'));
      } else {  //catch-all
          tep_redirect(tep_href_link('index.php'));
      }    
  } 
    
  $config_sql_array = array(array("ALTER TABLE products_description DROP products_head_title_tag"),
                            array("ALTER TABLE products_description DROP products_head_title_tag_alt"),
                            array("ALTER TABLE products_description DROP products_head_title_tag_url"),
                            array("ALTER TABLE products_description DROP products_head_desc_tag"),
                            array("ALTER TABLE products_description DROP products_head_keywords_tag"),
                            array("ALTER TABLE products_description DROP products_head_listing_text"),
                            array("ALTER TABLE products_description DROP products_head_sub_text"),
                            array("ALTER TABLE products_description DROP products_head_breadcrumb_text"),
                            array("ALTER TABLE products_description DROP products_head_additional_words"),

                            array("ALTER TABLE categories_description DROP categories_htc_title_tag"),
                            array("ALTER TABLE categories_description DROP categories_htc_title_tag_alt"),
                            array("ALTER TABLE categories_description DROP categories_htc_title_tag_url"),
                            array("ALTER TABLE categories_description DROP categories_htc_desc_tag"),
                            array("ALTER TABLE categories_description DROP categories_htc_keywords_tag"),
                            array("ALTER TABLE categories_description DROP categories_htc_description"),
                            array("ALTER TABLE categories_description DROP categories_htc_breadcrumb_text"),

                            array("ALTER TABLE manufacturers_info DROP manufacturers_htc_title_tag"),
                            array("ALTER TABLE manufacturers_info DROP manufacturers_htc_title_tag_alt"),
                            array("ALTER TABLE manufacturers_info DROP manufacturers_htc_title_tag_url"),
                            array("ALTER TABLE manufacturers_info DROP manufacturers_htc_desc_tag"),
                            array("ALTER TABLE manufacturers_info DROP manufacturers_htc_keywords_tag"),
                            array("ALTER TABLE manufacturers_info DROP manufacturers_htc_description"),
                            array("ALTER TABLE manufacturers_info DROP manufacturers_htc_breadcrumb_text"),

                            array("DELETE FROM configuration_group WHERE configuration_group_title LIKE '%Header Tags%'"),
                            array("DELETE FROM configuration WHERE configuration_key LIKE '%HEADER_TAGS%'"),

                            array("DROP TABLE IF EXISTS headertags_cache"),
                            array("DROP TABLE IF EXISTS headertags_default"),
                            array("DROP TABLE IF EXISTS headertags"),
                            array("DROP TABLE IF EXISTS headertags_keywords"),
                            array("DROP TABLE IF EXISTS headertags_search"),
                            array("DROP TABLE IF EXISTS headertags_silo"),
                            array("DROP TABLE IF EXISTS headertags_social"),
                            array("DROP TABLE IF EXISTS headertags_ip_tracker"));


  foreach ($config_sql_array as $sql_array) {
    foreach ($sql_array as $value) {
      //echo $value . '<br>';
      if (tep_db_query($value) == false) {
        $db_error = true;
      }
    }
  }

?>
<div class="pageHeading"><?php echo 'Header Tags SEO Database Un-installer'; ?></div>
<div style="padding:10px 0">
<?php
  if ($db_error == false) {
    echo 'Header Tags SEO successfully removed from database!!!';
  } else {
    echo 'Errors encountered during database deletions!!!';
  }
?>
</div>

<?php echo tep_draw_form('headertags_seo_install', 'headertags_seo_uninstall.php', 'post') . tep_hide_session_id() . tep_draw_hidden_field('action', 'process'); ?>
  <div style="padding-bottom:10px"><input type="submit" name="install" value="Run the Header Tags SEO Installer"></div>
  <div style="padding-bottom:10px"><input type="submit" name="rerun" value="Run the Header Tags SEO Installer AFTER deleting this file (recommended)"></div>
  <div style="padding-bottom:10px"><input type="submit" name="delete" value="Go To Home Page AFTER deleting this file (recommended)"></div>
  <div style="padding-bottom:10px"><input type="submit" name="goto" value="Go To Home Page"></div>
</form> 

