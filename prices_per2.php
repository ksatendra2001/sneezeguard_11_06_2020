<?php
/*
  $Id: mail.php,v 1.1.1.1 2004/03/04 23:38:43 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');
  
  if (isset($_GET["action"]) && $_GET["action"]=='modify_prices') {
	$category_id = $_POST["category_id"];
	if (trim($_POST["amount"])!="0") {
		$amount = $_POST["amount"];
	}
	elseif (trim($_POST["amount_dollars"]!="0")) {
		$amount_dollars = $_POST["amount_dollars"];
	}
	$operation = $_POST["operation"];
	if ($category_id != 'all') {
		$query = 'select p.products_id, p.products_price, ptc.categories_id from 
			 products p,
			 products_to_categories ptc
			 where p.products_id = ptc.products_id
			 and ptc.categories_id = "'.(int)$category_id.'"';
		$result = mysql_query($query) or die(mysql_error());
		$number_of_modified_records = 0;
		while ($row = mysql_fetch_array($result)) {
			if ($operation == "plus") {
				if (isset($amount) && trim($amount)!="0") {
					$new_price = ceil(round((1+($amount/100)) * $row["products_price"],2));
				}
				elseif (isset($amount_dollars) && trim($amount_dollars)!="0") {
					$new_price = ceil($row["products_price"] + $amount_dollars);
				}
			}
			else {
				if (isset($amount) && trim($amount)!="0") {
					$new_price = ceil(round((1-($amount/100)) * $row["products_price"],2));
				}
				elseif (isset($amount_dollars) && trim($amount_dollars)!="0") {
					$new_price = ceil($row["products_price"] - $amount_dollars);
				}
			}
			$query_update = 'update products set products_price = '.$new_price.' where products_id = '.$row["products_id"];
			mysql_query($query_update);
			$number_of_modified_records++;
		}
	}
	else {
		$query = 'select products_id, products_price from products';
		$result = mysql_query($query) or die(mysql_error());
		$number_of_modified_records = 0;
		while ($row = mysql_fetch_array($result)) {
			if ($operation == "plus") {
				if (isset($amount) && trim($amount)!="0") {
					$new_price =ceil( round((1+($amount/100)) * $row["products_price"],2));
				}
				elseif (isset($amount_dollars) && trim($amount_dollars)!="0") {
					$new_price = ceil($row["products_price"] + $amount_dollars);
				}
			}
			else {
				if (isset($amount) && trim($amount)!="0") {
					$new_price = ceil(round((1-($amount/100)) * $row["products_price"],2));
				}
				elseif (isset($amount_dollars) && trim($amount_dollars)!="0") {
					$new_price = ceil($row["products_price"] - $amount_dollars);
				}
			}
			$query_update = 'update products set products_price = '.$new_price.' where products_id = '.$row["products_id"];
			mysql_query($query_update);
			$number_of_modified_records++;
		}	
	}
  }

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo MODIFY_PRICES_TITLE;?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/menu.js"></script>

       <script language="Javascript1.2"><!-- // load htmlarea
// MaxiDVD Added WYSIWYG HTML Area Box + Admin Function v1.7 - 2.2 MS2 HTML Email HTML - <head>
      _editor_url = "<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_ADMIN; ?>htmlarea/";  // URL to htmlarea files
        var win_ie_ver = parseFloat(navigator.appVersion.split("MSIE")[1]);
         if (navigator.userAgent.indexOf('Mac')        >= 0) { win_ie_ver = 0; }
          if (navigator.userAgent.indexOf('Windows CE') >= 0) { win_ie_ver = 0; }
           if (navigator.userAgent.indexOf('Opera')      >= 0) { win_ie_ver = 0; }
       <?php if (HTML_AREA_WYSIWYG_BASIC_EMAIL == 'Basic'){ ?>  if (win_ie_ver >= 5.5) {
       document.write('<scr' + 'ipt src="' +_editor_url+ 'editor_basic.js"');
       document.write(' language="Javascript1.2"></scr' + 'ipt>');
          } else { document.write('<scr'+'ipt>function editor_generate() { return false; }</scr'+'ipt>'); }
       <?php } else{ ?> if (win_ie_ver >= 5.5) {
       document.write('<scr' + 'ipt src="' +_editor_url+ 'editor_advanced.js"');
       document.write(' language="Javascript1.2"></scr' + 'ipt>');
          } else { document.write('<scr'+'ipt>function editor_generate() { return false; }</scr'+'ipt>'); }
       <?php }?>
// --></script>
       <script language="JavaScript" src="htmlarea/validation.js"></script>
       <script language="JavaScript">
<!-- Begin
       function init() {
define('customers_email_address', 'string', 'Customer or Newsletter Group');
}
//  End -->
</script>
</head>
<body OnLoad="init()" marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">

<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo MODIFY_PRICES_TITLE;?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr><?php echo tep_draw_form('mail', 'prices_per.php', 'action=modify_prices'); ?>
            <td><table border="0" width="100%" cellpadding="0" cellspacing="2">
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo MODIFY_PRICES_CATEGORY;?></b><br>
					<select name='category_id'>
						<option value='all'><?php echo MODIFY_PRICES_ALL;?></option>
						<?php
							$query = "select categories_id, categories_name from categories_description";
							$result = mysql_query($query);
							while ($row = mysql_fetch_array($result)) {
								echo '<option value='.$row["categories_id"].'>'.$row["categories_name"].'</option>';
							}
							mysql_free_result($result);
						?>
					</select>
                </td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b>+/-</b><br>
					<select name='operation'>
						<option value='plus'>+</option>
						<option value='minus'>-</option>
					</select>
                </td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo MODIFY_PRICES_AMOUNT;?></b><br><input type=text name=amount size=4 value=0>%</td>
              </tr>
              <tr>
                <td class="smallText"><b><?php echo MODIFY_PRICES_OR;?></b><br>$<input type=text name=amount_dollars size=4 value=0></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="smallText"><b>
                	<?php
                		if (isset($_GET["action"]) && $_GET["action"]=='modify_prices') {
                			echo $number_of_modified_records . " products modified";
                			if (isset($amount) && trim($amount)!="0") echo " by ".$amount."% ";
                			else echo " by $".$amount_dollars;
                		}
                	?>
                </b></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td>
                    <tr>
                    <td align="right"><?php echo '<a href="' . tep_href_link('prices_per.php') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_SEND_EMAIL); ?></td>
                    </tr>
                    <td class="smallText"></td>
                  </tr>
                </table></td>
             </tr>
            </table></td>
          </form></tr>
          <tr height="30%"><td></td></tr>
          <tr height="30%"><td></td></tr>
          <tr height="30%"><td></td></tr>
          <tr>
          <td>
          <?php
			if (isset($_GET["action"]) && $_GET["action"]=='modify_prices')
			//if(isset($_POST['submit']))
			{
				$category = $_POST['category_id'];
				$oper = $_POST['operation'];
				$amount1 = $_POST['amount'];
				$amount_dollars1 = $_POST['amount_dollars'];
				$dateposted = date("m/d/Y H:i:s");
				
				//$query = "select categories_name from categories_description where categories_id = '$category'";
							$result12 = mysql_query($query);
			
				//$file_contents= "\n ". $dateposted ."</td><td> ".$result12."</td><td> ".$oper."</td><td>".$amount1."</td><td>".$amount_dollars1;
				
				$file_contents= "\n ". $dateposted ."</td><td> ".$category."</td><td> ".$oper."</td><td>".$amount1."</td><td>".$amount_dollars1;
				$file_contents .= file_get_contents('ChangeLog.txt');
				file_get_contents('ChangeLog.txt', $file_contents);
				$file_handle = fopen("ChangeLog.txt", "w");

				fwrite($file_handle, $file_contents);
				fclose($file_handle);
				//print "file created and written to"; 

}
?> 
          
           <table width="50%" style="font-size:12px; ">
                    
              <tr style="background-color:#999; color:#FFF;">
              <td width="18%" ><b>DATE & TIME</b></td>
              <td width="15%" ><b>MODIFY PRICES CATEGORY</b></td>
              <td width="10%" ><b>+/-</b></td>
              <td width="15%" ><b>MODIFY PRICES AMOUNT(%)</b></td>
              <td width="15%" ><b>MODIFY PRICES OR($)</b></td>
              </tr>

               	<?php 
					$handle = fopen("ChangeLog.txt", "r");
					if ($handle) {
   					 	while (($line = fgets($handle)) !== false) {
     				   	$array = explode('|', trim($line));
        				foreach($array as $td) {
            				echo '<tr><td>'.$td.'</td></tr>';
        				}
   					 }
				}
				?>
              
              
             
                    </table>
    </td></tr>
<!-- body_text_eof //-->
        </table></td>
      </tr>
      
      <tr>
      <td>
      		<?php
			if (isset($_GET["action"]) && $_GET["action"]=='modify_prices')
			//if(isset($_POST['submit']))
			{
				$category = $_get['category_id'];
				$oper = $_get['operation'];
				$amount1 = $_get['amount'];
				$amount_dollars1 = $_get['amount_dollars'];
				$dateposted = date("m/d/Y H:i:s");
			
				$file_contents= "\n ". $dateposted ."</td><td> ".$category."</td><td> ".$oper."</td><td>".$amount1."</td><td>".$amount_dollars1;
				$file_contents .= file_get_contents('ChangeLog.txt');
				file_get_contents('ChangeLog.txt', $file_contents);
				$file_handle = fopen("ChangeLog.txt", "w");

				fwrite($file_handle, $file_contents);
				fclose($file_handle);
				//print "file created and written to"; 

}
?> 
      </td>
      </tr>
    </table>
    
   <?php /*?> <table>
                    
              <tr>
              <td width="25%"><b>Date & Time</b></td>
              <td width="20%"><b>Modify Price category</b></td>
              <td width="15%"><b>+/-</b></td>
              <td width="15%"><b>Modyfy Prices Amount (%)</b></td>
              <td width="15%"><b>Modyfy Prices or ($)</b></td>
              </tr>

               	<?php 
					$handle = fopen("ChangeLog.txt", "r");
					if ($handle) {
   					 	while (($line = fgets($handle)) !== false) {
     				   	$array = explode('|', trim($line));
        				foreach($array as $td) {
            				echo '<tr><td>'.$td.'</td></tr>';
        				}
   					 }
				}
				?>
              
              
             
                    </table>
    <?php */?>
    </td>
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<table aling=center><tr><td>Powered by <a href=http://www.PinkCrow.net>PinkCrow</a></td></tr></table>
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
