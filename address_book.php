<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ADDRESS_BOOK);

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL'));

  require(DIR_WS_INCLUDES . 'template_top.php');
?>



<?
  if (!$detect->isMobile())
{
?>

<style>
.contentText, .contentText table {
    /* padding: 5px 0 5px 0; */
    font-size: 15px;
    line-height: 1.5;
}

p {
    font-family: "Times New Roman", Times, serif;
    color: ffffff;
    font-size: 17px;
    /* padding-left: 35px; */
}

.ui-widget {
    font-family: Lucida Grande, Lucida Sans, Arial, sans-serif;
    font-size: 1.1em;
    width: 50%;
}

.button2 {
    border-radius: 10px;
    padding-top: 0px;
}


.button224 {
        background-color: #0971dad4;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 23px;
    cursor: pointer;
    width: 141px;
    height: 49px;
}


.button3 {
    border-radius: 10px;
    padding-top: 0px;
}


.button223 {
        background-color: #0971dad4;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 23px;
    cursor: pointer;
    width: 206px;
    height: 49px;
}
</style>
<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td  valign="top" >
<h2 style=" height:26px;"><?php echo HEADING_TITLE; ?></h2>

<?php
  if ($messageStack->size('addressbook') > 0) {
    echo $messageStack->output('addressbook');
  }
?>

<div class="contentContainer" style="text-align:left">
  <h2><?php echo PRIMARY_ADDRESS_TITLE; ?></h2>

  <div class="contentText">
    <div class="ui-widget infoBoxContainer" style="float: right;">
    <table>
    	<tr><td width="100px" valign="top"><b><?php echo PRIMARY_ADDRESS_TITLE; ?></b></td><td rowspan="2"><?php echo tep_address_label($customer_id, $customer_default_address_id, true, ' ', '<br />'); ?></td></tr>
        <tr><td width="100px" valign="top"><img src="images/arrow_south_east.gif" border="0" alt="" width="50" height="31"></td></tr>
    </table>
      <!--div class="ui-widget-header infoBoxHeading" style="background:none;border:none;color:#f4f4f4;"><?php echo PRIMARY_ADDRESS_TITLE; ?></div>

      <div class="ui-widget-content infoBoxContents" style="background:none;border:none;color:#f4f4f4;">
        <?php echo tep_address_label($customer_id, $customer_default_address_id, true, ' ', '<br />'); ?>
      </div-->
    </div>

    <?php echo PRIMARY_ADDRESS_DESCRIPTION; ?>
  </div>

  <div style="clear: both;"></div>

  <h2><?php echo ADDRESS_BOOK_TITLE; ?></h2>

  <div class="contentText">

<?php
  $addresses_query = tep_db_query("select address_book_id, entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_street_address as street_address, entry_suburb as suburb, entry_city as city, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' order by firstname, lastname");
  while ($addresses = tep_db_fetch_array($addresses_query)) {
    $format_id = tep_get_address_format_id($addresses['country_id']);
?>

    <div>
      <span style="float: right;" class="edit_account"><?php echo '<a href="'.tep_href_link(FILENAME_ADDRESS_BOOK_PROCESS, 'edit=' . $addresses['address_book_id'], 'SSL').'">'.tep_image_button('small_edit.gif', SMALL_IMAGE_BUTTON_EDIT).'</a>'.' ' . '<a href="'.tep_href_link(FILENAME_ADDRESS_BOOK_PROCESS, 'delete=' . $addresses['address_book_id'], 'SSL').'">'.tep_image_button('small_delete.gif', SMALL_IMAGE_BUTTON_DELETE).'</a>'; ?></span>
      <p><strong><?php echo tep_output_string_protected($addresses['firstname'] . ' ' . $addresses['lastname']); ?></strong><?php if ($addresses['address_book_id'] == $customer_default_address_id) echo '&nbsp;<small><i>' . PRIMARY_ADDRESS . '</i></small>'; ?></p>
      <p style="padding-left: 20px;"><?php echo tep_address_format($format_id, $addresses, true, ' ', '<br />'); ?></p>
    </div>

<?php
  }
?>

  </div>

  <div>
  <table width="100%">
    <tr>
  	 <td>
	 <?php //echo '<a href="'.tep_href_link(FILENAME_ACCOUNT, '', 'SSL').'">'.tep_image_button('button_back.gif', IMAGE_BUTTON_BACK).'</a>'; ?>
	<a href="<?php echo tep_href_link(FILENAME_ACCOUNT, '', 'SSL') ?>"><button class="button224 button2">Back</button></a>
	 </td>
<?php
  if (tep_count_customer_address_book_entries() < MAX_ADDRESS_BOOK_ENTRIES) {
?>

    <td align="right">
	<?php //echo '<a href="'.tep_href_link(FILENAME_ADDRESS_BOOK_PROCESS, '', 'SSL').'">'.tep_image_button('button_add_address.gif', IMAGE_BUTTON_ADD_ADDRESS).'</a>'; ?>
	<a href="<?php echo tep_href_link(FILENAME_ADDRESS_BOOK_PROCESS, '', 'SSL') ?>"><button class="button223 button3">Add Address</button></a>
	</td>

<?php
  }
?>
  </tr>
</table>
    
  </div>

  <p><?php echo sprintf(TEXT_MAXIMUM_ENTRIES, MAX_ADDRESS_BOOK_ENTRIES); ?></p>
</div>
</td></tr></table></td></tr></table>





<?
}

else{
?>
<td id="ex1" align=center width="190" valign="top">

<style>
.contentText, .contentText table {
    /* padding: 5px 0 5px 0; */
    font-size: 15px;
    line-height: 1.5;
}

p {
    font-family: "Times New Roman", Times, serif;
    color: ffffff;
    font-size: 17px;
    /* padding-left: 35px; */
}

.ui-widget {
    font-family: Lucida Grande, Lucida Sans, Arial, sans-serif;
    font-size: 1.1em;
    width: 50%;
}

.button2 {
    border-radius: 10px;
    padding-top: 0px;
}


.button224 {
    background-color: #0971dad4;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 32px;
    cursor: pointer;
    width: 188px;
    height: 64px;
}


.button3 {
    border-radius: 10px;
    padding-top: 0px;
}


.button223 {
    background-color: #0971dad4;
    border: none;
    color: white;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 37px;
    cursor: pointer;
    width: 333px;
    height: 71px;
}
.form_white td {
    background: #FFFFFF !important;
    color: #000000;
    font-size: 31px;
}
</style>
<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td  valign="top" >
<h2 style=" height:43px;font-size: 43px;"><?php echo HEADING_TITLE; ?></h2>

<?php
  if ($messageStack->size('addressbook') > 0) {
    echo $messageStack->output('addressbook');
  }
?>

<div class="contentContainer" style="text-align:left">
  <h2><?php echo PRIMARY_ADDRESS_TITLE; ?></h2>

  <div class="contentText">
    <div class="ui-widget infoBoxContainer" style="float: right;">
    <table>
    	<tr><td width="100px" valign="top"><b><?php echo PRIMARY_ADDRESS_TITLE; ?></b></td><td rowspan="2"><?php echo tep_address_label($customer_id, $customer_default_address_id, true, ' ', '<br />'); ?></td></tr>
        <tr><td width="100px" valign="top"><img src="images/arrow_south_east.gif" border="0" alt="" ></td></tr>
    </table>
      <!--div class="ui-widget-header infoBoxHeading" style="background:none;border:none;color:#f4f4f4;"><?php echo PRIMARY_ADDRESS_TITLE; ?></div>

      <div class="ui-widget-content infoBoxContents" style="background:none;border:none;color:#f4f4f4;">
        <?php echo tep_address_label($customer_id, $customer_default_address_id, true, ' ', '<br />'); ?>
      </div-->
    </div>

    <?php echo PRIMARY_ADDRESS_DESCRIPTION; ?>
  </div>

  <div style="clear: both;"></div>

  <h2><?php echo ADDRESS_BOOK_TITLE; ?></h2>

  <div class="contentText">

<?php
  $addresses_query = tep_db_query("select address_book_id, entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_street_address as street_address, entry_suburb as suburb, entry_city as city, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' order by firstname, lastname");
  while ($addresses = tep_db_fetch_array($addresses_query)) {
    $format_id = tep_get_address_format_id($addresses['country_id']);
?>

    <div>
      <span style="float: right;" class="edit_account">
	  <?php echo '<a href="'.tep_href_link(FILENAME_ADDRESS_BOOK_PROCESS, 'edit=' . $addresses['address_book_id'], 'SSL').'">'.tep_image_button('small_edit.gif', SMALL_IMAGE_BUTTON_EDIT).'</a>'.' ' . '<a href="'.tep_href_link(FILENAME_ADDRESS_BOOK_PROCESS, 'delete=' . $addresses['address_book_id'], 'SSL').'">'.tep_image_button('small_delete.gif', SMALL_IMAGE_BUTTON_DELETE).'</a>'; ?></span>
      <p><strong><?php echo tep_output_string_protected($addresses['firstname'] . ' ' . $addresses['lastname']); ?></strong><?php if ($addresses['address_book_id'] == $customer_default_address_id) echo '&nbsp;<small><i>' . PRIMARY_ADDRESS . '</i></small>'; ?></p>
      <p style="padding-left: 20px;"><?php echo tep_address_format($format_id, $addresses, true, ' ', '<br />'); ?></p>
    </div>

<?php
  }
?>

  </div>

  <div>
  <table width="100%">
    <tr>
  	 <td>
	 <?php //echo '<a href="'.tep_href_link(FILENAME_ACCOUNT, '', 'SSL').'">'.tep_image_button('button_back.gif', IMAGE_BUTTON_BACK).'</a>'; ?>
	<a href="account.php"><button class="button224 button2">Back</button></a>
	 </td>
<?php
  if (tep_count_customer_address_book_entries() < MAX_ADDRESS_BOOK_ENTRIES) {
?>

    <td align="right">
	<?php //echo '<a href="'.tep_href_link(FILENAME_ADDRESS_BOOK_PROCESS, '', 'SSL').'">'.tep_image_button('button_add_address.gif', IMAGE_BUTTON_ADD_ADDRESS).'</a>'; ?>
	<a href="address_book_process.php"><button class="button223 button3">Add Address</button></a>
	</td>

<?php
  }
?>
  </tr>
</table>
    
  </div>

  <p><?php echo sprintf(TEXT_MAXIMUM_ENTRIES, MAX_ADDRESS_BOOK_ENTRIES); ?></p>
</div>
</td></tr></table></td></tr></table>



<?
}
?>





<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
