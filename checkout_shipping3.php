


<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/



  require('includes/application_top.php');
  require('includes/classes/http_client.php');
  define('FREE_SHIPPING_TITLE', ' Free Shipping');

// if the customer is not logged on, redirect them to the login page
  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

// if there is nothing in the customers cart, redirect them to the shopping cart page
  if ($cart->count_contents() < 1) {
    tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
  }
/*if ($_SESSION['product_final'][$l]['name']=='EP5') {
    $cart->reset();
	//tep_redirect(tep_href_link(FILENAME_DEFAULT, '', 'SSL'));
  }*/
// if no shipping destination address was selected, use the customers own address as default
  if (!tep_session_is_registered('sendto')) {
    tep_session_register('sendto');
    $sendto = $customer_default_address_id;
  } else {
// verify the selected shipping address
    if ( (is_array($sendto) && empty($sendto)) || is_numeric($sendto) ) {
      $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$sendto . "'");
      $check_address = tep_db_fetch_array($check_address_query);

      if ($check_address['total'] != '1') {
        $sendto = $customer_default_address_id;
        if (tep_session_is_registered('shipping')) tep_session_unregister('shipping');
      }
    }
  }

  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;

// register a random ID in the session to check throughout the checkout procedure
// against alterations in the shopping cart contents
  if (!tep_session_is_registered('cartID')) tep_session_register('cartID');
  $cartID = $cart->cartID;

// if the order contains only virtual products, forward the customer to the billing page as
// a shipping address is not needed
  if ($order->content_type == 'virtual') {
    if (!tep_session_is_registered('shipping')) tep_session_register('shipping');
    $shipping = false;
    $sendto = false;
    tep_redirect(tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
  }

  $total_weight = $cart->show_weight();
  $total_count = $cart->count_contents();

// load all enabled shipping modules
  require(DIR_WS_CLASSES . 'shipping.php');
  $shipping_modules = new shipping;

  if ( defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') ) {
    $pass = false;

    switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
      case 'national':
        if ($order->delivery['country_id'] == STORE_COUNTRY) {
          $pass = true;
        }
        break;
      case 'international':
        if ($order->delivery['country_id'] != STORE_COUNTRY) {
          $pass = true;
        }
        break;
      case 'both':
        $pass = true;
        break;
    }

    $free_shipping = false;
    if ( ($pass == true) && ($order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
      $free_shipping = true;

      include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
    }
  } else {
    $free_shipping = false;
  }

// process the selected shipping method
  if ( isset($HTTP_POST_VARS['action']) && ($HTTP_POST_VARS['action'] == 'process') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken) ) {
    if (!tep_session_is_registered('comments')) tep_session_register('comments');
    if (tep_not_null($HTTP_POST_VARS['comments'])) {
      $comments = tep_db_prepare_input($HTTP_POST_VARS['comments']);
    }

    if (!tep_session_is_registered('shipping')) tep_session_register('shipping');

    if ( (tep_count_shipping_modules() > 0) || ($free_shipping == true) ) {
      if ( (isset($HTTP_POST_VARS['shipping'])) && (strpos($HTTP_POST_VARS['shipping'], '_')) ) {
        $shipping = $HTTP_POST_VARS['shipping'];

        list($module, $method) = explode('_', $shipping);
        if ( is_object($$module) || ($shipping == 'free_free') ) {
          if ($shipping == 'free_free') {
            $quote[0]['methods'][0]['title'] = FREE_SHIPPING_TITLE;
            $quote[0]['methods'][0]['cost'] = '0';
          } else {
            $quote = $shipping_modules->quote($method, $module);
          }
          if (isset($quote['error'])) {
            tep_session_unregister('shipping');
          } else {
            if ( (isset($quote[0]['methods'][0]['title'])) && (isset($quote[0]['methods'][0]['cost'])) ) {
              $shipping = array('id' => $shipping,
                                'title' => (($free_shipping == true) ?  $quote[0]['methods'][0]['title'] : $quote[0]['module'] . ' (' . $quote[0]['methods'][0]['title'] . ')'),
                                'cost' => $quote[0]['methods'][0]['cost']);

              tep_redirect(tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
            }
          }
        } else {
          tep_session_unregister('shipping');
        }
      }
    } else {
      $shipping = false;
                
      tep_redirect(tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
    }    
  }

// get all available shipping quotes
  $quotes = $shipping_modules->quote();
  

$addresses_query = tep_db_query("select  entry_zone_id as zone_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . $customer_id . "'");
$address = tep_db_fetch_array($addresses_query);
$tee=$address['zone_id'];
$states_query=tep_db_query("select  zone_code from " . TABLE_ZONES . " where zone_id = '" . $tee . "'");
$state = tep_db_fetch_array($states_query);
//$state1=$state['zone_code'];
// if no shipping method has been selected, automatically select the cheapest method.
// if the modules status was changed when none were available, to save on implementing
// a javascript force-selection method, also automatically select the cheapest shipping
// method if more than one module is now enabled
  if ( !tep_session_is_registered('shipping') || ( tep_session_is_registered('shipping') && ($shipping == false) && (tep_count_shipping_modules() > 1) ) ) $shipping = $shipping_modules->cheapest();

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_SHIPPING);

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));

  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<table width="100%" border="0" style="font:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bolder;  ">
  <tr>
    <td>1. Log In	</td>
    <td>2. Address Information</td>
    <td><font color="#A24E2A;">3. Shipping & Delivery</font></td>
    <td>4. Payment Options</td>
    <td>5. Order Review</td>
    <td>6. Order Receipt</td>
  </tr>
</table>

<div class="form_white" style="height:height:auto !important;; border: 1px #666666 solid;" >
<h3  align="left" style="padding:5px; font-family:Arial, Helvetica, sans-serif; color:#000000; "> <?php //print_r($address5) ?>  Please review your shipping and delivery options below.</h3>
<script type="text/javascript"><!--
var selected;

function selectRowEffect(object, buttonSelect) {
  if (!selected) {
    if (document.getElementById) {
      selected = document.getElementById('defaultSelected');
    } else {
      selected = document.all['defaultSelected'];
    }
  }

  if (selected) selected.className = 'moduleRow';
  object.className = 'moduleRowSelected';
  selected = object;

// one button is not an array
  if (document.checkout_address.shipping[0]) {
    document.checkout_address.shipping[buttonSelect].checked=true;
  } else {
    document.checkout_address.shipping.checked=true;
  }
}

function rowOverEffect(object) {
  if (object.className == 'moduleRow') object.className = 'moduleRowOver';
}

function rowOutEffect(object) {
  if (object.className == 'moduleRowOver') object.className = 'moduleRow';
}
function check_pickup()
{
	for(i=0;i<10;i++)
	{
		if(document.checkout_address.shipping[i].checked)
		{
			if(document.checkout_address.shipping[i].value=="free_free")
			{
				var aa=confirm("Are you sure you want to will call from our Concord, California location?");
				if(aa)
				{
				}
				else
				{
				return false;
				}
			}
		}
	}	
}
//--></script>
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 
<tr>
<td colspan="3" style="background:url(img/TopBak.jpg) !important ; height:26px; border:1px solid #ccc;">&nbsp;</td>
</tr>
<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td  valign="top" width="55%"  style="border-right:1px solid #ccc;" >
<!-- <h1 style="background:url(images/m99.gif) -5px 0; height:26px;padding:5px 0 0 10px"><?php// echo HEADING_TITLE; ?></h1>-->

<table border="0" width="100%" cellspacing="1" cellpadding="2">
      <tr>

<?php
  if ($sendto != false) {
?>

       

<?php
  }
?>

        <td width="<?php echo (($sendto != false) ? '70%' : '100%'); ?>" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">

<?php
  if (sizeof($order->info['tax_groups']) > 1) {
?>

          <tr>
            <td colspan="2">
<?php // echo '<strong>' . HEADING_PRODUCTS . '</strong> <a href="' . tep_href_link(FILENAME_SHOPPING_CART) . '"><span class="orderEdit">(' . TEXT_EDIT . ')</span></a>'; ?></td>
            <td align="right"><strong><?php echo HEADING_TAX; ?></strong></td>
            <td align="right"><strong><?php echo HEADING_TOTAL; ?></strong></td>
          </tr>

<?php
  } else {
?>

          <tr>
            <td colspan="3"><h2>The following product(s) will be shipped </h2> 
            <?php // echo '<strong>' . HEADING_PRODUCTS . '</strong> <a href="' . tep_href_link(FILENAME_SHOPPING_CART) . '"><span class="orderEdit">(' . TEXT_EDIT . ')</span></a>'; ?></td>
          </tr>

<?php
  }
  
  
  /* ani code if custom product */
  if(sizeof($_SESSION['product_final'])>=1){
    $l=0;
  	foreach($_SESSION['product_final'] as $val){
    echo '<tr>' . "\n" .
         '<td align="Left" valign="top" width="30">' . $_SESSION['product_final'][$l]['qty'] . '&nbsp;x</td>' . "\n" .
         '<td valign="top">' . $_SESSION['product_final'][$l]['name'];

    if (STOCK_CHECK == 'true') {
      echo tep_check_stock($order->products[$i]['id'], $order->products[$i]['qty']);
    }

    if ( (isset($order->products[$i]['attributes'])) && (sizeof($order->products[$i]['attributes']) > 0) ) {
      for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
        echo '<br /><nobr><small>&nbsp;<i> - ' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'] . '</i></small></nobr>';
      }
    }

    echo '</td>' . "\n";

    if (sizeof($order->info['tax_groups']) > 1) 
    echo '<td valign="top" align="right">' . tep_display_tax_value($order->products[$i]['tax']) . '%</td>' . "\n";

    echo '            <td align="right" valign="top">' . $_SESSION['product_final'][$l]['price'] . '</td>' . "\n" .
         '          </tr>' . "\n";
  
  $l++;
  }
 // print_r($_SESSION['product_final']);
 // exit;
  }
  else{
/* ani code if custom product */
  for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
    echo '<tr>' . "\n" .
         '<td align="Left" valign="top" width="30">' . $order->products[$i]['qty'] . '&nbsp;x</td>' . "\n" .
         '<td valign="top">' . $order->products[$i]['name'];

    if (STOCK_CHECK == 'true') {
      echo tep_check_stock($order->products[$i]['id'], $order->products[$i]['qty']);
    }

    if ( (isset($order->products[$i]['attributes'])) && (sizeof($order->products[$i]['attributes']) > 0) ) {
      for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
        echo '<br /><nobr><small>&nbsp;<i> - ' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'] . '</i></small></nobr>';
      }
    }

    echo '</td>' . "\n";

    if (sizeof($order->info['tax_groups']) > 1) 
    echo '<td valign="top" align="right">' . tep_display_tax_value($order->products[$i]['tax']) . '%</td>' . "\n";

    echo '            <td align="right" valign="top">' . $currencies->display_price($order->products[$i]['final_price'], $order->products[$i]['tax'], $order->products[$i]['qty']) . '</td>' . "\n" .
         '          </tr>' . "\n";
  }
  }
?>

        </table>
		
		<p>&nbsp;</p>
		<p>&nbsp;</p>
<font size="-3" ><?php echo tep_draw_form('checkout_address', tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'), 'post', '', true) . tep_draw_hidden_field('action', 'process'); ?>

  <h2><?php echo TABLE_HEADING_SHIPPING_ADDRESS; ?></h2>


   
	
	 <table style="padding-left:20px">
    	<tr><td rowspan="2"><?php echo tep_address_label($customer_id, $sendto, true, ' ', '<br />'); ?></td></tr>
        <tr><td valign="top" align="center" width="200px"> <?php //echo TEXT_SELECTED_BILLING_DESTINATION; ?><br /><br /><div class="buttonAction"><u><?php echo tep_draw_button(IMAGE_BUTTON_CHANGE_ADDRESS, 'home', tep_href_link(FILENAME_CHECKOUT_SHIPPING_ADDRESS, '', 'SSL')); ?></u></td>
        </tr>
    </table>
    
  
  </td>
      </tr>
    </table>

  
</td>
<td valign="top">
  <div style="clear: both;"></div>

<?php
  if (tep_count_shipping_modules() > 0) {
?>

  <h2><?php echo TABLE_HEADING_SHIPPING_METHOD; ?></h2>
<img src="img/multiShip.gif" />
<?php
    if (sizeof($quotes) > 1 && sizeof($quotes[0]) > 1) {
?>

  <div class="contentText">
    <div style="float: right;">
      <?php //echo '<strong>' . TITLE_PLEASE_SELECT . '</strong>'; ?>
    </div>

    <?php  echo TEXT_CHOOSE_SHIPPING_METHOD; ?>
  </div>

<?php
    } elseif ($free_shipping == false) {
?>

  <div class="contentText">
    <?php echo TEXT_ENTER_SHIPPING_INFORMATION; ?>
  </div>

<?php
    }
?>

  <div class="form_white4">
    <table  border="0" bgcolor="#AAAAAA"  cellspacing="1" cellpadding="3">

<?php
 //$addre= echo tep_address_label($customer_id, $sendto, true, ' ', '<br />');
define('FREE_SHIPPING_DESCRIPTION',$addre );

    if ($free_shipping == true) {
?>
 <tr >
        <td  style="background-color:#003399"><strong><?php echo FREE_SHIPPING_TITLE; ?></strong>&nbsp;<?php echo $quotes[$i]['icon']; ?></td>
      </tr>
      <tr id="defaultSelected" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, 0)">
        <td style="padding-left: 15px;" bgcolor="#AAAAAA"><?php echo sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format(MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER)) . tep_draw_hidden_field('shipping', 'free_free'); ?></td>
      </tr>

     

<?php
    } else { 
	
	
	?>
	    <?
      
      for ($i=0, $n=sizeof($quotes); $i<$n; $i++) {
	  $radio_buttons = 1;
?>
      <tr>
        <td colspan="3"><strong><?php echo $quotes[$i]['module']; ?></strong>&nbsp;<?php if (isset($quotes[$i]['icon']) && tep_not_null($quotes[$i]['icon'])) { echo $quotes[$i]['icon']; } ?></td>
      </tr>

<?php
        if (isset($quotes[$i]['error'])) {
?>

      <tr>
        <td colspan="3" bgcolor="#AAAAAA"><?php echo $quotes[$i]['error']; ?></td>
      </tr>

<?php
        } else {
          for ($j=0, $n2=sizeof($quotes[$i]['methods']); $j<$n2; $j++) {
// set the radio button to be checked if it is the method chosen
            $checked = (($quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'] == $shipping['id']) ? true : false);

            if ( ($checked == true) || ($n == 1 && $n2 == 1) ) {
              echo '      <tr id="defaultSelected" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, ' . $radio_buttons . ')">' . "\n";
            } else {
              echo '      <tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, ' . $radio_buttons . ')">' . "\n";
            }
?>

        <td width="75%" style="padding-left: 15px;" bgcolor="#AAAAAA"><?php echo $quotes[$i]['methods'][$j]['title']; ?></td>

<?php
            if ( ($n > 1) || ($n2 > 1) ) {
?>

        <td bgcolor="#AAAAAA"><?php echo $currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))); ?></td>
        <td align="right" bgcolor="#AAAAAA"><?php echo tep_draw_radio_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'], $checked); ?></td>

<?php
            } else {
?>

        <td align="right" colspan="2" bgcolor="#AAAAAA"><?php echo $currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))) . tep_draw_hidden_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id']); ?></td>

<?php
            }
?>
        

<?php
            $radio_buttons++;
          } ?>
		  
		  <?php if($tee=='12') { ?>
   <tr>
        <td colspan="3" style="background-color:#FF0000"><strong>Pick up</strong>&nbsp;</td>
      </tr>
       
      <tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, 6)">

        <td width="75%" style="padding-left: 15px;" bgcolor="#AAAAAA"><strong>Will call hours 6-3 we will advise by phone when order is completed.</strong><br>1281-A Franquette Ave. Concord, CA 94520<br>
Phone: 1-800-690-0002<br>
Fax: 925-680-7252<br>Hours: 6:00AM - 3:00PM PST</td>


        <td bgcolor="#AAAAAA">$0.00</td><td align="right" bgcolor="#AAAAAA"><?php echo sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format(MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER)) . tep_draw_radio_field('shipping', 'free_free'); ?></td></td>
      </tr><?php } ?>
    <?php    }
      }
    }
?>

    </table>
  </div>

<?php
  }
?>

  <h2><?php echo TABLE_HEADING_COMMENTS; ?></h2>

  <div class="contentText">
    <?php echo tep_draw_textarea_field('comments', 'soft', '50', '5'); ?>
  </div>

  <!--<div class="contentText">
    <div style="float: left; width: 60%; padding-top: 5px; padding-left: 15%;">
      <div id="coProgressBar" style="height: 5px;"></div>

      <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td align="center" width="33%" class="checkoutBarCurrent"><?php //echo CHECKOUT_BAR_DELIVERY; ?></td>
          <td align="center" width="33%" class="checkoutBarTo"><?php //echo CHECKOUT_BAR_PAYMENT; ?></td>
          <td align="center" width="33%" class="checkoutBarTo"><?php //echo CHECKOUT_BAR_CONFIRMATION; ?></td>
        </tr>
      </table>
    </div> -->

<?
/* $doc = new DOMDocument();
    $doc->loadHTML(htmlstring);
    $imageTags = $doc->getElementsByTagName('img');

    foreach($imageTags as $tag) {
        echo $tag->getAttribute('src');
    }*/
	
	
	

   

?>

   <input type="image" src="includes/languages/english/images/buttons/continue.gif" alt="Continue" title=" Continue" button="" onclick="return check_pickup()">
	
  </div>
</div>
</td>
</tr>
</table>

</td>
</tr>
</table>

<script type="text/javascript">
$('#coProgressBar').progressbar({
  value: 33
});
</script>

</form>

<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>