<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_DEFAULT));

  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<?php
    // Open Table
    $result = mysql_query(
              "SELECT * FROM Models");
    if (!$result) {
      echo("<P>Error performing query: " .
           mysql_error() . "</P>");
      exit();
    }
    
  
    // Display all models
    while ( $row = mysql_fetch_array($result) ) {
    //Now only show the data for the Model in the address bar
	if($row["Name"] == "Orbit-100" || $row["Name"] == "Orbit-360" || $row["Name"] == "Orbit-500" || $row["Name"] == "Orbit-720" || $row["Name"] == "Orbit-900"){
	
	$Height = $row["Height"];
      $EndPan = $row["EndPan"];
      $Depth = $row["Depth"];
      $TopGlass = $row["TopGlass"];
      $HeatLamps = $row["HeatLamps"];
      $Lighting = $row["Lighting"];
      $FrontGlass = $row["FrontGlass"];
      $Tubing = $row["Tubing"];
      
      echo('<TABLE WIDTH="751" HEIGHT="112" BORDER="0" BACKGROUND="images/square.jpg" CELLPADDING=5>');
      echo('<tr>');
      echo('<td align=right width=20%>');
      echo('<A HREF=info.php?Model=' . $row["Name"] . '>' . '<IMG SRC="images/Models/' . $row["Name"] . '.jpg" BORDER=0>' . '</A>');
      echo('</td>');
      echo('<td width=5 align=right>');
      echo('<IMG SRC=images/shortVertSep.jpg>');
      echo('</td>');
      echo('<td valign=top>');
      echo('<div class="linkClass">');
      echo('<A HREF=/info.php?Model=' . $row["Name"] . '>Model Name: ' . $row["Name"] . '</A><BR>');
      echo('</div>');
      echo('<div class="modText">');
      echo('Height: ' . $Height . '<br>');
      if($Depth != "0"){
      echo('Depth: ' . $Depth . '<br>');
      }
      echo('Glass: ' . $FrontGlass . '<br>');
      echo('Tubing: ' . $Tubing . '<br>');
      echo('</div>');
      echo('</td>');
      echo('<td valign=center align=left width="5">');
      //if($EndPan || $HeatLamps || $Lighting  != "0"){
      echo('<IMG SRC=images/shortVertSep.jpg>');
      //}
      echo('</td>');
      echo('<td valign=top width=11%>');
      //if($EndPan || $HeatLamps || $Lighting  != "0"){
      echo('<div class="modName">');
      echo("<B>Options:</B><BR>");
      echo('</div>');
      echo('<div class="modText">');
      if($EndPan != "0")
      echo("End Panels " . "<br>");
      if($HeatLamps != "0")
      echo("Heat Lamps " . "<br>");
      if($Lighting != "0")
      echo("Lighting " . "<br>");
      //}
      echo('</div>');
      echo('</td>');
      echo('</tr>');
      echo('</TABLE>');
      echo('<P>');
      
      }
      }
      
?>
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>