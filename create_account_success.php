<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CREATE_ACCOUNT_SUCCESS);

  $breadcrumb->add(NAVBAR_TITLE_1);
  $breadcrumb->add(NAVBAR_TITLE_2);

  if (sizeof($navigation->snapshot) > 0) {
    $origin_href = tep_href_link($navigation->snapshot['page'], tep_array_to_string($navigation->snapshot['get'], array(tep_session_name())), $navigation->snapshot['mode']);
    $navigation->clear_snapshot();
  } else {
    $origin_href = tep_href_link(FILENAME_DEFAULT);
  }

  require(DIR_WS_INCLUDES . 'template_top.php');

 if($_GET['message']=='success')
  {
  $oscsID=$_GET['osCsid'];
  // tep_redirect(tep_href_link(FILENAME_CREATE_ACCOUNT_SUCCESS, '', 'SSL'));
  ?>
<script>
window.parent.location='create_account_success.php?osCsid=<?php echo $oscsID;?>';

</script>
<?php
}
?>
<script>
	$(document).ready(function(){
		if (document.all && !document.querySelector) {
   			$("#ex1").css('width',"100%");
   			$(".price_table").css("text-align","left");
		}
   	});

</script>
<h1><?php //echo HEADING_TITLE; ?></h1>
 <table width="100%" border="0" style="font:Arial, Helvetica, sans-serif; font-size:18px; font-weight:bolder;  ">
  <tr>
    <td>1. Log In	</td>
    <td><font color="#A24E2A;">2. Account Information </font></td>
    <td>3. Shipping & Delivery</td>
    <td>4. Payment Options</td>
    <td>5. Order Review</td>
    <td>6. Order Receipt</td>
  </tr>
</table>
<div class="form_white" style="height:400px; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 
<tr>
<td colspan="3" style="background:url(img/TopBak.jpg) !important ; height:26px; border:1px solid #ccc;">&nbsp;</td>
</tr>
<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td  >
 <h3  align="left" style="padding:5px; font-family:Arial, Helvetica, sans-serif; color:#000000; ">Your Account Has Been Created!</h3> 
<div class="contentContainer">
  <div class="contentText">
  	<table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td><img src="images/table_background_man_on_board.gif" border="0" alt="Your Account Has Been Created!" title=" Your Account Has Been Created! " width="175" height="198"></td>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tbody><tr>
                <td class="pageHeading" align="center">&nbsp;</td>
              </tr>
              <tr>
                <td><img src="images/pixel_trans.gif" border="0" alt="" width="100%" height="10"></td>
              </tr>
              <tr>
                <td class="main">Congratulations! Your new account has been successfully created! You can now take advantage of member priviledges to enhance your online shopping experience with us. If you have <small><b>ANY</b></small> questions about the operation of this online shop, please email the <a href="http://www.esneezeguards.com/catalog/contact_us.php">store owner</a>.<br><br>A confirmation has been sent to the provided email address. If you have not received it within the hour, please <a href="http://www.esneezeguards.com/catalog/contact_us.php">contact us</a>.</td>
              </tr>
            </tbody></table></td>
          </tr>
        </tbody></table></td>
      </tr>
      <tr>
        <td><img src="images/pixel_trans.gif" border="0" alt="" width="100%" height="10"></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          <tbody><tr class="infoBoxContents">
            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tbody><tr>
                <td width="10"><img src="images/pixel_trans.gif" border="0" alt="" width="10" height="1"></td>
                <td align="right"><a href="<?=tep_href_link(FILENAME_ACCOUNT)?>"><img src="includes/languages/english/images/buttons/continue.gif" border="0" alt="Continue" title=" Continue "></a></td>
                <td width="10"><img src="images/pixel_trans.gif" border="0" alt="" width="10" height="1"></td>
              </tr>
            </tbody></table></td>
          </tr>
        </tbody></table></td>
      </tr>
    </tbody></table>
    <?php //echo TEXT_ACCOUNT_CREATED; ?>
  </div>
</div></td></tr></table></td></tr></table>
  <!--div class="buttonSet">
    <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', $origin_href); ?></span>
  </div-->
</div>


<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
