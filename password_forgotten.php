<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');
  
  
  
  
 // ReCaptcha Start
  require(DIR_WS_FUNCTIONS . 'ReCaptcha/autoload.php'); // reCAPTCHA
  // ReCaptcha End



  
  
  
  

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PASSWORD_FORGOTTEN);

  if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'process') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)) {
	  
	  
	  // reCAPTCHA - start
  $recaptcha = new \ReCaptcha\ReCaptcha(RECAPTCHA_PRIVATE_KEY);
  $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
 
  if ($resp->isSuccess()) {
	  
	  
	  
    $email_address = tep_db_prepare_input($HTTP_POST_VARS['email_address']);

    $check_customer_query = tep_db_query("select customers_firstname, customers_lastname, customers_password, customers_id from " . TABLE_CUSTOMERS . " where customers_email_address = '" . encrypt_email(tep_db_input($email_address), ENCRYPTION_KEY_EMAIL) . "'");
    if (tep_db_num_rows($check_customer_query)) {
      $check_customer = tep_db_fetch_array($check_customer_query);

      $new_password = tep_create_random_value(ENTRY_PASSWORD_MIN_LENGTH);
      $crypted_password = tep_encrypt_password($new_password);

      tep_db_query("update " . TABLE_CUSTOMERS . " set customers_password = '" . tep_db_input($crypted_password) . "' where customers_id = '" . (int)$check_customer['customers_id'] . "'");

      tep_mail($check_customer['customers_firstname'] . ' ' . $check_customer['customers_lastname'], $email_address, EMAIL_PASSWORD_REMINDER_SUBJECT, sprintf(EMAIL_PASSWORD_REMINDER_BODY, $new_password), STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);

      $messageStack->add_session('login', SUCCESS_PASSWORD_SENT, 'success');

      tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
    } else {
      $messageStack->add('password_forgotten', TEXT_NO_EMAIL_ADDRESS_FOUND);
    }
	
	
  }
	
	
	
	
  }
  
  
  
  
  
  
  

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL'));

  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<script>
	$(document).ready(function(){
		if (document.all && !document.querySelector) {
   			$("#ex1").css('width',"100%");
   			$(".price_table").css("text-align","left");
		}
   	});

</script>

<!-- ReCaptcha Start -->
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<!-- ReCaptcha End -->


<?php
if (!$detect->isMobile())
{
?> 
<style>
.button222 {
    background-color: #076dd5d4;
    border: none;
    color: white;
    padding: 13px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    /* margin: 4px 2px; */
    cursor: pointer;
    width: 96px;
    height: 35px;
}
.button2 {border-radius: 10px;padding-top: 10px;}

.button223 {
    background-color: #076dd5d4;
    border: none;
    color: white;
    padding: 12px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    /* margin: 4px 2px; */
    cursor: pointer;
    width: 207px;
}
.button3 {border-radius: 10px;padding-top: 10px;}
</style>

<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td  valign="top" >
<h2><?php echo HEADING_TITLE; ?></h2>

<?php
  if ($messageStack->size('password_forgotten') > 0) {
    echo $messageStack->output('password_forgotten');
  }
?>

<?php echo tep_draw_form('password_forgotten', tep_href_link(FILENAME_PASSWORD_FORGOTTEN, 'action=process', 'SSL'), 'post', '', true); ?>

<div class="contentContainer">
  <div class="contentText">
    <div><?php echo TEXT_MAIN; ?></div>

    <table border="0" width="100%" cellspacing="0" cellpadding="2">
	
	
      <tr>
        <!--<td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
        <td class="fieldValue"><?php echo tep_draw_input_field('email_address'); ?></td>-->
		<td colspan="2" class="fieldValue" style="text-align:center;"><input type="text" name="email_address" placeholder="<?php echo ENTRY_EMAIL_ADDRESS; ?>" style="width:231px; height:26px;border: 2px solid #d1cbcb;border-radius: 8px;"></td>
      </tr>
	  
	    <tr>
	  <td>
	    <!-- ReCaptcha Start -->
  <?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?>
  <div class="g-recaptcha" data-sitekey="<?php echo RECAPTCHA_PUBLIC_KEY; ?>"></div>
  <!-- ReCaptcha End -->
		
	  </td>
	  </tr>
	  
    </table>
  </div>

  <!--<div class="buttonSet" style="float:right; width:60%; padding-top:20px;" >
    <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', null, 'primary'); ?></span>

    <?php echo tep_draw_button(IMAGE_BUTTON_BACK, 'triangle-1-w', tep_href_link(FILENAME_LOGIN, '', 'SSL')); ?>
  </div>-->
  
  <div class="buttonSet" style="float:right; width:60%; padding-top:20px;">
    <span class="buttonAction"><span class=""><button id="" type="submit" class="button223 button3" role="button" aria-disabled="false">Continue</span></button></span><script type="text/javascript">$("#tdb1").button({icons:{primary:"ui-icon-triangle-1-e"}}).addClass("ui-priority-primary").parent().removeClass("tdbLink");</script></span>

    <span class=""><a id="" href="login.php"   role="button"><button class="button222 button2">Back</button></span></a></span><script type="text/javascript">$("#tdb2").button({icons:{primary:"ui-icon-triangle-1-w"}}).addClass("ui-priority-secondary").parent().removeClass("tdbLink");</script>  </div>
  
  
  
  
</div>

</form>
</td></tr></table></td></tr></table>
<?php
}
else{
?>

<style>
.processccc{color:#605b5b; font-size:22px;}

.form_white h2 {
    color: black;
    font-size: 30px;
}

.contentText, .contentText table {
    padding: 5px 0 5px 0;
    font-size: 19px;
    line-height: 1.5;
}


.button222 {
    background-color: #076dd5d4;
    border: none;
    color: white;
    padding: 13px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    /* margin: 4px 2px; */
    cursor: pointer;
    width: 154px;
    height: 50px;
    font-size: 30px;
}
.button2 {border-radius: 10px;padding-top: 10px;}

.button223 {
        background-color: #076dd5d4;
    border: none;
    color: white;
    padding: 12px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    /* margin: 4px 2px; */
    cursor: pointer;
    width: 248px;
    height: 53px;
    font-size: 30px;
}
.button3 {border-radius: 10px;padding-top: 10px;}
</style>


<td id="ex1" align=center width="190" valign="top">


<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td  valign="top" >
<h2><?php echo HEADING_TITLE; ?></h2>

<?php
  if ($messageStack->size('password_forgotten') > 0) {
    echo $messageStack->output('password_forgotten');
  }
?>

<?php echo tep_draw_form('password_forgotten', tep_href_link(FILENAME_PASSWORD_FORGOTTEN, 'action=process', 'SSL'), 'post', '', true); ?>

<div class="contentContainer">
  <div class="contentText">
    <div><?php echo TEXT_MAIN; ?></div>

    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <!--<td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
        <td class="fieldValue"><?php echo tep_draw_input_field('email_address'); ?></td>-->
		<td colspan="2" class="fieldValue" style="text-align:center;"><input type="text" name="email_address" placeholder="<?php echo ENTRY_EMAIL_ADDRESS; ?>" style="width: 377px;height: 52px;border: 2px solid #d1cbcb;border-radius: 27px;font-size: 30px;"></td>
      </tr>
	  <tr>
	  <td>
	    <!-- ReCaptcha Start -->
  <?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?>
  <div class="g-recaptcha" data-sitekey="<?php echo RECAPTCHA_PUBLIC_KEY; ?>"></div>
  <!-- ReCaptcha End -->
		
	  </td>
	  </tr>
	  
    </table>
  </div>

  <!--<div class="buttonSet" style="float:right; width:60%; padding-top:20px;" >
    <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', null, 'primary'); ?></span>

    <?php echo tep_draw_button(IMAGE_BUTTON_BACK, 'triangle-1-w', tep_href_link(FILENAME_LOGIN, '', 'SSL')); ?>
  </div>-->
  
  <div class="buttonSet" style="float:right; width:60%; padding-top:20px;">
    <span class="buttonAction"><span class=""><button id="" type="submit" class="button223 button3" role="button" aria-disabled="false">Continue</span></button></span><script type="text/javascript">$("#tdb1").button({icons:{primary:"ui-icon-triangle-1-e"}}).addClass("ui-priority-primary").parent().removeClass("tdbLink");</script></span>
<br /><br />
    <span class=""><a id="" href="login.php"   role="button"><button class="button222 button2">Back</button></span></a></span><script type="text/javascript">$("#tdb2").button({icons:{primary:"ui-icon-triangle-1-w"}}).addClass("ui-priority-secondary").parent().removeClass("tdbLink");</script>  </div>
  
  
  
  
</div>

</form>
</td></tr></table></td></tr></table>
<?php
}

?>



<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
