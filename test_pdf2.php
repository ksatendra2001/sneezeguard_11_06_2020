<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/*require_once 'dompdf/autoload.inc.php';

use Dompdf\Dompdf;
$dompdf=new Dompdf();
$dompdf->loadHtml('<h1>this is my first </h1>');
$dompdf->setPaper('A4', 'landscape');


$dompdf->render();

$dompdf->stream('codexworld',array('Attachment'=>0));*/


require_once( 'fpdf/mpdf.php'); // Include mdpf
$stylesheet = file_get_contents('assets/css/pdf.css'); // Get css content
$html = '<div id="pdf-content">
              Your PDF Content goes here (Text/HTML)
         </div>';
// Setup PDF
$mpdf = new mPDF('utf-8', 'A4-L'); // New PDF object with encoding & page size
$mpdf->setAutoTopMargin = 'stretch'; // Set pdf top margin to stretch to avoid content overlapping
$mpdf->setAutoBottomMargin = 'stretch'; // Set pdf bottom margin to stretch to avoid content overlapping
// PDF header content
$mpdf->SetHTMLHeader('<div class="pdf-header">
                          <img class="left" src="assets/img/pdf_header.png"/>                      
                      </div>'); 
// PDF footer content                      
$mpdf->SetHTMLFooter('<div class="pdf-footer">
                        <a href="http://www.lubus.in">www.lubus.in</a>
                      </div>'); 
 
$mpdf->WriteHTML($stylesheet,1); // Writing style to pdf
$mpdf->WriteHTML($html); // Writing html to pdf
// FOR EMAIL
$content = $mpdf->Output('', 'S'); // Saving pdf to attach to email 
$content = chunk_split(base64_encode($content));
// Email settings
$mailto = $email;
$from_name = 'LUBUS PDF Test';
$from_mail = 'email@domain.com';
$replyto = 'email@domain.com';
$uid = md5(uniqid(time())); 
$subject = 'mdpf email with PDF';
$message = 'Download the attached pdf';
$filename = 'lubus_mpdf_demo.pdf';
$header = "From: ".$from_name." <".$from_mail.">\r\n";
$header .= "Reply-To: ".$replyto."\r\n";
$header .= "MIME-Version: 1.0\r\n";
$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
$header .= "This is a multi-part message in MIME format.\r\n";
$header .= "--".$uid."\r\n";
$header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
$header .= $message."\r\n\r\n";
$header .= "--".$uid."\r\n";
$header .= "Content-Type: application/pdf; name=\"".$filename."\"\r\n";
$header .= "Content-Transfer-Encoding: base64\r\n";
$header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
$header .= $content."\r\n\r\n";
$header .= "--".$uid."--";
$is_sent = @mail($mailto, $subject, "", $header);
//$mpdf->Output(); // For sending Output to browser
$mpdf->Output('lubus_mdpf_demo.pdf','D'); // For Download
exit;
?>
