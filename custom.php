<?php

ob_start();
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_DEFAULT));

//  require(DIR_WS_INCLUDES . 'template_top.php');
  
  
   
 
 /*Replace Titles start ob_start(); is on top*/
 //ob_start();
 /*
    $buffer=ob_get_contents();
    ob_end_clean();
 
 
 
 $titlessss = 'ADM Sneezeguards - Online Custom Restaurant Supply Sneeze Guards';

    $buffer = preg_replace('/(<title>)(.*?)(<\/title>)/i', '$1' . $titlessss . '$3', $buffer);

    $keyword = 'Sneeze Guard, custom sneeze guards, Food service products, Sneeze Guard For Restaurants';

	//add anew desc and author
$buffer = str_replace('name="keywords" content="Sneeze Guard, sneeze guard glass, Portable Food Guards, Sneezeguards, Restaurant Supply Equipment"', 'name="keywords" content="'.$keyword.'"', $buffer);

    echo $buffer;	
*/


  require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28436015-1', { 'optimize_id': 'GTM-5ZJRN8F'});
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>
<title>ADM Sneezeguards - Online Custom Restaurant Supply Sneeze Guards</title>
<!-- End Google Add Conversion -->

<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<!--<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">-->
<meta name="description" content="Custom Sizing sneeze guard is now available on the in-stock line at ADM Sneezeguards. Get online quote for Glass Barrier custom face lenght and Post Height.">
<meta name="keywords" content="Sneeze Guard, custom sneeze guards, Food service products, Sneeze Guard For Restaurants">
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">
<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>




<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />




<link rel="stylesheet" type="text/css" href="stylesheet.css">

<style type="text/css">
<!--
.style2 {font-family: Tahoma; font-size: 12; line-height: 13px}
.style3 {font-family: Tahoma; font-size: 12; line-height: 17px; font-weight: bold}
.style6 {font-weight: bold; font-size: 12}
.TopLargeText {font-family: Tahoma; font-weight: bold; color: #C7F917; font-size: 20}
-->


p {
    font-family: "Times New Roman", Times, serif;
    color: #000000;
  
}
</style>


<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>
</head>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>


<script>

//$("meta[name='keywords']").attr("content","Sneeze Guard, custom sneeze guards, Food service products, Sneeze Guard For Restaurants");
</script>
<?php
  /*Replace Titles End */
 
 
  
  
  
  $ImageData2=mysql_query("SELECT * FROM  custompopup WHERE publish='1' ") or die(mysql_error());
	 $ROWDATA=mysql_fetch_array($ImageData2)
?>
<link rel="stylesheet" href="colorbox4.css" />
<script type="text/javascript" src="jquery.colorbox4.js"></script>
<?php if ($ROWDATA['publish']==1): ?>
 <script type="text/javascript">
    $(document).ready(function(){
			
				//Examples of how to assign the Colorbox event to elements
				$("#cboxContent").addClass("blue_border_example");
				$("#cboxTopLeft").hide();
$("#cboxTopRight").hide();
$("#cboxBottomLeft").hide();
$("#cboxBottomRight").hide();
$("#cboxMiddleLeft").hide();
$("#cboxMiddleRight").hide();
$("#cboxTopCenter").hide();
$("#cboxBottomCenter").hide();
var tis='<table width="800" bordercolor="#000000" border="2" height="400" cellpadding="0" cellspacing="0"><tr>  <td  rowspan="2"><img src="sneezegaurd/custompagepopup/images/<?php echo $ROWDATA['imagename1'];?>" alt="t1" width="400" height="405" /></td> <td ><img src="sneezegaurd/custompagepopup/images2/<?php echo $ROWDATA['imagename1'];?>" alt="t2" width="400" height="328" /></td></tr><tr>  <td height="75" align="center"><a style="color:#CCC; border-bottom:0px solid #CCC;" href="<?php echo $ROWDATA['linkname'];?>"><img src="images/popup/learnMore2.png"  /></a></td></tr></table>';
              $.colorbox({html:tis, open:true, width:"677", height:"410"});
			   
				
			});
 </script>
 <?php endif; ?>
 
	<?php
	if (!$detect->isMobile())
	{
	
	?>
	<style>
	.modText {
    font-size: 12px;
    color: #CCCCCC;
    line-height: 16px;
    text-align: left;
	}
	
	</style>
	
	
	
	<?php
	}
	else{
	?>
 
	<style>
	.modText {
    font-size: 16px !important;
    
	}
	
	.linkClass A:visited {
    
    font-size: 16px !important;
   
}

#modelnameid{font-size: 16px !important;line-height: 30px;}
	</style>
	
	
	<?php
	}
	?>
 
 
 
 
 
 
 <?php
  if (!$detect->isMobile())
{
	//echo'<td id="ex1" align=center width="190" valign="top">';
}
else{
	echo'<td id="ex1" align=center width="190" valign="top">';

}

?>
<?php
    // Open Table
    $result = mysql_query(
              "SELECT * FROM Models");
    if (!$result) {
      echo("<P>Error performing query: " .
           mysql_error() . "</P>");
      exit();
    }
    
  
    // Display all models
    while ( $row = mysql_fetch_array($result) ) {
    //Now only show the data for the Model in the address bar
	if($row["Name"] == "Orbit-360" || $row["Name"] == "P-100" || $row["Name"] == "P-400" || $row["Name"] == "P-150" || $row["Name"] == "P-263" || $row["Name"] == "P-445" || $row["Name"] == "S-295" || $row["Name"] == "S-221" || $row["Name"] == "S-280" || $row["Name"] == "S-280IS" || $row["Name"] == "S-277"  || $row["Name"] == "S-553" || $row["Name"] == "S-445" || $row["Name"] == "S-211" || $row["Name"] == "S-828" || $row["Name"] == "S-439"){//|| $row["Name"] == "D-467" This condition has removed as client said!!
	
      $Height = $row["Height"];
      $EndPan = $row["EndPan"];
      $Depth = $row["Depth"];
      $TopGlass = $row["TopGlass"];
      $HeatLamps = $row["HeatLamps"];
      $Lighting = $row["Lighting"];
      $FrontGlass = $row["FrontGlass"];
      $Tubing = $row["Tubing"];
     echo('<P>');
      
      echo('<TABLE WIDTH="751" HEIGHT="110" BORDER="0" class="custom" CELLPADDING=5>');
      echo('<tr>');
      echo('<td align=right width=20%>');
      echo('<A HREF="'.tep_href_link('info.php', 'Model='.$row["Name"]).'">' . '<IMG alt="sneeze guard" title="sneeze guard for office" SRC="images/Models/' . $row["Name"] . '.jpg" BORDER=0>' . '</A>');
      echo('</td>');
      echo('<td width=5 align=right>');
      echo('<IMG alt="sneeze guard" title="sneeze guard for office" SRC=images/shortVertSep.jpg>');
      echo('</td>');
      echo('<td valign=top>');
      echo('<div class="linkClass">');
      echo('<A HREF="'.tep_href_link('info.php', 'Model='.$row["Name"]).'"><span id="modelnameid">Model Name: ' . $row["Name"] . '</span></A><BR>');
      echo('</div>');
      echo('<div class="modText">');
      echo('Height: ' . $Height . '<br>');
      if($Depth != "0"){
      echo('Depth: ' . $Depth . '<br>');
      }
      echo('Glass: ' . $FrontGlass . '<br>');
      echo('Tubing: ' . $Tubing . '<br>');
      echo('</div>');
      echo('</td>');
      echo('<td valign=center align=left width="5">');
      //if($EndPan || $HeatLamps || $Lighting  != "0"){
      echo('<IMG alt="sneeze guard" title="sneeze guard for office" SRC=images/shortVertSep.jpg>');
      //}
      echo('</td>');
      echo('<td valign=top width=11%>');
      //if($EndPan || $HeatLamps || $Lighting  != "0"){
      echo('<div class="modName">');
      echo("<B><span id='modelnameid'>Options:</span></B><BR>");
      echo('</div>');
      echo('<div class="modText">');
      if($EndPan != "0")
      echo("End Panels " . "<br>");
      if($HeatLamps != "0")
      echo("Heat Lamps " . "<br>");
      if($Lighting != "0")
      echo("Lighting " . "<br>");
      //echo('</div>');
      
      //}
      //echo('<div class="modName">');
      echo("Finishes");
      echo('</div>');
      echo('</td>');
      echo('</tr>');
      echo('</TABLE>');
      echo('<P>');
      
      }
      }
      
?>

<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>

