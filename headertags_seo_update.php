<?php
/*
  $Id: headertags_seo_update.php by Jack_mcs

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce
  Portions Copyright 2009 oscommerce-solution.com

  Released under the GNU General Public License
*/

/**************************************************************************
 This file will update existing installations of Header Tags Controller
 and Header Tags SEO. It assumes the changes to the categories, products
 and manufacturers tables have been made since they were all common to
 previous installations. If the structure of a table was changed with an
 update, that change will have to be done manually. See the individual 
 update files for how to do that. If the url to run this script is changed
 to http://YOUR DOMAIN NAME/headertags_seo_update.php?reset_options=true,
 the configuration options will be reset. This is useful since the order
 of the options can be confusing otherwise.
**************************************************************************/
  require('includes/application_top.php');
 
  if (isset($_POST['action']) && $_POST['action'] == 'process') {
      if (isset($_POST['delete'])) {
          unlink('headertags_seo_update.php');
          tep_redirect(tep_href_link('index.php'));
          exit;
      } else if (isset($_POST['goto'])) {    
          tep_redirect(tep_href_link('index.php'));
      } else {
          echo 'Re-Running update...<br>'; 
      }    
  } 

  $db_error = false;

  $hts_check_query = tep_db_query("select configuration_group_id as id from configuration_group where configuration_group_title = 'Header Tags SEO'");

  if (tep_db_num_rows($hts_check_query) == 0) {
      $hts_check_query = tep_db_query("select max(configuration_group_id) as id from configuration_group ");
      $max = tep_db_fetch_array($hts_check_query);
      $configuration_group_id = $max['id'] + 1;

      // create configuration group
      $group_query = "INSERT INTO configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible ) VALUES ('" . $configuration_group_id . "', 'Header Tags SEO', 'Header Tags SEO site wide options', '22' , '1')";
      if (tep_db_query($group_query) == false) {
        $db_error = true;
      }
  } else {
      $id = tep_db_fetch_array($hts_check_query);
      $configuration_group_id = $id['id'];

      if (isset($_GET['reset_options']) && $_GET['reset_options'] == true) {
         $delete_query = "DELETE FROM configuration where configuration_group_id = " . (int)$configuration_group_id ;
         tep_db_query($delete_query);
      }
  }

  if (! $db_error) {
    $options = array();
    $sortID = 1;

   /*********************** ADD ANY MISSING CONFIGURATION OPTIONS *************************/
         //store the existing options
    $hts_check_query = tep_db_query("select configuration_key from configuration where configuration_group_id = " . (int)$configuration_group_id);
    while ($hts_check = tep_db_fetch_array($hts_check_query)) {
       if ($hts_check['configuration_key'] == 'HEADER_TAGS_AUTO_ADD_PAGES') {
          tep_db_query("DELETE FROM configuration where configuration_key = 'HEADER_TAGS_AUTO_ADD_PAGES'") ;
          continue;
       } 
       $options[] = $hts_check['configuration_key']; // ] =  $hts_check['configuration_value'];
    }

    // create configuration variables
    $fields = " configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added, use_function ";
    $fields_short = " configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added ";
    $hts_sql_array = array();

    if (! in_array('HEADER_TAGS_AUTO_ADD_PAGES', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('Automatically Add New Pages', 'HEADER_TAGS_AUTO_ADD_PAGES', 'true', '<center><b><h2>Header Tags SEO</h2><i>Developed by:</i><br>Jack York @ Oscommerce Solution<br><a href=\"\/\/oscommerce-solution.com\//check_unreleased_updates.php?id=3.3.4&name=HeaderTagsSEO\" target=\"_blank\">Check Updates</a></b></center><br>Adds any new pages in Page Control', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_BREADCRUMB_MODEL_OVERRIDE', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('Breadcrumb Model Override', 'HEADER_TAGS_BREADCRUMB_MODEL_OVERRIDE', 'false', 'Force the breadrumb to use the model field for the breadcrumb trail.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'),', now(), NULL)");
    if (! in_array('HEADER_TAGS_BYPASS_ISTEMPLATE', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('ByPass New Pages Check', 'HEADER_TAGS_BYPASS_ISTEMPLATE', 'false', 'If enabled, all files in the root will be added to the list in Page Control - only use if needed<br>(true=on false=off)', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_CANONICAL_PATH', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('Canonical Path', 'HEADER_TAGS_CANONICAL_PATH', 'full', 'Canonical url will use all of the ID\'s in the url or just the last one.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'full\', \'last\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_CHECK_TAGS', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('Check for Missing Tags', 'HEADER_TAGS_CHECK_TAGS', 'true', 'Check to see if any products, categories or manufacturers contain empty meta tag fields<br>(true=on false=off)', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_CLEAR_CACHE', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('Clear Cache', 'HEADER_TAGS_CLEAR_CACHE', 'false', 'Remove all Header Tags cache entries from the database.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'clear\', \'false\'), ', now(), 'header_tags_reset_cache')");

    if (! in_array('HEADER_TAGS_ADD_CATEGORY_PARENTS', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=purple>Display Category Parents in Title and Tags</font>', 'HEADER_TAGS_ADD_CATEGORY_PARENTS', 'Standard', 'Adds all categories in the current path (Full), all immediate categories if the product is in more than one category (Duplicate) or only the immediate category (Standard). These settings only work if the Category checkbox is enabled in Page Control.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(''Full Category Path'', ''Duplicate Categories'', ''Standard''), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_DISPLAY_CATEGORY_SHORT_DESCRIPTION', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields_short . ") VALUES ('<font color=purple>Display Category Short Description</font>', 'HEADER_TAGS_DISPLAY_CATEGORY_SHORT_DESCRIPTION', 'Off', 'If a number is entered, that many characters of the category description will be displayed under the category name on the category listing page. <br><br>Leave blank to display all of the text (not recommended). <br><br>Enter \'Off\' to disable this option.', '" . $configuration_group_id . "', '" . ($sortID++). "', now())");
    if (! in_array('HEADER_TAGS_DISPLAY_COLUMN_BOX', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=purple>Display Column Box</font>', 'HEADER_TAGS_DISPLAY_COLUMN_BOX', 'false', 'Display product box in column while on product page<br>(true=on false=off)', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_DISPLAY_CURRENTLY_VIEWING', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=purple>Display Currently Viewing</font>', 'HEADER_TAGS_DISPLAY_CURRENTLY_VIEWING', 'true', 'Display a link near the bottom of the product page.<br>(true=on false=off)', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_DISPLAY_HELP_POPUPS', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=purple>Display Help Popups</font>', 'HEADER_TAGS_DISPLAY_HELP_POPUPS', 'true', 'Display short popup messages that describes a feature<br>(true=on false=off)', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_DIABLE_PERMISSION_WARNING', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=purple>Disable Permission Warning</font>', 'HEADER_TAGS_DIABLE_PERMISSION_WARNING', 'false', 'Prevent the warning that appears if the permissions for the includes/header_tags.php file appear to be incoorect.<br>(true=on false=off)', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_DISPLAY_PAGE_TOP_TITLE', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=purple>Display Page Top Title</font>', 'HEADER_TAGS_DISPLAY_PAGE_TOP_TITLE', 'true', 'Displays the page title at the very top of the page<br>(true=on false=off)', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_DISPLAY_SEE_MORE', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=purple>Display See More</font>', 'HEADER_TAGS_DISPLAY_SEE_MORE', 'short', 'Display see more on the category and product listing pages. This option can be set as:<br><br>off - do not show see more link<br>short - link just shows see more<br>full - link shows see more with item name', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'off\', \'short\', \'full\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_DISPLAY_SILO_BOX', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=purple>Display Silo Links</font>', 'HEADER_TAGS_DISPLAY_SILO_BOX', 'false', 'Display a box displaying links based on the settings in Silo Control<br>(true=on false=off)', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_DISPLAY_SOCIAL_BOOKMARKS', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=purple>Display Social Bookmark</font>', 'HEADER_TAGS_DISPLAY_SOCIAL_BOOKMARKS', 'true', 'Display social bookmarks on the product page<br>(true=on false=off)', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_DISPLAY_TAG_CLOUD', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=purple>Display Tag Cloud</font>', 'HEADER_TAGS_DISPLAY_TAG_CLOUD', 'false', 'Display the Tag Cloud infobox<br>(true=on false=off)', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");

    if (! in_array('HEADER_TAGS_ENABLE_ADDITIONAL_WORDS', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=blue>Enable Additional Words</font>', 'HEADER_TAGS_ENABLE_ADDITIONAL_WORDS', 'false', 'If true, text will be shown on the product page that lists any additional words entered for that product. If no words are entered, nothing will be displayed.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'),', now(), NULL)");
    if (! in_array('HEADER_TAGS_ENABLE_AUTOFILL_LISTING_TEXT', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=blue>Enable AutoFill - Listing Text</font>', 'HEADER_TAGS_ENABLE_AUTOFILL_LISTING_TEXT', 'false', 'If true, text will be shown on the product listing page automatically. If false, the text only shows if the field has text in it.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'),', now(), NULL)");
    if (! in_array('HEADER_TAGS_ENABLE_CACHE', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=blue>Enable Cache</font>', 'HEADER_TAGS_ENABLE_CACHE', 'None', 'Enables cache for Header Tags. The GZip option will use gzip to try to increase speed but may be a little slower if the Header Tags data is small.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'None\', \'Normal\', \'GZip\'),', now(), NULL)");
    if (! in_array('HEADER_TAGS_ENABLE_HTML_EDITOR', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=blue>Enable an HTML Editor</font>', 'HEADER_TAGS_ENABLE_HTML_EDITOR', 'No Editor', 'Use an HTML editor, if selected. !!! Warning !!! The selected editor must be installed for it to work!!!)', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'CKEditor\', \'FCKEditor\', \'TinyMCE\', \'No Editor\'),', now(), NULL)");
    if (! in_array('HEADER_TAGS_ENABLE_EDITOR_CATEGORIES', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=blue>Enable HTML Editor for Category Descriptions</font>', 'HEADER_TAGS_ENABLE_EDITOR_CATEGORIES', 'false', 'Enables the selected HTML editor for the categories description box. The editor must be installed and enabled for this to work.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_ENABLE_EDITOR_MANUFACTURERS', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=blue>Enable HTML Editor for Manufacturer Descriptions</font>', 'HEADER_TAGS_ENABLE_EDITOR_MANUFACTURERS', 'false', 'Enables the selected HTML editor for the manufacturer description box. The editor must be installed and enabled for this to work.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_ENABLE_EDITOR_PRODUCTS', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=blue>Enable HTML Editor for Products Descriptions</font>', 'HEADER_TAGS_ENABLE_EDITOR_PRODUCTS', 'false', 'Enables the selected HTML editor for the products description box. The editor must be installed and enabled for this to work.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_ENABLE_EDITOR_LISTING_TEXT', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=blue>Enable HTML Editor for Product Listing text</font>', 'HEADER_TAGS_ENABLE_EDITOR_LISTING_TEXT', 'false', 'Enables the selected HTML editor for the Header Tags text on the product listing page. The editor must be installed and enabled for this to work.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_ENABLE_EDITOR_SUB_TEXT', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=blue>Enable HTML Editor for Product Sub Text</font>', 'HEADER_TAGS_ENABLE_EDITOR_SUB_TEXT', 'false', 'Enables the selected HTML editor for the sub text on the products page. The editor must be installed for and enabled this to work.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_ENABLE_GOOGLE_PLUS_ONE', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=blue>Enable Google +1</font>', 'HEADER_TAGS_ENABLE_GOOGLE_PLUS_ONE', 'true', 'Enables the display of the google +1 social icon.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_ENABLE_VERSION_CHECKER', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('<font color=blue>Enable Version Checker</font>', 'HEADER_TAGS_ENABLE_VERSION_CHECKER', 'true', 'Enables the code that checks if updates are available.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");

    if (! in_array('HEADER_TAGS_KEYWORD_DENSITY_RANGE', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields_short . ") VALUES ('Keyword Density Range', 'HEADER_TAGS_KEYWORD_DENSITY_RANGE', '0.02,0.06', 'Set the limits for the keyword density use to dynamically select the keywords. Enter two figures, separated by a comma.', '" . $configuration_group_id . "', '" . ($sortID++). "', now())");
    if (! in_array('HEADER_TAGS_KEYWORD_HIGHLIGHTER', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('Keyword Highlighter', 'HEADER_TAGS_KEYWORD_HIGHLIGHTER', 'No Highlighting', 'Bold any keywords found on the page.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'No Highlighting\', \'Highlight Full Words Only\', \'Highlight Individual Words\'),', now(), NULL)");
        
    if (! in_array('HEADER_TAGS_POSITION_DOMAIN', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields_short . ") VALUES ('Position Domain', 'HEADER_TAGS_POSITION_DOMAIN', '', 'Set the domain name to be used in the keyword position checking code, like www.domain_name.com or domain_name.com/shop.', '" . $configuration_group_id . "', '" . ($sortID++). "', now())");
    if (! in_array('HEADER_TAGS_POSITION_PAGE_COUNT', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields_short . ") VALUES ('Position Page Count', 'HEADER_TAGS_POSITION_PAGE_COUNT', '2', 'Set the number of pages to search when checking keyword positions (10 urls per page).', '" . $configuration_group_id . "', '" . ($sortID++). "', now())");
    if (! in_array('HEADER_TAGS_SEPARATOR_DESCRIPTION', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields_short . ") VALUES ('Separator - Description', 'HEADER_TAGS_SEPARATOR_DESCRIPTION', '-', 'Set the separator to be used for the description (and titles and logo).', '" . $configuration_group_id . "', '" . ($sortID++). "', now())");
    if (! in_array('HEADER_TAGS_SEPARATOR_KEYWORD', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields_short . ") VALUES ('Separator - Keywords', 'HEADER_TAGS_SEPARATOR_KEYWORD', ',', 'Set the separator to be used for the keywords.', '" . $configuration_group_id . "', '" . ($sortID++). "', now())");

    if (! in_array('HEADER_TAGS_SEARCH_KEYWORDS', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('Search Keywords', 'HEADER_TAGS_SEARCH_KEYWORDS', 'false', 'This option allows keywords stored in the Header Tags SEO search table to be searched when a search is performed on the site.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_STORE_KEYWORDS', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('Store Keywords', 'HEADER_TAGS_STORE_KEYWORDS', 'true', 'This option stores the searched for keywords so they can be used by other parts of Header Tags, like in the Tag Cloud option.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'), ', now(), NULL)");
    if (! in_array('HEADER_TAGS_TAG_CLOUD_COLUMN_COUNT', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields_short . ") VALUES ('Tag Cloud Column Count', 'HEADER_TAGS_TAG_CLOUD_COLUMN_COUNT', '8', 'Set the number of keywords to display in a row in the Tag Cloud box.', '" . $configuration_group_id . "', '" . ($sortID++). "', now())");
    if (! in_array('HEADER_TAGS_USE_PAGE_NAME', $options)) $hts_sql_array[] = array("INSERT INTO configuration (" . $fields . ") VALUES ('Use Item Name on Page</font>', 'HEADER_TAGS_USE_PAGE_NAME', 'false', 'If true, the title on the page will be the name of the item (category, manufacturer or product). If false, the Header Tags SEO title will be used.', '" . $configuration_group_id . "', '" . ($sortID++). "', 'tep_cfg_select_option(array(\'true\', \'false\'),', now(), NULL)");

    foreach ($hts_sql_array as $sql_array) {
      foreach ($sql_array as $value) {
     //   echo 'update '.$value . '<br>';
        if (tep_db_query($value) == false) {
           $db_error = true;
        }
      }
    }
    
    /*********************** ADD ANY MISSING TABLES *************************/
    $hts_sql_array = array(
                     array("CREATE TABLE IF NOT EXISTS headertags_cache (title text, data text)"),
                     array("CREATE TABLE IF NOT EXISTS headertags_default (default_title varchar(255) default '' NOT NULL, default_description varchar(255) default '' NOT NULL, default_keywords varchar(255) default '' NOT NULL, default_logo_text varchar(255) default '' NOT NULL, home_page_text text default '' NOT NULL, default_logo_append_group tinyint(1) default 1 NOT NULL, default_logo_append_category tinyint(1) default 1 NOT NULL, default_logo_append_manufacturer tinyint(1) default 1 NOT NULL, default_logo_append_product tinyint(1) default 1 NOT NULL, meta_google tinyint(1) default 0 NOT NULL, meta_language tinyint(1) default 0 NOT NULL, meta_noodp tinyint(1) default 1 NOT NULL, meta_noydir tinyint(1) default 1 NOT NULL, meta_replyto tinyint(1) default 0 NOT NULL, meta_revisit tinyint(1) default 0 NOT NULL, meta_robots tinyint(1) default 0 NOT NULL, meta_unspam tinyint(1) default 0 NOT NULL, meta_canonical tinyint(1) default 1 NOT NULL, meta_og tinyint(1) default 1 NOT NULL, language_id int DEFAULT '1' NOT NULL, PRIMARY KEY (default_title, language_id))"),
                     array("CREATE TABLE IF NOT EXISTS headertags (page_name varchar(64) default '' NOT NULL, page_title varchar(120) default '' NOT NULL, page_description varchar(255) default '' NOT NULL, page_keywords varchar(255) default '' NOT NULL, page_logo varchar(255) default '' NOT NULL, page_logo_1 varchar(255) default '' NOT NULL, page_logo_2 varchar(255) default '' NOT NULL, page_logo_3 varchar(255) default '' NOT NULL, page_logo_4 varchar(255) default '' NOT NULL, append_default_title tinyint(1) default 0 NOT NULL, append_default_description tinyint(1) default 0 NOT NULL, append_default_keywords tinyint(1) default 0 NOT NULL, append_default_logo tinyint(1) default 0 NOT NULL, append_category tinyint(1) default 0 NOT NULL, append_manufacturer tinyint(1) default 0 NOT NULL, append_model tinyint(1) default 0 NOT NULL, append_product tinyint(1) default 1 NOT NULL, append_root tinyint(1) default 1 NOT NULL, sortorder_title tinyint(2) default 0 NOT NULL, sortorder_description tinyint(2) default 0 NOT NULL, sortorder_keywords tinyint(2) default 0 NOT NULL, sortorder_logo tinyint(2) default 0 NOT NULL, sortorder_logo_1 tinyint(2) default 0 NOT NULL, sortorder_logo_2 tinyint(2) default 0 NOT NULL, sortorder_logo_3 tinyint(2) default 0 NOT NULL, sortorder_logo_4 tinyint(2) default 0 NOT NULL, sortorder_category tinyint(2) default 0 NOT NULL, sortorder_manufacturer tinyint(2) default 0 NOT NULL, sortorder_model tinyint(2) default 0 NOT NULL, sortorder_product tinyint(2) default 10 NOT NULL, sortorder_root tinyint(2) default 1 NOT NULL, sortorder_root_1 tinyint(2) default 1 NOT NULL, sortorder_root_2 tinyint(2) default 1 NOT NULL, sortorder_root_3 tinyint(2) default 1 NOT NULL, sortorder_root_4 tinyint(2) default 1 NOT NULL, language_id int DEFAULT '1' NOT NULL, KEY idx_page_name (page_name), KEY idx_page_description (page_description), KEY idx_page_keywords (page_keywords) )"),
                     array("CREATE TABLE IF NOT EXISTS headertags_silo (category_id int NOT NULL DEFAULT '0', box_heading VARCHAR (60) NOT NULL, is_disabled TINYINT (1) DEFAULT 0 NOT NULL, max_links int DEFAULT '6' NOT NULL, sorton TINYINT (2) DEFAULT 0 NOT NULL, language_id int DEFAULT '1' NOT NULL, PRIMARY KEY ( category_id, language_id ))"),
                     array("CREATE TABLE IF NOT EXISTS headertags_keywords (id int(11) NOT NULL AUTO_INCREMENT, keyword varchar(120) NOT NULL DEFAULT '', counter int(11) NOT NULL DEFAULT '1', last_search datetime NOT NULL DEFAULT '0000-00-00 00:00:00', google_last_position tinyint(4) NOT NULL, google_date_position_check datetime NOT NULL DEFAULT '0000-00-00 00:00:00', found TINYINT( 1 ) NOT NULL DEFAULT 0, language_id int(11) NOT NULL DEFAULT '1', PRIMARY KEY (id), KEY keyword (keyword), KEY found (found)) AUTO_INCREMENT=1"),
                     array("CREATE TABLE IF NOT EXISTS headertags_search (product_id INT( 11 ) NOT NULL, keyword VARCHAR( 64 ) NOT NULL, language_id INT( 11 ) NOT NULL, KEY keyword (keyword))"),
                     array("CREATE TABLE IF NOT EXISTS headertags_social (unique_id INT ( 4 ) NOT NULL AUTO_INCREMENT , section VARCHAR( 48 ) NOT NULL , groupname VARCHAR (24 ) NOT NULL, url VARCHAR ( 255 ) NOT NULL, data TEXT NOT NULL , PRIMARY KEY (unique_id), KEY idx_section (section)) ENGINE = InnoDB"),
                     array("CREATE TABLE IF NOT EXISTS headertags_ip_tracker (ip_number INT( 64 ) UNSIGNED NOT NULL , source TINYINT ( 4 ) NOT NULL , id_reference INT ( 11 ) NOT NULL, date_added datetime NOT NULL , index idx_ip_number (ip_number), index idx_date_added (date_added)) ENGINE = InnoDB"));
    // create tables
    foreach ($hts_sql_array as $sql_array) {
      foreach ($sql_array as $value) {
    //    echo 'val '.$value.'<br><br>';
        if (tep_db_query($value) == false) {
          $db_error = true;
        }
      }
    }    
    /*********************** ADD MISSING FIELDS TO STANDARD TABLES *************************/
    $hts_sql_array = array();
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM categories_description LIKE 'categories_htc_desc_tag'"))) {
      $hts_sql_array[] = array("ALTER TABLE categories_description ADD categories_htc_desc_tag VARCHAR( 80 ) NULL");
    };        
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM categories_description LIKE 'categories_htc_keywords_tag'"))) {
      $hts_sql_array[] = array("ALTER TABLE categories_description ADD categories_htc_keywords_tag VARCHAR( 80 ) NULL AFTER categories_htc_desc_tag");
    };            
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM categories_description LIKE 'categories_htc_title_tag'"))) {
      $hts_sql_array[] = array("ALTER TABLE categories_description ADD categories_htc_title_tag VARCHAR( 80 ) NULL AFTER categories_htc_keywords_tag");
    };     
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM categories_description LIKE 'categories_htc_title_tag_alt'"))) {
      $hts_sql_array[] = array("ALTER TABLE categories_description ADD categories_htc_title_tag_alt VARCHAR( 80 ) NULL AFTER categories_htc_title_tag");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM categories_description LIKE 'categories_htc_title_tag_url'"))) {
      $hts_sql_array[] = array("ALTER TABLE categories_description ADD categories_htc_title_tag_url VARCHAR( 80 ) NULL AFTER categories_htc_title_tag_alt");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM categories_description LIKE 'categories_htc_description'"))) {
      $hts_sql_array[] = array("ALTER TABLE categories_description ADD categories_htc_description VARCHAR( 80 ) NULL AFTER categories_htc_title_tag_url");
    };    
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM categories_description LIKE 'categories_htc_breadcrumb_text'"))) {
      $hts_sql_array[] = array("ALTER TABLE categories_description ADD categories_htc_breadcrumb_text VARCHAR( 80 ) NULL AFTER categories_htc_description");
    };
    
                   
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM manufacturers_info LIKE 'manufacturers_htc_title_tag'"))) {
      $hts_sql_array[] = array("ALTER TABLE manufacturers_info ADD manufacturers_htc_title_tag VARCHAR( 80 ) NULL ");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM manufacturers_info LIKE 'manufacturers_htc_title_tag_alt'"))) {
      $hts_sql_array[] = array("ALTER TABLE manufacturers_info ADD manufacturers_htc_title_tag_alt VARCHAR( 80 ) NULL AFTER manufacturers_htc_title_tag");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM manufacturers_info LIKE 'manufacturers_htc_title_tag_url'"))) {
      $hts_sql_array[] = array("ALTER TABLE manufacturers_info ADD manufacturers_htc_title_tag_url VARCHAR( 80 ) NULL AFTER manufacturers_htc_title_tag_alt");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM manufacturers_info LIKE 'manufacturers_htc_desc_tag'"))) {
      $hts_sql_array[] = array("ALTER TABLE manufacturers_info ADD manufacturers_htc_desc_tag VARCHAR( 80 ) NULL AFTER manufacturers_htc_title_tag_alt");
    };    
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM manufacturers_info LIKE 'manufacturers_htc_keywords_tag'"))) {
      $hts_sql_array[] = array("ALTER TABLE manufacturers_info ADD manufacturers_htc_keywords_tag VARCHAR( 80 ) NULL AFTER manufacturers_htc_title_tag_alt");
    };    
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM manufacturers_info LIKE 'manufacturers_htc_description'"))) {
      $hts_sql_array[] = array("ALTER TABLE manufacturers_info ADD manufacturers_htc_description VARCHAR( 80 ) NULL AFTER manufacturers_htc_title_tag_url");
    };    
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM manufacturers_info LIKE 'manufacturers_htc_breadcrumb_text'"))) {
      $hts_sql_array[] = array("ALTER TABLE manufacturers_info ADD manufacturers_htc_breadcrumb_text VARCHAR( 80 ) NULL AFTER manufacturers_htc_description");
    };

    
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM products_description LIKE 'products_head_desc_tag'"))) {
       $hts_sql_array[] = array("ALTER TABLE " . TABLE_PRODUCTS_DESCRIPTION . " ADD products_head_desc_tag VARCHAR(160) NULL");
    }
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM products_description LIKE 'products_head_keywords_tag'"))) {
       $hts_sql_array[] = array("ALTER TABLE " . TABLE_PRODUCTS_DESCRIPTION . " ADD products_head_keywords_tag TEXT NULL");
    }

                    
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM products_description LIKE 'products_head_listing_text'"))) {
      $hts_sql_array[] = array("ALTER TABLE products_description ADD products_head_listing_text TEXT NULL AFTER products_head_keywords_tag");
    }
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM products_description LIKE 'products_head_sub_text'"))) {
      $hts_sql_array[] = array("ALTER TABLE products_description ADD products_head_sub_text TEXT NULL AFTER products_head_listing_text");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM products_description LIKE 'products_head_breadcrumb_text'"))) {
      $hts_sql_array[] = array("ALTER TABLE products_description ADD products_head_breadcrumb_text VARCHAR( 80 ) NULL AFTER products_head_keywords_tag");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM products_description LIKE 'products_head_title_tag'"))) {
      $hts_sql_array[] = array("ALTER TABLE products_description ADD products_head_title_tag VARCHAR( 80 ) NULL AFTER products_head_keywords_tag");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM products_description LIKE 'products_head_title_tag_alt'"))) {
      $hts_sql_array[] = array("ALTER TABLE products_description ADD products_head_title_tag_alt VARCHAR( 80 ) NULL AFTER products_head_title_tag");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM products_description LIKE 'products_head_title_tag_url'"))) {
      $hts_sql_array[] = array("ALTER TABLE products_description ADD products_head_title_tag_url VARCHAR( 80 ) NULL AFTER products_head_title_tag_alt");
    };    
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM products_description LIKE 'products_head_additional_words'"))) {
      $hts_sql_array[] = array("ALTER TABLE products_description ADD products_head_additional_words VARCHAR( 256 ) NULL AFTER products_head_title_tag_url");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM headertags_default LIKE 'meta_og'"))) {
      $hts_sql_array[] = array("ALTER TABLE headertags_default ADD meta_og TINYINT( 1 ) NOT NULL default 1 AFTER meta_canonical");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM headertags_default LIKE 'home_page_text'"))) {
      $hts_sql_array[] = array("ALTER TABLE headertags_default ADD home_page_text TEXT NOT NULL default '' AFTER default_logo_text");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM headertags_keywords LIKE 'ip_number'"))) {
      $hts_sql_array[] = array("ALTER TABLE `headertags_keywords` ADD `ip_number` INT( 64 ) UNSIGNED NOT NULL DEFAULT 0 AFTER found");
    };
    if (! tep_db_num_rows(tep_db_query("SHOW COLUMNS FROM headertags_keywords LIKE 'ip_number'"))) {
      $hts_sql_array[] = array("ALTER TABLE `headertags_keywords` ADD `ip_number` INT( 64 ) UNSIGNED NOT NULL DEFAULT 0 AFTER found");
    };    
     

    if (count($hts_sql_array) > 0) {
      foreach ($hts_sql_array as $sql_array) {
        foreach ($sql_array as $value) {
          if (tep_db_query($value) == false) {
            $db_error = true;
          }
        }
      }
    }
  }
 
 
  $db_query = tep_db_query("select section from headertags_social");
  $db = tep_db_fetch_array($db_query);
  if (! tep_not_null($db['section'])) {
      tep_db_query("INSERT INTO headertags_social (unique_id, section, groupname, url, data) VALUES
          ('1','socialicons', 'digg', 'http://digg.com/submit?phase=2&url=URL&TITLE', '16x16'),
          ('2','socialicons', 'facebook', 'http://www.facebook.com/share.php?u=URL&TITLE', '16x16'),
          ('3','socialicons', 'google', 'http://www.google.com/bookmarks/mark?op=edit&bkmk=URL&TITLE', '16x16'),
          ('4','socialicons', 'pintrest', 'http://pinterest.com/pin/create/button/?url=URL&TITLE', '16x16'),
          ('5','socialicons', 'reddit', 'http://reddit.com/submit?url=URL&TITLE', '16x16'),
          ('6', 'socialicons', 'google+', 'https://plus.google.com/share?url=URL&TITLE', '16x16'),
          ('7', 'socialicons', 'linkedin', 'http://www.linkedin.com/shareArticle?mini=true&url=&title=TITLE=&source=URL', '16x16'),
          ('8', 'socialicons', 'newsvine', 'http://www.newsvine.com/_tools/seed&amp;save?u=URL&h=TITLE', '16x16'),
          ('9', 'socialicons', 'stumbleupon', 'http://www.stumbleupon.com/submit?url=URL&TITLE', '16x16'),
          ('10', 'socialicons', 'twitter', 'http://twitter.com/home?status=URL&TITLE', '16x16')
          ");
  }
?>
<div class="pageHeading"><?php echo 'Header Tags SEO Database Updater'; ?></div>
<div style="padding:10px 0">
<?php
  if ($db_error == false) {
    echo 'Database successfully updated for Header Tags SEO!!!';
  } else {
    echo 'Update failed!!! Header Tags SEO must be installed in order to use this update file.';
  }
?>
</div>

<?php echo tep_draw_form('headertags_seo_update', 'headertags_seo_update.php?reset_options=true', 'post') . tep_hide_session_id() . tep_draw_hidden_field('action', 'process'); ?>
  <div style="padding-bottom:10px"><input type="submit" name="delete" value="Go To Home Page AFTER deleting this file (recommended)"></div>
  <div style="padding-bottom:10px"><input type="submit" name="goto" value="Go To Home Page"></div>
  <div><input type="submit" name="rerun" value="Rerun This Script"></div>
</form> 
