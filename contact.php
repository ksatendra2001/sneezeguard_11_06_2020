<?php

ob_start();

require('includes/application_top.php');


  
 // ReCaptcha Start
  require(DIR_WS_FUNCTIONS . 'ReCaptcha/autoload.php'); // reCAPTCHA
  // ReCaptcha End


/*

  require(DIR_WS_INCLUDES . 'template_top.php');
  

    $buffer=ob_get_contents();
    ob_end_clean();


$titlessss = 'ADM Sneezeguards - Sneeze Guard | Contact | Phone | Address';
$buffer = preg_replace('/(<title>)(.*?)(<\/title>)/i', '$1' . $titlessss . '$3', $buffer);
$keyword = 'Sneeze Guard store, ADM Sneezeguards Phone Number, Sneeze Guard company address, sneeze guard Manufacturer US, custom sneeze guard';
  //add anew desc and author
$buffer = str_replace('name="keywords" content="Sneeze Guard, sneeze guard glass, Portable Food Guards, Sneezeguards, Restaurant Supply Equipment"', 'name="keywords" content="'.$keyword.'"', $buffer);

echo $buffer;
*/




  require_once("Mobile_Detect.php");
        $detect = new Mobile_Detect();
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28436015-1', { 'optimize_id': 'GTM-5ZJRN8F'});
</script>

<!-- Event snippet for Page_view conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1072651700/Aj7aCLK3n60BELS7vf8D'});
</script>

<!-- Event snippet for Purchase_product conversion page -->
<script>
  gtag('event', 'conversion', {
      'send_to': 'AW-1072651700/vBmHCIOlk60BELS7vf8D',
      'transaction_id': ''
  });
</script>
<title>Sneeze Guard Manufacturer | Kitchen Equipment - ADM Sneezeguards</title>
<!-- End Google Add Conversion -->

<!-- End Google Add Conversion -->
<meta name="google-site-verification" content="qCniVCJ6BLGaxFIjLt_Le0HrCeDwZJAzR-UrXe-8poc" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
<meta name="msvalidate.01" content="A671AB7AE7F959666D7415258AF5DF66" />
<meta name="viewport" content="width=device-width">
<!--<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">-->
<meta name="description" content="Checkout ADM Sneezeguards In-Stock line features structures complete with optional sneeze guards with latest design and models. Shop online NOW at sneezeguard.com.">
<meta name="keywords" content="Buffet Sneeze Guards, Commercial Kitchen Equipment online, online restaurant supply store, Shop sneeze guard, Manufacturer Sneeze Guard">
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<script type="text/javascript" src="jquery-latest.js"></script>
<script type="text/javascript" src="thickbox.js"></script>

<link rel="icon" href="images/favicon.ico" type="img/ioc">
<link rel="canonical" href="https://www.sneezeguard.com<?php echo $_SERVER['REQUEST_URI']; ?>">
<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.8.6.css" />
<script type="text/javascript" src="ext/jquery/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.8.6.min.js"></script>



<meta property="og:url" content="https://www.sneezeguard.com/"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sneeze Guard Portable | Glass Barrier - ADM Sneezeguards"/>
<meta property="og:description" content="ADM Sneezeguards manufactures for the food service industry, we offer industry standard sneeze guards with latest innovative designs." />
<meta property="og:image" content="https://www.sneezeguard.com/images/new_logo_main.png" />
<meta property="og:site_name" content="sneeze guard"/>
<meta property="fb:app_id" content="2368710130085474"/>
<meta name="twitter:card" content="summary"></meta>
<meta name="twitter:image" content="https://www.sneezeguard.com/images/new_logo_main.png"/>
<meta name="twitter:site" content="@ASneezeguards"/>
<meta name="twitter:url" content="https://twitter.com/ASneezeguards"/>
<meta name="twitter:description" content="ADM Sneezeguards provide a wide variety of fully customizable sneeze guard. We are 100% American made and built, right here in California. "/>
<meta name="twitter:title" content="Sneeze Guard Portable Glass Barrier" />




<link rel="stylesheet" type="text/css" href="stylesheet.css">

<style type="text/css">
<!--
.style2 {font-family: Tahoma; font-size: 12; line-height: 13px}
.style3 {font-family: Tahoma; font-size: 12; line-height: 17px; font-weight: bold}
.style6 {font-weight: bold; font-size: 12}
.TopLargeText {font-family: Tahoma; font-weight: bold; color: #C7F917; font-size: 20}
-->


p {
    font-family: "Times New Roman", Times, serif;
    color: #000000;
  
}
</style>


<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "WebSite",
  "name": "sneeze guard",
  "url": "https://www.sneezeguard.com",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://www.sneezeguard.com/index.php{search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>



<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sneeze Guard",
  "alternateName": "Sneeze Guard",
  "url": "https://www.sneezeguard.com",
  "logo": "https://ibb.co/12PH7w8",
  "sameAs": [
    "https://www.facebook.com/admsneezeguards",
    "https://twitter.com/ASneezeguards",
    "https://www.instagram.com/nickpadm",
    "https://www.youtube.com/channel/UCXn-Tc-uqqY8blZZapPDNXg",
    "https://www.linkedin.com/company/adm-sneezeguards",
    "https://www.pinterest.com/admsneezeguards1",
    "https://www.sneezeguard.com/"
  ]
}
</script>
</head>
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
















<!-- ReCaptcha Start -->
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<!-- ReCaptcha End -->


 <?php
  if (!$detect->isMobile())
{
?>

<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 

<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td  valign="top" >
<table border="0" height="40" valign=center align=center>
<tr>
<td align=center>
<font color=White size=4>
<?php
echo "<b><h2>Contact Us</h2></b><br><br>
</font>
<table width=751 height=133 align=center BACKGROUND='squareContSm.jpg'>
<tr>
<td align=center>
<font  size=2>
<b>In Stock Sales Phone:</b> 800-690-0002<br>
<b>E-Mail: </b><A HREF='mailto:info@sneezeguard.com'>info@sneezeguard.com</a><br><br>

<b>Custom Sales Phone:</b> 800-805-1114<br>
<b>E-Mail: </b><A HREF='mailto:sales@sneezeguards.com'>sales@sneezeguards.com</a><br><br>
<b>Address:</b><br>
2300 Wilbur Ave.<br>
Antioch CA 94509<br>
</font>
</td>
</tr>
</table>
<br><br>
<font size=2 >
Please Feel free to send us a message with any questions or comments you may have.";
?>

</font>
</td>
</tr>
</table>

<table border="0" height="40" align="center">
<tr>
<td align="left">

<?php

  
  
    // Open Table
    $result = mysql_query(
              "SELECT * FROM Models");
    if (!$result) {
      echo("<P>Error performing query: " .
           mysql_error() . "</P>");
      exit();
    }
    
    while ( $row = mysql_fetch_array($result) ) {
    //Now only show the data for the Model in the address bar
	if($row["Name"] == $Model){
	
      $EndPan = $row["EndPan"];
      $Depth = $row["Depth"];
      $TopGlass = $row["TopGlass"];
      $HeatLamps = $row["HeatLamps"];
      $Lighting = $row["Lighting"];
      }}
      
?>
      
<?php
function spamcheck($field)
  {
  //filter_var() sanitizes the e-mail 
  //address using FILTER_SANITIZE_EMAIL
  $field=filter_var($field, FILTER_SANITIZE_EMAIL);
  
  //filter_var() validates the e-mail
  //address using FILTER_VALIDATE_EMAIL
  if(filter_var($field, FILTER_VALIDATE_EMAIL))
    {
    return TRUE;
    }
  else
    {
    return FALSE;
    }
  }

if (isset($_REQUEST['email']))
  {//if "email" is filled out, proceed

  //check if the email address is invalid
  $mailcheck = spamcheck($_REQUEST['email']);
  
  
    // reCAPTCHA - start
  $recaptcha = new \ReCaptcha\ReCaptcha(RECAPTCHA_PRIVATE_KEY);
  $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
 
  if ($resp->isSuccess()) {
  
  
  
  
  
  
  
  if ($mailcheck==FALSE)
    {
    echo "Invalid input";
    }
  else
    {//send email
    $hatHeat = $_REQUEST['hatHeat'] ;
    $hatLight = $_REQUEST['hatLight'] ;
    $endPanLeft = $_REQUEST['endPanLeft'] ;
    $endPanRight = $_REQUEST['endPanRight'] ;
    $strLength = $_REQUEST['strLength'] ;
    $email = $_REQUEST['email'] ; 
    $name = $_REQUEST['name'];
    $phNumb = $_REQUEST['phNum'];
    $subject = "Quote Request for model $Model.";
    $message = $_REQUEST['message'] ;
	mail("sales@sneezeguards.com", $subject,
    "This is a quote request from: $name \n
    Phone Number: $phNumb \n\n
    $message", "From: $email" );
    echo "
    <table align=center>
    <tr>
    <td align=center>
    <font size=2 >
    Thank you for contacting us, you should recieve a response in 24-48 hours.
    </font>
    </td>
    </tr>
    </table>";
    }
	
  }
else{
	
	echo'<script>alert("Please Select correct Captcha")</script>';
}	
	
	
	
  }
else
{
echo '<table width="751" height="343" align=center border="0" cellpadding="15" background="squareCont.jpg">
<tr>
<td align=right>';

  //if "email" is not filled out, display the form
  echo
  "
  <font size=2 >
  <form method='post' action='contact.php'>
  <input name='name' type='text' size='99' placeholder='Name' style='width: 313px;height: 23px;font-size: 14px;border: 2px solid #d1cbcb;border-radius: 8px;padding: 5px;' /><br /><br />
  <input name='email' type='text' size='99' placeholder='Email' style='width: 313px;height: 23px;font-size: 14px;border: 2px solid #d1cbcb;border-radius: 8px;padding: 5px;' /><br /><br />
  <input name='phNum' size='99' type='text' placeholder='Phone Number' style='width: 313px;height: 23px;font-size: 14px;border: 2px solid #d1cbcb;border-radius: 8px;padding: 5px;' /><br /><br />
  
    <textarea name='message' placeholder='Message' rows='10' cols='75'  style='border: 2px solid #d1cbcb;border-radius: 8px;padding: 5px;'></textarea><br />
  
  
  ";
  
  
  echo'
  
    <!-- ReCaptcha Start -->
  '.tep_draw_separator("pixel_trans.gif", "100%", "10").'
  <div class="g-recaptcha" data-sitekey="'.RECAPTCHA_PUBLIC_KEY.'"></div>
  <!-- ReCaptcha End -->
		
  ';
  
   echo
  "

  
  <input type='submit' value='Send Message' style='font-size: 14px;' />
  </td>
  </tr>
  </table>
  </font>";
  
  
}
?>

</tr>
</td>
</table>

</tr>
</td>
</table>
</tr>
</td>
</table>
</div>



 <?php
}
  else
{
?>
<style>
.form_white h2 {
    color: black;
    font-size: 30px;
}
</style>

<td id="ex1" align=center width="190" valign="top">

<div class="form_white" style="height:auto !important; border: 1px #666666 solid;" >
<table width="100%" cellpadding="0" cellspacing="0" style="padding:5px;"> 
<!--<tr>
<td colspan="3" style="background:url(img/TopBak.jpg) !important ; height:26px; border:1px solid #ccc;">&nbsp;</td>
</tr>-->
<tr  >

<td style="border-bottom:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc;">
<table width="100%" cellpadding="2" cellspacing="2">
<tr>

<td  valign="top" >
<table border="0" height="40" valign=center align=center>
<tr>
<td align=center>
<font color=White size=4>
<?php
echo "<b><h2>Contact Us</h2></b><br><br>
</font>
<table width=751 height=133 align=center BACKGROUND='squareContSm.jpg'>
<tr>
<td align=center>
<font  size=4>
<b>In Stock Sales Phone:</b> 800-690-0002<br>
<b>E-Mail: </b><A HREF='mailto:info@sneezeguard.com'>info@sneezeguard.com</a><br><br>

<b>Custom Sales Phone:</b> 800-805-1114<br>
<b>E-Mail: </b><A HREF='mailto:sales@sneezeguards.com'>sales@sneezeguards.com</a><br><br>
<b>Address:</b><br>
2300 Wilbur Ave.<br>
Antioch CA 94509<br>
</font>
</td>
</tr>
</table>
<br><br>
<font size=4 >
Please Feel free to send us a message with any questions or comments you may have.";
?>

</font>
</td>
</tr>
</table>

<table border="0" height="40" align="center">
<tr>
<td align="left">

<?php

  
  
    // Open Table
    $result = mysql_query(
              "SELECT * FROM Models");
    if (!$result) {
      echo("<P>Error performing query: " .
           mysql_error() . "</P>");
      exit();
    }
    
    while ( $row = mysql_fetch_array($result) ) {
    //Now only show the data for the Model in the address bar
	if($row["Name"] == $Model){
	
      $EndPan = $row["EndPan"];
      $Depth = $row["Depth"];
      $TopGlass = $row["TopGlass"];
      $HeatLamps = $row["HeatLamps"];
      $Lighting = $row["Lighting"];
      }}
      
?>
      
<?php
function spamcheck($field)
  {
  //filter_var() sanitizes the e-mail 
  //address using FILTER_SANITIZE_EMAIL
  $field=filter_var($field, FILTER_SANITIZE_EMAIL);
  
  //filter_var() validates the e-mail
  //address using FILTER_VALIDATE_EMAIL
  if(filter_var($field, FILTER_VALIDATE_EMAIL))
    {
    return TRUE;
    }
  else
    {
    return FALSE;
    }
  }

if (isset($_REQUEST['email']))
  {
	  
	  // reCAPTCHA - start
  $recaptcha = new \ReCaptcha\ReCaptcha(RECAPTCHA_PRIVATE_KEY);
  $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
 
  if ($resp->isSuccess()) {  
	  
	  
	  
	  
	  
	  //if "email" is filled out, proceed

  //check if the email address is invalid
  $mailcheck = spamcheck($_REQUEST['email']);
  if ($mailcheck==FALSE)
    {
    echo "Invalid input";
    }
  else
    {//send email
    $hatHeat = $_REQUEST['hatHeat'] ;
    $hatLight = $_REQUEST['hatLight'] ;
    $endPanLeft = $_REQUEST['endPanLeft'] ;
    $endPanRight = $_REQUEST['endPanRight'] ;
    $strLength = $_REQUEST['strLength'] ;
    $email = $_REQUEST['email'] ; 
    $name = $_REQUEST['name'];
    $phNumb = $_REQUEST['phNum'];
    $subject = "Quote Request for model $Model.";
    $message = $_REQUEST['message'] ;
	mail("sales@sneezeguards.com", $subject,
    "This is a quote request from: $name \n
    Phone Number: $phNumb \n\n
    $message", "From: $email" );
    echo "
    <table align=center>
    <tr>
    <td align=center>
    <font size=2 >
    Thank you for contacting us, you should recieve a response in 24-48 hours.
    </font>
    </td>
    </tr>
    </table>";
    }
	
	
	
  }
  else{
	
	echo'<script>alert("Please Select correct Captcha")</script>';
}	
	
	
  }
else
{
echo '<table width="751" height="343" align=center border="0" cellpadding="15" background="squareCont.jpg">
<tr>
<td align=right>';

  //if "email" is not filled out, display the form
  echo
  "
  <font size=4 >
  <form method='post' action='contact.php'>
  <input name='name' type='text' size='99' placeholder='Name' style='width: 370px;height:44px;font-size: 22px;border: 2px solid #d1cbcb;border-radius: 8px;padding: 5px;' /><br /><br />
  <input name='email' type='text' size='99' placeholder='Email' style='width: 370px;height:44px;font-size: 22px;border: 2px solid #d1cbcb;border-radius: 8px;padding: 5px;' /><br /><br />
  <input name='phNum' size='99' type='text' placeholder='Phone Number' style='width: 370px;height:44px;font-size: 22px;border: 2px solid #d1cbcb;border-radius: 8px;padding: 5px;' /><br /><br />
  

  <textarea name='message' rows='10' cols='75' placeholder='Message' style='width: 570px;height:300px;font-size: 22px;border: 2px solid #d1cbcb;border-radius: 8px;padding: 5px;'></textarea><br />
  ";
  
  
  echo'
  
    <!-- ReCaptcha Start -->
  '.tep_draw_separator("pixel_trans.gif", "100%", "10").'
  <div class="g-recaptcha" data-sitekey="'.RECAPTCHA_PUBLIC_KEY.'"></div>
  <!-- ReCaptcha End -->
		
  ';
  
   echo
  "
  
  <input type='submit' value='Send Message' style='font-size: 30px;' />
  </td>
  </tr>
  </table>
  </font>";
  
  
}
?>

</tr>
</td>
</table>

</tr>
</td>
</table>
</tr>
</td>
</table>
</div>


 <?php
}

?>









<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>

